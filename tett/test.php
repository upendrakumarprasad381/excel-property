<?php
include_once './get-reviews.php';
$options = array(
  'googlemaps_free_apikey' => 'AIzaSyARcSpDt_SEZfexNRiEAv5D-IHPA9OCBvc',       // Google API Key
  'google_maps_review_cid' => 'ChIJf5HFUbBDXz4Rsk1KTNO_9RM', // Google Placec ID
  'cache_data_xdays_local' => 30,       // every x day the reviews are loaded from google
  'your_language_for_tran' => 'en',     // give you language for auto translate reviews
  'show_not_more_than_max' => null,        // (0-5) only show first x reviews
  'show_only_if_with_text' => false,    // true = show only reviews that have text
  'show_only_if_greater_x' => 0,        // (0-4) only show reviews with more than x stars
  'sort_reviews_by_a_data' => 'rating', // sort by 'time' or by 'rating' (newest/best first)
  'show_cname_as_headline' => true,     // true = show customer name as headline
  'show_stars_in_headline' => true,     // true = show customer stars after name in headline
  'show_author_avatar_img' => true,     // true = show the author avatar image (rounded)
  'show_blank_star_till_5' => true,     // false = don't show always 5 stars e.g. ⭐⭐⭐☆☆
  'show_txt_of_the_review' => true,     // true = show the text of each review
  'show_author_of_reviews' => true,     // true = show the author of each review
  'show_age_of_the_review' => true,     // true = show the age of each review
  'dateformat_for_the_age' => 'Y.m.d',  // see https://www.php.net/manual/en/datetime.format.php
  'show_rule_after_review' => true,     // false = don't show <hr> Tag after/before each review
  'add_schemaorg_metadata' => true,     // add schemo.org data to loop back your rating to SERP
);
echo getReviews($options);