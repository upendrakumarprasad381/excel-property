<?php

namespace App\Helpers; 

use App\Database;
use Config;
use Session;

class HTMLHelper {

    public static function locationSearchHomepage() {
        ob_start();
        $search = !empty($_POST['search']) ? $_POST['search'] : '';
        $html = "";
        if (!empty($search)) {
            $sql1 = "SELECT location_embedded AS address FROM `my_property` WHERE status=0 AND archive=0";
            $Sql = "SELECT address FROM `my_property` WHERE status=0 AND archive=0 UNION ALL  $sql1 ";
            $list = Database::select($Sql);
            for ($i = 0; $i < count($list); $i++) {
                $d = $list[$i];
                ?>
                <li><?= $d->address ?></li>
                <?php
            }
            $html = ob_get_clean();
        }
        die(json_encode(array('status' => true, 'HTML' => $html)));
    }

    public static function sendMailforcontactUs($contactId = '') {
        $Sql = "SELECT * FROM `contact_us` WHERE id='$contactId'";
        $contact = Database::selectSingle($Sql);
        $normal_msg = 'We thank you for your inquiry. Our Excel Specialists are now processing your request and will contact you shortly. If you require immediate assistance, please contact us at your convenience.';
        $enq_logo = 'https://demo.softwarecompany.ae/excel_properties/email_template/img/img.jpg';
        $emailAttachment = '';
        $downloadBtnName = '';
        $is_file = false;
        if ($contact->page_id == 'NEW_DEVELOPMENTS') {
            $basepath = Config::get('constants.HOME_DIR') . "files/hostgallery/";
            $devlpm = LibHelper::GetnewdevelopmentsByndid($contact->primary_id);


            $enq_logo = "files/hostgallery/" . (!empty($devlpm->banner) ? $devlpm->banner : '');
            if (is_file(Config::get('constants.HOME_DIR') . $enq_logo)) {
                $enq_logo = url($enq_logo);
            } else {
                $enq_logo = url("public/images/new_developmentsbnr.jpg");
            }

            if (!empty($devlpm)) {
                if ($contact->messages == 'Download Brochure') {
                    $is_file = true;
                    $normal_msg = "We thank you for the Request for Brochure.<br>If you require immediate assistance, please contact us at your convenience.";
                    $emailAttachment = $devlpm->brochure_file;
                    $downloadBtnName = 'Download Brochure';
                }
                if ($contact->messages == 'Download Floorplan') {
                    $is_file = true;
                    $normal_msg = "We thank you for the Request for Floor Plan.<br>If you require immediate assistance, please contact us at your convenience.";
                    $emailAttachment = $devlpm->floorplan_file;
                    $downloadBtnName = 'Download Floorplan';
                }
                $emailAttachment = is_file($basepath . $emailAttachment) ? url("files/hostgallery/" . $emailAttachment) : '';
            }
        } else if ($contact->page_id == 'AREA_GUIDE') {

            $pArray = \App\Helpers\LibHelper::GetareaguidesByguidesId($contact->primary_id);
            $enq_logo = "files/hostgallery/" . (!empty($pArray->banner_logo) ? $pArray->banner_logo : '');
            if (is_file(Config::get('constants.HOME_DIR') . $enq_logo)) {
                $enq_logo = url($enq_logo);
            } else {
                $enq_logo = url("public/images/area_guide_details_bnr.jpg");
            }
        } else if ($contact->page_id == 'BLOG_DETAILS') {
            $enq_logo = url('public/images/Blog_banner.jpg');
        } else if ($contact->page_id == 'CAREERS') {
            $enq_logo = url('files/hostgallery/6107a259ab47f.jpg');
        } else if ($contact->page_id == 'CONTACT_US') {
            $enq_logo = url('public/images/contact_us.jpg');
        } else if ($contact->page_id == 'DEVELOPMENTS') {
            $devlpm = LibHelper::GetnewdevelopmentsByndid($contact->primary_id);
            $enq_logo = "files/hostgallery/" . (!empty($devlpm->banner) ? $devlpm->banner : '');
            if (is_file(Config::get('constants.HOME_DIR') . $enq_logo)) {
                $enq_logo = url($enq_logo);
            } else {
                $enq_logo = url("public/images/new_developmentsbnr.jpg");
            }
        } else if ($contact->page_id == 'LIST_WITH_US') {
            $enq_logo = url('public/images/list_with_us.jpg');
        }

        $messages = view("admin.EmailTemplate.general_enquiry")->with([
            'name' => $contact->name,
            'normal_msg' => $normal_msg,
            'enq_logo' => $enq_logo,
            'is_file' => $is_file,
            'downloadBtnName' => $downloadBtnName,
            'emailAttachment' => $emailAttachment
        ]);
        $subject = "Request submitted successfully.";

        CommonHelper::send_mail($contact->email, $subject, $messages, $cc = '', $attachment = '', $lay = 'No');

        $adminmsg = ""
                . "<p>Dear admin,</p>"
                . "<p>We have received one inquiry</p>"
                . "<p>Name : $contact->name</p>"
                . "<p>Email : $contact->email</p>"
                . "<p>Reference : <a href='$contact->page_url'>$contact->page_url</a></p>"
                . "";
        CommonHelper::send_mail('upendra@alwafaagroup.com', $subject, $adminmsg, $cc = '', $attachment = '', $lay = 'Yes');
    }

    public static function sendMailforcontactUs222XXXXXX($contactId = '') {
        $Sql = "SELECT * FROM `contact_us` WHERE id='$contactId'";
        $contact = Database::selectSingle($Sql);
        $html = ""
                . "<p>Dear $contact->name,<p>"
                . "<p>Thank you for submitting your request.</p>"
                . "<p>Our team will contact you shortly.</p>"
                . ""
                . "";
        $subject = "Request submitted successfully.";
        $fileAttact = '';
        if ($contact->page_id == 'NEW_DEVELOPMENTS') {
            $fileAttact = Config::get('constants.HOME_DIR') . "files/hostgallery/";
            $devlpm = LibHelper::GetnewdevelopmentsByndid($contact->primary_id);
            if (!empty($devlpm)) {
                if ($contact->messages == 'Download Brochure') {
                    $fileAttact = $fileAttact . $devlpm->brochure_file;
                }
                if ($contact->messages == 'Download Floorplan') {
                    $fileAttact = $fileAttact . $devlpm->floorplan_file;
                }
                $fileAttact = is_file($fileAttact) ? $fileAttact : '';
            }
        }

        CommonHelper::send_mail($contact->email, $subject, $html, $cc = '', $fileAttact);
    }

}
