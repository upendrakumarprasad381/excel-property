<?php

namespace App\Helpers;

use App\Database;
use Config;
use Session;

class LibHelper {

    public static function GetpropertytypeBy($ptypeId = '') {
        $Sql = "SELECT SC.* FROM `property_type` SC WHERE SC.ptype_id='$ptypeId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetUrl($string = '') {
        $string = strtolower(str_replace(' ', '-', $string));
        return $string;
    }

    public static function GetpropertytypeByslug($ptype = '', $slugs = '') {
        $Sql = "SELECT SC.* FROM `property_type` SC WHERE SC.ptype='$ptype' AND slugs='$slugs'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GettowerlistBy($towerId = '') {
        $Sql = "SELECT SC.* FROM `tower_list` SC WHERE SC.tower_id='$towerId'";
        $dArray = Database::selectSingle($Sql);

        return $dArray;
    }

    public static function GettowerlistByslugsId($slugsId = '') {
        $Sql = "SELECT SC.* FROM `tower_list` SC WHERE SC.slugs='$slugsId'";
        $dArray = Database::selectSingle($Sql);

        return $dArray;
    }

    public static function GetpropertypriceBy($priceId = '') {
        $Sql = "SELECT SC.* FROM `property_price` SC WHERE SC.price_id='$priceId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetpropertytypeothersById($Id = '') {
        $Sql = "SELECT SC.* FROM `property_type_others` SC WHERE SC.id='$Id'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }
 public static function GetkeywordsBykeywordsId($keywordsId = '') {
        $Sql = "SELECT SC.* FROM `keywords` SC WHERE SC.keywords_id='$keywordsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }
    public static function GetEmirates() {
        $Sql = "SELECT SC.* FROM `emirates` SC WHERE 1";
        $dArray = Database::select($Sql);
        return $dArray;
    }

    public static function GettagseBytagId($tagId = '') {
        $Sql = "SELECT SC.* FROM `tags` SC WHERE SC.tag_id='$tagId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GettagseByslugsId($slugsId = '') {
        $Sql = "SELECT SC.* FROM `tags` SC WHERE SC.slugs='$slugsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetbannermanagementBybannertype($bannerType = '') {
        $Sql = "SELECT SC.* FROM `banner_management` SC WHERE SC.banner_type='$bannerType' ORDER BY SC.position ASC";
        $dArray = Database::select($Sql);
        return $dArray;
    }

    public static function GetarealistByareaId($areaId = '') {
        $Sql = "SELECT SC.* FROM `area_list` SC WHERE SC.area_id='$areaId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetarealistByslugId($slugsId = '') {
        $Sql = "SELECT SC.* FROM `area_list` SC WHERE SC.slugs='$slugsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetmasterdetailsBy($masterId = '') {
        $Sql = "SELECT * FROM `master_details` WHERE master_id='$masterId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetsubmasterdetailsBysubmasterId($submasterId = '') {
        $Sql = "SELECT * FROM `submaster_details` WHERE submaster_id='$submasterId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetnewdevelopmentsByndid($ndId = '') {
        $Sql = "SELECT SC.* FROM `new_developments` SC WHERE SC.ndid='$ndId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetareaguidesByguidesId($guidesId = '') {
        $Sql = "SELECT SC.* FROM `area_guides` SC WHERE SC.guides_id='$guidesId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetteamId($teamId = '') {
        $Sql = "SELECT SC.* FROM `team` SC WHERE SC.team_id='$teamId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetagentsByagentsId($agentsId = '') {
        $Sql = "SELECT SC.* FROM `agents` SC WHERE SC.agents_id='$agentsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetbannermanagementBybannerId($bannerId = '') {
        $Sql = "SELECT SC.* FROM `banner_management` SC WHERE SC.banner_id='$bannerId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetpropertyfacilityBy($facilityId = '') {
        $Sql = "SELECT * FROM `property_facility` WHERE facility_id ='$facilityId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetmypropertyBy($propertyId = '') {
        $Sql = "SELECT *,getpropertyImage(property_id) AS first_image FROM `my_property` WHERE property_id ='$propertyId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function roomfacility() {
        $qry = "SELECT * FROM `property_facility` WHERE archive=0";
        $dArray = Database::select($qry);
        return $dArray;
    }

    public static function GetmypropertygalleryBy($propertyId = '') {
        $qry = "SELECT * FROM `my_property_gallery` WHERE property_id='$propertyId' ORDER BY orderview ASC";
        $dArray = Database::select($qry);
        return $dArray;
    }

    public static function GetpropertyTypeByptypeId($ptypeId = '') {
        $ptypeId = isset($_POST['ptypeId']) ? $_POST['ptypeId'] : $ptypeId;
        $encode = !empty($_POST['encode']) ? $_POST['encode'] : '';
        $qry = "SELECT * FROM `property_type` WHERE ptype='$ptypeId' AND archive=0 ORDER BY position ASC";
        $dArray = Database::select($qry);
        if (!empty($encode)) {
            die(json_encode(array('status' => true, 'message' => '', 'data' => $dArray)));
        }
        return $dArray;
    }

    public static function GetTowerlist() {
        $qry = "SELECT * FROM `tower_list` WHERE archive=0";
        $dArray = Database::select($qry);
        return $dArray;
    }

    public static function GetpropertyTypeByptypeHTMLId($ptypeId = '') {

        $ptypeId = isset($_POST['ptypeId']) ? $_POST['ptypeId'] : $ptypeId;
        $encode = !empty($_POST['encode']) ? $_POST['encode'] : '';
        $min_prices_sel = !empty($_POST['min_prices_sel']) ? $_POST['min_prices_sel'] : '0';
        $max_prices_sel = !empty($_POST['max_prices_sel']) ? $_POST['max_prices_sel'] : '0';
           $sel_ptype_name = !empty($_POST['sel_ptype_name']) ? $_POST['sel_ptype_name'] : '';
        $qry = "SELECT * FROM `property_type_others` WHERE  archive=0";
        $dArray = Database::select($qry);
        $phtml = '<option value="">PROPERTY TYPE</option>';
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $phtml = $phtml . '<option ' .($sel_ptype_name==$d->id ? 'selected' : ''). ' value="' . $d->id . '">' . $d->name . '</option>';
        }


        $qry = "SELECT * FROM `property_price` WHERE ptype='$ptypeId' AND archive=0 ORDER BY price ASC";
        $dArray = Database::select($qry);

        $minPrice = '<option value="" >Min. Price</option>';
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $minPrice = $minPrice . '<option ' . ($min_prices_sel == $d->price ? 'selected' : '') . ' value="' . $d->price . '">' . CommonHelper::DecimalAmount($d->price) . '.00 AED</option>';
        }

        $maxPrice = '<option value="" >Max. Price</option>';
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $maxPrice = $maxPrice . '<option ' . ($max_prices_sel == $d->price ? 'selected' : '') . ' value="' . $d->price . '">' . CommonHelper::DecimalAmount($d->price) . '.00 AED</option>';
        }


        $towerList = " UNION ALL SELECT TL.tower_name AS address,COUNT(MP.property_id) AS total FROM `my_property` MP LEFT JOIN tower_list TL ON TL.tower_id=MP.tower_id WHERE MP.status=0 AND MP.archive=0  AND MP.ptype='$ptypeId' GROUP BY MP.tower_id";
        $Sql = "SELECT address,COUNT(property_id) AS total FROM `my_property` WHERE status=0 AND archive=0 AND ptype='$ptypeId'  GROUP BY address $towerList";
        $list = \App\Database::select($Sql);
        $searchHTML = '';
        for ($i = 0; $i < count($list); $i++) {
            $d = $list[$i];
            $searchHTML = $searchHTML . '<li ><a value="' . $d->address . '" class="searchable" href="javascript:void(0)">' . $d->address . '  (' . $d->total . ')</a></li>';
        }

        die(json_encode(array('status' => true, 'message' => '', 'property_type' => $phtml, 'min_price' => $minPrice, 'max_price' => $maxPrice, 'searchHTML' => $searchHTML)));
    }

    public static function GetblogsByblogsId($blogsId = '') {
        $Sql = "SELECT * FROM `blogs` WHERE blog_id ='$blogsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GettestimonialById($Id = '') {
        $Sql = "SELECT * FROM `testimonial` WHERE id ='$Id'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetcmsBycmsId($cmsId = '') {
        $Sql = "SELECT SC.* FROM `cms` SC WHERE SC.cms_id='$cmsId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function getNewenquiryno() {
        $Sql = "SELECT MAX(enqid) AS max_num FROM `enquiry_management`";
        $dArray = Database::selectSingle($Sql);
        $max_num = !empty($dArray->max_num) ? ($dArray->max_num + 1) : '1';
        $max_num = 'ENQ-' . str_pad($max_num, 5, "0", STR_PAD_LEFT);
        return $max_num;
    }

    public static function GetenquirymanagementByenqId($enqId = '') {
        $Sql = "SELECT * FROM `enquiry_management` WHERE enqid ='$enqId'";
        $dArray = Database::selectSingle($Sql);
        return $dArray;
    }

    public static function GetFootermenuClick() {
        $listData = [];
//                $listData[]=array('title'=>'','url'=>url('/'));
//        $listData[] = array('title' => 'About Us', 'url' => url('dubai/about-us'));
//        $listData[] = array('title' => 'Services', 'url' => url('dubai/services'));
//        $listData[] = array('title' => 'Careers', 'url' => url('dubai/careers'));
//        $listData[] = array('title' => 'Blog', 'url' => url('dubai/blog'));
//        $listData[] = array('title' => 'LIST WITH US', 'url' => url('dubai//list-with-us'));
//        $listData[] = array('title' => 'Team', 'url' => url('dubai/team'));
//        $listData[] = array('title' => 'Testimonals', 'url' => url('dubai/testimonals'));
//        $listData[] = array('title' => 'Contact Us', 'url' => url('dubai/contact-us'));
//        $listData[] = array('title' => 'Privacy Policy', 'url' => url('dubai/privacy-policy'));
//        $listData[] = array('title' => 'Terms & Condition', 'url' => url('dubai/terms-conditios'));


        $dArray = LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $listData[] = array('title' => $d->ptype_name, 'url' => url("dubai/properties-for-sale/$d->slugs"));
        }
        $dArray = LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
        for ($i = 0; $i < count($dArray); $i++) {
            $d = $dArray[$i];
            $listData[] = array('title' => $d->ptype_name, 'url' => url("dubai/properties-for-rent/$d->slugs"));
        }
        return $listData;
    }

}
