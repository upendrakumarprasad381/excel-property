<?php

namespace App\Helpers;

use App\Database;
use Config;
use Session;
use DateTime;
use App\Helpers\LibHelper;

class CommonHelper {

    public static function autoupdate() {
        $updatejson = !empty($_POST['updatejson']) ? json_decode($_POST['updatejson'], true) : [];
        $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
        $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
        if (!empty($dbtable) && is_array($updatejson) && is_array($condjson)) {
            Database::updates($dbtable, $updatejson, $condjson);
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
    }

    public static function autodelete() {
        $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
        $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
        if (!empty($dbtable) && is_array($condjson)) {
            Database::deletes($dbtable, $condjson);
            $filepath = !empty($_POST['filepath']) ? $_POST['filepath'] : '';
            if (is_file($filepath)) {
                unlink($filepath);
            }
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
    }

    public static function GetvendorBy($vendorId = '') {
        $select = ",CONCAT(VD.first_name,' ',VD.last_name) AS vendor_name";
        $join = " ";
        $Sql = "SELECT VD.* $select FROM `vendor_details` VD $join WHERE VD.vendor_id='$vendorId'";
        $dArray = Database::selectSingle($Sql);
        if (!empty($dArray)) {
            $fullImg = is_file(Config::get('constants.HOME_DIR') . 'files/vendor-profile/' . $dArray->profile_image) ? 'files/vendor-profile/' . $dArray->profile_image : Config::get('constants.DEFAULT_LOGO_VENDOR');
            $dArray->fullImg = url($fullImg);
        }
        return $dArray;
    }

    public static function areaguidesUpdatespos() {
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];

        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['position'] = $d['orderview'];
            Database::updates('area_guides', $update, ['guides_id' => $d['id']]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function newdevelopmentsUpdatespos() {
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];

        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['position'] = $d['orderview'];
            Database::updates('new_developments', $update, ['ndid' => $d['id']]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function mypropertyUpdatespos() {
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];

        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['position'] = $d['orderview'];
            Database::updates('my_property', $update, ['property_id' => $d['id']]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function teamUpdatespos() {
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];

        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['position'] = $d['orderview'];
            Database::updates('team', $update, ['team_id' => $d['id']]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function imageProperyUpdatespos() {
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];

        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['orderview'] = $d['orderview'];
            Database::updates('my_property_gallery', $update, ['pgallery_id' => $d['id']]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function myTagUpdatespos() {
        $tag_id = !empty($_POST['tag_id']) ? $_POST['tag_id'] : '';
        $orderview = !empty($_POST['orderview']) ? json_decode($_POST['orderview'], true) : [];
        $orderview = !empty($orderview) && is_array($orderview) ? $orderview : [];
        Database::deletes('tag_position', ['tag_id' => $tag_id]);
        for ($i = 0; $i < count($orderview); $i++) {
            $d = $orderview[$i];
            $update['position'] = $d['orderview'];
            $update['foreign_id'] = $d['id'];
            $update['tag_id'] = $tag_id;
            $update['lastupdate'] = date('Y-m-d H:i:s');
            Database::insert('tag_position', $update);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function deleteAllImages() {
        $propertyId = !empty($_POST['propertyId']) ? $_POST['propertyId'] : '';
        $Sql = "SELECT * FROM `my_property_gallery` WHERE property_id='$propertyId'";
        $img = Database::select($Sql);
        for ($i = 0; $i < count($img); $i++) {
            $d = $img[$i];
            $fileName = Config::get('constants.HOME_DIR') . 'files/hostgallery/' . $d->file_name;
            if (is_file($fileName)) {
                unlink($fileName);
            }
            Database::deletes('my_property_gallery', ['pgallery_id' => $d->pgallery_id]);
        }
        die(json_encode(array('status' => true, 'message' => 'updated successfully')));
    }

    public static function GetSessionArrayVendor() {
        $sArray = Session::get(Config::get('constants.VENDOR_SESSION_NAME'));
        if (!empty($sArray)) {
            $sArray = CommonHelper::GetvendorBy($sArray->vendor_id);
        }

        return $sArray;
    }

    public static function permissionFormate($stringPr = '') {
        $permission = !empty($stringPr) ? json_decode($stringPr) : [];
        $permission = !empty($permission) && is_array($permission) ? $permission : [];
        return $permission;
    }

    public static function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function DecimalAmount($Amount = 0) {
        $Amount = sprintf('%0.2f', $Amount);
        return number_format($Amount, 0);
    }

    public static function removeuploadgallery() {
        $file_id = !empty($_POST['fileId']) ? $_POST['fileId'] : '';
        $qry = "SELECT * FROM my_property_gallery WHERE pgallery_id='$file_id'";

        $imgArray = Database::select($qry);
        if (!empty($imgArray)) {
            $imgArray = $imgArray[0];
            $file_name = Config::get('constants.HOME_DIR') . 'files/hostgallery/' . $imgArray->file_name;
            if (is_file($file_name)) {
                unlink($file_name);
            }
            $cond = array('pgallery_id' => $file_id);
            Database::deletes('my_property_gallery', $cond);
        }
        die(json_encode(array('status' => true, '' => '')));
    }

    public static function GetnewPropertyNo() {
        $qry = "SELECT COUNT(property_no) AS total FROM `my_property`";
        $dArray = Database::selectSingle($qry);
        $totalTimesheet = $dArray->total + 1;
        $totalTimesheet = str_pad($totalTimesheet, 3, "0", STR_PAD_LEFT);
        return 'REF-' . $totalTimesheet;
    }

    public static function GetPropertyImagesCover($pArray = []) {
        $defaultLogo = Config::get('constants.DEFAULT_PROPERTY_LOGO');
        $coverphoto = !empty($pArray->coverphoto) ? '/files/hostgallery/' . $pArray->coverphoto : '';
        $first_image = !empty($pArray->first_image) ? '/files/hostgallery/' . $pArray->first_image : '';
        if (is_file(Config::get('constants.HOME_DIR') . $coverphoto)) {
            return url($coverphoto);
        } else if (is_file(Config::get('constants.HOME_DIR') . $first_image)) {
            return url($first_image);
        } else {
            return url($defaultLogo);
        }
    }

    public static function listwithusEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.LIST_WITH_US'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function newdelevopmentEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.NEW_DEVELOPMENTS'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function areaGuideEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.AREA_GUIDE'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function delevopmentEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.DEVELOPMENTS'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function careersEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];

        $file = !empty($_FILES['cv']) ? $_FILES['cv'] : [];

        if (!empty($file['name'])) {
            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
            $baseDir = Config::get('constants.HOME_DIR') . "files/careers-cv/" . $fileName;
            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                $json['cv_attached'] = $fileName;
            }
        }


        $primaryId = Database::insert('careers_enquiry', $json);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.CAREERS'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function contactusEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.CONTACT_US'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function blogdetailpageEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.BLOG_DETAILS'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function propertylistPageNoRecEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];
        $primaryId = Database::insert('contact_us', $json);
        \App\Helpers\HTMLHelper::sendMailforcontactUs($primaryId);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.BLOG_DETAILS'));
        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function mypropertyEnq() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $json['enq_number'] = LibHelper::getNewenquiryno();

        $json['lastupdate'] = date('Y-m-d H:i:s');
        $json['timestamp'] = $json['lastupdate'];

        $pArray = LibHelper::GetmypropertyBy($json['property_id']);

        $images = CommonHelper::GetPropertyImagesCover($pArray);

        $primaryId = Database::insert('enquiry_management', $json);
        CommonHelper::addNotifaction($primaryId, Config::get('constants.ADMIN_NOTIFY.NEW_PROPERTY_ENQ'));

        $normal_msg = 'We thank you for your inquiry. Our Excel Specialists are now processing your request and will contact you shortly. If you require immediate assistance, please contact us at your convenience.';
        $messages = view("admin.EmailTemplate.general_enquiry")->with([
            'name' => $json['name'],
            'normal_msg' => $normal_msg,
            'enq_logo' => $images,
            'is_file' => false,
            'downloadBtnName' => false,
            'emailAttachment' => false
        ]);
        $subject = "Request submitted successfully.";

        CommonHelper::send_mail($json['email'], $subject, $messages, $cc = '', $attachment = '', $lay = 'No');

        $adminmsg = ""
                . "<p>Dear admin,</p>"
                . "<p>We have received one inquiry</p>"
                . "<p>Name : " . $json['name'] . "</p>"
                . "<p>Email : " . $json['email'] . "</p>"
                . "<p>Reference : <a href='" . $json['page_url'] . "'>".$json['page_url']."</a></p>"
                . "";
        CommonHelper::send_mail('upendra@alwafaagroup.com', $subject, $adminmsg, $cc = '', $attachment = '', $lay = 'Yes');


        die(json_encode(array('status' => true, 'messages' => 'Request sent successfully')));
    }

    public static function subscribeBlog() {
        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
        $Sql = "SELECT * FROM `blog_subscribe` WHERE email='" . $json['email'] . "'";
        $data = Database::selectSingle($Sql);
        $json['timestamp'] = date('Y-m-d H:i:s');
        if (empty($data)) {
            $primaryId = Database::insert('blog_subscribe', $json);
        }
        die(json_encode(array('status' => true, 'messages' => 'Subscribe successfully')));
    }

    public static function addWaterMarkImage($fileType = '', $fullpathImg = '') {
        error_reporting(0);
        $stampImg = Config::get('constants.HOME_DIR') . 'public/images/water-mark-logo.png';
        $stamp = imagecreatefrompng($stampImg);
        $fileType = strtolower($fileType);
        switch ($fileType) {
            case 'jpg':
                $im = imagecreatefromjpeg($fullpathImg);
                break;
            case 'jpeg':
                $im = imagecreatefromjpeg($fullpathImg);
                break;
            case 'png':
                $im = imagecreatefrompng($fullpathImg);
                break;
            default:
                $im = imagecreatefromjpeg($fullpathImg);
        }

        $sx = imagesx($stamp);
        $sy = imagesy($stamp);
        imagecopy($im, $stamp, (imagesx($im) - $sx) / 2, (imagesy($im) - $sy) / 2, 0, 0, imagesx($stamp), imagesy($stamp));
        // header('Content-type: image/png');
        imagepng($im, $fullpathImg);
        imagedestroy($im);
        return true;
    }

    public static function addNotifaction($primaryId = '', $notificationType = '') {
        $add['primary_id'] = $primaryId;
        $add['notification_type'] = $notificationType;
        $add['notification_title'] = 'Unknown notification';
        $add['notification_url'] = '';
        $add['timestamp'] = date('Y-m-d H:i:s');
        if ($notificationType == Config::get('constants.ADMIN_NOTIFY.LIST_WITH_US')) {
            $add['notification_title'] = "New Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.NEW_DEVELOPMENTS')) {
            $add['notification_title'] = "New Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.DEVELOPMENTS')) {
            $add['notification_title'] = "Devlopment Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.AREA_GUIDE')) {
            $add['notification_title'] = "New Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.CAREERS')) {
            $add['notification_title'] = "New careers Inquiry.";
            $add['notification_url'] = '/admin/careersenquiry';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.CONTACT_US')) {
            $add['notification_title'] = "New Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.BLOG_DETAILS')) {
            $add['notification_title'] = "New blog Inquiry.";
            $add['notification_url'] = '/admin/contactus';
        } else if ($notificationType == Config::get('constants.ADMIN_NOTIFY.NEW_PROPERTY_ENQ')) {
            $enq = LibHelper::GetenquirymanagementByenqId($primaryId);
            $add['notification_title'] = "New property Inquiry. #" . $enq->enq_number;
            $add['notification_url'] = 'admin/enquiry-details?enqId=' . base64_encode($primaryId);
        }
        $notifactionId = Database::insert('notification', $add);
        return $notifactionId;
    }

    public static function send_mail($email_id, $subject, $messages, $cc = '', $file_path = '', $useTemp = 'Yes') {
        if ($useTemp == 'Yes') {
            $messages = view("admin.EmailTemplate.general")->with([
                'emailbody' => $messages,
            ]);
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://demo.softwarecompany.ae/facility-management-system/login/send_mail', // url('data/fronted/send_mail')
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
            $params = array(
        'email_id' => $email_id,
        'subject' => $subject,
        'messages' => $messages,
        'file_path' => $file_path,
        'cc' => $cc
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return true;
//        $headers = "MIME-Version: 1.0" . "\r\n";
//        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//        $headers .= 'From: <info@alwafaagroup.com>' . "\r\n";
//        //    $headers .= 'Cc: sales@example.com' . "\r\n";
//        mail($email_id, $subject, $messages, $headers);
    }

    public static function Getaccesstoken() {
        set_time_limit(0);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dubaipulse.gov.ae/oauth/client_credential/accesstoken?grant_type=client_credentials",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "client_id=XkbaOEJetyVxnMtjArp5jS1WlGkvExNX&client_secret=lGTY6KrDo75wsLfi",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: 2b251fd1-a9fd-0f66-ac39-1b0519e4b783"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            die();
        } else {
            $response = json_decode($response, true);
            $access_token = !empty($response['access_token']) ? $response['access_token'] : false;
            return $access_token;
        }
    }

    public static function Getrta_tram_stations() {
        set_time_limit(0);

        $accesstoken = \App\Helpers\CommonHelper::Getaccesstoken();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dubaipulse.gov.ae/shared/rta/rta_tram_stations-open-api",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $accesstoken",
                "cache-control: no-cache",
                "postman-token: f05d86b5-7a5d-0c9a-95c5-413ee2a308a5"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);
            return (!empty($response['results']) ? $response['results'] : '');
        }
    }

}
