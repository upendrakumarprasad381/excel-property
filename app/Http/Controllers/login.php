<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Database;
use Illuminate\Support\Facades\Session;
use Config;

class login extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function adminlogin() {
        if (!empty($_POST['json']) && is_array($_POST['json'])) {

            $json = $_POST['json'];
            $user_name = $json['user_name'];
            $password = $json['password'];
            $Md5Password = md5($password);
            $Sql = "SELECT VD.*  FROM vendor_details VD WHERE VD.email='$user_name' AND VD.password='$Md5Password' ";
            $lArray = Database::select($Sql);

            if (!empty($lArray) && count($lArray) == '1') {
                $lArray = $lArray[0];
                if ($lArray->archive == 0) {
                    session()->put(Config::get('constants.VENDOR_SESSION_NAME'), $lArray);
                    Session::save();
                    $control = 'admin';
                    die(json_encode(array('status' => true, 'URL' => url($control), 'message' => 'Login Successful')));
                } else {
                    die(json_encode(array('status' => false, 'message' => 'Your account has been blocked.')));
                }
            } else {
                die(json_encode(array('status' => false, 'message' => 'Invalid username or password.')));
            }
        }
        return view('admin.adminlogin');
    }

    public function helper() {
     
        $function = !empty($_POST['function']) ? $_POST['function'] : '';
        $helper = !empty($_POST['helper']) ? $_POST['helper'] : '';
        if ($helper == 'Common') {
          
            \App\Helpers\CommonHelper::$function();
        } else if ($helper == 'Lib') {
            \App\Helpers\LibHelper::$function();
        }else if ($helper == 'HTML') {
            \App\Helpers\HTMLHelper::$function();
        } else {
            die(json_encode(array('status' => false, 'message' => 'Invalid helper name')));
        }
    }

}
