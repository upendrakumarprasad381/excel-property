<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Database;
use Illuminate\Support\Facades\Session;
use App\Helpers\CommonHelper;
use App\Helpers\LibHelper;
use Illuminate\Support\Facades\Redirect;
use Config;
use App\Jobs\SendEmailJob;

class admin extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (empty(Session::get(Config::get('constants.VENDOR_SESSION_NAME')))) {
                return Redirect::to('admin-login');
            } else {
                return $next($request);
            }
        });
    }

    public function index() {

        return view('admin.index');
    }

    public function propertytype() {
        return view('admin.propertytype');
    }

    public function addpropertytype() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');


            $pArray = LibHelper::GetpropertytypeBy($json['ptype_id']);

            $file = !empty($_FILES['banner']) ? $_FILES['banner'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
                    $json['banner'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            if (!empty($json['ptype_id'])) {
                $cond = array('ptype_id' => $json['ptype_id']);
                Database::updates('property_type', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('property_type', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addpropertytype');
    }

    public function normalpropertytype() {
        return view('admin.normalpropertytype');
    }

    public function addnormalpropertytype() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($json['id'])) {
                $cond = array('id' => $json['id']);
                Database::updates('property_type_others', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('property_type_others', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addnormalpropertytype');
    }

    public function keywords() {
        return view('admin.keywords');
    }

    public function addkeywords() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($json['keywords_id'])) {
                $cond = array('keywords_id' => $json['keywords_id']);
                Database::updates('keywords', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('keywords', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addkeywords');
    }

    public function propertyprice() {
        return view('admin.propertyprice');
    }

    public function addpropertyprice() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($json['price_id'])) {
                $cond = array('price_id' => $json['price_id']);
                Database::updates('property_price', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('property_price', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addpropertyprice');
    }

    public function tower() {
        return view('admin.tower');
    }

    public function addtower() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($json['tower_id'])) {
                $cond = array('tower_id' => $json['tower_id']);
                Database::updates('tower_list', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('tower_list', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addtower');
    }

    public function tags() {
        return view('admin.tags');
    }

    public function addtags() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $offplain = [21, 22];
            
            
            
             $pArray = LibHelper::GettagseBytagId($json['tag_id']);

            $file = !empty($_FILES['banner']) ? $_FILES['banner'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
                    $json['banner'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            if (!empty($json['tag_id'])) {
                $cond = array('tag_id' => $json['tag_id']);
                if (in_array($json['tag_id'], $offplain)) {
                    unset($json['slugs']);
                }
                Database::updates('tags', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('tags', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addtags');
    }

    public function submaster() {
        return view('admin.submaster');
    }

    public function addsubmaster() {
        return view('admin.addsubmaster');
    }

    public function newdevelopments() {
        return view('admin.newdevelopments');
    }

    public function projects() {
        return view('admin.projects');
    }

    public function addnewdevelopments() {


        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $pArray = LibHelper::GetnewdevelopmentsByndid($json['ndid']);

            $file = !empty($_FILES['banner']) ? $_FILES['banner'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
                    $json['banner'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $file = !empty($_FILES['featured_image']) ? $_FILES['featured_image'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->featured_image) ? $pArray->featured_image : '');
                    $json['featured_image'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $file = !empty($_FILES['logo_on_banner']) ? $_FILES['logo_on_banner'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->logo_on_banner) ? $pArray->logo_on_banner : '');
                    $json['logo_on_banner'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $file = !empty($_FILES['register_interest_logo']) ? $_FILES['register_interest_logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->register_interest_logo) ? $pArray->register_interest_logo : '');
                    $json['register_interest_logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $file = !empty($_FILES['brochure_file']) ? $_FILES['brochure_file'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->brochure_file) ? $pArray->brochure_file : '');
                    $json['brochure_file'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            $file = !empty($_FILES['floorplan_file']) ? $_FILES['floorplan_file'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->floorplan_file) ? $pArray->floorplan_file : '');
                    $json['floorplan_file'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $json['completion'] = $json['completion'];
            if (!empty($json['ndid'])) {
                $cond = array('ndid' => $json['ndid']);
                Database::updates('new_developments', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                $json['ndid'] = Database::insert('new_developments', $json);


                $updatepos['position'] = $json['ndid'];
                Database::updates('new_developments', $updatepos, ['ndid' => $json['ndid']]);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addnewdevelopments');
    }

    public function subadmin() {
        return view('admin.subadmin');
    }

    public function addsubadmin() {
        if (isset($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $vendorId = $json['vendor_id'];
            $email = $json['email'];
            if ($vendorId) {
                $validate = "SELECT * FROM `vendor_details` WHERE email LIKE '$email' AND vendor_id!=$vendorId";
            } else {
                $validate = "SELECT * FROM `vendor_details` WHERE email LIKE '$email'";
            }
            $validateAr = Database::select($validate);
            if ($validateAr) {
                die(json_encode(array('status' => false, 'message' => 'This email or user name already exist.')));
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                die(json_encode(array('status' => false, 'message' => 'Invalid email format')));
            }
            if (empty($json['password'])) {
                unset($json['password']);
            } else {
                $json['password'] = md5($json['password']);
            }
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($vendorId)) {
                $cond = array('vendor_id' => $vendorId);
                Database::updates('vendor_details', $json, $cond);
            } else {
                $json['timestamp'] = $json['lastupdate'];
                Database::insert('vendor_details', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully updated.')));
        }
        return view('admin.addsubadmin');
    }

    public function propertyfacility() {
        return view('admin.propertyfacility');
    }

    public function addpropertyfacility() {
        return view('admin.addpropertyfacility');
    }

    public function customers() {
        return view('admin.customers');
    }

    public function myproperty() {
        return view('admin.myproperty');
    }

    public function addmyproperty() {
        if (!empty($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $pArray = LibHelper::GetmypropertyBy($json['property_id']);



            $file = !empty($_FILES['pdf_map_loc']) ? $_FILES['pdf_map_loc'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->pdf_map_loc) ? $pArray->pdf_map_loc : '');
                    $json['pdf_map_loc'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $json['facility'] = $json['facility'];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $isNew = false;
            if (!empty($json['property_id'])) {
                $cond = array('property_id' => $json['property_id']);
                Database::updates('my_property', $json, $cond);
            } else {
                $isNew = true;
                $json['timestamp'] = date('Y-m-d H:i:s');
                $json['property_id'] = Database::insert('my_property', $json);

                $updatepos['position'] = $json['property_id'];
                Database::updates('my_property', $updatepos, ['property_id' => $json['property_id']]);
            }

            $condddrt = array('property_id' => 0);
            $updateghy = array('property_id' => $json['property_id']);
            Database::updates('my_property_gallery', $updateghy, $condddrt);

            die(json_encode(array('status' => true, 'propertyId' => $json['property_id'], 'isNew' => $isNew, 'message' => 'Successfully')));
        }
        return view('admin.addmyproperty');
    }

    public function uploadpropertygallery() {

        $coverphoto = !empty($_FILES['file']) && empty($_FILES['file']['error']) ? $_FILES['file'] : [];
        $extension = array("png", "jpg", "jpeg");
        if (!empty($coverphoto) && in_array(pathinfo($coverphoto['name'], PATHINFO_EXTENSION), $extension)) {
            $data = getimagesize($coverphoto['tmp_name']);
            $width = !empty($data[0]) ? $data[0] : '0';
            $height = !empty($data[1]) ? $data[1] : '0';
            if ($width < 750 || $height < 595) {
                die(json_encode(array('status' => false, 'message' => 'Invalid height width please try again')));
            }
            $extFile = pathinfo($coverphoto['name'], PATHINFO_EXTENSION);
            $fileName = uniqid() . '.' . $extFile;
            $movePath = Config::get('constants.HOME_DIR') . 'files/hostgallery/' . $fileName;
            if (move_uploaded_file($coverphoto['tmp_name'], $movePath)) {
                $insert = array(
                    'property_id' => !empty($_POST['property_id']) ? $_POST['property_id'] : '',
                    'file_name' => $fileName,
                    'timestamp' => date('Y-m-d H:i:s'),
                );
                $InsertId = Database::insert('my_property_gallery', $insert);
                CommonHelper::addWaterMarkImage($extFile, $movePath);
                die(json_encode(array('status' => true, 'insertId' => $InsertId)));
            }
        } else {
            die(json_encode(array('status' => false, 'message' => 'Invalid file')));
        }
        die(json_encode(array('status' => true, '' => '')));
    }

    public function myenquiry() {
        return view('admin.myenquiry');
    }

    public function blogs() {
        return view('admin.blogs');
    }

    public function addblogs() {
        if (!empty($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $pArray = LibHelper::GetblogsByblogsId($json['blog_id']);


            $UploadExcel = !empty($_FILES['attachment']) ? $_FILES['attachment'] : [];
            if (!empty($UploadExcel['name'])) {
                $fileName = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->attachment) ? $pArray->attachment : '');
                    $json['attachment'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            $UploadExcel = !empty($_FILES['attachment_home']) ? $_FILES['attachment_home'] : [];
            if (!empty($UploadExcel['name'])) {
                $fileName = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->attachment_home) ? $pArray->attachment_home : '');
                    $json['attachment_home'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }




            $json['lastupdate'] = date('Y-m-d H:i:s');
            $isNew = false;
            if (!empty($json['blog_id'])) {
                $cond = array('blog_id' => $json['blog_id']);
                Database::updates('blogs', $json, $cond);
            } else {
                $isNew = true;
                $json['timestamp'] = date('Y-m-d H:i:s');
                $json['blog_id'] = Database::insert('blogs', $json);
            }
            die(json_encode(array('status' => true, 'blogId' => $json['blog_id'], 'isNew' => $isNew, 'message' => 'Successfully')));
        }
        return view('admin.addblogs');
    }

    public function testimonial() {

//        
        \App\Helpers\HTMLHelper::sendMailforcontactUs($contactId = '99');
//        
//        
//       
//       // CommonHelper::send_mail('upendra@alwafaagroup.com', $subject='dasdadas', $messages,$cc = '', $file_path = '', $useTemp = 'No');
//    
//       exit;
        return view('admin.testimonial');
    }

    public function addtestimonial() {
        if (!empty($_POST['json']) && is_array($_POST['json'])) {
            $json = $_POST['json'];
            $pArray = LibHelper::GettestimonialById($json['id']);
            $UploadExcel = !empty($_FILES['attachment']) ? $_FILES['attachment'] : [];
            if (!empty($UploadExcel['name'])) {
                $fileName = uniqid() . '.' . pathinfo($UploadExcel['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($UploadExcel['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->attachment) ? $pArray->attachment : '');
                    $json['attachment'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $isNew = false;
            if (!empty($json['id'])) {
                $cond = array('id' => $json['id']);
                Database::updates('testimonial', $json, $cond);
            } else {
                $isNew = true;
                $json['timestamp'] = date('Y-m-d H:i:s');
                $json['id'] = Database::insert('testimonial', $json);
            }
            die(json_encode(array('status' => true, 'id' => $json['id'], 'isNew' => $isNew, 'message' => 'Successfully')));
        }
        return view('admin.addtestimonial');
    }

    public function areaguides() {
        return view('admin.areaguides');
    }

    public function addareaguides() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $pArray = LibHelper::GetareaguidesByguidesId($json['guides_id']);


            $file = !empty($_FILES['logo']) ? $_FILES['logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                    $json['logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            $file = !empty($_FILES['banner_logo']) ? $_FILES['banner_logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->banner_logo) ? $pArray->banner_logo : '');
                    $json['banner_logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            if (!empty($json['guides_id'])) {
                $cond = array('guides_id' => $json['guides_id']);
                Database::updates('area_guides', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('area_guides', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addareaguides');
    }

    public function agents() {
        return view('admin.agents');
    }

    public function addagents() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $pArray = LibHelper::GetagentsByagentsId($json['agents_id']);


            $file = !empty($_FILES['logo']) ? $_FILES['logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                    $json['logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            if (!empty($json['agents_id'])) {
                $cond = array('agents_id' => $json['agents_id']);
                Database::updates('agents', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('agents', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addagents');
    }

    public function team() {
        return view('admin.team');
    }

    public function addteam() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $pArray = LibHelper::GetteamId($json['team_id']);


            $file = !empty($_FILES['logo']) ? $_FILES['logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                    $json['logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }


            if (!empty($json['team_id'])) {
                $cond = array('team_id' => $json['team_id']);
                Database::updates('team', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('team', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addteam');
    }

    public function contactus() {
        return view('admin.contactus');
    }

    public function cms() {
        return view('admin.cms');
    }

    public function listwithuscms() {
        return view('admin.listwithuscms');
    }

    public function servicescms() {
        return view('admin.servicescms');
    }

    public function privacypolicycms() {
        return view('admin.privacypolicycms');
    }

    public function termsconditioscms() {
        return view('admin.termsconditioscms');
    }

    public function aboutuscms() {
        return view('admin.aboutuscms');
    }

    public function enquirydetails() {
        return view('admin.enquirydetails');
    }

    public function bannermanagement() {
        return view('admin.bannermanagement');
    }

    public function addbannermanagement() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            $pArray = LibHelper::GetbannermanagementBybannerId($json['banner_id']);


            $file = !empty($_FILES['logo']) ? $_FILES['logo'] : [];
            if (!empty($file['name'])) {
                $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                    $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                    $json['logo'] = $fileName;
                    if (is_file($oldfile)) {
                        unlink($oldfile);
                    }
                }
            }

            if (!empty($json['banner_id'])) {
                $cond = array('banner_id' => $json['banner_id']);
                Database::updates('banner_management', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('banner_management', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addbannermanagement');
    }

    public function arealist() {
        return view('admin.arealist');
    }

    public function addarealist() {
        if (!empty($_POST['json'])) {
            $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
            $json['lastupdate'] = date('Y-m-d H:i:s');
            if (!empty($json['area_id'])) {
                $cond = array('area_id' => $json['area_id']);
                Database::updates('area_list', $json, $cond);
            } else {
                $json['timestamp'] = date('Y-m-d H:i:s');
                Database::insert('area_list', $json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        return view('admin.addarealist');
    }

    public function careerscms() {
        return view('admin.careerscms');
    }

    public function careersenquiry() {
        return view('admin.careersenquiry');
    }

    public function tagdetails() {
        return view('admin.tagdetails');
    }

    public function adminlogout() {
        session()->forget(Config::get('constants.VENDOR_SESSION_NAME'));
        session()->save();
        return Redirect::to('admin-login');
    }

}
