<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;

class properties extends Controller {

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('properties.index');
    }

    public function property() {
        return view('properties.property');
    }

    public function propertydetails($stringId, $propertyslugId) {

        return view('properties.propertydetails')->with([
                    'stringId' => $stringId,
                 
                    'propertyslugId' => $propertyslugId,
        ]);
    }

    public function listwithus() {
        return view('properties.listwithus');
    }

    public function developments($developerId = '') {
        return view('properties.developments')->with([
                    'developerId' => $developerId,
        ]);
    }

    public function newdevelopments($developerId = '', $projectsId = '') {
        return view('properties.newdevelopments')->with([
                    'developerId' => $developerId,
                    'projectsId' => $projectsId,
        ]);
    }

    public function areaguidedetails($areaId = '') {
        return view('properties.areaguidedetails')->with([
                    'areaId' => $areaId,
        ]);
    }

    public function moreareaguides() {
        return view('properties.moreareaguides');
    }
    public function areaguideproperties($areaId = '') {
        return view('properties.areaguideproperties')->with([
                    'areaId' => $areaId,
        ]);
    }
    public function aboutus() {
        return view('properties.aboutus');
    }

    public function services() {
        return view('properties.services');
    }

    public function team() {
        return view('properties.team');
    }

    public function careers() {
        return view('properties.careers');
    }

    public function testimonals() {
        return view('properties.testimonals');
    }

    public function contactus() {
        return view('properties.contactus');
    }

    public function blog() {
        return view('properties.blog');
    }

    public function blogdetail($blogslugId) {
        return view('properties.blogdetail')->with([
                    'blogslugId' => $blogslugId
        ]);
    }

    public function privacypolicy() {
        return view('properties.privacypolicy');
    }

    public function termsconditios() {
        return view('properties.termsconditios');
    }

    public function sitemap() {
        return view('properties.sitemap');
    }

    public function thankyou($typeId = '') {
        $msg = '<h3>Thank you for registering your Inquiry. </h3>
            <h6>Our team will get in touch with you, shortly.</h6>';
        if ($typeId == 'for-download-brochure') {
            $msg = '<h3>Thankyou !<br>Please check your email for brochure</h3>';
        }
        if ($typeId == 'for-download-floor-plan') {
            $msg = '<h3>Thankyou !<br>Please check your email for Floor plan</h3>';
        }
        return view('properties.thankyou')->with([
                    'message' => $msg
        ]);
    }

    public function saletransaction() {
        return view('properties.saletransaction');
    }

    public function moredevelopments() {
        return view('properties.moredevelopments');
    }

    public function offplanFrommenu() {
        return view('properties.offplanFrommenu');
    }

    public function realestatedevelopers() {
        return view('properties.realestatedevelopers');
    }

}
