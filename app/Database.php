<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DB;

class Database extends Model {

    public static function select($Sql) {
        $results = DB::select($Sql);
        return $results;
    }

    public static function selectSingle($Sql) {
        $results = DB::select($Sql);
        $results = !empty($results) ? $results[0] : '';
        return $results;
    }

    public static function insert($table, $insert) {
        $results = DB::table($table)->insertGetId($insert);
        return $results;
    }

    public static function updates($table, $update, $cond) {
        $affected = DB::table($table)->where($cond)->update($update);
        return $affected;
    }

    public static function updateOrinsert($table, $update, $matches) {
        $result = DB::table($table)->where($matches)->get()->first();
        if (!empty($result)) {
            return Database::updates($table, $update, $matches);
        } else {
            return Database::insert($table, $update);
        }
    }

    public static function deletes($table, $cond) {
        return DB::table($table)->where($cond)->delete();
    }

}
