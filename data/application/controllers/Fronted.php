<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fronted extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->Model('Database');
        date_default_timezone_set('Asia/Dubai');
    }

    public function index() {
        echo 'dasdasd';
    }

    public function send_mail() {
        if (isset($_POST['email_id']) && !empty($_POST['subject']) && !empty($_POST['messages'])) {
            $file_path = !empty($_POST['file_path']) ? $_POST['file_path'] : '';
            $cc = !empty($_POST['cc']) ? $_POST['cc'] : '';
            send_mail($_POST['email_id'], $_POST['subject'], $_POST['messages'], $file_path, $cc);
            echo 'sent..';
        }
    }

    public function viewPdf() {
        // error_reporting(0);

        $propertyId = !empty($_REQUEST['propertyId']) ? base64_decode($_REQUEST['propertyId']) : '5';
        $Sql = "SELECT * FROM `my_property` WHERE property_id ='$propertyId'";
        $pArray = $this->Database->select_qry_array($Sql);
        if (empty($pArray)) {
            die();
        }
        $pArray = $pArray[0];


        set_time_limit(0);
        $mpdfConfig = array(
            'mode' => 'utf-8',
            'format' => 'A4',
            'default_font_size' => 0,
            'default_font' => '',
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => '28',
            'margin_bottom' => '30',
            'margin_header' => 0,
            'margin_footer' => 0,
            'orientation' => 'P'
        );

        $this->load->library("pdf");
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);


        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = 'UTF-8';
        $mpdf->SetHTMLHeader($this->loadHeader($pArray));
        $mpdf->SetHTMLFooter($this->loadFooter());

        $html = $this->load->view('productspdf', false, true);


        $mpdf->WriteHTML(utf8_encode($html));
        $mpdf->Output('excel-properties.pdf', 'I');
    }

    public function loadFooter() {




        ob_start();
        ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#2a2a2a; color:#FFF; font-family:Tahoma, Geneva, sans-serif; ">
            <tr>
                <td width="300" height="115" align="left" valign="middle" style="padding-left:40px; ">

                    <h4 style="margin:0 0 5px;; padding:0; font-size:18px; color:#fff;">Phone: 
                        <a style="color:#fff; text-decoration:none;" href="tel:+97143364447">+971 43364447</a></h4>
                    <h6 style="margin:0 0; padding:0; font-size:12px; font-weight:normal;">Email:
                        <a  style="color:#fff; text-decoration:none;" href="mailto:inquiry@excelproperties.ae">inquiry@excelproperties.ae</a></h6>


                </td>
                <td width="553"  height="115" align="right" valign="middle" 
                    style="padding-right:40px; font-size:11px; line-height:20px;
                    "><a  style="color:#fff; text-decoration:none; font-size:14px;" href="https://excelproperties.ae/">www.excelproperties.ae</a><br />
                    Burj Al Salam Tower, Office No. 1804, Trade Center First, Shk. Zayed Road. Dubai, UAE</td>
            </tr>

        </table>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    public function loadHeader($pArray) {
        $ptype = empty($pArray->ptype) ? 'SALE' : 'RENT';
        ob_start();
        ?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Tahoma, Geneva, sans-serif;  color:#fff; background:#000000;">
            <tr>
                <td width="60%" height="100" align="left" valign="middle" style="padding-left:40px;"><img width="171" src="<?= base_url('data/pdf-image') ?>/logo.svg" /></td>
                <td width="40%" align="center" valign="middle"  style="font-family:Tahoma, Geneva, sans-serif;  color:#fff; font-size:28px; font-weight:bold;">FOR <?= $ptype ?></td>
            </tr>

        </table>
        <?php
        $html = ob_get_clean();
        return $html;
    }

}
