<?php

use Facebook\FacebookRequest;

class Facebook {

    private $Fb;

    public function __construct() {
        require_once HOME_DIR . '/application/third_party/Facebook/vendor/autoload.php';
        $this->Fb = new \Facebook\Facebook([
            'app_id' => '195287524504296',
            'app_secret' => '66dcf742646c5aad9cdf855a27a98023',
            'default_graph_version' => 'v3.0',
                //'default_access_token' => '{access-token}', // optional
        ]);
    }

    public function Login() {
        $RedirectUrl = base_url() . 'Login/FacebookLoginDetails';
        $helper = $this->Fb->getRedirectLoginHelper();
        $permissions = ['email'];
        $loginUrl = $helper->getLoginUrl($RedirectUrl, $permissions);
        header('Location: ' . filter_var($loginUrl, FILTER_SANITIZE_URL));
    }

    public function UserInfo($state) {
        $helper = $this->Fb->getRedirectLoginHelper();
        if ($state != '') {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }
        try {
            $accessToken = $helper->getAccessToken();
            $response = $this->Fb->get('/me?fields=id,name,email,picture,birthday,gender', $accessToken);
            $Array = $response->getGraphUser();
            if (isset($Array['id'])) {
                $ReturnArray = array(
                    'Live' => true,
                    'SocialType' => 1,
                    'AccountId' => $Array['id'],
                    'AccountName' => $Array['name'],
                    'UserProfilePic' => $Array['picture']['url'],
                    'Email' => $Array['email'],
                    'Gender' => $Array->getGender(),
                    'Birthday' => $Array->getBirthday(),
                );
                return $ReturnArray;
            }
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'ERROR returned an error: ' . $e->getMessage();
            exit;
        }
    }

    public function post() {
        $attachment = array(
            'caption' => 'goorlon is a programming blog.',
            'url' => 'http://www.codexworld.com/wp-content/uploads/2015/12/www-codexworld-com-programming-blog.png',
        );
        $helper = $this->Fb->getRedirectLoginHelper();
        if ($helper->getAccessToken()) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
            $accessToken = $helper->getAccessToken();
            $client = $this->Fb->getOAuth2Client();
            $accessToken = $client->getLongLivedAccessToken($accessToken);

            try {





                $response = $this->Fb->post('me/photos', $attachment, $accessToken);

                echo '<pre>';
                print_r($response);
                exit;
            } catch (FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                exit;
            }
        } else {
            $RedirectUrl = base_url() . 'Share/Facebook';
            $helper = $this->Fb->getRedirectLoginHelper();
            $permissions = ['email'];
            $loginUrl = $helper->getLoginUrl($RedirectUrl, $permissions);
            header('Location: ' . filter_var($loginUrl, FILTER_SANITIZE_URL));
        }
    }

}
