<?php

class Google {

    private $Client;
    private $Plus;

    public function __construct() {
        $dir = HOME_DIR . 'application/third_party/Google/';
        require_once $dir . 'vendor/autoload.php';
        $this->Client = new Google_Client();
        $this->Client->setAuthConfig($dir . 'client_secret.json');
        $this->Plus = new Google_Service_Plus($this->Client);
        $Scope = array(
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
        );
        $this->Client->addScope($Scope);
        $RedirectUri = base_url('Login/GoogleLoginDetails');
        $this->Client->setRedirectUri($RedirectUri);
    }

    public function Login() {
        $auth_url = $this->Client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
    }

    public function UserInfo($Code) {
        $this->Client->authenticate($Code);
        $TockenArray = $this->Client->getAccessToken();
        if ($this->Client->getAccessToken()) {
            $me = $this->Plus->people->get('me');
            $returnArray = array(
                'Live' => true,
                'SocialType' => 2,
                'AccountId' => $me['id'],
                'AccountName' => $me['displayName'],
                'UserProfilePic' => $me['modelData']['image']['url'],
                'Email' => $me['modelData']['emails'][0]['value'],
                'Gender' => $me['gender'],
                'Birthday' => $me['birthday'],
            );
            return $returnArray;
        }
    }

}
