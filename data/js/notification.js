$(document).ready(function () {
    $('#NextYearCollection').click(function () {
        var NextYear = $('#NextYearCollection').attr('NextYear');
        OrderCollectionYear(NextYear);
    });
    $('#PreYearCollection').click(function () {
        var PreYear = $('#PreYearCollection').attr('PreYear');
        OrderCollectionYear(PreYear);
    });
    $('body').on('click', '.ReadMe', function () {
        var CallBackURL = $(this).attr('CallBackURL');
        var data = {function: 'ReadMe', tablename: $(this).attr('tablename'), 'fieldname': $(this).attr('fieldname'), primaryid: $(this).attr('primaryid'), PrimaryIdName: $(this).attr('PrimaryIdName')};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    window.location = CallBackURL;
                }
            }
        });
    });
    $('.RejectApproveReview').click(function () {
        var status = $(this).attr('status');
        var primaryid = $(this).attr('primaryid');
        var message = status == 1 ? 'Are you sure want to approved.' : 'Are you sure want to rejected.'
        $.confirm({
            title: message,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var data2 = {id: primaryid, status: status};
                        ChangeReviewStatus(data2, true);
                    }
                },
                cancel: {
                    text: 'cancel',
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });
    });
});












function AdminHeaderNotification() {
    var data = {function: 'AdminHeaderNotification'};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                $('#HeaderNotificationHtml').empty();
                $('#HeaderNotificationHtml').append(json.HTML);
                $('.TodayNotifaction').html(json.TotalVal);
            }
        }
    });
}
function LobiboxNotifyOrder() {
    var data = {function: 'LobiboxNotifyOrder'};
    var OrderId = '';

    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                var NewOrder = json.NewOrder;
                if (!$('.jconfirm').hasClass('jconfirm-modern') && NewOrder.length > 0) {
                    var d = NewOrder[0];
                    OrderId = d.order_id;
                    $.confirm({
                        title: 'New Order!',
                        content: 'New order has been received by ' + d.user_name + ' Order Number:' + d.order_number + '.',
                        icon: 'fab fa-first-order',
                        theme: 'modern',
                        closeIcon: true,
                        animation: 'scale',
                        type: 'blue',
                        buttons: {
                            okay: {
                                text: 'Read More',
                                btnClass: 'btn-success',
                                action: function () {
                                    CloseNotifyOrder(OrderId, true)
                                }
                            },
                            Read: {
                                text: 'Make AS READ',
                                btnClass: 'btn-danger',
                                action: function () {
                                    CloseNotifyOrder(OrderId, false);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}
function CloseNotifyOrder(OrderId, IsRedirect = false) {
    var data = {function: 'CloseNotifyOrder', OrderId: OrderId}
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if (IsRedirect == true) {
                window.location = base_url + 'Admin/order_summary/' + json.OrderId;
            }
        }
    });
}

function TopSellingProducts(Year = false) {
    var data = {function: 'TopSellingProducts', Year: Year};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var Json = jQuery.parseJSON(data);
            AmCharts.makeChart("chartdiv2", {
                "type": "serial",
                "theme": "light",
                "dataProvider": Json,
                "valueAxes": [{
                        "gridColor": "#FFFFFF",
                        "gridAlpha": 0.2,
                        "dashLength": 0
                    }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "TotalSell"
                    }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "ProductName",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "labelsEnabled": false,
                },
                "export": {
                    "enabled": true
                }

            });
        }
    });
}
function OrderCollectionYear(Year = false) {
    var data = {function: 'OrderCollectionYear', Year: Year};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var Json = jQuery.parseJSON(data);
            if (Json.status == true) {
                $('.TitleView').html(Json.Year);
                $('#NextYearCollection').attr('NextYear', parseInt(Json.Year) + 1);
                $('#PreYearCollection').attr('PreYear', parseInt(Json.Year) - 1);
                var chart = AmCharts.makeChart("chartdiv", {
                    "type": "serial",
                    "theme": "light",
                    "marginRight": 40,
                    "marginLeft": 40,
                    "autoMarginOffset": 20,
                    "mouseWheelZoomEnabled": true,
                    "dataDateFormat": "YYYY-MM-DD",
                    "valueAxes": [{
                            "id": "v1",
                            "axisAlpha": 0,
                            "position": "left",
                            "ignoreAxisWidth": true
                        }],
                    "balloon": {
                        "borderThickness": 1,
                        "shadowAlpha": 0
                    },
                    "graphs": [{
                            "id": "g1",
                            "balloon": {
                                "drop": true,
                                "adjustBorderColor": false,
                                "color": "#ffffff"
                            },
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "lineThickness": 2,
                            "title": "red line",
                            "useLineColorForBulletBorder": true,
                            "valueField": "value",
                            "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
                        }],
                    "chartScrollbar": {
                        "graph": "g1",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 80,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 1,
                        "cursorColor": "#258cbb",
                        "limitToGraph": "g1",
                        "valueLineAlpha": 0.2,
                        "valueZoomable": true
                    },
                    "valueScrollbar": {
                        "oppositeAxis": false,
                        "offset": 50,
                        "scrollbarHeight": 10
                    },
                    "categoryField": "date",
                    "categoryAxis": {
                        "parseDates": true,
                        "dashLength": 1,
                        "minorGridEnabled": true
                    },
                    "export": {
                        "enabled": true
                    },
                    "dataProvider": Json.Data
                });
                chart.addListener("rendered", zoomChart);
                zoomChart();
                function zoomChart() {
                    chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
                }
            } else {
                alert(Json.message)
            }
        }
    });
}
function ChangeReviewStatus(json, IsRefrsh = false) {
    var data = {function: 'ChangeReviewStatus', json: json};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                if (IsRefrsh == true) {
                    window.location = '';
                }
            } catch (e) {
                alert(e)
            }

        }
    });
}
    function UserReviewAlert() {
        var data = {function: 'UserReviewAlert'};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    var IsAlerted = $('.jconfirm-light').length
                    if (json.status == true && IsAlerted == 0) {
                        $.confirm({
                            title: json.title,
                            content: json.comments,
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                confirm: {
                                    text: 'Approve',
                                    btnClass: 'btn-success',
                                    action: function () {
                                        var data2 = {id: json.id, status: 1};
                                        ChangeReviewStatus(data2);
                                    }
                                },
                                cancel: {
                                    text: 'Remove',
                                    btnClass: 'btn-danger',
                                    action: function () {
                                        var data2 = {id: json.id, status: 2};
                                        ChangeReviewStatus(data2);
                                    }
                                },
                            }
                        });
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    }














// Calling this All function

setTimeout(function () {
    AdminHeaderNotification();
}, 1000);
//setInterval(function () {
//    LobiboxNotifyOrder();
//}, 50000);
//setInterval(function () {
//    UserReviewAlert();
//}, 50000);
//300000
if (Method == 'index') {
    TopSellingProducts();
    OrderCollectionYear();
}



