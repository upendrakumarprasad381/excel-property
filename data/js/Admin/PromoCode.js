$(document).ready(function () {
    $('#AddNewPromoCode').click(function () {
        var brands = $('#SelectedProductId option:selected');
        var SelectedProductId = [];
        $(brands).each(function () {
            SelectedProductId.push($(this).val());
        });
        var json = '';
        json = json + '{';
        if ($('#PromoCode').val() == '') {
            $('#PromoCode').css('border-color', 'red');
            return false;
        } else {
            json = json + '"PromoCode":"' + $('#PromoCode').val() + '",';
            $('#PromoCode').css('border-color', '');
        }
        json = json + '"product_id":"' + SelectedProductId + '",';
        json = json + '"dateFrom":"' + $('#dateFrom').val() + '",';
        json = json + '"dateTo":"' + $('#dateTo').val() + '",';
        json = json + '"purchase_amount":"' + $('#purchase_amount').val() + '",';
        json = json + '"code_apply":"' + $('#code_apply').val() + '",';
        json = json + '"Discount":"' + $('#Discount').val() + '",';
        json = json + '"DiscountType":"' + $('#DiscountType').val() + '",';
        json = json + '"PromoCodeId":"' + $('#PromoCodeId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: 'Are you sure want to add.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/AddNewPromoCode",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/PromoCode';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });


    $('.RemovePromoCode').click(function () {
        var PromoCodeId = $(this).attr('PromoCodeId');
        var data = {function: 'RemovePromoCode', PromoCodeId: PromoCodeId};
        $.confirm({
            title: 'Are you sure want to make remove.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Remove',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('.InactiveActivePromoCode').click(function () {
        var PrimaryId = $(this).attr('PrimaryId');
        var message = $(this).attr('message');
        var archive = $(this).attr('archive');
        var data = {function: 'InactiveActivePromoCode', PrimaryId: PrimaryId, archive: archive};
        $.confirm({
            title: message,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-info',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-success',
                    action: function () {
                    }
                },
            }
        });
    });
    GetPromoCodeProductFliter();
    $('#PromoCodeCategoryId').change(function () {
        var CategoryId = $(this).val();
        $('#SubCatId2').html('<option value="">select</option>');
        GetPromoCodeProductFliter();
        var data = {function: 'GetSubCategoryByCategoryId', CategoryId: CategoryId, Encode: true};
        $.ajax({
            async: false,
            data: data,
            type: 'POST',
            url: base_url + "Admin/Helper",
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    var SubCategory = json.SubCategory;
                    var html = '';
                    html = html + '<option value="">select</option>';
                    for (var i = 0; i < SubCategory.length; i++) {
                        var d = SubCategory[i];
                        html = html + '<option value="' + d.id + '">' + d.sub_category_name + '</option>';
                    }
                    $('#PromoCodeSubcategoryId').empty();
                    $('#PromoCodeSubcategoryId').append(html);
                } catch (e) {
                    alert(e)
                }

            }
        });
    });
    $('#PromoCodeSubcategoryId').change(function () {
        GetPromoCodeProductFliter();
        var SubCatId = $(this).val();
        var data = {function: 'GetChildSubCategoryBySubCatId', SubCatId: SubCatId, Encode: true};
        $.ajax({
            async: false,
            data: data,
            type: 'POST',
            url: base_url + "Admin/Helper",
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    var ChildSubCat = json.ChildSubCat;
                    var html = '';
                    html = html + '<option value="">select</option>';
                    for (var i = 0; i < ChildSubCat.length; i++) {
                        var d = ChildSubCat[i];
                        html = html + '<option value="' + d.id + '">' + d.sub_category2_name + '</option>';
                    }
                    $('#SubCatId2').empty();
                    $('#SubCatId2').append(html);

                } catch (e) {
                    alert(e)
                }
            }
        });
    });
    $('#SubCatId2').change(function () {
        GetPromoCodeProductFliter();
    });
    $('.ApplyedProductsPromocode').click(function () {
        var PromoCode = $(this).attr('PromoCode');
        var data = {function: 'HTMLAddedPromoCodeProduct', PromoCode: PromoCode};
        $.ajax({
            type: 'POST',
            url: base_url + "Admin/Helper",
            data: data,
            async: false,
            success: function (data) {
                $.dialog({
                    title: false,
                    content: data,
                    animation: 'scale',
                    columnClass: 'medium',
                    closeAnimation: 'scale',
                    backgroundDismiss: true,
                });
                $('.jconfirm-title-c').css('text-align','center');
            }
        });

    });

});
function GetPromoCodeProductFliter() {
    var CategoryId = $('#PromoCodeCategoryId').val();
    var SubcategoryId = $('#PromoCodeSubcategoryId').val();
    var SubCatId2 = $('#SubCatId2').val();
    var PromoCodeId = $('#PromoCodeId').val();
    var data = {function: 'GetPromoCodeProductFliter', CategoryId: CategoryId, SubcategoryId: SubcategoryId, SubCatId2: SubCatId2, PromoCodeId: PromoCodeId};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            try {
                var Json = jQuery.parseJSON(data);
                $("#SelectedProductId").empty();
                $("#SelectedProductId").append(Json.HTML).multiselect("destroy").multiselect({
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: true,
                    buttonWidth: '400px'
                });
            } catch (e) {
                alert(e);
            }
        }
    });

}