$(document).ready(function () {
    if (location.protocol !== "https:") {
        location.protocol = "https:";
    }
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    $('.maxlength').keypress(function () {
        var maxlength = $(this).attr('maxlength');
        if (maxlength == undefined) {
            maxlength = 5;
        }
        if (this.value.length >= maxlength) {
            return false;
        }
    });
    $(function () {
        var Obj = {dateFormat: 'dd-mm-yy'};
        $(".datepicker").datepicker(Obj);
    });

    $('body').on('click', '.autoupdate', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var data = {function: 'autoupdate', updatejson: updatejson, condjson: condjson, dbtable: dbtable};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    $('body').on('click', '.autodelete', function () {
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var removefile = $(this).attr('removefile');
        var data = {function: 'autodelete', condjson: condjson, dbtable: dbtable, removefile: removefile};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });

    /*
     *  Add New Category Start
     */
    $('#SubmitNewCategory').click(function () {
        var json = '';
        json = json + '{';
        var CategoryName = $('#CategoryName');
        if (CategoryName.val() == '') {
            CategoryName.focus();
            CategoryName.css('border-color', 'red');
            return false;
        } else {
            CategoryName.css('border-color', '');
            json = json + '"category_name":"' + CategoryName.val() + '",';
        }
        json = json + '"old_image":"' + $('#old_image').val() + '",';
        json = json + '"status":"' + $('#CategoryStatus').val() + '",';
        json = json + '"CategoryId":"' + $('#CategoryId').val() + '"';
        json = json + '}';
        var fd = new FormData(document.getElementById('profile_pic'));
        fd.append("json", json);
        fd.append("meta_title", $('#meta_title').val());
        fd.append("meta_description", $('#meta_description').val());
        fd.append("meta_keywords", $('#meta_keywords').val());
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Categories/Add-New",
                            type: 'POST',
                            data: fd,
                            async: false,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = base_url + 'Admin/Categories';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
        $('#SubmitNewSubCategoryMain').click(function () {
          var json = '';
        json = json + '{';
        if ($('#CategoryId').val() == '') {
            $('.dropdown-toggle').css('border-color', 'red');
            return false;
        } else {
            json = json + '"category_id":"' + $('#CategoryId').val() + '",';
            $('.dropdown-toggle').css('border-color', '');
        }
        if ($('#SubCategoryName').val() == '') {
            $('#SubCategoryName').focus();
            $('#SubCategoryName').css('border-color', 'red');
            return false;
        } else {
            json = json + '"sub_category_name":"' + $('#SubCategoryName').val() + '",';
            $('#SubCategoryName').css('border-color', '');
        }
        json = json + '"meta_title":"' + $('#meta_title').val() + '",';
        json = json + '"meta_description":"' + $('#meta_description').val() + '",';
        json = json + '"meta_keywords":"' + $('#meta_keywords').val() + '",';

        json = json + '"SubCategoryId":"' + $('#SubCategoryId').val() + '"';
        json = json + '}';
        var fd = new FormData(document.getElementById('profile_pic'));
        fd.append("json", json);
        fd.append("meta_title", $('#meta_title').val());
        fd.append("meta_description", $('#meta_description').val());
        fd.append("meta_keywords", $('#meta_keywords').val());
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/AddNewSubCategory",
                            type: 'POST',
                            data: fd,
                            async: false,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                         window.location = base_url + 'Admin/SubCategory';
                                    } else {
                                        alert(json.message)
                                    }
                                } catch (e) {
                                    alert(e)
                                }

                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
return false;
            
            
            
          
        var data = {json: json};

       
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                             url: base_url + "Admin/AddNewSubCategory",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                     window.location = base_url + 'Admin/SubCategory';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#SubmitNewSubCategory').click(function () {
        var json = {};
        if ($('#emirate').val() == '') {
            $('.dropdown-toggle').css('border-color', 'red');
            return false;
        } else {
            $('.dropdown-toggle').css('border-color', '');
        }
        if ($('#region').val() == '') {
            $('#region').focus();
            $('#region').css('border-color', 'red');
            return false;
        } else {

            $('#region').css('border-color', '');
        }
        json['country_code'] = 'AE';
        json['emirate'] = $('#emirate').val();
        json['region'] = $('#region').val();


        var data = {json: json};
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/addarea",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/area';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('body').on('click', '.InactiveSubcategory', function () {
        var SubCategoryId = $(this).attr('SubCategoryId');
        var Status = $(this).attr('Status');

        var data = {function: 'InactiveSubcategory', SubCategoryId: SubCategoryId, Status: Status};
        $.confirm({
            title: 'Are you sure want to make ' + (Status == 1 ? 'inactive' : 'active') + '.',
            content: false,
            type: (Status == 1 ? 'red' : 'green'),
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: (Status == 1 ? 'Inactive' : 'Active'),
                    btnClass: (Status == 1 ? 'btn-danger' : 'btn-success'),
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/SubCategory';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('#AddNewSupplier').click(function () {
        var json = {};
        if ($('#name').val() == '') {
            $('#name').css('border-color', 'red');
            return false;
        } else {
            json['user_name'] = $('#name').val();
            $('#name').css('border-color', '');
        }
        if ($('#email').val() == '') {
            $('#email').css('border-color', 'red');
            return false;
        } else {
            json['email'] = $('#email').val();
            $('#email').css('border-color', '');
        }
        if ($('#mobile').val() == '') {
            $('#mobile').css('border-color', 'red');
            return false;
        } else {
            json['mobile_no'] = $('#mobile').val();
            $('#mobile').css('border-color', '');
        }
        json['mobile_code'] = $('#mobile_code').val();
        json['id'] = $('#SupplierId').val();
        var data = {json: json};
        $.confirm({
            title: 'Are you sure want to update.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/AddNewSupplier",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/Supplier';
                                } else {
                                    alert(json.message)
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('.InactiveSupplier').click(function () {
        var SupplierId = $(this).attr('SupplierId');
        var Status = $(this).attr('Status');

        var data = {function: 'InactiveSupplier', SupplierId: SupplierId, Status: Status};
        $.confirm({
            title: 'Are you sure want to make ' + (Status == 1 ? 'inactive' : 'active') + '.',
            content: false,
            type: (Status == 1 ? 'red' : 'green'),
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: (Status == 1 ? 'Inactive' : 'Active'),
                    btnClass: (Status == 1 ? 'btn-danger' : 'btn-success'),
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('body').on('click', '.CategoryDelete', function () {
        var CategoryId = $(this).attr('CategoryId');
        var CategoryName = $(this).attr('CategoryName');
        var data = {function: 'CategoryDelete', CategoryId: CategoryId};
        $.confirm({
            title: "Are you sure you want to delete this category : " + CategoryName + " ?",
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('body').on('click', '.ProductDelete', function () {
        var ProductId = $(this).attr('ProductId');
        var ProductName = $(this).attr('ProductName');
        var data = {function: 'ProductDelete', ProductId: ProductId};
        $.confirm({
            title: "Are you sure you want to delete this product : " + ProductName + " ?",
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    $('#SubmitNewUser').click(function () {
        var json = '';
        json = json + '{';
        if ($('#UserName').val() == '') {
            $('#UserName').focus();
            $('#UserName').css('border-color', 'red');
            return false;
        } else {
            $('#UserName').css('border-color', '');
            json = json + '"name":"' + $('#UserName').val() + '",';
        }

        if ($('#UserEmail').val() == '') {
            $('#UserEmail').focus();
            $('#UserEmail').css('border-color', 'red');
            return false;
        } else {
            $('#UserEmail').css('border-color', '');
            json = json + '"email":"' + $('#UserEmail').val() + '",';
        }
        if ($('#UserPassword').val() != '') {
            json = json + '"password":"' + $('#UserPassword').val() + '",';
        }
      
         json = json + '"user_type":"' + $('#user_type').val() + '",';
        json = json + '"archive":"' + $('#UserStatus').val() + '",';
        json = json + '"UserId":"' + $('#UserId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.ajax({
            url: base_url + "Admin/AddNewUser",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    window.location = base_url + 'Admin/UserManagement';
                }else{
                    alert(json.message);
                }
            }
        });
    });
    $('.CustomerBlockUnblock').dblclick(function () {
        var CustomerId = $(this).attr('CustomerId');
        var Archive = $(this).attr('Archive');
        var data = {function: 'CustomerBlockUnblock', CustomerId: CustomerId, archive: Archive};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        window.location = '';
                    }
                }
            }
        });
    });
    $('#ChangeOrderStatus').change(function () {
        var OrderStatus = $(this).val();
        var OrderId = $('#OrderId').val();
        var CustomerNotify = 0;
        if ($("#CustomerNotify").is(":checked")) {
            CustomerNotify = 1;
        }
        var Title = $("#ChangeOrderStatus option:selected").text();
        var data = {function: 'ChangeOrderStatus', OrderStatus: OrderStatus, OrderId: OrderId, CustomerNotify: CustomerNotify};
        $.confirm({
            title: 'Are you sure want to make as ' + Title + '.',
            type: 'green',
            typeAnimated: true,
            content: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    ' <label for="comment">Write any message:</label>' +
                    ' <textarea class="form-control WritenQuery" rows="5" id="WritenQuery"></textarea>' +
                    '</div>' +
                    '</form>',
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var WritenQuery = this.$content.find('.WritenQuery').val();
                        data['WritenQuery'] = WritenQuery;
                        if (OrderStatus == 4) {
                            SendForShipping(data);
                            setTimeout(function () {
                                window.location.reload(true);
                            }, 100);
                        } else {
                            $.ajax({
                                url: base_url + "Admin/Helper",
                                type: 'POST',
                                async: false,
                                data: data,
                                success: function (data) {
                                    try {
                                        var json = jQuery.parseJSON(data);
                                        //window.location = '';
                                        window.location.reload(true);
                                    } catch (e) {
                                        alert(e);
                                    }

                                }
                            });
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });


    });
    
    
       $('#sendOrderwing').click(function () {
        var OrderStatus = $(this).val();
        var OrderId = $('#OrderId').val();
        var CustomerNotify = 0;
        if ($("#CustomerNotify").is(":checked")) {
            CustomerNotify = 1;
        }
        var data = {function: 'ChangeOrderStatus', OrderStatus: OrderStatus, OrderId: OrderId, CustomerNotify: CustomerNotify};
        $.confirm({
            title: 'Are you sure want to submit.',
            type: 'green',
            typeAnimated: true,
            contenttt: '' +
                    '<form action="" class="formName">' +
                    '<div class="form-group">' +
                    ' <label for="comment">Write any message:</label>' +
                    ' <textarea class="form-control WritenQuery" rows="5" id="WritenQuery"></textarea>' +
                    '</div>' +
                    '</form>',
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var WritenQuery = this.$content.find('.WritenQuery').val();
                        data['WritenQuery'] = WritenQuery;
                      
                            SendForShipping(data);
                            setTimeout(function () {
                               // window.location.reload(true);
                            }, 100);
                       
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });


    });
    $('#ChildAddPageCategoryId').change(function () {
        var CategoryId = $(this).val();
        var data = {function: 'GetSubCategoryByCategoryId', CategoryId: CategoryId, Encode: true};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        var SubCategory = json.SubCategory;
                        $('#SubcategoryId').empty();
                        var html = '';
                        html = html + '<option value="">select</option>';
                        for (var i = 0; i < SubCategory.length; i++) {
                            var d = SubCategory[i];
                            html = html + '<option value="' + d.id + '">' + d.sub_category_name + '</option>';
                        }
                        $('#SubcategoryId').append(html);
                    }
                }
            }
        });
    });
    $('#SubmitNewChildCategory').click(function () {
        var json = '';
        json = json + '{';
        if ($('#ChildAddPageCategoryId').val() == '') {
            $('.dropdown-toggle').css('border-color', 'red');
            return false;
        } else {
            json = json + '"category_id":"' + $('#ChildAddPageCategoryId').val() + '",';
            $('.dropdown-toggle').css('border-color', '');
        }
        if ($('#SubcategoryId').val() == '') {
            $('#SubcategoryId').focus();
            $('#SubcategoryId').css('border-color', 'red');
            return false;
        } else {
            json = json + '"sub_category_id":"' + $('#SubcategoryId').val() + '",';
            $('#SubcategoryId').css('border-color', '');
        }

        if ($('#ChildCategoryName').val() == '') {
            $('#ChildCategoryName').focus();
            $('#ChildCategoryName').css('border-color', 'red');
            return false;
        } else {
            json = json + '"sub_category2_name":"' + $('#ChildCategoryName').val() + '",';
            $('#ChildCategoryName').css('border-color', '');
        }
        json = json + '"meta_title":"' + $('#meta_title').val() + '",';
        json = json + '"meta_description":"' + $('#meta_description').val() + '",';
        json = json + '"meta_keywords":"' + $('#meta_keywords').val() + '",';



        json = json + '"CSubCategoryId":"' + $('#CSubCategoryId').val() + '"';
        json = json + '}';
        var data = {json: json};
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/AddNewChildSubCategory",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/ChildSubCategory';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    });
    tooltip();
});
var ADMIN=(function (){
    var fn={};
        fn.updateOrderstatus333 = function (e) {
        var data = {function: 'updateOrderstatus333'};
        data['status_id'] = $('#request_status').val();
        data['orderId'] = $('#OrderId').val();
        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "admin/helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = '';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                        window.location = '';
                    }
                },
            }
        });
    }
    return fn;
})();
function SendForShipping(data) {
    $.ajax({
        url: base_url + "Wingapi/SendNewOrder",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'error') {
                    AlertMessage(json.message);
                    setTimeout(function () {
                        window.location = '';
                    }, 1500);

                }
            } catch (e) {
                alert(e)
            }
        }
    });
}

function AlertBootstrapMessage(AlertType, Message, DivId) {
    $('#' + DivId).removeClass('alert-success');
    $('#' + DivId).removeClass('alert-danger');
    $('#Message' + DivId).html(Message);
    if (AlertType == 'Error') {
        $('#' + DivId).addClass('alert-danger');
    } else if (AlertType == 'Success') {
        $('#' + DivId).addClass('alert-success');
    }
    $('#' + DivId).show(500);
}
function AlertMessage(Message, Type) {
    $.confirm({
        title: Message,
        content: false,
        type: Type,
        typeAnimated: true,
        buttons: {
            Okay: function () {
            }
        }
    });
}
function tooltip() {
    $('[data-toggle="tooltip"]').tooltip();
}