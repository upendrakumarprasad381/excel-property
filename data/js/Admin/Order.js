$(document).ready(function () {
    $('body').on('keyup', '.onlynumber', function () {
        var tytu = $('.onlynumber').val();
        tytu = tytu.replace(".", "");
        tytu = tytu.replace("e", "")
        $('.onlynumber').val(tytu);
    });
    $('body').on('click', '.DownloadWingPdf', function () {
        var OrderId = $(this).attr('OrderId');
        var data = {function: 'GetOrderInvoiceWing', OrderIdNR: OrderId, Encode: true};
        $.ajax({
            data: data,
            type: 'POST',
            async: false,
            url: base_url + "Admin/Helper",
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.value != undefined) {
                    window.location = json.value;
                }
            }
        });
    });
    $('body').on('click', '.DownloadManifestsPdf', function () {
        var OrderId = $(this).attr('OrderId');
        var data = {function: 'GetOrderminifestsWing', OrderIdNR: OrderId, Encode: true};
        $.ajax({
            data: data,
            type: 'POST',
            async: false,
            url: base_url + "Admin/Helper",
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.value != undefined) {
                    window.location = json.value;
                }
            }
        });
    });

    $('body').on('click', '.RemoveOrderProducts', function () {
        var OrderProductId = $(this).attr('OrderProductId');
        var json = {};
        json['OrderProductId'] = OrderProductId;
        var data = {function: 'RemoveOrderProducts', json: json};


        $.confirm({
            title: 'Are you sure want to remove.',
            content: false,
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {

                                    window.location.reload(true);

                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });



    });
    if (Method == 'order_summary') {
        setInterval(function () {
            autoRefreshorderdetailspage();
        }, 10000);

        autoRefreshorderdetailspage();
      
    }
});
function autoRefreshorderdetailspage() {
    var orderId = segment3;
    var data = {function: 'autoRefreshorderdetailspage', orderId: orderId};
    $.ajax({
        url: base_url + "Admin/Helper"
        , async: true
        , type: 'POST'
        , data: data
        , success: function (data) {
            $('#cobdbbd').html(data);

        }
    });
}
var ord = (function () {
    var fn = {};


    fn.orderproductApproved = function (e) {
        var data = {};
        data['status'] = $('#order_status').val();
        data['orderId'] = $('#OrderId').val();
        data['function'] = 'orderproductApproved';
        $.confirm({
            title: 'Are you sure want to update ?',
            content: false,
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location.reload(true);
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                        window.location.reload(true);
                    }
                },
            }
        });
    }

    fn.orderQuantituAndRejectUpdate = function (e) {
        var data = {};
        var status = $(e).attr('status');
        data['status'] = status;
        var quantity = $(e).attr('quantity');
        var product_price = $(e).attr('product_price');
        data['old_quantity'] = quantity;
        data['orderId'] = $('#OrderId').val();
        data['orderproductId'] = $(e).attr('orderproductId');
        data['function'] = 'orderQuantituAndRejectUpdate';
        var statusTitle = data['status'] == '1' ? 'Are you sure want to update ? ' : 'Are you sure want to reject ?';

        var product_quantity = '<div class="form-group">' +
                ' <label for="comment">Quantity:</label>' +
                ' <input type="number" class="form-control product_quantity" value="' + quantity + '" pattern="[0-9]" id="product_quantity" required>' +
                '</div>';

        var priceBox = '<div class="form-group">' +
                ' <label for="comment">Product Price:</label>' +
                ' <input type="number" class="form-control onlynumber product_price" value="' + product_price + '" pattern="[0-9]" id="product_price" disabled required>' +
                '</div>';
        var confirmBox = '<form action="" class="formName">' +
                (status == '1' ? priceBox + product_quantity : '') +
                '<div class="form-group">' +
                ' <label for="comment">Write message:</label>' +
                ' <textarea class="form-control messagedata" rows="5" id="messagedata" required></textarea>' +
                '</div>' +
                '</form>';
        $.confirm({
            title: statusTitle,
            content: confirmBox,
            type: 'red',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var messagedata = this.$content.find('.messagedata').val();
                        if (messagedata != undefined && messagedata != false) {
                            data['message'] = messagedata;
                        }
                        var product_quantity = this.$content.find('.product_quantity').val();
                        if (product_quantity != undefined && product_quantity != false) {
                            data['product_quantity'] = product_quantity;
                        }
                        if (parseInt(quantity) < parseInt(data['product_quantity'])) {
                            alert('Maximum ' + quantity + ' quantity allowed.');
                            return false;
                        }


                        var product_price = this.$content.find('.product_price').val();
                        if (product_price != undefined && product_price != false) {
                            data['product_price'] = product_price;
                        }
                        if (product_price <= 0) {
                            alert('Invalid product price');
                            return false;
                        }



                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "Admin/Helper",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {

                                    window.location.reload(true);

                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });
    }
    fn.adminViewReply = function (e) {
        var commentId = $(e).attr('commentId');
        var data = {};
        data['commentId'] = commentId;
        data['function'] = 'adminViewReply';
        $.ajax({
            data: data,
            type: 'POST',
            async: false,
            url: base_url + "Admin/Helper",
            success: function (data) {
                $('#replybdbdj').html(data);
                $('#basicExampleModal').modal('show');
            }
        });
    }
    fn.adminReadNotifyme = function (e) {
        var productId = $(e).attr('productId');
        var data = {};
        data['productId'] = productId;
        data['function'] = 'adminReadNotifyme';
        $.ajax({
            data: data,
            type: 'POST',
            async: false,
            url: base_url + "Admin/Helper",
            success: function (data) {
               try {
                var json = jQuery.parseJSON(data);
                window.location = '';
            } catch (e) {
                alert(e)
            }
               
            }
        });
    }
   
    return fn;
})();