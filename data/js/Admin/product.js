$(document).ready(function () {
    $('#SubmitNewProduct').click(function () {
        var json = {};
        json['category_id'] = $('#category_id').val();
        json['sub_category_id'] = $('#sub_category_id').val();
        json['product_name'] = $('#product_name').val();
        json['product_name_ar'] = $('#product_name_ar').val();
        json['vendor_id'] = $('#vendor_id').val();
        json['status'] = $('#status').val();
        json['fabric_id'] = $('#fabric_id').val();
        json['short_description'] = $('#short_description').val();
        json['short_description_ar'] = $('#short_description_ar').val();
        json['price'] = $('#price').val();
        json['country_id'] = $('#country_id').val();

//        json['chest'] = $('#chest').val();
//        json['shoulder'] = $('#shoulder').val();
//        json['sleeve'] = $('#sleeve').val();
//        json['waist'] = $('#waist').val();
//        json['hips'] = $('#hips').val();
//        json['length'] = $('#length').val();
//        json['height'] = $('#height').val();
//        var size_id = $('#sizelist .selected').attr('submasterid');
//        json['size_id'] = $.isNumeric(size_id) ? size_id : 0;

        var color_id = $('#colorlist .selected').attr('submasterid');
        json['color_id'] = $.isNumeric(color_id) ? color_id : 0;


        json['id'] = $('#id').val();
        if ($('#product_name').val() == '') {
            $('#product_name').focus();
            $('#product_name').css('border-color', 'red');
            return false;
        } else {
            $('#product_name').css('border-color', '');
        }

        if ($('#price').val() == '') {
            $('#price').focus();
            $('#price').css('border-color', 'red');
            return false;
        } else {
            $('#price').css('border-color', '');
        }


        if ($('#quantity').val() == '') {
            $('#quantity').focus();
            $('#quantity').css('border-color', 'red');
            return false;
        } else {
            $('#quantity').css('border-color', '');
        }


        if ($('#short_description').val() == '') {
            $('#short_description').focus();
            $('#short_description').css('border-color', 'red');
            return false;
        } else {
            $('#short_description').css('border-color', '');
        }

        var data = {json: json};





        $.confirm({
            title: 'Are you sure want to submit.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/AddNewProduct",
                            type: 'POST',
                            async: false,
                            data: data,
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    AlertBootstrapMessage('Success', json.message, 'BtAlert');
                                    setTimeout(function () {
                                        window.location = base_url + 'Admin/Products';
                                    }, 1000);
                                } else {
                                    AlertBootstrapMessage('Error', json.message, 'BtAlert');
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });

    });
    $('#CategoryIds').change(function () {
        var CategoryId = $('#CategoryIds').val();
        var data = {function: 'GetSubCategoryByCategoryId', CategoryId: CategoryId, Encode: true};
        $('#SubCategoryId').empty();
        $('#ChildSubCatId').empty();
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    var SubCategory = json.SubCategory;
                    var html = '';
                    html = html + '<option value="">select</option>';
                    for (var i = 0; i < SubCategory.length; i++) {
                        html = html + '<option value="' + SubCategory[i].id + '">' + SubCategory[i].sub_category_name + '</option>';
                    }
                    $('#SubCategoryId').append(html);
                    var html2 = '<option value="">select</option>';
                    $('#ChildSubCatId').append(html2);
                }
            }
        });
    });
    $('#SubCategoryId').change(function () {
        var SubCatId = $(this).val();
        $('#ChildSubCatId').empty();
        var data = {function: 'GetChildSubCategoryBySubCatId', SubCatId: SubCatId, Encode: true};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    var ChildSubCat = json.ChildSubCat;
                    var html = '';
                    html = html + '<option value="">select</option>';
                    for (var i = 0; i < ChildSubCat.length; i++) {
                        html = html + '<option value="' + ChildSubCat[i].id + '">' + ChildSubCat[i].sub_category2_name + '</option>';
                    }
                    $('#ChildSubCatId').append(html);
                }
            }
        });

    });
    $('.changecategory').change(function () {
        var catid = $(this).val();

        var data = {function: 'GetsubcategoriesdetailsBycatId', catId: catid, encode: true};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    var html = '<option value="">select</option>';
                    for (var i = 0; i < json.list.length; i++) {
                        var d = json.list[i];
                        html = html + '<option value="' + d.id + '">' + d.category_name + ' -> ' + d.sub_category_name + '</option>';
                    }
                    $('#sub_category_id').html(html);
                    $('.selectpicker').selectpicker('refresh');
                }
            }
        });
    });
});
var PRDUCT = (function () {
    var fn = {};
    fn.selectthisItem = function (e) {
        var uniqid = $(e).attr('uniqid');
        var submasterId = $(e).attr('submasterId');
        var html = $('#iconid-' + uniqid).html();
        $('#sizelist .iconview').html('');
        var HTMLImage = '<i class="fas fa-check-circle selected" submasterId="' + submasterId + '"></i>';
        if (html == '') {
            $('#iconid-' + uniqid).html(HTMLImage);
        } else {
            $('#iconid-' + uniqid).html('');
        }
    }
    fn.selectthisItemasColor = function (e) {
        var uniqid = $(e).attr('uniqid');
        var submasterId = $(e).attr('submasterId');
        var html = $('#iconid-' + uniqid).html();
        $('#colorlist .iconview').html('');
        var HTMLImage = '<i class="fas fa-check-circle selected" submasterId="' + submasterId + '"></i>';
        if (html == '') {
            $('#iconid-' + uniqid).html(HTMLImage);
        } else {
            $('#iconid-' + uniqid).html('');
        }
    }
    return fn;
})();
