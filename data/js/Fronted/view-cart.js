$(document).ready(function () {
    $('#ApplyPromoCode').click(function () {
        var PromoCode = $('#PromoCode');
        if (PromoCode.val() == '') {
            PromoCode.css('border-color', 'red');
            PromoCode.focus();
            return false;
        } else {
            PromoCode.css('border-color', '');
        }
        var data = {function: 'ApplyPromoCode', PromoCode: PromoCode.val()};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    AlertMessage(json.message, json.status, true);
                    if (json.status == true) {
                        setTimeout(function () {
                            window.location = '';
                        }, 1500);
                    }
                } catch (e) {
                    alert(e);
                }

            }
        });
    });
});