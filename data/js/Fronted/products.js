$(document).ready(function () {
    /* AddToCart */
    var CallBackURL = $('#CallBackURL').val();
    $('.JavaScriptLogin').click(function () {
        var CallBackURL = $(this).attr('CallBackURL');
        var Status = SetLoginCallBackURl(CallBackURL);
        if (Status == true) {
            window.location = base_url + 'Login/call-back';
        }
    });
    var CustomerRated = $('#RateThisProduct').val();
    if (Method == 'Product') {
        $(".AdminRated").starRating({
            initialRating: $('#AdminRated').val(),
            strokeColor: '#894A00',
            readOnly: true,
            strokeWidth: 10,
            starSize: 25
        });
        $(".Ratethisproduct").starRating({
            initialRating: $('#RateThisProduct').val(),
            strokeColor: '#894A00',
            strokeWidth: 10,
            starSize: 25,
            callback: function (currentRating, $el) {
                CustomerRated = currentRating;
            }
        });
    }
    $('body').on('click', '.AddToCart', function () {
        var ProductId = $(this).attr('ProductId');
        var IsSize = $(this).attr('IsSize');
        var ProductSize = '';
        if (IsSize == true) {
            if (Method == 'Product') {
                ProductSize = ValidateColorSize();
                if (ProductSize == false) {
                    return false;
                }
            } else {
                window.location = base_url + 'Product/' + btoa(ProductId);
                return false;
            }
        } else if (IsSize == undefined) {
            alert('Size is undefined contact to admin.');
            return false;
        }
        var data = {function: 'AddToCart', ProductId: ProductId, ProductSize: ProductSize};
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data,
            async: false,
            type: 'POST',
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        AddToCartAddclass(ProductId);
                    } else if (json.status == 'error') {
                        AlertHeader(json.message);
                    }
                } catch (e) {

                }
            }
        });
    });
    $('body').on('click', '.ColorSize', function () {
        var SizeName = $(this).attr('SizeName');
        var ProductId = $(this).attr('ProductId');
        var data = {function: 'colorsizeAvailability', SizeName: SizeName, ProductId: ProductId};
        $('.ColorSize').removeClass('SelectedProduct');
        $(this).addClass('SelectedProduct');
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data,
            async: false,
            type: 'POST',
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        $('#ProductId_' + ProductId).removeClass('AddToCart');
                        $('#ProductId_' + ProductId).addClass('RemoveFromCart atv');
                        $('#ProductId_' + ProductId).attr('title', 'Remove from cart');
                    } else if (json.status == false) {
                        $('#ProductId_' + ProductId).removeClass('RemoveFromCart atv');
                        $('#ProductId_' + ProductId).addClass('AddToCart');
                        $('#ProductId_' + ProductId).attr('title', 'Add to card');
                    } else {
                        window.location = '';
                    }
                } catch (e) {
                    alert(e);
                }

            }
        });

    });
    $('body').on('click', '.RemoveFromCart', function () {
        var ProductId = $(this).attr('ProductId');
        var Size = $('.SelectedProduct').attr('SizeName');
        Size == undefined ? '' : Size;
        var data = {function: 'RemoveFromCart', ProductId: ProductId, Size: Size};
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data,
            async: false,
            type: 'POST',
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    RemoveFromCartAddclass(ProductId);
                } else if (json.status == 'error') {
                    window.location = '';
                    alert(json.message)
                }
            }
        });
    });
    $('.CartPageRemoveFromCart').click(function () {
        var ProductId = $(this).attr('ProductId');
        var Size = $(this).attr('Size');
        var data = {function: 'RemoveFromCart', ProductId: ProductId, Size: Size};
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data,
            async: false,
            type: 'POST',
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    window.location = '';
                } else {
                    alert('Something went wrong')
                }
            }
        });
    });
    $('body').on('keyup', '.CartPageProductQuantity', function () {
        var ProductId = $(this).attr('ProductId');
        var ProductQuantity = parseInt($(this).val());
        var Size = $(this).attr('Size');
        if (!$.isNumeric(ProductQuantity)) {
            ProductQuantity = 0;
        }
        var data = {ProductId: ProductId, ProductQuantity: ProductQuantity, size: Size};
        CartPageProductQuantity(data);
    });
    $('.adbtn').click(function () {
        var type = $(this).attr('type');
        var ProductId = $(this).attr('ProductId');
        var Size = $(this).attr('Size');
        var id = Size + ProductId;
        var qty = $('#ProductQuantity_' + id).val();
        if (qty == 1 && type == 'sub') {
            return false;
        }
        qty = type == 'plus' ? parseInt(qty) + parseInt(1) : parseInt(qty) - parseInt(1);
        var data = {ProductId: ProductId, ProductQuantity: qty, size: Size};
        CartPageProductQuantity(data);
    });
    /* Shipping address saving  */
    $('#SaveShippingAddress').click(function () {
        var json = '';
        json = json + '{';
        if ($('#ShipUserName').val() == '') {
            $('#ShipUserName').css('border-color', 'red');
            $('#ShipUserName').focus();
            return false;
        } else {
            $('#ShipUserName').css('border-color', '');
            json = json + '"user_name":"' + $('#ShipUserName').val() + '",';
        }
        if ($('#street').val() == '') {
            $('#street').css('border-color', 'red');
            $('#street').focus();
            return false;
        } else {
            $('#street').css('border-color', '');
            json = json + '"street":"' + $('#street').val() + '",';
        }
        if ($('#building').val() == '') {
            $('#building').css('border-color', 'red');
            $('#building').focus();
            return false;
        } else {
            $('#building').css('border-color', '');
            json = json + '"building":"' + $('#building').val() + '",';
        }
        if ($('#area').val() == '') {
            $('#area').css('border-color', 'red');
            $('#area').focus();
            return false;
        } else {
            $('#area').css('border-color', '');
            json = json + '"area":"' + $('#area').val() + '",';
        }
        if ($('#city').val() == '') {
            $('#city').css('border-color', 'red');
            $('#city').focus();
            return false;
        } else {
            $('#city').css('border-color', '');
            json = json + '"city":"' + $('#city').val() + '",';
        }
        json = json + '"location":"' + $('#location').val() + '",';
        if ($('#mobile_no').val() == '') {
            $('#mobile_no').css('border-color', 'red');
            $('#mobile_no').focus();
            return false;
        } else {
            $('#mobile_no').css('border-color', '');
            json = json + '"mobile_no":"' + $('#mobile_no').val() + '",';
        }
        json = json + '"mobile_code":"' + $('#mobile_code').val() + '",';
        json = json + '"note":"' + $('#note').val() + '",';
        json = json + '"UpdateId":"' + $('#UpdateId').val() + '"';
        json = json + '}';
        var data = {function: 'SaveNewUserAddress', json: json};
        $.ajax({
            url: base_url + "User/Helper",
            async: false,
            data: data,
            type: 'POST',
            success: function (data) {
                var json = jQuery.parseJSON(data);
                   $('#message').css('color', 'red');
                if (json.status == 'success') {
                    $('#message').css('color', 'green');
                    $('#message').html(json.message);
                    window.location = '';
                } else {
                    $('#message').html(json.message);
                }
            }
        });
    });
    $('#AddNewAddress').click(function () {
        $('#NewAddressForm').show(500);
        $('#UpdateId').val('');

        $('#street').val('');
        $('#building').val('');
        $('#area').val('');
        $('#city').html($('#city').html().replace('selected', ''));
        $('#city option[value="Dubai"]').attr('selected', true);
        $('#mobile_no').val('');
        $('#note').val('');
    });
    $('.EditAddress').click(function () {
        if (jQuery.parseJSON($(this).attr('json'))) {
            var json = jQuery.parseJSON($(this).attr('json'));
            $('#UpdateId').val(json.id);

            $('#street').val(json.street);
            $('#building').val(json.building);
            $('#area').val(json.area);

            $('#city').html($('#city').html().replace('selected', ''));
            $('#city option[value="' + json.city + '"]').attr('selected', true);

            //$('#city').val(json.city);
            $('#mobile_no').val(json.mobile_no);
            $('#note').val(json.note);
            $('#location').html($('#location').html().replace('selected', ''));
            $('#location option[value="' + json.location_type + '"]').attr('selected', true);
            $('#mobile_code').html($('#mobile_code').html().replace('selected', ''));
            $('#mobile_code option[value="' + json.mobile_code + '"]').attr('selected', true);

            $('#NewAddressForm').show(500);
        }
    });
    $('.DeleteAddress').click(function () {
        var DeleteId = $(this).attr('DeleteId');
        var data = {function: 'DeleteAddress', DeleteId: DeleteId};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    window.location = '';
                } else {
                    alert(json.message);
                }
            }
        });
    });
    // $("input[name='ShipAddressId']");
    $(".SetShippingSession").click(function () {
        var ShippingId = $('input[name=ShipAddressId]:checked').val();
        var PaymentType = $('input[name=PaymentType]:checked').val();
        var data = {function: 'SaveSessionUserShippingAddress', ShippingId: ShippingId, PaymentType: PaymentType};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
            }
        });
    });
    $('#SubmitMyOrder').click(function () {
        var PaymentType = $('input[name=PaymentType]:checked').val();
        if (!$('#Termsconditions').is(":checked")) {
            AlertMessage('Please accept all terms and conditions to continue.');
            return false;
        }
        if (PaymentType == '1') {
            AlertMessage('Please wait few seconds.', false, false, 'messagebox', 6000);
            Checkout.showLightbox();
            return false;
        }

        var data = {function: 'SubmitMyOrder'};
        if (PaymentType == '0') {
            $.ajax({
                url: base_url + "User/Helper",
                type: 'POST',
                data: data,
                async: false,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == 'success') {
                            window.location = base_url + 'order-successfully/' + json.OrderNumber;
                        } else {
                            AlertHeader(json.message);
                        }
                    } catch (e) {
                        alert(e);
                    }

                }
            });
        }
    });
    $('#EditPersonalInformation').click(function () {
        $('#AccountInformationDiv').hide();
        $('#PersonalInformationDiv').show();
    });
    $('#EditAccountInfo').click(function () {
        $('#PersonalInformationDiv').hide();
        $('#AccountInformationDiv').show();
    });
    $('.LikeThisProduct').click(function () {
        var ProductId = $(this).attr('ProductId');
        var data = {function: 'LikeThisProduct', ProductId: ProductId};
        $.ajax({
            url: base_url + "User/Helper",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status = 'success') {
                        if (json.like == true) {
                            $('#UserLike' + ProductId).addClass('atv');
                            $('#TotalLikes' + ProductId).text(parseInt($('#TotalLikes' + ProductId).text()) + parseInt(1));
                        } else {
                            $('#UserLike' + ProductId).removeClass('atv');
                            $('#TotalLikes' + ProductId).text(parseInt($('#TotalLikes' + ProductId).text()) - parseInt(1));
                        }
                    } else {
                        alert(json.message);
                    }
                } else {
                    alert('Something went wrong try again');
                }
            }
        });
    });

    $('#LoginAndReview').click(function () {
        var ReviewDescription = $('#ReviewDescription');
        var RateThisProduct = CustomerRated;
        if (ReviewDescription.val() == '') {
            ReviewDescription.css('border-color', 'red');
            ReviewDescription.focus();
            return false;
        }
        var data1 = {function: 'SetAdditionalSession', ReviewDescription: ReviewDescription.val(), RateThisProduct: RateThisProduct};
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data1,
            type: 'POST',
            async: false,
            success: function (data) {
                var Status = SetLoginCallBackURl(CallBackURL);
                if (Status == true) {
                    window.location = base_url + 'Login/call-back';
                }
            }
        });
    });
    $('#ReviewThisProduct').click(function () {
        var ReviewDescription = $('#ReviewDescription');
        var ProductId = segment2;
        var RateThisProduct = CustomerRated;
        if (ReviewDescription.val() == '') {
            ReviewDescription.css('border-color', 'red');
            ReviewDescription.focus();
            return false;
        }
        var data1 = {function: 'ReviewThisProduct', ProductId: ProductId, ReviewDescription: ReviewDescription.val(), RateThisProduct: RateThisProduct};
        $.ajax({
            url: base_url + "Fronted/Helper",
            data: data1,
            type: 'POST',
            async: false,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status = 'success') {
                        window.location = '';
                    }
                }
            }
        });
    });
    $('#CancelledMyOrder').click(function () {
        var OrderId = $(this).attr('orderid');
        var data = {function: 'CancelledMyOrder', OrderId: OrderId};
        $.confirm({
            title: 'Are you sure want to cancel this order ?',
            type: 'red',
            typeAnimated: true,
            content: '' +
                    '<form action="" class="formName" id="formName">' +
                    '<div class="form-group">' +
                    '<label for="comment">Why Cancel:</label><br/>' +
                    '<input type="radio" name="WritenQuery" id="WritenQuery" value="The order took too long" /> The order took too long <br/>' +
                    '<input type="radio" name="WritenQuery" id="WritenQuery" value="I changed my mind" /> I changed my mind <br/>' + 
                    '<input type="radio" name="WritenQuery" id="WritenQuery" value="The price is too high" /> The price is too high <br/>' +  
                    '<input type="radio" name="WritenQuery" id="WritenQuery" value="I found it cheaper somewhere else" /> I found it cheaper somewhere else <br/>' + 
                    '<input type="radio" name="WritenQuery" id="WritenQuery" value="Other" /> Other<br/>' +   
                    'Please specify <textarea class="form-control WritenQuery" rows="5" id="WritenQuery"></textarea>' +                                                                                                 
                    '</div>' +
                    '</form>',
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var WritenQueryop = this.$content.find($('input[name=WritenQuery]:checked', '#formName')).val();
                        var WritenQuery ='';
                        if(WritenQueryop == 'Other') {
                            var WritenQuery = this.$content.find('.WritenQuery').val();
                        }
                        data['WritenQuery'] = WritenQuery;
                        data['WritenQueryOp'] = WritenQueryop;                        
                        
                        $.ajax({
                            url: base_url + "User/Helper",
                            type: 'POST',
                            async: false,
                            data: data,
                            success: function (data) {
                                if (jQuery.parseJSON(data)) {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        AlertHeader(json.message, json.status);
                                        setTimeout(function () {
                                            window.location = base_url;
                                        }, 4000);
                                    }
                                }
                            }
                        });

                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
    RefreshTooltip();
});
function CartPageProductQuantity(data) {
    var ProductId = data.ProductId;
    var ProductQuantity = data.ProductQuantity;
    ProductQuantity = ProductQuantity <= 0 ? 1 : ProductQuantity;
    var size = data.size;
    var id = size + ProductId;
    var ProductPrice = $('#ProductDivId_' + id).attr('ProductPrice');
    var CrossPerProduct = $('#ProductDivId_' + id).attr('CrossPerProduct');
    var Total = parseInt(ProductQuantity) * parseFloat(ProductPrice);
    var CrossPrice = Total + (parseFloat(CrossPerProduct) * ProductQuantity)

    var data2 = {function: 'AddToCartProductQuantity', ProductId: ProductId, ProductQuantity: ProductQuantity, size: size};
    $.ajax({
        url: base_url + "Fronted/Helper",
        data: data2,
        async: false,
        type: 'POST',
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if (json.status == 'success') {
                $('#EachPerductTotal_' + id).text(Total.toFixed(2));
                $('#CrossEachPerductTotal_' + id).text(CrossPrice.toFixed(2));
                $('#ProductQuantity_' + id).val(ProductQuantity);
                $('#Qtytext' + id).text(ProductQuantity);
                SetCartPagePriceDetails(json);
            } else if (json.status == 'error') {
                AlertHeader(json.message);
            } else {
                alert('Something went wrong');
            }
        }
    });
}
function SetCartPagePriceDetails(json) {
    if (json.result !== undefined) {
        var result = json.result;
        $('#SubTotal').text(result.SubTotal.toFixed(2));
        $('#ShippingCharges').text(result.ShippingCharges);
        $('#discounts').text(result.discount.toFixed(2));
        $('#TotalItome').text(result.TotalItem);
        $('#PayableTotalPrice').text(result.TotalPrice.toFixed(2));
    }

}
function AddToCartAddclass(ProductId) {
    $('#ProductId_' + ProductId).removeClass('AddToCart');
    $('#ProductId_' + ProductId).addClass('RemoveFromCart atv');
    $('#ProductId_' + ProductId).attr('title', 'Remove from cart');
    SetCartActiveInactive('Add');
}
function RemoveFromCartAddclass(ProductId) {
    $('#ProductId_' + ProductId).removeClass('RemoveFromCart atv');
    $('#ProductId_' + ProductId).addClass('AddToCart');
    $('#ProductId_' + ProductId).attr('title', 'Add to card');
    SetCartActiveInactive('Remove');
}
function SetCartActiveInactive(Type) {
    var TotalCart = parseInt($('#TotalCart').text());
    if (Type == 'Add') {
        TotalCart = TotalCart + 1;
    } else {
        TotalCart = TotalCart - 1;
    }
    if (TotalCart > 0) {
        $('#TotalCarthref').addClass('atv');
        $('#TotalCart').show(500);
    } else {
        $('#TotalCarthref').removeClass('atv');
        $('#TotalCart').hide(500);
    }
    $('#TotalCart').text(TotalCart);
}
function ValidateColorSize() {
    var Size = false;
    if ($('.ColorSize').hasClass('SelectedProduct')) {
        Size = $('.SelectedProduct').attr('SizeName');
    } else {
        AlertMessage('Please choose the size.', false, true, 'messagebox');
    }
    return Size;
}

