$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            900: {
                items: 2,
                nav: false
            },
            1200: {
                items: 4,
                nav: false
            },

            1500: {
                items: 4,
                nav: true,
                loop: false,
                margin: 5
            }
        }
    });
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
//    $('body').on('click', ' #content', function () {
//        if (Method == 'Product') {
//            $('#sidebar').removeClass('active');
//        } else {
//            $('#sidebar').addClass('active');
//        }
//    });
});
new UISearch(document.getElementById('sb-search'));


