$(document).ready(function () {
    if (location.protocol !== "https:") {
        location.protocol = "https:";
    }
    $('body ').on('keydown', '.only_number', function () {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
                || event.keyCode == 27 || event.keyCode == 13
                || (event.keyCode == 65 && event.ctrlKey === true)
                || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    $('.HoverPlay').mouseover(function () {
        var GIF = $(this).attr('GIF');
        var JPG = $(this).attr('JPG');
        $(this).attr('src', GIF);
    });
    $('.HoverPlay').mouseout(function () {
        var GIF = $(this).attr('GIF');
        var JPG = $(this).attr('JPG');
        $(this).attr('src', JPG);
    });
    $('body').on('click', '.right_bnt .atv', function () {
        window.location = base_url + 'View-Cart';
    });
    $('#UpdateProfile').click(function () {
        var json = '';
        json = json + '{';
        if ($('#user_name').val() == '') {
            $('#user_name').css('border-color', 'red');
            $('#user_name').focus();
            return false;
        } else {
            $('#user_name').css('border-color', '');
            json = json + '"user_name":"' + $('#user_name').val() + '",';
        }
        if ($('#mobile_no').val() == '') {
            $('#mobile_no').css('border-color', 'red');
            $('#mobile_no').focus();
            return false;
        } else {
            $('#mobile_no').css('border-color', '');
            json = json + '"mobile_no":"' + $('#mobile_no').val() + '",';
        }
        if ($('#mobile_no').val() == '') {
            $('#mobile_no').css('border-color', 'red');
            $('#mobile_no').focus();
            return false;
        } else {
            $('#mobile_no').css('border-color', '');
            json = json + '"mobile_no":"' + $('#mobile_no').val() + '",';
        }
        var Gender = $('input[name=Gender]:checked').val();
        json = json + '"gender":"' + Gender + '",';
        if ($('#birthday').val() == '') {
            $('#birthday').css('border-color', 'red');
            $('#birthday').focus();
            return false;
        } else {
            $('#birthday').css('border-color', '');
            json = json + '"birthday":"' + $('#birthday').val() + '",';
        }
        json = json + '"trn_number":"' + $('#trn_number').val() + '",';
        json = json + '"mobile_code":"' + $('#UserMobileCode').val() + '",';
        json = json + '"country_residence":"' + $('#country_residence').val() + '"';
        json = json + '}';
        var data = {json: json, function: 'UpdateUserProfile'};
        $.ajax({
            url: base_url + "User/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        window.location = '';
                    }
                }
            }
        });
    });
    $('#UpdateAccountInfo').click(function () {
        var json = '';
        json = json + '{';
        if ($('#OldPassword').val() == '') {
            $('#OldPassword').css('border-color', 'red');
            $('#OldPassword').focus();
            return false;
        } else {
            $('#OldPassword').css('border-color', '');
            json = json + '"OldPassword":"' + $('#OldPassword').val() + '",';
        }
        if ($('#NewPassword').val() == '') {
            $('#NewPassword').css('border-color', 'red');
            $('#NewPassword').focus();
            return false;
        } else {
            $('#NewPassword').css('border-color', '');
            json = json + '"NewPassword":"' + $('#NewPassword').val() + '"';
        }
        json = json + '}';
        var data = {json: json, function: 'UpdateUserAccountIfo'};
        $.ajax({
            url: base_url + "User/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        window.location = '';
                    } else {
                        $('#Message').html(json.message);
                    }
                }
            }
        });
    });
    $('.removeProductFromWishlist').click(function () {
        var ProductId = $(this).attr('ProductId');
        var UserId = $(this).attr('UserId');
        var data = {ProductId: ProductId, UserId: UserId, function: 'removeProductFromWishlist'};
        $.ajax({
            url: base_url + "User/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        window.location = '';
                    } else {
                        $('#Message').html(json.message);
                    }
                }
            }
        });
    });
    $('#srch-term').keyup(function () {
        var value = $(this).val();
        $('#HeaderFullScreenSearch').empty();
        var data = {function: 'HeaderProductSearch', value: value, smallScreen: false};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                $('#HeaderFullScreenSearch').append(data);
            }
        });
    });
    $('#HeaderSearchSM').keyup(function () {
        var value = $(this).val();
        $('#smallwidthseacrh').empty();
        var data = {function: 'HeaderProductSearch', value: value, smallScreen: true};
        $.ajax({
            url: base_url + "Fronted/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                $('#smallwidthseacrh').append(data);
            }
        });
    });
    $('#srch-term, #HeaderSearchSM').on("keypress", function (e) {
        if (e.keyCode == 13) {
            var Redirec = $('.UlSuggested').attr('href');
            if (Redirec == undefined) {
                Redirec = base_url + 'Category/MDAwMA==/popularity';
            }
            window.location = Redirec;
            return false; // prevent the button click from happening
        }
    });
    RefreshTooltip();
});
function SetCallBackURLAndRedirectLogin(CallBackURL) {
    var Status = SetLoginCallBackURl(CallBackURL);
    if (Status == true) {
        window.location = base_url + 'Login/call-back';
    }
}
function SetLoginCallBackURl(CallBackURL) {
    if (localStorage['CallBackURL'] = CallBackURL) {
        return true;
    }
}
function GetLoginCallBackURl() {
    return localStorage['CallBackURL'];
}
function AlertHeader(Message = 'Invalid messaage', type = false) {
    $('#HeaderAlert').removeClass('alert-danger');
    $('#HeaderAlert').removeClass('alert-success');
    var classAdd = type == false ? 'alert-danger' : 'alert-success';
    $('#HeaderAlert').addClass(classAdd);
    $('#HeaderAlertText').html(Message);
    $('#HeaderAlert').show(500);
    setTimeout(function () {
        $('#HeaderAlert').hide(500);
    }, 4000);

}
function AlertMessage(Message = 'Invalid messaage', type = false, close = false, div_id = 'messagebox', disbledTime = 3000) {
    var html = '';
    var AlertType = 'alert-danger';
    if (type == true) {
        AlertType = 'alert-success';
    }
    html = html + '<div class="alert ' + AlertType + ' promo_code_alert" role="alert">';
    if (close == true) {
        html = html + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="    margin-top: -3px;">';
        html = html + '                    <span aria-hidden="true">&times;</span>';
        html = html + '               </button> ';
    }
    html = html + Message;
    html = html + '</div>';
    $('#' + div_id).html(html);
    setTimeout(function () {
        $('#' + div_id).empty();
    }, disbledTime);

}
function RefreshTooltip() {
    $('[data-toggle="tooltip"]').tooltip();
}
function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ar'}, 'google_translate_element');
}