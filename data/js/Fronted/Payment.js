$(document).ready(function () {
    var Price = $('#Pricedetails').val();
    try {
        var Price = jQuery.parseJSON(Price);
        Checkout.configure({
            merchant: '3008869',
            order: {
                amount: function () {
                    return  Price.TotalPrice;
                },
                currency: 'AED',
                description: 'Ordered goods',
                id: Price.UniqId
            },
            interaction: {
                merchant: {
                    name: 'AL KABAYEL OASIS-ECOM',
                    address: {
                        line1: '200 Sample St',
                        line2: '1234 Example Town',
                    }
                },
                displayControl: {
                    billingAddress: 'HIDE',
                }
            }
        });
    } catch (e) {
        alert('Payment not working please submit your order cash on delivery');
    }


});
/* Function */
function errorCallback(error) {
    console.log(JSON.stringify(error));
    AlertMessage('Payment cancelled.');
}
function cancelCallback() {
    console.log('Payment cancelled');
    AlertMessage('Payment cancelled.');
}
function completeCallback(resultIndicator, sessionVersion) {
    var Price = $('#Pricedetails').val();
    var Price = jQuery.parseJSON(Price);
    var data = {function: 'SubmitMyOrder', UniqId: Price.UniqId};
    $.ajax({
        url: base_url + "User/Helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == 'success') {
                    window.location = base_url + 'order-successfully/' + json.OrderNumber;
                } else {
                    AlertHeader(json.message);
                }
            } catch (e) {
                alert(e);
            }

        }
    });
}