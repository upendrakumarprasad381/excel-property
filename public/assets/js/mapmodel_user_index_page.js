(function ($) {
    var map;
    var initMap = function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var mapCenter = new google.maps.LatLng(parseFloat(defaultLat), parseFloat(defaultLong));

                var infowindow = new google.maps.InfoWindow();

                map = new google.maps.Map(document.getElementById('map'), {
                    center: mapCenter,
                    zoom: 17,
                    draggable: true
                });

                map.setCenter(mapCenter);
                console.log(locEncode);
                for (var i = 0; i < locEncode.length; i++) {
                    var d = locEncode[i];
                    var marker2 = new google.maps.LatLng(parseFloat(d.lat), parseFloat(d.lng));
                    marker = new google.maps.Marker({
                        position: marker2,
                        map: map,
                        animation: google.maps.Animation.BOUNCE
                    });

                    google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                        return function () {
                            var htmlop = htmlpreview(locEncode[i]);

                            infowindow.setContent(htmlop);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                    marker.addListener('mouseout', function () {
                        infowindow.close();
                    });

                }
            });
        }
    }
    window.initMap = initMap;
})($);
function htmlpreview(d) {
    var html = '';
    html = html + '<div class="infoBox">';
    html = html + '    <div class="slidebox">';
    html = html + '         <div class="proimg radius5 wd100">';
    html = html + '             <a  href="javascript:void(0)"><img class="img-fluid" src="' + d.logo + '" > </a>';
    html = html + '         </div>';
    html = html + '         <a  class="cname wd100" href="javascript:void(0)">' + d.title + '</a>';
    //  html = html + '         <a target="__blank" class="name wd100" href="javascript:void(0)">' + d.subtitle + '</a>';

    //html = html + '         <a class="wd100 price_block">'+ d.currency + d.price + '</a>';
    html = html + '     </div>';
    html = html + '  </div>';
    return html;
}


/* $(document).ready(function () {
 initMap();
 });
 */