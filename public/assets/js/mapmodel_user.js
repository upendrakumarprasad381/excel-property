
var map;


var mapCenter = new google.maps.LatLng(parseFloat(defaultLat), parseFloat(defaultLong));
map = new google.maps.Map(document.getElementById('map'), {
    center: mapCenter,
    zoom: 17
});
marker = new google.maps.Marker({
    position: mapCenter,
    map: map,
    animation: google.maps.Animation.BOUNCE
});
var infowindow = new google.maps.InfoWindow({
    content: '<p style="color:black">' + locationName + '</p>'
});
infowindow.open(map, marker);
map.setCenter(mapCenter);







/* $(document).ready(function () {
 initMap();
 });
 */