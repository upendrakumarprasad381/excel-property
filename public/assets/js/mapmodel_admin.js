(function ($) {

    var map,
            marker = false,
            geocoder;
    var initMap = function () {
        var defaultLat = 25.276987;
        var defaultLong = 55.296249;
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {


//                defaultLat = position.coords.latitude;
//                defaultLong = position.coords.longitude;
                defaultLat = parseFloat($('#lat').val());
                defaultLong = parseFloat($('#lng').val());
                var mapCenter = new google.maps.LatLng(defaultLat, defaultLong);
                map = new google.maps.Map(document.getElementById('map'), {
                    center: mapCenter,
                    zoom: 17,
                    draggable: true
                });

                $('#party_venue_map').on('hidden.bs.modal', function (e) {
                    $("#location").val($("#store-location").val());
                });
                $('#party_venue_map').on('shown.bs.modal', function () {
                    google.maps.event.trigger(map, "resize");
                });

                var input = document.getElementById('store-location');
                var autocomplete = new google.maps.places.Autocomplete(input);

                // autocomplete.setComponentRestrictions({'country': 'ae'});

                marker = new google.maps.Marker({
                    position: mapCenter,
                    map: map,
                    draggable: true
                });

                google.maps.event.addListener(marker, 'dragend', function (event) {
                    console.log(event)
                    geocodePositionByAddress(marker.getPosition());

                    markerLocation();
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function () {

                    var place = autocomplete.getPlace();
                    latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

                    map.setCenter(latlng);

                    map.setZoom(13);

                    marker.setPosition(latlng);

                    markerLocation();
                });
                map.setCenter(mapCenter);
            });
        }
    }

    function markerLocation() {
        var currentLocation = marker.getPosition();

        $('#lat').val(currentLocation.lat()); //latitude
        $('#lng').val(currentLocation.lng()); //latitude
        $('#location_lat').val(currentLocation.lat()); //latitude
        $('#location_lng').val(currentLocation.lng()); //latitude
        $('.latitude').val(currentLocation.lat()); //latitude
        $('.longitude').val(currentLocation.lng()); //latitude
        geocodePosition(currentLocation);
    }

    function geocodePosition(pos) {
        $('.delete-icon').css('display', 'block');
        $('.pointer').css('display', 'none');
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({latLng: pos
        }, function (responses) {
            //console.log(responses);
            var arr = [];
            if (responses && responses.length > 0) {
                console.log(responses);
                for (i = 0; i < responses.length; i++) {
                    //  if(responses[i].geometry.location_type=='APPROXIMATE'){
                    var resp = responses[i].formatted_address;
                    arr.push(resp);

                    // document.getElementById('store-location').value = responses[0].formatted_address;
                    // document.getElementById('venue').value = responses[0].formatted_address;
                    // }


                }
                $('#sublocality_level_1,#autocomplete').val($('#store-location').val());
                return false;
                console.log(arr);
                var loc = document.getElementById('store-location').value;
                var loc1 = loc.split("-");
                var locality = arr[0].split("-");
                var array = [];
                if (loc1.length > 2 || locality.length < 3)
                {
                    // if(locality.length<3){
                    for (i = 0; i < responses.length; i++)
                    {

                        if (responses[i].geometry.location_type == 'GEOMETRIC_CENTER')
                        {
                            var resp = responses[i].formatted_address;
                            // console.log(resp);
                            array.push(resp);

                            // document.getElementById('store-location').value = responses[0].formatted_address;
                            // document.getElementById('venue').value = responses[0].formatted_address;
                        }


                    }                     // }
                }

                if (locality.length < 3 && array.length != 0) {
                    if (array[1] != '')
                    {
                        var data_value = array[1];
                    } else {
                        var data_value = array[0];
                    }
                    if (data_value != undefined)
                    {
                        var datas = data_value;
                    } else {
                        var datas = array[0];
                    }
                    var data_locality = datas.split(" - ");
                    var data = data_locality[1];
                } else {
                    var datas = arr[0];
                    var data_locality = datas.split(" - ");
                    if (isNaN(data_locality[1]))
                    {
                        var data = data_locality[1];
                    } else {
                        var data = data_locality[2];
                    }
                }


                if (loc1.length == 2)
                {
                    // console.log('d');
                    document.getElementById('autocomplete').value = loc1[0];
                    document.getElementById('sublocality_level_1').value = loc1[0];
                    var list = document.getElementsByClassName('get_location');
                    var n;
                    for (n = 0; n < list.length; ++n) {
                        list[n].value = loc1[0];
                    }
                }
                // if(arr.length<=4 && locality.length>=3 && loc1.length>2)
                else if (arr.length <= 4)
                {                     //console.log(locality);
                    document.getElementById('autocomplete').value = locality[0];
                    document.getElementById('sublocality_level_1').value = locality[0];
                    var list = document.getElementsByClassName('get_location');
                    var n;
                    for (n = 0; n < list.length; ++n) {
                        list[n].value = locality[0];
                    }
                    //$('.get_location').val() = datas;
                }
                else if (arr.length > 4)
                        // else if(arr.length>4 && locality.length<=3 && loc1.length<2)
                        {
                            //console.log('cc');
                            var locality1 = arr[1].split("-");
                            document.getElementById('autocomplete').value = locality1[0];
                            document.getElementById('sublocality_level_1').value = locality1[0];
                            var list = document.getElementsByClassName('get_location');
                            var n;
                            for (n = 0; n < list.length; ++n) {
                                list[n].value = locality1[0];
                            }
                        } else {
                    //console.log('a');
                    document.getElementById('autocomplete').value = loc1[0];
                    document.getElementById('sublocality_level_1').value = loc1[0];
                    var list = document.getElementsByClassName('get_location');
                    var n;
                    for (n = 0; n < list.length; ++n) {
                        list[n].value = loc1[0];
                    }
                    //$('.get_location').val() = loc1[0];
                }

                google.maps.event.addListener(marker, 'dragend', function (event) {

                    document.getElementById('store-location').value = datas;
                });

                // console.log(datas);

                // document.getElementById('venue').value = datas;
                // document.getElementById('autocomplete').value = document.getElementById('store-location').value;
                // document.getElementById('approx_location').value = datas;
                document.getElementById('locality').value = data;
                //document.getElementById('store-location').value = datas;


            } else {

                document.getElementById('store-location').value = '';
            }
        });
    }

    var pacContainerInitialized = false;
    $('#store-location').keypress(function () {
        if (!pacContainerInitialized) {
            $('.pac-container').css('z-index', '9999');
            pacContainerInitialized = true;
        }
    });
    window.initMap = initMap;

})($);
$('#autocomplete').click(function () {
    $('#party_venue_map').modal('show');
});
function geocodePositionByAddress(pos) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos
    }, function (responses) {
        if (responses && responses.length > 0) {
            var addressName = responses[0].formatted_address;
            $('#store-location').val(addressName);
        } else {
            return '';
        }
    });
}

/* $(document).ready(function () {
 initMap();
 });
 */