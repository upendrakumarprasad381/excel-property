(function ($) {
    var postjson = '';
    "use strict";
    $('.input100').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            } else {
                $(this).removeClass('has-val');
            }
        })
    });
    var input = $('.validate-form .validate-input .input100');
    $('.validate-form').on('submit', function () {
        var check = true;
        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }
        if (check == true) {
            var form = new FormData();
            form.append('_token', CSRF_TOKEN);
            form.append('json[user_name]', $('#user_name').val());
            form.append('json[password]', $('#password').val());

            var json = ajaxpost(form, "/admin-login");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    try {
                        if (json.status == true) {
                            $('#loginresult').html('<span style="color: #8bc34a;">' + json.message + '</span>');
                            window.location = json.URL;
                        } else {
                            $('#loginresult').html(' <span style="color: #e91e63;">' + json.message + '</span>');
                        }
                    } catch (e) {

                    }
                }
            } catch (e) {
                alert(e);
            }
           
        }
        return check;
    });
    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    var inputreg = $('.validate-formreg .validate-input .input100');
    $('.validate-formreg').on('submit', function () {
        var check = true;
        for (var i = 0; i < inputreg.length; i++) {
            if (validate(inputreg[i]) == false) {
                showValidate(inputreg[i]);
                check = false;
            }
        }
        if (check == true) {
            var json = {};
            var checked = $('#exampleCheck1:checked').val();
            if (checked == undefined) {
                $('#registerresult').html(' <span style="color: #e91e63;">Please accept terms and conditions</span>');
                return false;
            }

            json['regname'] = $('#regname').val();
            json['regemail'] = $('#regemail').val();
            json['regphone'] = $('#regphone').val();
            json['regphonecode'] = $('#regphonecode').val();
            json['regpassword'] = $('#regpassword').val();
            if (json['regpassword'] != $('#regcpassword').val()) {
                $('#regcpassword').focus();
                $('#registerresult').html(' <span style="color: #e91e63;">Invalid confirm password</span>');
                return false;
            }
            var isvalidate = 0;
            if ($('#regoptcode').val() !== undefined) {
                if ($('#regoptcode').val() == atob($('#regoptcode').attr('yum'))) {
                    isvalidate = 1;
                    json = postjson;
                } else {
                    $('#registerresult').html(' <span style="color: #e91e63;">Invalid otp code</span>');
                    return false;
                }

            }
            json['idorpassport'] = $('#idorpassport').attr('href');
            if (json['idorpassport'] == '') {
                $('#registerresult').html(' <span style="color: #e91e63;">ID or passport is required.</span>');
                return false;
            }
            var data = {isvalidate: isvalidate, json: json};
            $.ajax({
                url: base_url + "login/vendorregistration",
                async: false,
                type: 'POST',
                data: data,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == true) {
                            $('#registerresult').html(' <span style="color: #8bc34a;">' + json.message + '</span>');
                            if (json.isvalidate == false) {
                                postjson = json.formdata;
                                $('otpdiv').html(json.HTML);
                            } else {
                                window.location = json.URL;
                            }

                        } else {
                            $('#registerresult').html(' <span style="color: #e91e63;">' + json.message + '</span>');
                        }
                    } catch (e) {

                    }

                }
            });

        }
        return check;
    });
    $('.validate-formreg .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });
    $('#donthaveaccount').click(function () {
        $('#vendorlogin').slideUp();
        setTimeout(function () {
            $('#vendorregistration').slideDown();
        }, 400);
    });
    $('#haveanacoount').click(function () {
        $('#vendorregistration').slideUp();
        setTimeout(function () {
            $('#vendorlogin').slideDown();
        }, 400);
    });
    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        } else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }
    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }
    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);

function encodeidorpassport(element) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        $("#idorpassport").attr("href", reader.result);
    }
    reader.readAsDataURL(file);
}
function ajaxpost(form, url) {
    var res = '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', base_url + url, false);
    xhr.onload = function () {
        res = xhr.responseText;
    };
    xhr.send(form);
    return res;
}