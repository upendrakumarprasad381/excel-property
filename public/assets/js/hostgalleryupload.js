Dropzone.autoDiscover = false;
var uploadedDropzone = [];


var myDropzone = new Dropzone("div#uploadgallery", {
    url: base_url + "/admin/uploadpropertygallery",
    addRemoveLinks: true,
    success: function (file, response) {
        try {
            var json = jQuery.parseJSON(response);
            $(file.previewTemplate).find('.dz-remove').attr('id', json.insertId);
            if (json.status == false) {
                alert(json.message);
                $(file.previewElement).remove();
            } else {
                uploadedDropzone.push(json.insertId);
            }
        } catch (e) {

        }

    },
    init: function () {
        this.on("sending", function (file, xhr, formData) {
            formData.append("function", 'uploadgallery');
            formData.append("_token", CSRF_TOKEN);
            formData.append("property_id", $('#property_id').val());
        });
        this.on("removedfile", function (file) {
            console.log(file);
            var fileId = $(file.previewTemplate).find('.dz-remove').attr('id');
            var data = {function: 'removeuploadgallery', fileId: fileId, helper: 'Common', _token: CSRF_TOKEN};
            $.ajax({
                url: base_url + "/helper",
                data: data,
                type: 'POST',
                success: function (data) {

                }
            });
        });
    }
});
