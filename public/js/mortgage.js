   jQuery(document).ready(function($) {
	   
	function change() {

        var loanamount_total = $("#loanamount").val();

        var loanintrest = $("#loanintrest").val();

        var period = $("#period").val();
        var downpayment_percent = $("#downpayment").val();
        var downpayment = (Number(downpayment_percent) * Number(loanamount_total)) / 100;

        var loanamount = Number(loanamount_total) - Number(downpayment);
        var intrest = (loanintrest / 100) / 12;
		
	
		
        var permonth = calculate(loanamount , Number(period) * 12 , intrest);
		var total_amount = permonth * Number(period) * 12;
		var total_intrest =  total_amount - loanamount;

        $("#permonth").text("AED " + n( permonth ) );
        $("#downpament_a").text("AED " + n( downpayment.toFixed(2) ) );
        $("#loanamount_total").text("AED " + n( Number(loanamount_total).toFixed(2) ));
        $("#total_interest").text("AED " + n( total_intrest.toFixed(2) ));
        $("#total_amount").text("AED " + n( total_amount.toFixed(2) ));
		
		$("#loanamount_total").html("AED "  +loanamount_total);
		$("#loanintrest_total").html(loanintrest + "%");
		$("#downpayment_total").html(downpayment_percent + "%");
		$("#period_total").html(period + " Years (" + Number(period) * 12 + " Month)");
		
		var total_intrest_percent = (Number(total_intrest) * 100) / Number(total_amount);
        var total_amount_precent = (Number(loanamount) * 100) / Number(total_amount);
		
		var left_amount = Number(total_amount);
		var loanamount_left = Number(loanamount);
		$( "#tbody" ).html("");
		for(var i=1; i <= Number(period) * 12; i++) { 
			var intrest_l = intrest * loanamount_left;
			left_amount = left_amount - permonth;
			var principle = permonth - intrest_l;
			loanamount_left = loanamount_left - principle;
				$( "#tbody" ).append( "<tr style='background-color: #FBFCFF;border-top: dotted #DEEEFE 1px;'><td style='padding:5px'> "+ i +" </td><td> AED "+ n(permonth)  +" </td> <td>AED " + n(principle.toFixed(2)) +" </td><td>AED " + n(intrest_l.toFixed(2))  + "</td>  <td> AED "+ n(left_amount.toFixed(2)) +" </td></tr>" );
		}

		//renderChart(loanamount_total,total_amount,downpayment,total_intrest, permonth);


    }
	
	
	function n(t) {
        return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    $("body").on("keyup", "#downpayment , #loanintrest , #loanamount , #period, #per_year", function () {
        change();
    });

    $(function () {
        change();
    });
	
	
function calculate(amount , term , loanintrest) {
	var P = amount;
	var i = loanintrest;
	var n = term;
	var x = Math.pow((1 + i ), n);
	var M = ( P * ((i * x) / (x - 1)) ).toFixed(2);
	return M;
}

function printData() {
   change();
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$('#printForm').on('click',function(){
printData();
})

});