$(document).ready(function () {
     if (location.protocol !== "https:") {
        location.protocol = "https:";
    }
    $(document).on('click', '.datepicker', function () {
        $(this).datetimepicker({
            format: 'DD-MM-YYYY',
        }).focus();
        $(this).removeClass('datepicker');
    });
    $('body').on('focus', ".datetimepicker", function () {
        $(this).datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY HH:mm:ss',
        });
    });
    $('body').on('click', '.__mob_srclose', function () {
        $('#searchboxdiv').show();
        $('.section.__bnrbtmscrwrp').hide();
    });
    $('body').on('click', '.__closeMmne', function () {
        menuu.close();
    });
    if (Method == 'areaguides' || Method == 'newdevelopments' || Method == 'projects' || Method == 'myproperty' || Method == 'team' || Method == 'addmyproperty' || Method == 'tagdetails') {
        $(function () {
            $("#sortable").sortable({
                stop: function (e, ui) {
                    console.log($.map($(this).find('li'), function (el) {
                        return el.id + ' = ' + $(el).index();
                    }));
                }
            });

        });
    }
    $('body').on('click', '.autodelete', function () {
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var filepath = $(this).attr('filepath');
        // var data = {function: 'autodelete', condjson: condjson, dbtable: dbtable, filepath: filepath, _token: CSRF_TOKEN, helper: 'Common'};

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('dbtable', dbtable);
        form.append('condjson', condjson);
        form.append('filepath', filepath);
        form.append('helper', 'Common');
        form.append('function', 'autodelete');

        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var json = ajaxpost(form, "/helper");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                try {
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    $('body').on('click', '.autoupdate', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        //var data = {function: 'autoupdate', updatejson: updatejson, condjson: condjson, dbtable: dbtable, _token: CSRF_TOKEN, helper: 'Common'};
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('dbtable', dbtable);
        form.append('condjson', condjson);
        form.append('updatejson', updatejson);
        form.append('helper', 'Common');
        form.append('function', 'autoupdate');
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var json = ajaxpost(form, "/helper");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                try {
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    $("#name").keypress(function (event) {
        var inputValue = event.charCode;
        if (!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123) || (inputValue == 32) || (inputValue == 0))) {
            event.preventDefault();
        }
    });
    $(".slugsvalidate").keypress(function (event) {
        var inputValue = event.charCode;
        if (!((inputValue > 64 && inputValue < 91) || (inputValue > 96 && inputValue < 123) || (inputValue == 32) || (inputValue == 0) || (inputValue == 45))) {
            event.preventDefault();
        }
    });
    $('.slugsvalidate').bind("paste", function (e) {
        e.preventDefault();
    });
});
var POSITIONSET = (function () {
    var fn = {};
    fn.areaguidesUpdatespos = function (e) {

        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        })

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('helper', 'Common');
        form.append('function', 'areaguidesUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
    }
    fn.newdevelopmentsUpdatespos = function () {
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        })
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('helper', 'Common');
        form.append('function', 'newdevelopmentsUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
    }

    fn.mypropertyUpdatespos = function () {
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        });

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('helper', 'Common');
        form.append('function', 'mypropertyUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }


    }
    fn.teamUpdatespos = function () {
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        });
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('helper', 'Common');
        form.append('function', 'teamUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
    }

    fn.imageProperyUpdatespos = function () {
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        });
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('helper', 'Common');
        form.append('function', 'imageProperyUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
    }
    
    
     fn.myTagUpdatespos = function () {
        var orderview = [];
        $.map($('#sortable').find('li'), function (el) {
            var d = {};
            d['id'] = el.id;
            d['orderview'] = $(el).index();
            orderview.push(d);
        });

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('orderview', JSON.stringify(orderview));
        form.append('tag_id', $('#tag_id').val());
        
        form.append('helper', 'Common');
        form.append('function', 'myTagUpdatespos');

        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }


    }
    
    return fn;
})();
function ajaxpost(form, url) {
    var res = '';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', base_url + url, false);
    xhr.onload = function () {
        res = xhr.responseText;
    };
    xhr.send(form);
    return res;
}
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}
function alertSimple(message) {
    $.dialog({
        title: false,
        content: message,
    });
}
function showLoader(id) {
    var html = '<img class="_loader_gif" src="' + base_url + '/public/images/loader.gif">';
    $('#' + id).html(html);
}
function hideLoader(id) {
    $('#' + id).html('');
}
function showmysearchBar() {
    $('#searchboxdiv').hide();
    $('.section.__bnrbtmscrwrp').show();
}
function convertToSlug(e) {
    var Text = $(e).val();
    var updateId = $(e).attr('updateId');
    var slug = Text
            .toLowerCase()
            .replace(/ /g, '-')
            .replace(/[^\w-]+/g, '')
            ;
    $('#' + updateId).val(slug);
}