var menuu = new Mmenu(document.querySelector('#menu'));

document.addEventListener('click', function (evnt) {
    var anchor = evnt.target.closest('a[href^="#/"]');
    if (anchor) {
        alert("Thank you for clicking, but that's a demo link.");
        evnt.preventDefault();
    }
});
document.addEventListener("DOMContentLoaded", function () {

    window.addEventListener('scroll', function () {

        if (window.scrollY > 50) {
            document.getElementById('navbar_top').classList.add('fixed-top');
            // add padding top to show content behind navbar
            navbar_height = document.querySelector('.navbar').offsetHeight;
            document.body.style.paddingTop = navbar_height + 'px';
        } else {
            document.getElementById('navbar_top').classList.remove('fixed-top');
            // remove padding top from body
            document.body.style.paddingTop = '0';
        }
    });
});
window.addEventListener("resize", function () {
    "use strict";
  //  window.location.reload();
});

document.addEventListener("DOMContentLoaded", function () {

    // make it as accordion for smaller screens
    if (window.innerWidth < 992) {
        // close all inner dropdowns when parent is closed
        document.querySelectorAll('.navbar .dropdown').forEach(function (everydropdown) {
            everydropdown.addEventListener('hidden.bs.dropdown', function () {
                // after dropdown is hidden, then find all submenus
                this.querySelectorAll('.submenu').forEach(function (everysubmenu) {
                    // hide every submenu as well
                    everysubmenu.style.display = 'none';
                });
            })
        });

        document.querySelectorAll('.dropdown-menu a').forEach(function (element) {
            element.addEventListener('click', function (e) {

                let nextEl = this.nextElementSibling;
                if (nextEl && nextEl.classList.contains('submenu')) {
                    // prevent opening link if link needs to open dropdown
                    e.preventDefault();

                    if (nextEl.style.display == 'block') {
                        nextEl.style.display = 'none';
                    } else {
                        nextEl.style.display = 'block';
                    }

                }
            });
        })
    }
    // end if innerWidth

});
 var swiper = new Swiper(".mySwiper_blog", {
		direction: "vertical",
		   loop: true,
		pagination: {
		  el: ".mySwiper_blog-pagination",
		  clickable: true,
		},
		     
		  	autoplay: {
            delay: 20000, 
        } 
	  });

var swiper = new Swiper('.__srbk1', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,
    // init: false,
    pagination: {
        // el: '.swiper-pagination',
        // clickable: true,
    },
    navigation: {
        nextEl: '.__srbk1-next',
        prevEl: '.__srbk1-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    autoplay: {
        delay: 2500,
    }
});



var swiper = new Swiper('.__srbk2', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,
    // init: false,
    pagination: {
        // el: '.swiper-pagination',
        // clickable: true,
    },
    navigation: {
        nextEl: '.__srbk2-next',
        prevEl: '.__srbk2-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    autoplay: {
        delay: 2700,
    }
});


var swiper = new Swiper('.__srbk3', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,
    // init: false,
    pagination: {
        // el: '.swiper-pagination',
        // clickable: true,
    },
    navigation: {
        nextEl: '.__srbk3-next',
        prevEl: '.__srbk3-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    autoplay: {
        delay: 2900,
    }
});




var swiper = new Swiper('.__srbk4', {
    slidesPerView: 1,
    spaceBetween: 15,
    loop: true,
    // init: false,
    pagination: {
        // el: '.swiper-pagination',
        // clickable: true,
    },
    navigation: {
        //  nextEl: '.__srbk4-next',
        // prevEl: '.__srbk4-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 1,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    autoplay: {
        delay: 2300,
    }
});



var swiper = new Swiper('.__srbk5', {
    slidesPerView: 3,
    spaceBetween: 15,
    loop: true,
    // init: false,
    pagination: {
        // el: '.swiper-pagination',
        // clickable: true,
    },
    navigation: {
        //  nextEl: '.__srbk5-next',
        // prevEl: '.__srbk5-prev',
    },
    breakpoints: {
        1024: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }
    },
    autoplay: {
        delay: 2900,
    }
});
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
});
$(function () {
    $("#slider-range").slider({
        range: true,
        min: 1000,
        max: 50000,
        values: [1000, 50000],
        slide: function (event, ui) {
            $("#amount").val("AED" + ui.values[ 0 ] + " - AED" + ui.values[ 1 ]);
        }
    });
    $("#amount").val("AED" + $("#slider-range").slider("values", 0) +
            " - AED" + $("#slider-range").slider("values", 1));
});
$(function () {
    $("#slider-range2").slider({
        range: true,
        min: 1000,
        max: 50000,
        values: [1000, 50000],
        slide: function (event, ui) {
            $("#amount2").val("AED" + ui.values[ 0 ] + " - AED" + ui.values[ 1 ]);
        }
    });
    $("#amount2").val("AED" + $("#slider-range2").slider("values", 0) +
            " - AED" + $("#slider-range").slider("values", 1));
});