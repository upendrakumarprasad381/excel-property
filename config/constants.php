<?php

return [
    'VENDOR_SESSION_NAME' => 'excel_properties_admin_session',
    'DEFAULT_LOGO_VENDOR' => 'images/default-user-image.jpg',
    'HOME_DIR' => base_path() . '/',
    'DEFAULT_PROPERTY_LOGO' => 'public/images/default-image-property.jpg',
    'DEFAULT_FACILITY_LOGO' => 'public/images/default-property-faclicy.png',
    'MAP_API_KEY' => 'AIzaSyARcSpDt_SEZfexNRiEAv5D-IHPA9OCBvc', //
    'ADMIN_NOTIFY' => array(
        'LIST_WITH_US' => 'LIST_WITH_US',
        'NEW_DEVELOPMENTS' => 'NEW_DEVELOPMENTS',
         'DEVELOPMENTS' => 'DEVELOPMENTS',
        'AREA_GUIDE' => 'AREA_GUIDE',
        'CAREERS' => 'CAREERS',
        'CONTACT_US' => 'CONTACT_US',
        'NEW_PROPERTY_ENQ' => 'NEW_PROPERTY_ENQ',
        'BLOG_DETAILS' => 'BLOG_DETAILS'
    )
];
