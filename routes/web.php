<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/', 'properties@index');
Route::get('/property', 'properties@property');



Route::get('/property-details/{propertyslugId}', 'properties@propertydetails');
Route::get('/list-with-us', 'properties@listwithus');
Route::get('/new-developments', 'properties@newdevelopments');
Route::get('/developments', 'properties@developments');
Route::get('/more-developments/{developerId?}', 'properties@moredevelopments');
Route::get('/area-guide-details', 'properties@areaguidedetails');
Route::get('/more-area-guides', 'properties@moreareaguides');
Route::get('/about-us', 'properties@aboutus');
Route::get('/services', 'properties@services');
Route::get('/team', 'properties@team');
Route::get('/careers', 'properties@careers');
Route::get('/testimonals', 'properties@testimonals'); 
Route::get('/contact-us', 'properties@contactus');
Route::get('/blog', 'properties@blog');
Route::get('/blog-detail/{blogslugId}', 'properties@blogdetail');

Route::get('/thank-you/{typeId?}', 'properties@thankyou');

Route::get('/privacy-policy', 'properties@privacypolicy');
Route::get('/terms-conditios', 'properties@termsconditios');
Route::get('/site-map', 'properties@sitemap');
Route::get('/sale-transaction', 'properties@saletransaction');
Route::any('/helper', 'login@helper');

Route::group(['prefix' => 'admin', 'as' => 'admin'], function() {
    Route::get('/', 'admin@index');
    Route::any('propertytype', 'admin@propertytype');
    Route::any('addpropertytype', 'admin@addpropertytype');



    Route::any('normal-propertytype', 'admin@normalpropertytype');
    Route::any('addnormal-propertytype', 'admin@addnormalpropertytype');

    Route::any('propertyprice', 'admin@propertyprice');
    Route::any('addpropertyprice', 'admin@addpropertyprice');

    Route::any('keywords', 'admin@keywords');
    Route::any('addkeywords', 'admin@addkeywords');

    Route::any('tower', 'admin@tower');
    Route::any('addtower', 'admin@addtower');

    Route::any('submaster', 'admin@submaster');
    Route::any('addsubmaster', 'admin@addsubmaster');
    Route::any('newdevelopments', 'admin@newdevelopments');
    Route::any('projects', 'admin@projects');
    Route::any('addnewdevelopments', 'admin@addnewdevelopments');
    Route::get('subadmin', 'admin@subadmin');
    Route::any('addsubadmin', 'admin@addsubadmin');
    Route::get('propertyfacility', 'admin@propertyfacility');
    Route::any('addpropertyfacility', 'admin@addpropertyfacility');
    Route::get('customers', 'admin@customers');
    Route::any('myproperty', 'admin@myproperty');
    Route::any('addmyproperty', 'admin@addmyproperty');
    Route::any('uploadpropertygallery', 'admin@uploadpropertygallery');
    Route::get('myenquiry', 'admin@myenquiry');

    Route::get('blogs', 'admin@blogs');
    Route::any('addblogs', 'admin@addblogs');

    Route::any('testimonial', 'admin@testimonial');
    Route::any('addtestimonial', 'admin@addtestimonial');

    Route::any('areaguides', 'admin@areaguides');
    Route::any('addareaguides', 'admin@addareaguides');

    Route::any('team', 'admin@team');
    Route::any('addteam', 'admin@addteam');

    Route::get('agents', 'admin@agents');
    Route::any('addagents', 'admin@addagents');


    Route::any('cms', 'admin@cms');
    Route::any('listwithuscms', 'admin@listwithuscms');
    Route::any('servicescms', 'admin@servicescms');

    Route::any('privacy-policy-cms', 'admin@privacypolicycms');
    Route::any('terms-conditios-cms', 'admin@termsconditioscms');
    Route::any('about-us-cms', 'admin@aboutuscms');

    Route::any('contactus', 'admin@contactus');
    Route::any('enquiry-details', 'admin@enquirydetails');

    Route::get('tags', 'admin@tags');
    Route::any('addtags', 'admin@addtags');
    Route::any('tagdetails', 'admin@tagdetails');


    Route::get('bannermanagement', 'admin@bannermanagement');
    Route::any('addbannermanagement', 'admin@addbannermanagement');

    Route::get('arealist', 'admin@arealist');
    Route::any('addarealist', 'admin@addarealist');

    Route::any('careerscms', 'admin@careerscms');
    Route::any('careersenquiry', 'admin@careersenquiry');
});


Route::get('/admin-login', 'login@adminlogin')->name('admin-login');
Route::post('/admin-login', 'login@adminlogin');
Route::get('/admin-logout', 'admin@adminlogout')->name('admin-logout');

Route::group(['prefix' => 'dubai', 'as' => 'dubai'], function() {


    Route::get('/off-plan-properties', 'properties@moredevelopments');


    Route::get('/list-with-us', 'properties@listwithus');
    Route::get('/about-us', 'properties@aboutus');
    Route::get('/services', 'properties@services');
    Route::get('/team', 'properties@team');
    Route::get('/careers', 'properties@careers');
    Route::get('/testimonals', 'properties@testimonals');
    Route::get('/contact-us', 'properties@contactus');
    Route::get('/blog', 'properties@blog');
    Route::get('/blog-detail', 'properties@blogdetail');
    Route::get('/privacy-policy', 'properties@privacypolicy');
    Route::get('/terms-conditios', 'properties@termsconditios');
    Route::get('/site-map', 'properties@sitemap');
    Route::get('/more-developments/{developerId?}', 'properties@moredevelopments');
    Route::group(['prefix' => 'properties-for-sale', 'as' => 'properties-for-sale'], function() {

        Route::get('/off-plan-apartments', 'properties@offplanFrommenu');
        Route::get('/off-plan-villas', 'properties@offplanFrommenu');

        // Route::get('/off-plan-villas', 'properties@moredevelopments');


        Route::get('/search', 'properties@property');
        Route::get('/tower/{towerName}', 'properties@property');
        Route::get('/area/{areaName}', 'properties@property');
        Route::get('/{ptypeId?}', 'properties@property');
    });
    Route::group(['prefix' => 'properties-for-rent', 'as' => 'properties-for-rent'], function() {
        Route::get('/search', 'properties@property');
        Route::get('/tower/{towerName}', 'properties@property');
        Route::get('/area/{areaName}', 'properties@property');
        Route::get('/{ptypeId?}', 'properties@property');
    });

    Route::group(['prefix' => 'real-estate-developers', 'as' => 'real-estate-developers'], function() {
        Route::get('/', 'properties@realestatedevelopers');
        Route::get('/{developerId}', 'properties@developments'); // developments
        Route::get('/{developerId}/{projectsId}', 'properties@newdevelopments'); // new developments
    });
    Route::group(['prefix' => 'area-guides', 'as' => 'area-guides'], function() {
        Route::get('/', 'properties@moreareaguides');
        Route::get('/{areaId}', 'properties@areaguidedetails');
        Route::get('/{areaId}/properties', 'properties@areaguideproperties');
    });
    Route::get('/{stringId}/{propertyslugId}', 'properties@propertydetails');
});


