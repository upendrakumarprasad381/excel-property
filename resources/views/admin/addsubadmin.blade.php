@include('admin.includes.header')
<?php
$vendorId = !empty($_REQUEST['vendorId']) ? base64_decode($_REQUEST['vendorId']) : '';
$UserArray = \App\Helpers\CommonHelper::GetvendorBy($vendorId);
?>
<style>

    .modal-title {
        margin: 0;
        line-height: 1.42857143;
        font-size: 16px;
        text-transform: uppercase;
        padding: 3px 0;
    }

    .pac-container, .pac-item{ z-index: 2147483647 !important; }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= !empty($UserArray->vendor_id) == '' ? 'Add New' : 'Update'; ?> sub admin</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="vendor_id" value="<?= !empty($UserArray->vendor_id) ? $UserArray->vendor_id : '' ?>">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">First Name</label>
                                    <div class="col-md-4">
                                        <input id="first_name" value="<?= isset($UserArray->first_name) ? $UserArray->first_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Last Name</label>
                                    <div class="col-md-4">
                                        <input id="last_name" value="<?= isset($UserArray->last_name) ? $UserArray->last_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" value="<?= isset($UserArray->email) ? $UserArray->email : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div><span class='email_error' id='email_error' style='color:red'></span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Phone</label>
                                    <div class="col-md-4">
                                        <input id="mobile" value="<?= isset($UserArray->mobile) ? $UserArray->mobile : '971' ?>" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password</label>
                                    <div class="col-md-4">
                                        <input id="password"  type="password" class="form-control" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="status">
                                            <option <?= isset($UserArray->status) ? $UserArray->status == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>
                                            <option <?= isset($UserArray->status) ? $UserArray->status == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Type</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="user_type">
                                            <option <?= empty($usertype) ? 'selected="selected"' : '' ?> value="0">Sub admin</option>
                                        </select>
                                    </div>
                                </div>







                                <div class="form-group" id="permissiondiv" >
                                    <label class="control-label col-md-3">Permission</label>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                $perAr = array(
                                                    array('permission_name' => 'Additional settings', 'permission_id' => 'Additionalsettings'),
                                                     array('permission_name' => 'Blogs', 'permission_id' => 'Blogs'),
                                                     array('permission_name' => 'Testimonial', 'permission_id' => 'Testimonial'),
                                                    array('permission_name' => 'Facility', 'permission_id' => 'Facility'),
                                                    array('permission_name' => 'Customers', 'permission_id' => 'Customers'),
                                                    array('permission_name' => 'My Property', 'permission_id' => 'MyProperty'),
                                                    array('permission_name' => 'Enquiry', 'permission_id' => 'Enquiry'),
                                                    array('permission_name' => 'Contact us', 'permission_id' => 'Contactus'),
                                                );
                                                $permission = !empty($UserArray->permission) ? json_decode($UserArray->permission) : [];
                                                $permission = !empty($permission) && is_array($permission) ? $permission : [];

                                                for ($i = 0; $i < count($perAr); $i++) {
                                                    $d = $perAr[$i];
                                                    ?>
                                                    <div class="col-sm-4">
                                                        <div class="checkbox">
                                                            <label><input class="permission" <?= in_array($d['permission_id'], $permission) ? 'checked' : '' ?>  type="checkbox" value="<?= $d['permission_id'] ?>"><?= $d['permission_name'] ?></label>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>













                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitNewUser" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '<?= url('admin/subadmin?usertype=0'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')
<script>
    $('#SubmitNewUser').click(function () {
        var json = {};

        if ($('#first_name').val() == '') {
            $('#first_name').focus();
            $('#first_name').css('border-color', 'red');
            return false;
        } else {
            $('#first_name').css('border-color', '');
        }

        if ($('#email').val() == '') {
            $('#email').focus();
            $('#email').css('border-color', 'red');
            return false;
        } else {
            if (!validateEmail($('#email').val())) {
                $('#email').focus();
                $('#email').css('border-color', 'red');
                return false;
            }
            $('#email').css('border-color', '');
        }

        if ($('#mobile').val() == '') {
            $('#mobile').focus();
            $('#mobile').css('border-color', 'red');
            return false;
        } else {
            $('#mobile').css('border-color', '');
        }
        json['first_name'] = $('#first_name').val();
        json['last_name'] = $('#last_name').val();
        json['email'] = $('#email').val();
        json['mobile'] = $('#mobile').val();
        json['password'] = $('#password').val();
        json['status'] = $('#status').val();
        json['user_type'] = $('#user_type').val();

        json['vendor_id'] = $('#vendor_id').val();


        var permission = [];
        $('.permission:checked').each(function () {
            permission.push($(this).val());
        });
        json['permission'] = JSON.stringify(permission);


        var data = {json: json, _token: CSRF_TOKEN};
        $.confirm({
            title: 'Are you sure you want to Save !',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            data: data,
                            type: 'POST',
                            async: false,
                            url: base_url + "/admin/addsubadmin",
                            success: function (data) {
                                var json = jQuery.parseJSON(data);
                                alertSimple(json.message);
                                if (json.status == true) {
                                    window.location = base_url + '/admin/subadmin';
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-info',
                    action: function () {
                    }
                },
            }
        });


    });
</script>