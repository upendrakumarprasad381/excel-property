@include('admin.includes.header')
<?php
$propertyId = !empty($_REQUEST['propertyId']) ? base64_decode($_REQUEST['propertyId']) : '';
$pArray = App\Helpers\LibHelper::GetmypropertyBy($propertyId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Host property</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->property_id) ? $pArray->property_id : '' ?>" id="property_id">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Reference No.<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->property_no) ? $pArray->property_no : \App\Helpers\CommonHelper::GetnewPropertyNo(); ?>"  id="property_no" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Property No.<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->property_no_1) ? $pArray->property_no_1 : '' ?>"  id="property_no_1" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Permit Number<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->permit_number) ? $pArray->permit_number : '' ?>"  id="permit_number" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>RENT/BUY <span style="color:red">*</span></label>
                                                            <select class="form-control" id="ptype">
                                                                <?php
                                                                $ptype = isset($pArray->ptype) ? $pArray->ptype : '1';
                                                                ?>
                                                                <option <?= $ptype == '1' ? 'selected' : '' ?> value="1">RENT</option>
                                                                <option <?= empty($ptype) ? 'selected' : '' ?> value="0">BUY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Property Type <span style="color:red">*</span></label>
                                                            <select class="form-control" id="property_type">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $property_type = isset($pArray->property_type) ? $pArray->property_type : '0';
                                                                $listd = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptype);
                                                                for ($i = 0; $i < count($listd); $i++) {
                                                                    $d = $listd[$i];
                                                                    ?> <option <?= $property_type == $d->ptype_id ? 'selected' : '' ?> value="<?= $d->ptype_id ?>"><?= $d->ptype_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Property Type Normal <span style="color:red">*</span></label>
                                                            <select class="form-control" id="property_type_normal">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $property_type_normal = isset($pArray->property_type_normal) ? $pArray->property_type_normal : '0';
                                                                $qry = "SELECT * FROM `property_type_others` WHERE  archive=0 ";
                                                                $listd = App\Database::select($qry);
                                                                for ($i = 0; $i < count($listd); $i++) {
                                                                    $d = $listd[$i];
                                                                    ?> <option <?= $property_type_normal == $d->id ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Tower Name <span style="color:red">*</span></label>
                                                            <select class="form-control" id="tower_id">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $tower_id = isset($pArray->tower_id) ? $pArray->tower_id : '0';
                                                                $listd = App\Helpers\LibHelper::GetTowerlist($ptype);
                                                                for ($i = 0; $i < count($listd); $i++) {
                                                                    $d = $listd[$i];
                                                                    ?> <option <?= $tower_id == $d->tower_id ? 'selected' : '' ?> value="<?= $d->tower_id ?>"><?= $d->tower_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Area Name <span style="color:red">*</span></label>
                                                            <select class="form-control" id="area_id">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $area_id = isset($pArray->area_id) ? $pArray->area_id : '0';
                                                                $qry = "SELECT * FROM `area_list` WHERE archive=0";
                                                                $listd = \App\Database::select($qry);
                                                                for ($i = 0; $i < count($listd); $i++) {
                                                                    $d = $listd[$i];
                                                                    ?> <option <?= $area_id == $d->area_id ? 'selected' : '' ?> value="<?= $d->area_id ?>"><?= $d->area_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Title <span style="color:red">*</span></label>
                                                            <input type="text" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= !empty($pArray->property_title) ? $pArray->property_title : '' ?>" id="property_title" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="control-label ">Slugs</label>
                                                            <input id="slugs" value="<?= isset($pArray->slugs) ? $pArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Position <span style="color:red">*</span></label>
                                                            <input type="number" disabled value="<?= !empty($pArray->position) ? $pArray->position : '' ?>" id="position" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Address <span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->address) ? $pArray->address : '' ?>" id="address" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Emirates </label>
                                                            <?php
                                                            $city = !empty($pArray->city) ? $pArray->city : '';
                                                            ?>
                                                            <select id="city" class="form-control" placeholder="" autocomplete="off">
                                                                <option <?= $city == 'Dubai' ? 'selected' : '' ?> value="Dubai">Dubai</option>
                                                                <option <?= $city == 'Abu Dhabi' ? 'selected' : '' ?> value="Abu Dhabi">Abu Dhabi</option>
                                                                <option <?= $city == 'Sharjah' ? 'selected' : '' ?> value="Sharjah">Sharjah</option>
                                                                <option <?= $city == 'Ajman' ? 'selected' : '' ?> value="Ajman">Ajman</option>
                                                                <option <?= $city == 'Umm Al-Quwain' ? 'selected' : '' ?> value="Umm Al-Quwain">Umm Al-Quwain</option>
                                                                <option <?= $city == 'Fujairah' ? 'selected' : '' ?> value="Fujairah">Fujairah</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Area (In sq ft)<span style="color:red">*</span></label>
                                                            <input type="number" value="<?= !empty($pArray->area) ? $pArray->area : '' ?>" id="area" class="form-control" placeholder="200 " autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Total Bed </label>
                                                            <input type="number" value="<?= !empty($pArray->bed) ? $pArray->bed : '0' ?>" id="bed" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" <?= !empty($pArray->is_studio) ? 'checked' : '' ?>   id="is_studio">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                Studio
                                                            </label>
                                                        </div>
                                                    </div>




                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Total Bathroom </label>
                                                            <input type="number" value="<?= !empty($pArray->bathroom) ? $pArray->bathroom : '2' ?>" id="bathroom" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Agent Name <span style="color:red">*</span></label>
                                                            <select value="" id="owner_id" class="form-control" >
                                                                <option value="">--select--</option>
                                                                <?php
                                                                $owner_id = !empty($pArray->owner_id) ? $pArray->owner_id : '';
                                                                $Sql = "SELECT * FROM `agents` WHERE archive=0";
                                                                $dlist = \App\Database::select($Sql);
                                                                for ($i = 0; $i < count($dlist); $i++) {
                                                                    $d = $dlist[$i];
                                                                    ?><option <?= $owner_id == $d->agents_id ? 'selected' : '' ?> value="<?= $d->agents_id ?>"><?= $d->name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
  <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Agent Phone</label>
                                                            <input type="text"  value='<?= !empty($pArray->agent_phone) ? $pArray->agent_phone : "+971506606100" ?>' id="agent_phone" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Tag <span style="color:red">*</span></label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="tag_id">
                                                                <?php
                                                                $tagId = !empty($pArray->tag_id) ? explode(',', $pArray->tag_id) : [];
                                                                $tagId = !empty($tagId) && is_array($tagId) ? $tagId : [];
                                                                $Sql = "SELECT * FROM `tags` WHERE data_list_from IN ('PROPERTY_LISTING','AREA_GUIDES','NEW_DEVELOPMENT') ORDER BY position ASC";
                                                                $dlist = \App\Database::select($Sql);
                                                                for ($i = 0; $i < count($dlist); $i++) {
                                                                    $d = $dlist[$i];
                                                                    ?><option <?= in_array($d->tag_id, $tagId) ? 'selected' : '' ?> value="<?= $d->tag_id ?>"><?= $d->tag_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Location 
                                                                <!--<a href="https://www.google.com/maps" target="_blank">Google Map</a>-->
                                                            </label>
                                                            <input type="text" readonly value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>' id="autocomplete" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                  



                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>PDF Description <span style="color:red">*</span>  </label>
                                                            <textarea value="" id="pdf_description" class="form-control" rows="5"><?= !empty($pArray->pdf_description) ? $pArray->pdf_description : '' ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>PDF Map Location 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->pdf_map_loc) ? $pArray->pdf_map_loc : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                                <span class="size-alert">Min width and height - 852 * 712 px</span>
                                                            </label>
                                                            <input type="file" id="pdf_map_loc" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>





                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Status <span style="color:red">*</span></label>
                                                            <select class="form-control" id="status">
                                                                <?php
                                                                $status = !empty($pArray->status) ? $pArray->status : '';
                                                                ?>
                                                                <option <?= empty($status) ? 'selected' : '' ?> value="0">Active</option>
                                                                <option <?= $status == '1' ? 'selected' : '' ?> value="1">Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h5 style="font-weight: 600;">Gallery images <span class="size-alert">Min width and height - 800 * 600 px</span> <span style="color:red"> *</span></h5>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group dropzone dz-clickable" id="uploadgallery">
                                                                    <div class="dz-default dz-message" data-dz-message="">
                                                                        <span>Drop files here to upload</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>  



                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" class="addhostproperty"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/myproperty') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>



                            <div class="col-md-6 " style="margin-top: 88px;">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="col-sm-12"> 
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <label>Price <span style="color:red">*</span></label>
                                                <input type="number" value="<?= !empty($pArray->price) ? $pArray->price : '' ?>" id="price" class="form-control" placeholder="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Type <span style="color:red">*</span></label>
                                                <select class="form-control" id="price_type">
                                                    <?php
                                                    $price_type = !empty($pArray->price_type) ? $pArray->price_type : '';
                                                    ?>
                                                    <option <?= empty($price_type) ? 'selected' : '' ?> value="0">Yearly</option>
                                                    <option <?= $price_type == '1' ? 'selected' : '' ?> value="1">Monthly</option>
                                                    <option <?= $price_type == '2' ? 'selected' : '' ?> value="2">Quarterly</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label>Down Payment (%) <span style="color:red">*</span></label>
                                        <input type="number" value="<?= !empty($pArray->downpayment) ? $pArray->downpayment : '' ?>" id="downpayment" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Interest Rate (%) <span style="color:red">*</span></label>
                                        <input type="number" value="<?= !empty($pArray->loanintrest) ? $pArray->loanintrest : '' ?>" id="loanintrest" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Loan Period Yearly <span style="color:red">*</span></label>
                                        <input type="number" value="<?= !empty($pArray->period) ? $pArray->period : '' ?>" id="period" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>


                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label>Meta Title</label>
                                        <input type="text" value="<?= !empty($pArray->meta_title) ? $pArray->meta_title : '' ?>" id="meta_title" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <input type="text" value="<?= !empty($pArray->meta_description) ? $pArray->meta_description : '' ?>" id="meta_description" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keywords</label>
                                        <input type="text" value="<?= !empty($pArray->meta_keywords) ? $pArray->meta_keywords : '' ?>" id="meta_keywords" class="form-control" placeholder="" autocomplete="off">
                                    </div>



                                </div>

                                <div class="col-sm-12" style="margin-top: 10px;"> 
                                    <form class="form" role="form">
                                        <div class="portlet box green" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Add Features
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body open" style="display: block;">
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="mt-checkbox-inline">
                                                            <ul class="hostpropertyul">
                                                                <?php
                                                                $dArray = App\Helpers\LibHelper::roomfacility();
                                                                $facility = !empty($pArray->facility) ? explode(',', $pArray->facility) : [];
                                                                for ($i = 0; $i < count($dArray); $i++) {
                                                                    $d = $dArray[$i];
                                                                    ?>
                                                                    <li>
                                                                        <label class="mt-checkbox">
                                                                            <input type="checkbox" <?= in_array($d->facility_id, $facility) ? 'checked' : '' ?> class="facilitycheckbox" value="<?= $d->facility_id ?>"> <?= $d->facility_name ?>
                                                                            <span></span>
                                                                        </label>
                                                                    <?php } ?>
                                                                </li>
                                                            </ul>
                                                        </div> 
                                                    </div>
                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->



                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="col-sm-12" style="margin-top: 10px;"> 
                                    <form class="form" role="form">
                                        <div class="portlet box green" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    About this property
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body open" style="display: block;">
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label> Description <span style="color:red">*</span>  </label>
                                                            <textarea value="" id="description" class="form-control summernote" rows="5"><?= !empty($pArray->description) ? $pArray->description : '' ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->





                                <div class="col-sm-12" style="margin-top: 10px;"> 
                                    <div class="portlet ">
                                        <form class="form" role="form">
                                            <div class="portlet box green" >
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        Image/Gallery <small style="color: #FFF" class="messagetitle345" > (Click on images to display as cover images) </small>
                                                    </div>
                                                    <div class="tools">
                                                        <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    </div> 
                                                </div>
                                                <div class="portlet-body open" style="display: block">
                                                    <div class="wd100">
                                                        <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                                                    </div>
                                                    <div class="row text-center text-lg-left images with-prob">
                                                        <ul id="sortable">
                                                            <?php
                                                            $igArray = App\Helpers\LibHelper::GetmypropertygalleryBy(!empty($propertyId) ? $propertyId : '0');
                                                            for ($i = 0; $i < count($igArray); $i++) {
                                                                $d = $igArray[$i];
                                                                $fileName = 'files/hostgallery/' . $d->file_name;
                                                                ?>
                                                                <li id="<?= $d->pgallery_id ?>" > 




                                                                    <a filepath="<?= Config::get('constants.HOME_DIR') . 'files/hostgallery/' . $d->file_name ?>" condjson='{"pgallery_id":"<?= $d->pgallery_id ?>"}' dbtable="my_property_gallery" style="position: absolute;color: #FF5722;" class="autodelete"><i class="fas fa-times-circle"></i></a>
                                                                    <a  data-toggle="tooltip" data-placement="top" cmessage="Are you sure want to make cover photo" title="Update cover photo" condjson='{"property_id":"<?= $propertyId ?>"}' updatejson='{"coverphoto":"<?= $d->file_name ?>"}' dbtable="my_property" href="javascript:void(0)" class="d-block mb-4 h-100 autoupdate">
                                                                        <img class="img-fluid img-thumbnail" src="<?= url($fileName) ?>" alt="">
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>


                                                    </div> 
                                                    <div class="row text-center text-lg-left">
                                                        <button type="button" onclick="POSITIONSET.imageProperyUpdatespos();" class="btn btn-primary">Save Position</button>
                                                        <button type="button" propertyId="<?= $propertyId ?>" onclick="deleteAllImages(this);" class="btn btn-danger">Delete All Images</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>




                        </div>




                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>


        <div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="hidden" id="lat" value="<?= !empty($pArray->lat) ? $pArray->lat : '' ?>">
                            <input type="hidden" id="lng" value="<?= !empty($pArray->lng) ? $pArray->lng : '' ?>">

                            <input type="text" id='store-location' value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>'  autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <div class="button-wrap wd100">
                            <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal">CONFIRM</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')
<style>
    .removeflatbtn{
        margin-top: 27px;  
    }
    #sortable li{

    }
    #sortable { list-style-type: none; margin: 0; padding: 0; width: 450px; }
    #sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; width: 100px;  text-align: center; }
</style>

<script src="{{url('/public')}}/assets/js/dropzone.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/js/form-dropzone.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/js/hostgalleryupload.js" type="text/javascript"></script>

<script src="{{url('/public')}}/assets/js/mapmodel_admin.js" ></script>   
<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
<script>
                                                            $('#ptype').change(function () {
                                                                var ptypeId = $('#ptype').val();

                                                                var form = new FormData();
                                                                form.append('_token', CSRF_TOKEN);
                                                                form.append('ptypeId', ptypeId);
                                                                form.append('encode', true);
                                                                form.append('helper', 'Lib');
                                                                form.append('function', 'GetpropertyTypeByptypeId');

//                                                                var data = {function: 'GetpropertyTypeByptypeId', encode: true, _token: CSRF_TOKEN, helper: 'Lib', ptypeId: ptypeId};
                                                                var json = ajaxpost(form, "/helper");
                                                                try {
                                                                    var json = jQuery.parseJSON(json);
                                                                    if (json.status == true) {
                                                                        try {

                                                                            if (json.status == true) {

                                                                                var html = '<option value="">--Select--</option>';
                                                                                var data = json.data;
                                                                                for (var i = 0; i < data.length; i++) {
                                                                                    var d = data[i];
                                                                                    html = html + '<option value="' + d.ptype_id + '">' + d.ptype_name + '</option>';
                                                                                }
                                                                                $('#property_type').html(html);
                                                                            }
                                                                        } catch (e) {

                                                                        }
                                                                    }
                                                                } catch (e) {
                                                                    alert(e);
                                                                }

                                                            });
                                                            function deleteAllImages(e) {
                                                                var propertyId = $(e).attr('propertyId');
//                                                                var data = {function: 'deleteAllImages', propertyId: propertyId, _token: CSRF_TOKEN, helper: 'Common'};

                                                                var form = new FormData();
                                                                form.append('_token', CSRF_TOKEN);
                                                                form.append('propertyId', propertyId);
                                                                form.append('encode', true);
                                                                form.append('helper', 'Common');
                                                                form.append('function', 'deleteAllImages');

                                                                $.confirm({
                                                                    title: 'Are you sure want to delete ?',
                                                                    content: false,
                                                                    type: 'green',
                                                                    typeAnimated: true,
                                                                    buttons: {
                                                                        confirm: {
                                                                            text: 'Submit',
                                                                            btnClass: 'btn-green',
                                                                            action: function () {
                                                                                var json = ajaxpost(form, "/helper");
                                                                                try {
                                                                                    var json = jQuery.parseJSON(json);
                                                                                    if (json.status == true) {
                                                                                        try {
                                                                                            if (json.status == true) {
                                                                                                window.location = '';
                                                                                            }
                                                                                        } catch (e) {

                                                                                        }
                                                                                    }
                                                                                } catch (e) {
                                                                                    alert(e);
                                                                                }

                                                                            }
                                                                        },
                                                                        cancel: {
                                                                            text: 'Cancel',
                                                                            btnClass: 'btn-warning',
                                                                            action: function () {
                                                                            }
                                                                        },
                                                                    }
                                                                });


                                                            }
                                                            $('.addhostproperty').click(function () {
                                                                var form = new FormData();
                                                                form.append('_token', CSRF_TOKEN);
                                                                form.append('json[property_title]', $('#property_title').val());
                                                                form.append('json[slugs]', $('#slugs').val());
                                                                form.append('json[position]', $('#position').val());
                                                                form.append('json[ptype]', $('#ptype').val());
                                                                form.append('json[property_type]', $('#property_type').val());
                                                                form.append('json[property_type_normal]', $('#property_type_normal').val());

                                                                form.append('json[tower_id]', $('#tower_id').val());
                                                                form.append('json[area_id]', $('#area_id').val());

                                                                form.append('json[address]', $('#address').val());
                                                                form.append('json[city]', $('#city').val());
                                                                form.append('json[price]', $('#price').val());
                                                                form.append('json[price_type]', $('#price_type').val());
                                                                form.append('json[downpayment]', $('#downpayment').val());
                                                                form.append('json[loanintrest]', $('#loanintrest').val());
                                                                form.append('json[period]', $('#period').val());
                                                                form.append('json[area]', $('#area').val());
                                                                form.append('json[bed]', $('#bed').val());
                                                                form.append('json[is_studio]', $('#is_studio').is(':checked') ? '1' : '0');
                                                                form.append('json[bathroom]', $('#bathroom').val());
                                                                form.append('json[owner_id]', $('#owner_id').val());

                                                                form.append('json[tag_id]', $('#tag_id').val());
                                                                form.append('json[location_embedded]', $('#autocomplete').val());
                                                                form.append('json[agent_phone]', $('#agent_phone').val());
                                                                form.append('json[pdf_description]', $('#pdf_description').val());





                                                                form.append('json[lat]', $('#lat').val());
                                                                form.append('json[lng]', $('#lng').val());
                                                                form.append('json[property_no]', $('#property_no').val());
                                                                form.append('json[property_no_1]', $('#property_no_1').val());
                                                                form.append('json[permit_number]', $('#permit_number').val());

                                                                form.append('json[description]', $('#description').val());


                                                                form.append('json[meta_title]', $('#meta_title').val());
                                                                form.append('json[meta_description]', $('#meta_description').val());
                                                                form.append('json[meta_keywords]', $('#meta_keywords').val());
                                                                if ($('#property_no_1').val() == '') {
                                                                    $('#property_no_1').css('border-color', 'red');
                                                                    $('#property_no_1').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#property_no_1').css('border-color', '');
                                                                }
                                                                if ($('#tower_id').val() == '') {
                                                                    $('#tower_id').css('border-color', 'red');
                                                                    $('#tower_id').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#tower_id').css('border-color', '');
                                                                }
                                                                if ($('#area_id').val() == '') {
                                                                    $('#area_id').css('border-color', 'red');
                                                                    $('#area_id').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#area_id').css('border-color', '');
                                                                }
                                                                if ($('#property_type').val() == '') {
                                                                    $('#property_type').css('border-color', 'red');
                                                                    $('#property_type').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#property_type').css('border-color', '');
                                                                }
                                                                if ($('#property_title').val() == '') {
                                                                    $('#property_title').css('border-color', 'red');
                                                                    $('#property_title').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#property_title').css('border-color', '');
                                                                }
                                                                if ($('#slugs').val() == '') {
                                                                    $('#slugs').css('border-color', 'red');
                                                                    $('#slugs').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#slugs').css('border-color', '');
                                                                }

                                                                if ($('#address').val() == '') {
                                                                    $('#address').css('border-color', 'red');
                                                                    $('#address').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#address').css('border-color', '');
                                                                }
                                                                if ($('#price').val() == '') {
                                                                    $('#price').css('border-color', 'red');
                                                                    $('#price').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#price').css('border-color', '');
                                                                }

                                                                if ($('#area').val() == '') {
                                                                    $('#area').css('border-color', 'red');
                                                                    $('#area').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#area').css('border-color', '');
                                                                }

                                                                if ($('#bathroom').val() == '') {
                                                                    $('#bathroom').css('border-color', 'red');
                                                                    $('#bathroom').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#bathroom').css('border-color', '');
                                                                }

                                                                if ($('#owner_id').val() == '') {
                                                                    $('#owner_id').css('border-color', 'red');
                                                                    $('#owner_id').focus();
                                                                    return false;
                                                                } else {
                                                                    $('#owner_id').css('border-color', '');
                                                                }




                                                                var facility = [];
                                                                $(".facilitycheckbox:checked").each(function () {
                                                                    facility.push($(this).val());
                                                                });
                                                                if (facility.length == false) {
                                                                    alertSimple('add features.');
                                                                    return false;
                                                                }
                                                                form.append('json[facility]', facility.join(','));
                                                                form.append('json[property_id]', $('#property_id').val());
                                                                form.append('json[status]', $('#status').val());




                                                                if (uploadedDropzone.length == false && $('#property_id').val() == false) {
                                                                    alertSimple('at least upload one images.');
                                                                    return false;
                                                                }


                                                                var fileInput = document.querySelector('#pdf_map_loc');
                                                                form.append('pdf_map_loc', fileInput.files[0]);


                                                                $.confirm({
                                                                    title: 'Are you sure want to submit ?',
                                                                    content: false,
                                                                    type: 'green',
                                                                    typeAnimated: true,
                                                                    buttons: {
                                                                        confirm: {
                                                                            text: 'Submit',
                                                                            btnClass: 'btn-green',
                                                                            action: function () {
                                                                                var json = ajaxpost(form, "/admin/addmyproperty");
                                                                                try {
                                                                                    var json = jQuery.parseJSON(json);
                                                                                    if (json.status == true) {
                                                                                        window.location = base_url + '/admin/myproperty';
                                                                                    }
                                                                                } catch (e) {
                                                                                    alert(e);
                                                                                }
                                                                            }
                                                                        },
                                                                        cancel: {
                                                                            text: 'Cancel',
                                                                            btnClass: 'btn-warning',
                                                                            action: function () {
                                                                            }
                                                                        },
                                                                    }
                                                                });
                                                            });

</script>