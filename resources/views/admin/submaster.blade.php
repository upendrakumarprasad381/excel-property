<!-- BEGIN CONTENT -->
@include('admin.includes.header')
<?php
$masterId=!empty($_REQUEST['masterId']) ? base64_decode($_REQUEST['masterId']) : '';
$mArray=  App\Helpers\LibHelper::GetmasterdetailsBy($masterId);
if(empty($mArray)){
    redirect(base_url());
}
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $mArray->master_name ?></span>
                            <a  href="<?= url('admin/addsubmaster?masterId='.  base64_encode($masterId)) ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>





                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>                                   
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qry = "SELECT * FROM `submaster_details` WHERE archive=0 AND master_id='$masterId' ORDER BY submaster_id DESC";
                                $Product =  \App\Database::select($qry);

                                for ($i = 0; $i < count($Product); $i++) {
                                    $d = $Product[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= ucwords($d->submaster_name) ?></td>
                                       

                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <!--<a  href="<= base_url('admin/addsubmaster?submasterId=' . base64_encode($d->submaster_id)); ?>"><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>-->
                                            <a href="javascript:void(0)" updatejson='{"archive":"1"}'  condjson='{"submaster_id":"<?= $d->submaster_id ?>"}' dbtable="submaster_details" class="autoupdate" title="Remove"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')