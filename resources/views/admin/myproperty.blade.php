@include('admin.includes.header')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">My Property</span>

                            <a href="<?= url('admin/addmyproperty') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">


                        <div class="panel panel-default">
                            <div class="panel-heading" >
                                <h4 class="panel-title">
                                    <div class="__filterTilhd" data-toggle="collapse" href="#PAGE_17" id="PAGE_17_HEADING">
                                        <a >
                                            <span class="glyphicon glyphicon-collapse-down"> </span> Filter
                                        </a>
                                    </div>
                                </h4>
                            </div>
                            <div id="PAGE_17" class="panel-collapse collapse ">
                                <div class="panel-body " style="padding: 15px;">
                                    <form method="post">
                                        @csrf
                                        <div class="row"> 

                                            <div class="col-sm-3">
                                                <label>Type</label>
                                                <select class="form-control" name="ptype" id="ptype">
                                                    <?php
                                                    $ptype = isset($_POST['ptype']) ? $_POST['ptype'] : '';
                                                    ?>
                                                    <option  value="">All</option>
                                                    <option <?= $ptype == '0' ? 'selected' : '' ?> value="0">BUY</option>
                                                    <option <?= $ptype == '1' ? 'selected' : '' ?> value="1">RENT</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Ref No</label>
                                                <input type="text" class="form-control" value="<?= !empty($_POST['property_no']) ? $_POST['property_no'] : '' ?>" name="property_no" id="property_no">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Tower</label>
                                                <select class="form-control" name="tower_id" id="tower_id">
                                                    <option value="">--Select--</option>
                                                    <?php
                                                    $tower_id = isset($_POST['tower_id']) ? $_POST['tower_id'] : '';
                                                    $listd = App\Helpers\LibHelper::GetTowerlist($ptype);
                                                    for ($i = 0; $i < count($listd); $i++) {
                                                        $d = $listd[$i];
                                                        ?> <option <?= $tower_id == $d->tower_id ? 'selected' : '' ?> value="<?= $d->tower_id ?>"><?= $d->tower_name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Area Name </label>
                                                <select value="" id="area_id" name="area_id" class="form-control" >
                                                    <option value="">--select--</option>
                                                    <?php
                                                    $area_id = isset($_POST['area_id']) ? $_POST['area_id'] : '';
                                                    $Sql = "SELECT * FROM `area_list` WHERE archive=0";
                                                    $dlist = \App\Database::select($Sql);
                                                    for ($i = 0; $i < count($dlist); $i++) {
                                                        $d = $dlist[$i];
                                                        ?><option <?= $area_id == $d->area_id ? 'selected' : '' ?> value="<?= $d->area_id ?>"><?= $d->area_name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>Bed</label>
                                                <input type="text" class="form-control" value="<?= !empty($_POST['bed']) ? $_POST['bed'] : '' ?>" name="bed" id="bed">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Sq Fit</label>
                                                <input type="text" class="form-control" value="<?= !empty($_POST['area']) ? $_POST['area'] : '' ?>" name="area" id="area">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Price</label>
                                                <input type="text" class="form-control" value="<?= !empty($_POST['price']) ? $_POST['price'] : '' ?>" name="price" id="price">
                                            </div>


                                            <div class="col-sm-3">
                                                <label>Agent Name </label>
                                                <select value="" id="owner_id" name="owner_id" class="form-control" >
                                                    <option value="">--select--</option>
                                                    <?php
                                                    $owner_id = isset($_POST['owner_id']) ? $_POST['owner_id'] : '';
                                                    $Sql = "SELECT * FROM `agents` WHERE archive=0";
                                                    $dlist = \App\Database::select($Sql);
                                                    for ($i = 0; $i < count($dlist); $i++) {
                                                        $d = $dlist[$i];
                                                        ?><option <?= $owner_id == $d->agents_id ? 'selected' : '' ?> value="<?= $d->agents_id ?>"><?= $d->name ?></option><?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Status</label>
                                                <select class="form-control" name="archive" id="archive">
                                                    <?php
                                                    $archive = isset($_POST['archive']) ? $_POST['archive'] : '';
                                                    ?>
                                                    <option  value="">All</option>
                                                    <option <?= $archive == '0' ? 'selected' : '' ?> value="0">Active</option>
                                                    <option <?= $archive == '1' ? 'selected' : '' ?> value="1">Inactive</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-3">
                                                <label>&nbsp;</label>
                                                <br>
                                                <button id="submitBtnname" type="submit" name="searchReport" class="btn btn-success">Submit</button>
                                                <button type="reset" name="reset" id="reset_form" class="btn btn-danger">Reset</button>
                                            </div>


                                        </div>
                                        <br>
                                    </form>


                                </div>
                            </div>
                        </div>        

                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Type</th>
                                    <th>Ref No</th>

                                    <th>Tower </th>
                                    <th>Area </th>

                                    <th>Bed</th>
                                    <th>Sqft</th>
                                    <th>Price</th>
                                    <th>Agent</th>



                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cond = '';

                                if (isset($_POST['searchReport'])) {

                                    if (isset($_POST['ptype']) && $_POST['ptype'] != '') {
                                        $cond = $cond . " AND HP.ptype='" . $_POST['ptype'] . "'";
                                    }
                                    if (isset($_POST['archive']) && $_POST['archive'] != '') {
                                        $cond = $cond . " AND HP.archive='" . $_POST['archive'] . "'";
                                    }
                                    if (!empty($_POST['property_no'])) {
                                        $cond = $cond . " AND HP.property_no='" . $_POST['property_no'] . "'";
                                    }
                                    if (!empty($_POST['tower_id'])) {
                                        $cond = $cond . " AND HP.tower_id='" . $_POST['tower_id'] . "'";
                                    }
                                    if (!empty($_POST['area_id'])) {
                                        $cond = $cond . " AND HP.area_id='" . $_POST['area_id'] . "'";
                                    }
                                    if (!empty($_POST['bed'])) {
                                        $cond = $cond . " AND HP.bed='" . $_POST['bed'] . "'";
                                    }
                                    if (!empty($_POST['area'])) {
                                        $cond = $cond . " AND HP.area='" . $_POST['area'] . "'";
                                    }
                                    if (!empty($_POST['price'])) {
                                        $cond = $cond . " AND HP.price='" . $_POST['price'] . "'";
                                    }
                                    if (!empty($_POST['owner_id'])) {
                                        $cond = $cond . " AND HP.owner_id='" . $_POST['owner_id'] . "'";
                                    }
                                  
                                }
                                $option = '';
                                $select = ",TL.tower_name, A.name AS agent_name,AL.area_name";
                                $join = " LEFT JOIN tower_list TL ON TL.tower_id=HP.tower_id LEFT JOIN agents A ON A.agents_id=HP.owner_id LEFT JOIN area_list AL ON AL.area_id=HP.area_id";
                                $Sql = "SELECT HP.* $select FROM `my_property` HP $join WHERE HP.archive=0 $cond ORDER BY HP.position ASC";
                                $pArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($pArray); $i++) {
                                    $d = $pArray[$i];

                                    $option = $option . ' <li id="' . $d->property_id . '" >' . $d->property_title . ' </li>';
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= empty($d->ptype) ? 'BUY' : 'RENT' ?></td>
                                        <td><?= $d->property_no ?></td>

                                        <td><?= $d->tower_name ?></td>
                                        <td><?= $d->area_name ?></td>

                                        <td><?= $d->bed ?></td>
                                        <td><?= \App\Helpers\CommonHelper::DecimalAmount($d->area) ?></td>
                                        <td><?= \App\Helpers\CommonHelper::DecimalAmount($d->price) ?></td>
                                        <td><?= $d->agent_name ?></td>



                                        <td><?= empty($d->archive) ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>'; ?></td>

                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a data-toggle="tooltip"  data-placement="top" title="Edit" href="<?= url('admin/addmyproperty?propertyId=' . base64_encode($d->property_id)); ?>" ><span class="label label-sm label-info"><i class="fas fa-edit"></i></span></a> 
                                            <a data-toggle="tooltip" data-placement="top" title="Remove" title="Remove" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"property_id":"<?= $d->property_id ?>"}' dbtable="my_property" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>


                                        </td>
                                    </tr>  
    <?php
}
?>
                            </tbody>
                        </table>



                        <div class="row">

                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
<?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="POSITIONSET.mypropertyUpdatespos();" type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')