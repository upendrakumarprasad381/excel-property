@include('admin.includes.header')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Careers Inquiry Details</span>

                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email Id</th>
                                    <th>Phone No</th>
                                    <th>Attachment</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                    <th style="width: 12%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $join = "";
                                $select = "";
                                $qry = "SELECT CD.* $select FROM `careers_enquiry` CD WHERE archive=0 ORDER BY CD.careers_enq_id DESC";
                                $dArray = \App\Database::select($qry);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?= $d->mobile_no ?></td>
                                        <td>
                                            <?php
                                            $file = "files/careers-cv/" . (!empty($d->cv_attached) ? $d->cv_attached : '');
                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                            }
                                            ?>
                                        </td>
                                        <td><?= $d->messages ?></td>
                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">

                                            <a  title="Remove"  href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"careers_enq_id":"<?= $d->careers_enq_id ?>"}' dbtable="careers_enquiry" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')