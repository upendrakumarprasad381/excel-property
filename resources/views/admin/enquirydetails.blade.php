@include('admin.includes.header')
<?php
$enqId = !empty($_REQUEST['enqId']) ? base64_decode($_REQUEST['enqId']) : '';
$eArray = App\Helpers\LibHelper::GetenquirymanagementByenqId($enqId);
if (empty($eArray)) {
    header("location: " . url('/'));
    exit;
}
$pArray = App\Helpers\LibHelper::GetmypropertyBy($eArray->property_id);
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Inquiry Details - <?= $eArray->enq_number ?></span>


                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fab fa-first-order"></i>  Property  Details 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Property Name: </div>
                                            <div class="col-md-7 value"> 
                                                <?= $pArray->property_title ?>                                     
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Address: </div>
                                            <div class="col-md-7 value">  <?= $pArray->address ?>    </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> City: </div>
                                            <div class="col-md-7 value">
                                                <?= $pArray->city ?>                                             </div>
                                        </div>
<?php
$owner = \App\Helpers\LibHelper::GetagentsByagentsId($pArray->owner_id);
?>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Owner Name: </div>
                                            <div class="col-md-7 value"> <?= !empty($owner->name) ? $owner->name : '' ?>   </div>
                                        </div>
                                       
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Enq No: </div>
                                            <div class="col-md-7 value"> <?= $eArray->enq_number ?> </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Name: </div>
                                            <div class="col-md-7 value"> <?= $eArray->name ?>   </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Email: </div>
                                            <div class="col-md-7 value"> <?= $eArray->email ?>   </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Mobile: </div>
                                            <div class="col-md-7 value"> <?= $eArray->mobile_no ?>   </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Messages: </div>
                                            <div class="col-md-7 value"> <?= $eArray->messages ?>   </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


@include('admin.includes.footer')