@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Property Price</span>
                            <a href="<?= url('admin/addpropertyprice') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Type</th>
                                    <th>Price</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $select = "";
                                $join = "  ";
                                $Sql = "SELECT SC.* $select FROM `property_price` SC $join WHERE SC.archive=0 ORDER BY SC.ptype ASC,SC.price ASC";
                                $dArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= empty($d->ptype) ? 'BUY' : 'RENT' ?></td>
                                        <td><?= $d->price ?> AED</td>

                                        <td><?= date('d/m/Y h:i A', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Edit" href="<?= url('admin/addpropertyprice?priceId=' . base64_encode($d->price_id)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
                                            <a title="Edit" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"price_id":"<?= $d->price_id ?>"}' dbtable="property_price" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')
