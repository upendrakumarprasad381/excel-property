<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Excel Property</title>
        <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
            <style>
                body, td, input, textarea, select {
                    font-family: "Open Sans", sans-serif;
                }
                p{
                    margin-bottom: -9px;
                }
            </style>

    </head>
    <body>

        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border: solid 1px #c5c5c5;border-top: 3px solid #4cc2c0;">
            <tr>
                <td style="border-bottom:solid 2px #4cc2c0;">
                    <table width="94%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:13px; color:#333; line-height:18px;">
                        <tr>
                            <td width="42%" align="left" valign="top" style="padding:15px 0px;">
                                <img src="<?= url('public/images/admin-logo_s.png') ?>" alt="rahma" style="width:150px; padding-top:10px;" /></td>
                            <td width="58%" align="right" valign="middle" style="padding:15px 0px;">
                                <b>Email: &nbsp;</b><a style="margin: 0px;" href="mailto:info@excelproperties.ae">info@excelproperties.ae</a><br/>
                                <b>Phone:&nbsp;</b><a align="left" href="tel:+97143364447" target="_blank">+97143364447</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding-top:5px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table width="94%" border="0" align="center" cellpadding="0" cellspacing="1" style="  font-size:13px; color:#000;">
                        <tr>
                            <td width="100%" style=" padding:5px;line-height:1.8">
                                <?= $emailbody ?><br>

                            </td>

                        </tr>
                    </table>
                </td>
            </tr>            		
            <tr>
                <td style="font-size:13px; color:#000;padding:5px;font-weight: 600;"><span style="margin-left: 14px;">Regards,</span></td>
            </tr>
            <tr>
                <td style="font-size:13px; color:#000;padding:5px;font-weight: 600;"><span style="margin-left: 14px;">Excel Property</span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="padding:5px 0; text-align:center; background:#4cc2c0; color:#fff; font-size:11px; "> Copyright &copy; Excel Property  <?= date('Y') ?> All rights reserved.</td>
            </tr>
        </table>

    </body>
</html>