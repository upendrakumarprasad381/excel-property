<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Inquiry</title>
    </head>
    <style>
        body{
            padding:0 auto;
            margin:0 auto;
        }

    </style>

    <body>



        <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>

                <td align="center" valign="middle" style="border-bottom:#000 5px solid; padding:15px;"><img width="250" src="https://demo.softwarecompany.ae/excel_properties/email_template/img/logo.png"  /></td>

            </tr>
            <tr>
                <td style="padding:20px 35px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:16px;
                    line-height:25px;
                    color:#000;
                    text-align:center;
                    ">
                    <strong>Dear {{$name}}</strong><br />

                    <?= $normal_msg ?></td>

            </tr>
            <tr>
                <td align="center" valign="top" style="  font-family:Arial, Helvetica, sans-serif; 
                    font-size:14px;
                    color:#000;
                    padding:0 0px;

                    "><img width="800" src="{{$enq_logo}}"  /></td>
            </tr>
            <tr>
                <td align="center" style="padding:10px 10px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:15px;
                    color:#000;
                    line-height:24px;
                    ">
                    <h4 style="font-size:17px; margin:1px 0px;padding:2px 0px; text-align:center;">Are You Looking For The Property Right Now?</h4>

                    Please Contact Our Experts Immediately <br />

                    Please call <a style="color:#000; text-decoration:none" href="tel:+971506606100">+971506606100</a> or send an email to <a  style="color:#000; text-decoration:none" href="mailto:inquiry@excelproperties.ae">Inquiry@excelproperties.ae</a><br />
                    We Are Available For You 24*7<br />


                </td>
            </tr>
            <tr>
                <td align="center" valign="middle" style="padding:10px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:14px;
                    color:#000;
                    ">
                        <?php if (empty($is_file)) { ?>
                        <a href="tel:+971506606100" style="
                           background:#000; padding:20px 100px; 
                           font-family:Arial, Helvetica, sans-serif;
                           font-size:18px;
                           color:#fff;
                           text-decoration:none;
                           display:inline-block;
                           ">Call Our Expert </a>
                           <?php
                       } else {
                           if (empty($emailAttachment)) {
                               ?><a href="tel:+971506606100" style="
                               background:#000; padding:20px 100px; 
                               font-family:Arial, Helvetica, sans-serif;
                               font-size:18px;
                               color:#fff;
                               text-decoration:none;
                               display:inline-block;
                               ">Document Not Found </a><?php
                           } else {
                               ?><a href="<?= $emailAttachment ?>" style="background:#000; padding:20px 100px; 
                           font-family:Arial, Helvetica, sans-serif;
                           font-size:18px;
                           color:#fff;
                           text-decoration:none;
                           display:inline-block;"><?= $downloadBtnName ?></a><?php
                        }
                    }
                    ?>

                </td>
            </tr>
            <tr>
                <td style="padding:20px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:14px;
                    color:#000;
                    text-align:center;
                    ">We work with passion towards crafting the best customer service experience for you</td>
            </tr>
            <tr>
                <td style="padding:0px 20px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:14px;
                    color:#000;
                    line-height:22px;
                    text-align:center;
                    ">T: <a style="color:#000; text-decoration:none" href="tel:+9713364447">+971 (4) 336 4447</a><br />
                    Email: <a style="color:#000; text-decoration:none"  href="mailto:info@excelproperties.ae">info@excelproperties.ae</a><br />
                    Head-Office: Burj Al Salam Tower, Office No. 1804, Trade Center First, Shk. Zayed Road. Dubai, UAE</td>
            </tr>

            <tr>
                <td style="padding:5px 20px 50px; font-family:Arial, Helvetica, sans-serif; 
                    font-size:14px;
                    color:#111;
                    text-align:center;
                    line-height:22px;
                    ">
                    Access  our Latest Online Channels for the latest news in Dubai luxury real estate.<br />

                    <table   border="0" align="center" cellpadding="0" cellspacing="5">
                        <tr>
                            <td><a href="https://www.facebook.com/ExcelProperties1" target="_blank"><img src="https://demo.softwarecompany.ae/excel_properties/email_template/img/facebook.png" /></a></td>
                            <td><a href="https://twitter.com/PropertiesExcel" target="_blank"><img src="https://demo.softwarecompany.ae/excel_properties/email_template/img/twitter.png" /></a></td>
                            <td><a href="https://www.youtube.com/channel/UCMJwg4ZzwspdObYR-uSRypA" target="_blank"><img src="https://demo.softwarecompany.ae/excel_properties/email_template/img/youtube.png" /></a></td>
                            <td><a href="https://www.instagram.com/excel_properties/" target="_blank"><img src="https://demo.softwarecompany.ae/excel_properties/email_template/img/instagram.png" /></a></td>
                        </tr>
                    </table>

                    <a style="color:#000; text-decoration:none" href="https://excelproperties.ae/" target="_blank">www.Excelproperties.ae</a>
                </td>
            </tr>
        </table>


    </body>
</html>
