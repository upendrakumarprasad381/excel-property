<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login Excel Properties</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{url('/public/')}}images/favicon.ico" >
        <link rel="stylesheet" type="text/css" href="{{url('/public')}}/assets/AdminLogin/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/vendor/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/vendor/css-hamburgers/hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/vendor/animsition/css/animsition.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/vendor/select2/select2.min.css">
        <!--<link rel="stylesheet" type="text/css" href="{{url('/public/')}}/vendor/daterangepicker/daterangepicker.css">-->
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/css/util.css">
        <link rel="stylesheet" type="text/css" href="{{url('/public/')}}/assets/AdminLogin/css/main.css">
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100" style="background-image: url('<?= url('public/images/bg-banner.jpg') ?>');">
                <div class="wrap-login100 p-l-30 p-r-30 p-t-35 p-b-35" id="vendorlogin">
                    <form class="login100-form validate-form" onsubmit="return false;">
                        <span class="login100-form-title p-b-49" >
                            Excel Property
                        </span>

                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is reauired">
                            <span class="label-input100">Username</span>
                            <input class="input100" type="text" name="username" id="user_name" placeholder="Type your username">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="password" name="pass" id="password" placeholder="Type your password">
                            <span class="focus-input100" data-symbol="&#xf190;"></span>
                        </div>

                        <div style="font-weight: 600;margin-top: 20px;text-align: center;" id="loginresult">

                        </div>

<!--                        <div class="text-right p-t-8 p-b-31">
                            <a href="javascript:void(0)">
                                Forgotten password?
                            </a>
                        </div>-->

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Login
                                </button>
                            </div>
                        </div>
<!--                        <div class="txt1 text-center p-t-54 p-b-20">
                            <span>
                                <a id="donthaveaccount" href="javascript:void(0)"> Don't have an account ? Sign up here</a>
                            </span>
                        </div>-->
                    </form>
                </div>


                <div class="wrap-login100 p-l-30 p-r-30 p-t-35 p-b-35" id="vendorregistration" style="display: none;">
                    <form class="login100-form validate-formreg" onsubmit="return false;">
                        <span class="login100-form-title p-b-49" >
                            Retreatmi Register
                        </span>

                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Name is reauired">
                            <span class="label-input100">Name</span>
                            <input class="input100" type="text" name="username" id="regname" placeholder="Type your name" autocomplete="off">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>
                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Email is reauired">
                            <span class="label-input100">Email</span>
                            <input class="input100" type="text" name="username" id="regemail" placeholder="Type your email id" autocomplete="off">
                            <span class="focus-input100" data-symbol="&#x2709;"></span>
                        </div>










                        <div class="wrap-input100 validate-input m-b-23" data-validate = "Phone is reauired">
                            <span class="label-input100">Phone</span>
                            <div class="input-group-prepend" style="position: absolute;margin-top: 16px;margin-left: 42px;">
                                <div class="input-group-text phone_list_cory">
                                    <select class="selectpicker" id="regphonecode">
                                      

                                    </select>
                                </div>
                            </div>
                            <input class="input100" style="padding-left: 120px;" type="number" name="regphone" id="regphone" placeholder="Type your mobile" autocomplete="off">
                            <span class="focus-input100" data-symbol="&#x260E;"></span>
                        </div>

                        <div class="row  p-l-15 p-r-15  ">
                            <div class="wrap-input100 wrap_input50 validate-input" data-validate="Password is required">
                                <span class="label-input100">Password</span>
                                <input class="input100" type="password" name="pass" id="regpassword" placeholder="Type your password" autocomplete="off">
                                <span class="focus-input100" data-symbol="&#xf190;"></span>
                            </div>

                            <div class="wrap-input100 wrap_input50 validate-input"  data-validate="Password is required">
                                <span class="label-input100">Confirm password</span>
                                <input class="input100" type="password" name="pass" id="regcpassword" placeholder="Type your password" autocomplete="off">
                                <span class="focus-input100" data-symbol="&#xf190;"></span>
                            </div>
                        </div>
                        <div class="wrap-input100 validate-input" style="margin-top: 15px;" data-validate="ID Or Passport is required">
                            <span class="label-input100">ID Or Passport Upload</span>
                            <input id="idorpassport" onchange="encodeidorpassport(this);" style="padding: 14px;margin-left: 31px;font-size: 13px;margin-top: 3px;    color: gray;" class="input100" type="file" name="" id="" placeholder="Upload file" autocomplete="off">
                            <span class="focus-input100" data-symbol="&#x2709;"></span>
                        </div>
                        <otpdiv>
                        </otpdiv>
                        <div style="font-weight: 600;margin-top: 20px;text-align: center;" id="registerresult">

                        </div>
                        <div style="  margin: 15px 0 20px;  margin-left: 23px " >         <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1"style=" padding-left: 0;"><a href="" target="_blank">Terms and conditions</a></label>
                            </div>
                        </div>
                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Register
                                </button>
                            </div>
                        </div>
                        <div class="txt1 text-center p-t-54 p-b-20">
                            <span>
                                <a id="haveanacoount" href="javascript:void(0)"> Already have an acoount ?</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="dropDownSelect1"></div>
        <script>
            var base_url = "{{url('/')}}";
           var CSRF_TOKEN = "{{ csrf_token() }}";
                </script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/animsition/js/animsition.min.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/bootstrap/js/popper.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/select2/select2.min.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/daterangepicker/moment.min.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/daterangepicker/daterangepicker.js"></script>
        <script src="{{url('/public/')}}/assets/AdminLogin/vendor/countdowntime/countdowntime.js"></script>
        <script src="{{url('/public/')}}/assets/js/admin-login.js"></script> 



        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>


    </body>
</html>