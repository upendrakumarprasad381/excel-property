@include('admin.includes.header')
<?php
$bannerId = !empty($_REQUEST['bannerId']) ? base64_decode($_REQUEST['bannerId']) : '';
$pArray = \App\Helpers\LibHelper::GetbannermanagementBybannerId($bannerId);
//print_r($pArray);
//exit;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Careers CMS Update</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">

                                                <div class="row">

                                                    <?php
                                                    if (isset($_POST['careerscms'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CAREERS_CMS');

                                                        $file = !empty($_FILES['col6']) ? $_FILES['col6'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col6) ? $pArray->col6 : '');
                                                                $json['col6'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }

                                                        $file = !empty($_FILES['col7']) ? $_FILES['col7'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                                                                $json['col7'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }

                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'CAREERS_CMS'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CAREERS_CMS');
                                                    ?>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Title 1 </label>
                                                            <input class="form-control" name="json[col1]" type="text" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>" required id="col1" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Title 2 </label>
                                                            <input class="form-control" name="json[col2]" type="text" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>" required id="col2" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description 1</label>
                                                            <textarea id="col3" name="json[col3]" class="form-control summernote" placeholder="" required><?= !empty($pArray->col3) ? $pArray->col3 : '' ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description 2 </label>
                                                            <textarea id="col4" name="json[col4]" class="form-control summernote" placeholder="" required><?= !empty($pArray->col4) ? $pArray->col4 : '' ?></textarea>                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description 3</label>
                                                            <textarea id="col5" name="json[col5]" class="form-control summernote" placeholder="" required><?= !empty($pArray->col5) ? $pArray->col5 : '' ?></textarea>                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner First
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->col6) ? $pArray->col6 : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 1920 * 600 px</span>
                                                            </label>
                                                            <input type="file" name="col6" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner Second
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 1200 * 503 px</span>
                                                            </label>
                                                            <input type="file" name="col7" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <br>
                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addteam" class="addteam"><button type="submit" name="careerscms" class="btn blue">Save</button></a>

                                                <a href=""> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 " style="    margin-top: 89px;" >


                            </div>

                        </div>

                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
@include('admin.includes.footer')


