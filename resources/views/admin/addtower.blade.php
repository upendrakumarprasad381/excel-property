@include('admin.includes.header')
<?php
$towerId = !empty($_REQUEST['towerId']) ? base64_decode($_REQUEST['towerId']) : '';
$aArray = \App\Helpers\LibHelper::GettowerlistBy($towerId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->tower_id) ? 'Add New' : 'Update'; ?> Tower</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="tower_id" value="<?= !empty($aArray->tower_id) ? $aArray->tower_id : '' ?>">
                            <div class="form-body">


                                <div class="form-group">
                                    <label class="control-label col-md-3">Tower Name</label>
                                    <div class="col-md-4">
                                        <input id="tower_name" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= isset($aArray->tower_name) ? $aArray->tower_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Slugs</label>
                                    <div class="col-md-4">
                                        <input id="slugs" value="<?= isset($aArray->slugs) ? $aArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description For Buy</label>
                                    <div class="col-md-6">
                                        <textarea id="description_buy" class="form-control summernote" autocomplete="off"><?= isset($aArray->description_buy) ? $aArray->description_buy : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description For Rent</label>
                                    <div class="col-md-6">
                                        <textarea id="description_rent" class="form-control summernote" autocomplete="off"><?= isset($aArray->description_rent) ? $aArray->description_rent : '' ?></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addtower" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addtower').click(function () {
        var form = new FormData();

        if ($('#tower_name').val() == '') {
            $('#tower_name').focus();
            $('#tower_name').css('border-color', 'red');
            return false;
        } else {
            $('#tower_name').css('border-color', '');
            form.append('json[tower_name]', $('#tower_name').val());
        }
        if ($('#slugs').val() == '') {
            $('#slugs').focus();
            $('#slugs').css('border-color', 'red');
            return false;
        } else {
            $('#slugs').css('border-color', '');
            form.append('json[slugs]', $('#slugs').val());
        }

  form.append('json[description_buy]', $('#description_buy').val());
          form.append('json[description_rent]', $('#description_rent').val());
          
        form.append('json[tower_id]', $('#tower_id').val());


        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addtower");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/tower';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
