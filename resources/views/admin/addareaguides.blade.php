@include('admin.includes.header')
<?php
$guidesId = !empty($_REQUEST['guidesId']) ? base64_decode($_REQUEST['guidesId']) : '';
$pArray = \App\Helpers\LibHelper::GetareaguidesByguidesId($guidesId);
//print_r($pArray);
//exit;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Area Guides <?= !empty($pArray->ndid) ? 'Updates' : 'Add' ?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->guides_id) ? $pArray->guides_id : '' ?>" id="guides_id">
                                                <div class="row">


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Area Name <span style="color:red">*</span></label>
                                                            <input class="form-control" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= !empty($pArray->area_name) ? $pArray->area_name : '' ?>" id="area_name" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Area Slug <span style="color:red">*</span></label>
                                                            <input class="form-control slugsvalidate"  value="<?= !empty($pArray->slugs) ? $pArray->slugs : '' ?>" id="slugs" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner Title <span style="color:red">*</span></label>
                                                            <input class="form-control" type="text" value="<?= !empty($pArray->banner_title) ? $pArray->banner_title : 'About Dubai Hills Estate' ?>" id="banner_title" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->banner_logo) ? $pArray->banner_logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 1920 * 990 px</span>
                                                            </label>
                                                            <input type="file" id="banner_logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Position<span style="color:red">*</span></label>
                                                            <input class="form-control" type="number" value="<?= !empty($pArray->position) ? $pArray->position : '' ?>" id="position" disabled="" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description<span style="color:red">*</span></label>
                                                            <textarea value="" id="description" class="form-control summernote" placeholder="n" autocomplete="off"><?= !empty($pArray->description) ? $pArray->description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.' ?></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Featured image 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 416 * 460 px</span>
                                                            </label>
                                                            <input type="file" id="logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Tag <span style="color:red">*</span></label>
                                                            <select class="form-control selectpicker" data-live-search="true" multiple id="tag_id">
                                                                <?php
                                                                $tagId = !empty($pArray->tag_id) ? explode(',', $pArray->tag_id) : [];
                                                                $tagId = !empty($tagId) && is_array($tagId) ? $tagId : [];
                                                                $Sql = "SELECT * FROM `tags` WHERE data_list_from='AREA_GUIDES' ORDER BY position ASC";
                                                                $dlist = \App\Database::select($Sql);
                                                                for ($i = 0; $i < count($dlist); $i++) {
                                                                    $d = $dlist[$i];
                                                                    ?><option <?= in_array($d->tag_id, $tagId) ? 'selected' : '' ?> value="<?= $d->tag_id ?>"><?= $d->tag_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>







                                                    <br>




                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addareaguides" class="addareaguides"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/newdevelopments') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>



                            <div class="col-md-6 " style="    margin-top: 89px;" >

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Video Embedded <a href="https://www.youtube.com/" target="_blank">You Tube</a></label>
                                        <input type="text" value='<?= !empty($pArray->video_url) ? $pArray->video_url : "" ?>' id="video_url" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Location Embedded 
                                            <!--<a href="https://www.google.com/maps" target="_blank">Google Map</a>-->
                                        </label>
                                        <input type="text" readonly value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>' id="autocomplete" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label>Meta Title</label>
                                        <input type="text" value="<?= !empty($pArray->meta_title) ? $pArray->meta_title : '' ?>" id="meta_title" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <input type="text" value="<?= !empty($pArray->meta_description) ? $pArray->meta_description : '' ?>" id="meta_description" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keywords</label>
                                        <input type="text" value="<?= !empty($pArray->meta_keywords) ? $pArray->meta_keywords : '' ?>" id="meta_keywords" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>





                            </div>

                        </div>




                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>



        <div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="hidden" id="lat" value="<?= !empty($pArray->lat) ? $pArray->lat : '' ?>">
                            <input type="hidden" id="lng" value="<?= !empty($pArray->lng) ? $pArray->lng : '' ?>">

                            <input type="text" id='store-location' value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>'  autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <div class="button-wrap wd100">
                            <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal">CONFIRM</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
@include('admin.includes.footer')
<script src="{{url('/public')}}/assets/js/mapmodel_admin.js" ></script>   
<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>

<script>
                                                                $('#addareaguides').click(function () {
                                                                    var form = new FormData();

                                                                    if ($('#area_name').val() == '') {
                                                                        $('#area_name').focus();
                                                                        $('#area_name').css('border-color', 'red');
                                                                        return false;
                                                                    } else {
                                                                        $('#area_name').css('border-color', '');
                                                                        form.append('json[area_name]', $('#area_name').val());
                                                                    }
                                                                    if ($('#slugs').val() == '') {
                                                                        $('#slugs').focus();
                                                                        $('#slugs').css('border-color', 'red');
                                                                        return false;
                                                                    } else {
                                                                        $('#slugs').css('border-color', '');
                                                                        form.append('json[slugs]', $('#slugs').val());
                                                                    }

                                                                    if ($('#banner_title').val() == '') {
                                                                        $('#banner_title').focus();
                                                                        $('#banner_title').css('border-color', 'red');
                                                                        return false;
                                                                    } else {
                                                                        $('#banner_title').css('border-color', '');
                                                                        form.append('json[banner_title]', $('#banner_title').val());
                                                                    }
                                                                    form.append('json[position]', $('#position').val());
                                                                    form.append('json[description]', $('#description').val());

                                                                    form.append('json[video_url]', $('#video_url').val());
                                                                    form.append('json[location_embedded]', $('#autocomplete').val());
                                                                    form.append('json[lat]', $('#lat').val());
                                                                    form.append('json[lng]', $('#lng').val());
                                                                    form.append('json[tag_id]', $('#tag_id').val());
                                                                    form.append('json[meta_title]', $('#meta_title').val());
                                                                    form.append('json[meta_description]', $('#meta_description').val());
                                                                    form.append('json[meta_keywords]', $('#meta_keywords').val());
                                                                    form.append('json[guides_id]', $('#guides_id').val());
                                                                    form.append('_token', CSRF_TOKEN);

                                                                    var fileInput = document.querySelector('#logo');
                                                                    form.append('logo', fileInput.files[0]);

                                                                    var fileInput = document.querySelector('#banner_logo');
                                                                    form.append('banner_logo', fileInput.files[0]);


                                                                    $.confirm({
                                                                        title: 'Are you sure want to submit ?',
                                                                        content: false,
                                                                        type: 'green',
                                                                        typeAnimated: true,
                                                                        buttons: {
                                                                            confirm: {
                                                                                text: 'Submit',
                                                                                btnClass: 'btn-green',
                                                                                action: function () {
                                                                                    var json = ajaxpost(form, "/admin/addareaguides");
                                                                                    try {
                                                                                        var json = jQuery.parseJSON(json);
                                                                                        if (json.status == true) {
                                                                                            window.location = base_url + '/admin/areaguides';
                                                                                        }
                                                                                    } catch (e) {
                                                                                        alert(e);
                                                                                    }
                                                                                }
                                                                            },
                                                                            cancel: {
                                                                                text: 'Cancel',
                                                                                btnClass: 'btn-warning',
                                                                                action: function () {
                                                                                }
                                                                            },
                                                                        }
                                                                    });
                                                                });
</script>
