@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">CMS</span>

                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Contact Us</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['ContactUs'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];



                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'CONTACT_US'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Banner Title <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>" id="col1" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <textarea name="json[col2]"  id="col2" required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col2) ? $pArray->col2 : '' ?></textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Tel. No.<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>" id="col4" required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col5]" value="<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>" id="col5" required class="form-control"  autocomplete="off">
                                                    </div>



                                                    <div class="form-group">
                                                        <label>Map Location</label>

                                                        <input type="hidden" id="contct_us_col8" name="json[col8]" value="<?= $pArray->col8 ?>">
                                                        <input type="hidden" id="contct_us_col9" name="json[col9]" value="<?= $pArray->col9 ?>">
                                                        <input type="text" name="json[col6]" lat="contct_us_col8" lng="contct_us_col9" readonly value='<?= !empty($pArray->col6) ? $pArray->col6 : "" ?>' id="contct_us_location" required class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label>Location <span style="color:red">*</span></label>
                                                        <input type="text" name="json[col10]" value="<?= !empty($pArray->col10) ? $pArray->col10 : '' ?>" required class="form-control"  autocomplete="off">
                                                    </div>


                                                    <div class="form-group">
                                                        <label>WhatsApp No<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col7]" value="<?= !empty($pArray->col7) ? $pArray->col7 : '' ?>" id="col7" required class="form-control"  autocomplete="off">
                                                    </div>


                                                    <div class="form-group">
                                                        <button type="submit" name="ContactUs" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>






                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">WHY CHOOSE EXCEL PROPERTIES</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['WHY_CHOOSE_EXCEL_PROPERTIES'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];


                                                        $file = !empty($_FILES['why_choose_logo_1']) ? $_FILES['why_choose_logo_1'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $json['col8'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['why_choose_logo_2']) ? $_FILES['why_choose_logo_2'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $json['col9'] = $fileName;
                                                            }
                                                        }
                                                        $file = !empty($_FILES['why_choose_logo_3']) ? $_FILES['why_choose_logo_3'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $json['col10'] = $fileName;
                                                            }
                                                        }

                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'WHY_CHOOSE_EXCEL_PROPERTIES'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'WHY_CHOOSE_EXCEL_PROPERTIES');
                                                    ?>

                                                    <div class="form-group">
                                                        <label>Title 1 <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>" id="col1" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 1<span style="color:red">*</span></label>
                                                        <textarea name="json[col2]" required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col2) ? $pArray->col2 : '' ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Logo 1 <small style="color: red;">Dimensions 103*103 px</small>
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col8) ? $pArray->col8 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="why_choose_logo_1" value="" id="why_choose_logo_1"  class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Title 2 <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>" id="col3" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 2<span style="color:red">*</span></label>
                                                        <textarea name="json[col4]"  required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col4) ? $pArray->col4 : '' ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Logo 2 <small style="color: red;">Dimensions 103*103 px</small>
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col9) ? $pArray->col9 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="why_choose_logo_2" value="" id="why_choose_logo_2"  class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 3 <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col5]" value="<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>" id="col5" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 3<span style="color:red">*</span></label>
                                                        <textarea name="json[col6]" required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col6) ? $pArray->col6 : '' ?></textarea>
                                                    </div> 





                                                    <div class="form-group">
                                                        <label>Logo 3 <small style="color: red;">Dimensions 103*103 px</small>
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col10) ? $pArray->col10 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="why_choose_logo_3" value="" id="why_choose_logo_3"  class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="WHY_CHOOSE_EXCEL_PROPERTIES" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-12">
                                <hr>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Home Page Location Updates</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['HOME_PAGE_LOCATION'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $json2 = !empty($_POST['json2']) && is_array($_POST['json2']) ? $_POST['json2'] : [];
                                                        $pArrayAd = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'HOME_PAGE_LOCATION_2');


                                                        $file = !empty($_FILES['img_home_col1']) ? $_FILES['img_home_col1'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArrayAd->col1) ? $pArrayAd->col1 : '');
                                                                $json2['col1'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }

                                                        $file = !empty($_FILES['img_home_col2']) ? $_FILES['img_home_col2'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArrayAd->col2) ? $pArrayAd->col2 : '');
                                                                $json2['col2'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }
                                                        $file = !empty($_FILES['img_home_col3']) ? $_FILES['img_home_col3'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArrayAd->col3) ? $pArrayAd->col3 : '');
                                                                $json2['col3'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }

                                                        $file = !empty($_FILES['img_home_col4']) ? $_FILES['img_home_col4'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArrayAd->col4) ? $pArrayAd->col4 : '');
                                                                $json2['col4'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }


                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        $json2['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'HOME_PAGE_LOCATION'));
                                                        \App\Database::updates('cms', $json2, array('cms_id' => 'HOME_PAGE_LOCATION_2'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'HOME_PAGE_LOCATION');
                                                    $pArrayAd = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'HOME_PAGE_LOCATION_2');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Location 1</label>

                                                        <input type="hidden" id="loc_col1" name="json[col1]" value="<?= $pArray->col1 ?>">
                                                        <input type="hidden" id="loc_col2" name="json[col2]" value="<?= $pArray->col2 ?>">
                                                        <input type="text" name="json[col3]" lat="loc_col1" lng="loc_col2" readonly value='<?= !empty($pArray->col3) ? $pArray->col3 : "" ?>' id="loc_col3" required class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Location 2</label>

                                                        <input type="hidden" id="loc_col4" name="json[col4]" value="<?= $pArray->col4 ?>">
                                                        <input type="hidden" id="loc_col5" name="json[col5]" value="<?= $pArray->col5 ?>">
                                                        <input type="text" name="json[col6]" lat="loc_col4" lng="loc_col5" readonly value='<?= !empty($pArray->col6) ? $pArray->col6 : "" ?>' id="loc_col6" required class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Location 3</label>

                                                        <input type="hidden" id="loc_col7" name="json[col7]" value="<?= $pArray->col7 ?>">
                                                        <input type="hidden" id="loc_col8" name="json[col8]" value="<?= $pArray->col8 ?>">
                                                        <input type="text" name="json[col9]" lat="loc_col7" lng="loc_col8" readonly value='<?= !empty($pArray->col9) ? $pArray->col9 : "" ?>' id="loc_col9" required class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Location 4</label>

                                                        <input type="hidden" id="loc_col10" name="json[col10]" value="<?= $pArray->col10 ?>">
                                                        <input type="hidden" id="loc_col11" name="json[col11]" value="<?= $pArray->col11 ?>">
                                                        <input type="text" name="json[col12]" lat="loc_col10" lng="loc_col11" readonly value='<?= !empty($pArray->col12) ? $pArray->col12 : "" ?>' id="loc_col12" required class="form-control" placeholder="" autocomplete="off">
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Image 1
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArrayAd->col1) ? $pArrayAd->col1 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArrayAd->col1)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" id="img_home_col1" name="img_home_col1" class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image 2
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArrayAd->col2) ? $pArrayAd->col2 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArrayAd->col2)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" id="img_home_col2" name="img_home_col2" class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image 3
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArrayAd->col3) ? $pArrayAd->col3 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArrayAd->col3)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" id="img_home_col3" name="img_home_col3" class="form-control" placeholder="" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Image 4
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArrayAd->col4) ? $pArrayAd->col4 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArrayAd->col4)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" id="img_home_col4" name="img_home_col4" class="form-control" placeholder="" autocomplete="off">
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Title 1<span style="color:red">*</span></label>
                                                        <input type="text" name="json2[col5]" value="<?= !empty($pArrayAd->col5) ? $pArrayAd->col5 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 2<span style="color:red">*</span></label>
                                                        <input type="text" name="json2[col6]" value="<?= !empty($pArrayAd->col6) ? $pArrayAd->col6 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 3<span style="color:red">*</span></label>
                                                        <input type="text" name="json2[col7]" value="<?= !empty($pArrayAd->col7) ? $pArrayAd->col7 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 4<span style="color:red">*</span></label>
                                                        <input type="text" name="json2[col8]" value="<?= !empty($pArrayAd->col8) ? $pArrayAd->col8 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="HOME_PAGE_LOCATION" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Property Description</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['PROPERTY_DESCRIPTION'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $json['lastupdate'] = date('Y-m-d H:i:s');

                                                        $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'PROPERTY_DESCRIPTION');


                                                        $file = !empty($_FILES['col5_propery_buy']) ? $_FILES['col5_propery_buy'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col5) ? $pArray->col5 : '');
                                                                $json['col5'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }
                                                        
                                                        
                                                        $file = !empty($_FILES['col6_propery_rent']) ? $_FILES['col6_propery_rent'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col6) ? $pArray->col6 : '');
                                                                $json['col6'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }

                                                        \App\Database::updates('cms', $json, array('cms_id' => 'PROPERTY_DESCRIPTION'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'PROPERTY_DESCRIPTION');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Property For Buy Title<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="">Property For Buy Banner 
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col5) ? $pArray->col5 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?> 
                                                            <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                        </label>
                                                        <input type="file" id="col5_propery_buy" name="col5_propery_buy" class="form-control" autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Property For Buy Description<span style="color:red">*</span></label>
                                                        <textarea class="form-control summernote" rows="5" name="json[col1]" id="col1" required ><?= !empty($pArray->col1) ? $pArray->col1 : '' ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Property For Rent Title<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="">Property For Rent Banner 
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col6) ? $pArray->col6 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?> 
                                                            <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                        </label>
                                                        <input type="file" id="col6_propery_rent" name="col6_propery_rent" class="form-control" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Property For Rent Description<span style="color:red">*</span></label>
                                                        <textarea class="form-control summernote" rows="5" name="json[col2]" id="col1" required ><?= !empty($pArray->col2) ? $pArray->col2 : '' ?></textarea>
                                                    </div>


                                                    <div class="form-group">
                                                        <button type="submit" name="PROPERTY_DESCRIPTION" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>




                            <div class="col-sm-12">
                                <hr>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Filter Settings</div>
                                    <div class="panel-body">
                                        <form action="" method="post">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['FILTER_SETTINGS_BTN'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'FILTER_SETTINGS'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'FILTER_SETTINGS');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>No of Bedroom<span style="color:red">*</span></label>
                                                        <input type="number" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>



                                                    <div class="form-group">
                                                        <button type="submit" name="FILTER_SETTINGS_BTN" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">New development Settings</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['NEW_DEVELOPMENT_SETTINGS'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'NEW_DEVELOPMENT_SETTINGS');

                                                        $file = !empty($_FILES['img_dev_banner_col7']) ? $_FILES['img_dev_banner_col7'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                                                                $json['col7'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }


                                                        \App\Database::updates('cms', $json, array('cms_id' => 'NEW_DEVELOPMENT_SETTINGS'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'NEW_DEVELOPMENT_SETTINGS');
                                                    ?>
                                                    <h3>For Real Estate Developers</h3>
                                                    <div class="form-group">
                                                        <label>Banner Title <a target="_blank" href="<?= url('dubai/real-estate-developers') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <input type="text" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title <a target="_blank" href="<?= url('dubai/real-estate-developers') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <input type="text" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Banner
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArray->col7)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?> <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                        </label>
                                                        <input type="file" id="img_dev_banner_col7" name="img_dev_banner_col7" class="form-control" placeholder="" autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Description <a target="_blank" href="<?= url('dubai/real-estate-developers') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <textarea class="form-control summernote" rows="5" name="json[col2]"  required ><?= !empty($pArray->col2) ? $pArray->col2 : '' ?></textarea>
                                                    </div>



                                                    <h3>For Projects</h3>
                                                    <div class="form-group">
                                                        <label>Banner Title <a target="_blank" href="<?= url('dubai/more-developments') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <input type="text" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title <a target="_blank" href="<?= url('dubai/more-developments') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <input type="text" name="json[col5]" value="<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Description <a target="_blank" href="<?= url('dubai/more-developments') ?>">Link to view</a><span style="color:red">*</span></label>
                                                        <textarea class="form-control summernote" rows="5" name="json[col6]"  required ><?= !empty($pArray->col6) ? $pArray->col6 : '' ?></textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="NEW_DEVELOPMENT_SETTINGS" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>







                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>

        <input type="text" readonly value='' id="autocomplete" class="form-control" placeholder="" autocomplete="off">
        <div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <input type="hidden" id="lat" value="">
                    <input type="hidden" id="lng" value="">

                    <input type="hidden" id="location_no">
                    <input type="hidden" id="lat_no">
                    <input type="hidden" id="lng_no">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="text" id='store-location' value=''  autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <div class="button-wrap wd100">
                            <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal">CONFIRM</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')


<script src="{{url('/public')}}/assets/js/mapmodel_admin.js" ></script>   
<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
<script>
$('#loc_col3,#loc_col6,#loc_col9,#loc_col12,#contct_us_location').click(function () {
    var location_no = this.id;
    var latId = $(this).attr('lat');
    var lngId = $(this).attr('lng');
    $('#location_no').val(location_no);
    $('#lat_no').val(latId);
    $('#lng_no').val(lngId);


    $('#autocomplete,#store-location').val($('#' + location_no).val());

    $('#lat').val($('#' + latId).val());
    $('#lng').val($('#' + lngId).val());
    setTimeout(function () {
        $('#autocomplete').click();
        initMap();
    }, 400);
});
$('.confirm_location').click(function () {
    setTimeout(function () {
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var autocomplete = $('#autocomplete').val();

        var location_no = $('#location_no').val();
        var latId = $('#lat_no').val();
        var lngId = $('#lng_no').val();
        $('#' + latId).val(lat);
        $('#' + lngId).val(lng);
        $('#' + location_no).val(autocomplete);
    }, 400);

});
</script>