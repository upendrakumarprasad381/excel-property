@include('admin.includes.header')

<style>
    .fasCd{
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
    #chartdiv {
        width	: 100%;
        height	: 500px;
    }
    #chartdiv2 {
        width	: 100%;
        height	: 500px;
        font-size: 11px;
    }
    .dashboard-stat .details .desc{
        font-weight: 600 !important;
    }
    .dashboard-stat .details .number{
        font-weight: 600 !important;
    }
</style>
<?php
$cond1 = "";
$cond = '';

$blog = ",(SELECT COUNT(blog_id) FROM `blogs` WHERE archive=0) AS blog";
$customer = ",(SELECT COUNT(name) FROM `enquiry_management` WHERE archive=0) AS customer";
$property = ",(SELECT COUNT(property_id) FROM `my_property` WHERE archive=0 $cond) AS property";
$enquiry = ",(SELECT COUNT(enqid) FROM `enquiry_management` WHERE archive=0) AS enquiry";
$guides = ",(SELECT COUNT(guides_id) FROM `area_guides` WHERE archive=0) AS guides";
$team = ",(SELECT COUNT(team_id) FROM `team` WHERE archive=0) AS team";
$testimonial = ",(SELECT COUNT(id) FROM `testimonial` WHERE archive=0) AS testimonial";
$Sql = "SELECT COUNT(vendor_id) AS subAdmin $blog $customer $property $enquiry $guides $team $testimonial FROM `vendor_details` WHERE archive=0 AND vendor_id NOT IN(1) AND user_type NOT IN (1)";
$dArray = \App\Database::selectSingle($Sql);

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <?php
            ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 purple" href="<?= url('admin/subadmin') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa-first-order"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->subAdmin ?>"><?= $dArray->subAdmin ?></span> </div>
                        <div class="desc">Sub Admin </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="<?= url('admin/blogs') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa-creative-commons-pd"></i>
                    </div>
                    <div class="details">
                        <div class="number"> 
                            <span data-counter="counterup" data-value="<?= $dArray->blog ?>"><?= $dArray->blog ?></span> </div>
                        <div class="desc">  Blogs </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="<?= url('admin/customers') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa fa-database"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->customer ?>"><?= $dArray->customer ?></span>
                        </div>
                        <div class="desc"> Customer</div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="<?= url('admin/myproperty') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa fa-at"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->property ?>"><?= $dArray->property ?></span>
                        </div>
                        <div class="desc"> My Property</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a  class="dashboard-stat dashboard-stat-v2 green-jungle" href="<?= url('admin/myenquiry') ?>">
                    <div class="visual">
                        <i class="fasCd fas fa-certificate"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->enquiry ?>"><?= $dArray->enquiry ?></span>
                        </div>
                        <div class="desc"> Inquiry  </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a title="Booking confirmed property" class="dashboard-stat dashboard-stat-v2 yellow" href="<?= url('admin/areaguides') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa fa-power-off"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->guides ?>"><?= $dArray->guides ?></span>
                        </div>
                        <div class="desc">  Area Guides </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a title="Booking confirmed retreat" class="dashboard-stat dashboard-stat-v2 purple-sharp" href="<?= url('admin/team') ?>">
                    <div class="visual">
                        <i class="fasCd fas fa-check-circle"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="<?= $dArray->team ?>"><?= $dArray->team ?></span>
                        </div>
                        <div class="desc">  Teams   </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a title="Booking confirmed service" class="dashboard-stat dashboard-stat-v2 yellow-casablanca" href="<?= url('admin/testimonial') ?>">
                    <div class="visual">
                        <i class="fasCd fab fa-cloudversify"></i>
                    </div>
                    <div class="details">
                        <div class="number"> 
                            <span data-counter="counterup" data-value="<?= $dArray->testimonial ?>"><?= $dArray->testimonial ?></span> </div>
                        <div class="desc">  Testimonial </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4><strong>Monthly Inquiry graph</strong></h4>
            </div>
        </div>
        <div class="row">
            <div id="chartdiv"></div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
@include('admin.includes.footer')
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<script>
    am4core.ready(function () {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

        var chart = am4core.create("chartdiv", am4charts.XYChart);

        var data = [];
        var value = 120;

        var names = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];

        for (var i = 0; i < names.length; i++) {
            value += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 5);
            data.push({category: names[i], value: value});
        }

        chart.data = data;
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.minGridDistance = 15;
        categoryAxis.renderer.grid.template.location = 0.5;
        categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
        categoryAxis.renderer.labels.template.rotation = -90;
        categoryAxis.renderer.labels.template.horizontalCenter = "left";
        categoryAxis.renderer.labels.template.location = 0.5;

        categoryAxis.renderer.labels.template.adapter.add("dx", function (dx, target) {
            return -target.maxRight / 2;
        })

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.ticks.template.disabled = true;
        valueAxis.renderer.axisFills.template.disabled = true;

        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "category";
        series.dataFields.valueY = "value";
        series.tooltipText = "{valueY.value}";
        series.sequencedInterpolation = true;
        series.fillOpacity = 0;
        series.strokeOpacity = 1;
        series.strokeDashArray = "1,3";
        series.columns.template.width = 0.01;
        series.tooltip.pointerOrientation = "horizontal";

        var bullet = series.bullets.create(am4charts.CircleBullet);

        chart.cursor = new am4charts.XYCursor();

        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();


    }); // end am4core.ready()
</script>