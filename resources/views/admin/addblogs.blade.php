@include('admin.includes.header')
<?php
$blogId = !empty($_REQUEST['blogId']) ? base64_decode($_REQUEST['blogId']) : '';
$bArray = \App\Helpers\LibHelper::GetblogsByblogsId($blogId);


?>
<style>

    .modal-title {
        margin: 0;
        line-height: 1.42857143;
        font-size: 16px;
        text-transform: uppercase;
        padding: 3px 0;
    }

    .pac-container, .pac-item{ z-index: 2147483647 !important; }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= !empty($bArray->blog_id) == '' ? 'Add New' : 'Update'; ?> Blog</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="blog_id" value="<?= !empty($bArray->blog_id) ? $bArray->blog_id : '' ?>">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title</label>
                                    <div class="col-md-4">
                                      
                                        <input id="blog_titile" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= isset($bArray->blog_titile) ? $bArray->blog_titile : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Slugs</label>
                                    <div class="col-md-4">
                                        <input id="slugs" value="<?= isset($bArray->slugs) ? $bArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Logo Title</label>
                                    <div class="col-md-4">
                                        <input id="logo_title" value="<?= isset($bArray->logo_title) ? $bArray->logo_title : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Position</label>
                                    <div class="col-md-4">
                                        <input id="position" value="<?= isset($bArray->position) ? $bArray->position : '' ?>" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Tags</label>
                                    <div class="col-md-4">
                                        <?php
                                        $tags = !empty($bArray->tags) ? explode(',', $bArray->tags) : [];
                                        $tags = !empty($tags) && is_array($tags) ? $tags : [];
                                        ?>
                                        <select id="tags" data-live-search="true" multiple class="form-control selectpicker" autocomplete="off">
                                            <option <?= in_array('1', $tags) ? 'selected' : '' ?> value="1">Home Page</option>
                                            <option <?= in_array('2', $tags) ? 'selected' : '' ?> value="2">Trending</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3">Featured Images 

                                        <?php
                                        $file = "files/hostgallery/" . (!empty($bArray->attachment) ? $bArray->attachment : '');
                                        if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                            ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                        }
                                        ?><br> <span class="size-alert">Min width and height - 948 * 600 px</span>
                                    </label>
                                    <div class="col-md-4">
                                        <input id="attachment"  type="file" class="form-control" autocomplete="off">
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="control-label col-md-3">Blog Images Home Page</small>

                                        <?php
                                        $file = "files/hostgallery/" . (!empty($bArray->attachment_home) ? $bArray->attachment_home : '');
                                        if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                            ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                        }
                                        ?><br> <span class="size-alert">Min width and height - 906 * 350  px</span>
                                    </label>
                                    <div class="col-md-4">
                                        <input id="attachment_home"  type="file" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description Home Page</label>
                                    <div class="col-md-6">
                                        <textarea value="" id="description_home_page" class="form-control summernote" rows="5"><?= !empty($bArray->description_home_page) ? $bArray->description_home_page : '' ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea value="" id="blog_description" class="form-control summernote" rows="5"><?= !empty($bArray->blog_description) ? $bArray->blog_description : '' ?></textarea>
                                    </div>
                                </div>







                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addblogs" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '<?= url('admin/blogs'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')
<script>
    $('#addblogs').click(function () {
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);

        form.append('json[blog_id]', $('#blog_id').val());
        form.append('json[blog_titile]', $('#blog_titile').val());
        form.append('json[slugs]', $('#slugs').val());
        form.append('json[logo_title]', $('#logo_title').val());
        form.append('json[blog_description]', $('#blog_description').val());
            form.append('json[description_home_page]', $('#description_home_page').val());
        
        form.append('json[tags]', $('#tags').val());
        form.append('json[position]', $('#position').val());

        if ($('#blog_titile').val() == '') {
            $('#blog_titile').css('border-color', 'red');
            $('#blog_titile').focus();
            return false;
        } else {
            $('#blog_titile').css('border-color', '');
        }
        if ($('#logo_title').val() == '') {
            $('#logo_title').css('border-color', 'red');
            $('#logo_title').focus();
            return false;
        } else {
            $('#logo_title').css('border-color', '');
        }
        if ($('#slugs').val() == '') {
            $('#slugs').css('border-color', 'red');
            $('#slugs').focus();
            return false;
        } else {
            $('#slugs').css('border-color', '');
        }
        var fileInput = document.querySelector('#attachment');
        form.append('attachment', fileInput.files[0]);

        var fileInput = document.querySelector('#attachment_home');
        form.append('attachment_home', fileInput.files[0]);




        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addblogs");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/blogs';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>