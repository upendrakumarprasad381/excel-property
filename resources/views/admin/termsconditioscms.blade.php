@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">CMS</span>

                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Terms Conditions cms</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['TERMS_CONDITIONS_PAGE'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];

                                                        $file = !empty($_FILES['company_profile']) ? $_FILES['company_profile'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $json['col1'] = $fileName;
                                                            }
                                                        }

                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'TERMS_CONDITIONS_PAGE'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'TERMS_CONDITIONS_PAGE');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Banner  <small>Width * Height 1920*500</small>
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col1) ? $pArray->col1 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="company_profile" value="" id="col8"  class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Banner Title <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col2]" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <textarea name="json[col3]"  id="col3" required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col3) ? $pArray->col3 : '' ?></textarea>
                                                    </div>

                                                    

                                                    
                                                    <div class="form-group">
                                                        <button type="submit" name="TERMS_CONDITIONS_PAGE" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>






                          


                          




                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>

    

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')

