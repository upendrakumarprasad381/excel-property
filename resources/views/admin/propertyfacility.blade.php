@include('admin.includes.header')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Property Amenities </span>
                            <a href="<?= url('admin/addpropertyfacility') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Amenities</th>
                                       <th>Icon</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qry = "SELECT RF.* FROM `property_facility` RF WHERE RF.archive=0 ORDER BY RF.facility_id DESC";
                                $dArray = \App\Database::select($qry);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                 $basepath=   'files/facility-logo/'.$d->rfacility_logo;
                                
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->facility_name ?></td>
                                        <td><?= is_file(Config::get('constants.HOME_DIR').$basepath) ? '<img style="height: 30px;width: 30px;" src="'.url($basepath).'">' : '' ?></td>
                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Edit" href="<?= url('admin/addpropertyfacility?facilityId=' . base64_encode($d->facility_id)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
                                            <a title="Edit" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"facility_id":"<?= $d->facility_id ?>"}' dbtable="property_facility" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')