@include('admin.includes.header')
<?php
$priceId = !empty($_REQUEST['priceId']) ? base64_decode($_REQUEST['priceId']) : '';
$aArray = \App\Helpers\LibHelper::GetpropertypriceBy($priceId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->ptype_id) ? 'Add New' : 'Update'; ?> Property Price</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="price_id" value="<?= !empty($aArray->price_id) ? $aArray->price_id : '' ?>">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Type</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="ptype">
                                            <?php
                                            $ptype = isset($aArray->ptype) ? $aArray->ptype : '';
                                            ?>
                                            <option  value="0">BUY</option>
                                            <option <?= $ptype == '1' ? 'selected' : '' ?> value="1">RENT</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Price</label>
                                    <div class="col-md-4">
                                        <input id="price" value="<?= isset($aArray->price) ? $aArray->price : '' ?>" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>



                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addpropertyprice" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addpropertyprice').click(function () {
        var form = new FormData();

        if ($('#price').val() == '') {
            $('#price').focus();
            $('#price').css('border-color', 'red');
            return false;
        } else {
            $('#price').css('border-color', '');
            form.append('json[price]', $('#price').val());
        }
        form.append('json[ptype]', $('#ptype').val());
        form.append('json[price_id]', $('#price_id').val());


        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addpropertyprice");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/propertyprice';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
