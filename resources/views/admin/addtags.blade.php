@include('admin.includes.header')
<?php
$tagId = !empty($_REQUEST['tagId']) ? base64_decode($_REQUEST['tagId']) : '';
$aArray = \App\Helpers\LibHelper::GettagseBytagId($tagId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->tag_id) ? 'Add New' : 'Update'; ?> Tag</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="tag_id" value="<?= !empty($aArray->tag_id) ? $aArray->tag_id : '' ?>">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Page Name</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="page_id">
                                            <?php
                                            $page_id = isset($aArray->page_id) ? $aArray->page_id : '';
                                            ?>
                                            <option <?= $page_id == 'HOME_PAGE' ? 'selected' : '' ?> value="HOME_PAGE">HOME PAGE</option>
                                            <option <?= $page_id == 'AREA_GUIDES' ? 'selected' : '' ?> value="AREA_GUIDES">AREA GUIDES PAGE</option>
                                            <option <?= $page_id == 'PROPERTY LIST FOR REAL ESTATE DEVELOPERS' ? 'selected' : '' ?> value="PROPERTY LIST FOR REAL ESTATE DEVELOPERS">PROPERTY LIST FOR REAL ESTATE DEVELOPERS</option>
                                            <option <?= $page_id == 'OTHER' ? 'selected' : '' ?> value="OTHER">OTHER</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">DATA LIST FROM</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="data_list_from">
                                            <?php
                                            $data_list_from = isset($aArray->data_list_from) ? $aArray->data_list_from : '';
                                            ?>
                                            <option <?= $data_list_from == 'AREA_GUIDES' ? 'selected' : '' ?> value="AREA_GUIDES">AREA GUIDES</option>
                                            <option <?= $data_list_from == 'PROPERTY_LISTING' ? 'selected' : '' ?> value="PROPERTY_LISTING">PROPERTY LISTING</option>
                                            <option <?= $data_list_from == 'NEW_DEVELOPMENT' ? 'selected' : '' ?> value="NEW_DEVELOPMENT">NEW DEVELOPMENT</option>
                                        </select>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="control-label col-md-3">Tag Name</label>
                                    <div class="col-md-4">
                                        <input id="tag_name" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= isset($aArray->tag_name) ? $aArray->tag_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <label class="control-label col-md-3">Banner 
                                            <?php
                                            $file = "files/hostgallery/" . (!empty($aArray->banner) ? $aArray->banner : '');
                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                            }
                                            ?> 
                                            <br><span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                        </label>
                                          <div class="col-md-4">
                                        <input type="file" id="banner" class="form-control" autocomplete="off">
                                          </div>
                                    </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Position</label>
                                    <div class="col-md-4">
                                        <input id="position" value="<?= isset($aArray->position) ? $aArray->position : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Slugs</label>
                                    <div class="col-md-4">
                                        <input id="slugs" value="<?= isset($aArray->slugs) ? $aArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea id="description" class="form-control summernote" autocomplete="off"><?= isset($aArray->description) ? $aArray->description : '' ?></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addtags" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '{{url('/tags')}}';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addtags').click(function () {
        var form = new FormData();

        if ($('#tag_name').val() == '') {
            $('#tag_name').focus();
            $('#tag_name').css('border-color', 'red');
            return false;
        } else {
            $('#tag_name').css('border-color', '');

        }
        if ($('#position').val() == '') {
            $('#position').focus();
            $('#position').css('border-color', 'red');
            return false;
        } else {
            $('#position').css('border-color', '');

        }
          if ($('#slugs').val() == '') {
            $('#slugs').focus();
            $('#slugs').css('border-color', 'red');
            return false;
        } else {
            $('#slugs').css('border-color', '');
            form.append('json[slugs]', $('#slugs').val());
        }
        form.append('json[page_id]', $('#page_id').val());
        form.append('json[data_list_from]', $('#data_list_from').val());
        
        form.append('json[tag_name]', $('#tag_name').val());
        form.append('json[position]', $('#position').val());
        form.append('json[description]', $('#description').val());
        form.append('json[tag_id]', $('#tag_id').val());

     var fileInput = document.querySelector('#banner');
        form.append('banner', fileInput.files[0]);

        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addtags");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/tags';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
