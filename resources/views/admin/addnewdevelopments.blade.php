@include('admin.includes.header')
<?php
$ndid = !empty($_REQUEST['ndid']) ? base64_decode($_REQUEST['ndid']) : '';
$pArray = \App\Helpers\LibHelper::GetnewdevelopmentsByndid($ndid);
if (isset($_POST['addBanner'])) {
    $json = !empty($_POST['store']) && is_array($_POST['store']) ? $_POST['store'] : [];
    $json['ndid'] = $ndid;
    $json['lastupdate'] = date('Y-m-d H:i:s');


    $file = !empty($_FILES['banner']) ? $_FILES['banner'] : [];
    if (!empty($file['name'])) {
        $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
        $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
        if (move_uploaded_file($file['tmp_name'], $baseDir)) {
            $json['banner_logo'] = $fileName;
            \App\Database::insert('new_developments_banner', $json);
        }
    }
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> New Developments <?= !empty($pArray->ndid) ? 'Updates' : 'Add' ?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->ndid) ? $pArray->ndid : '' ?>" id="ndid">
                                                <div class="row">

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Developer Name<span style="color:red">*</span></label>
                                                            <select id="parents_id" class="form-control">
                                                                <option value="0">New Developer</option>
                                                                <?php
                                                                $parents_id = !empty($pArray->parents_id) ? $pArray->parents_id : '';
                                                                $Sql = "SELECT ndid,heading FROM `new_developments` WHERE archive=0 AND parents_id=0 ORDER BY position ASC";
                                                                $dlist = App\Database::select($Sql);
                                                                for ($i = 0; $i < count($dlist); $i++) {
                                                                    ?>
                                                                    <option <?= $parents_id == $dlist[$i]->ndid ? 'selected' : '' ?> value="<?= $dlist[$i]->ndid ?>"><?= $dlist[$i]->heading ?></option>
                                                                <?php } ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!--                                                    <div class="col-sm-12">
                                                                                                            <div class="form-group">
                                                                                                                <label>Heading <span style="color:red">*</span></label>
                                                                                                                <input class="form-control" list="headings" name="heading" value="<?= !empty($pArray->heading) ? $pArray->heading : '' ?>" id="heading" autocomplete="off">
                                                                                                                <datalist id="headings">
                                                                                                                    <php
                                                                                                                    $Sql = "SELECT heading FROM `new_developments` WHERE archive=0 GROUP BY heading";
                                                                                                                    $dlist = App\Database::select($Sql);
                                                                                                                    for ($i = 0; $i < count($dlist); $i++) {
                                                                                                                        ?>
                                                                                                                        <option value="<= $dlist[$i]->heading ?>">
                                                                                                                        <php } ?>
                                                    
                                                                                                                </datalist>
                                                                                                            </div>
                                                                                                        </div>-->
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Heading <span style="color:red">*</span></label>
                                                            <input class="form-control" type="text" value="<?= !empty($pArray->heading) ? $pArray->heading : '' ?>" updateId="slugs" onkeyup="convertToSlug(this);" id="heading" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Slug <span style="color:red">*</span></label>
                                                            <input class="form-control slugsvalidate" type="text" value="<?= !empty($pArray->slugs) ? $pArray->slugs : '' ?>" id="slugs" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Position <span style="color:red">*</span></label>
                                                            <input class="form-control" disabled type="number" value="<?= !empty($pArray->position) ? $pArray->position : '' ?>" id="position" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                                <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                            </label>
                                                            <input type="file" id="banner" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Featured image 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->featured_image) ? $pArray->featured_image : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 416 * 460 px</span>
                                                            </label>
                                                            <input type="file" id="featured_image" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Logo On Banner
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->logo_on_banner) ? $pArray->logo_on_banner : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                                <span class="size-alert">Min width and height - 246 * 75 px</span>
                                                            </label>
                                                            <input type="file" id="logo_on_banner" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Contact Form Image
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->register_interest_logo) ? $pArray->register_interest_logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                                <span class="size-alert">Min width and height - 1920 * 1060 px</span>
                                                            </label>
                                                            <input type="file" id="register_interest_logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>





                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Brochure File
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->brochure_file) ? $pArray->brochure_file : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                            </label>
                                                            <input type="file" id="brochure_file" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Floorplan File
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->floorplan_file) ? $pArray->floorplan_file : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> 
                                                            </label>
                                                            <input type="file" id="floorplan_file" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>






                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Banner Title<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->banner_title) ? $pArray->banner_title : 'Offers Studios, 1,2 and 4-Bedroom Apartments 50/50 Payment plan' ?>" id="banner_title" class="form-control" placeholder="Offers Studios, 1,2 and 4-Bedroom Apartments 50/50 Payment plan" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description Heading<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->description_heading) ? $pArray->description_heading : 'Port De La Mer' ?>" id="description_heading" class="form-control" placeholder="Port De La Mer" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description<span style="color:red">*</span></label>
                                                            <textarea value="" id="description" class="form-control summernote" placeholder="" autocomplete="off"><?= !empty($pArray->description) ? $pArray->description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.' ?></textarea>
                                                        </div>
                                                    </div>





                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Starting Price <span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->price) ? $pArray->price : '5 % on Booking' ?>" id="price" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Property Type <span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->property_type) ? $pArray->property_type : 'Apartment' ?>" id="property_type" class="form-control" >

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Location <span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->location) ? $pArray->location : '' ?>" id="location" class="form-control" placeholder="Jumeirah" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Area (In sq ft)<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->area) ? $pArray->area : '' ?>" id="area" class="form-control" placeholder="200 " autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Bedrooms</label>
                                                            <input type="text" value="<?= !empty($pArray->bed) ? $pArray->bed : '2' ?>" id="bed" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Completion Date</label>
                                                            <input type="text" value="<?= !empty($pArray->completion) ? $pArray->completion : '' ?>" id="completion" class="form-control " placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-12" style="display: <?= empty($parents_id) ? 'none' : 'block' ?>" id="tag_idDiv">
                                                        <div class="form-group">
                                                            <label>Tag <span style="color:red">*</span></label>
                                                            <select class="form-control selectpicker"  data-live-search="true" multiple id="tag_id">
                                                                <?php
                                                                $tagId = !empty($pArray->tag_id) ? explode(',', $pArray->tag_id) : [];
                                                                $tagId = !empty($tagId) && is_array($tagId) ? $tagId : [];
                                                                $Sql = "SELECT * FROM `tags` WHERE data_list_from ='NEW_DEVELOPMENT' ORDER BY position ASC";
                                                                $dlist = \App\Database::select($Sql);
                                                                for ($i = 0; $i < count($dlist); $i++) {
                                                                    $d = $dlist[$i];
                                                                    ?><option <?= in_array($d->tag_id, $tagId) ? 'selected' : '' ?> value="<?= $d->tag_id ?>"><?= $d->tag_name ?></option><?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Status <span style="color:red">*</span></label>
                                                            <select class="form-control" id="status">
                                                                <?php
                                                                $status = !empty($pArray->status) ? $pArray->status : '';
                                                                ?>
                                                                <option <?= empty($status) ? 'selected' : '' ?> value="0">Active</option>
                                                                <option <?= $status == '1' ? 'selected' : '' ?> value="1">Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>




                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addnewdevelopments" class="addnewdevelopments"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/newdevelopments') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>



                            <div class="col-md-6 " style="    margin-top: 89px;" >
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Video Description</label>
                                        <input type="text" value="<?= !empty($pArray->video_description) ? $pArray->video_description : '' ?>" id="video_description" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Video Embedded <a href="https://www.youtube.com/" target="_blank">You Tube</a></label>
                                        <input type="text" value='<?= !empty($pArray->video_url) ? $pArray->video_url : "" ?>' id="video_url" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Location Embedded 
                                            <!--<a href="https://www.google.com/maps" target="_blank">Google Map</a>-->
                                        </label>
                                        <input readonly type="text" value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>' id="autocomplete" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label>Installment <a uniqId="" class="actbtn" onclick="addInstallment();"><span class="label label-sm label-success"><i class="fa fa-plus" aria-hidden="true"></i></span></a></label>
                                </div>
                                <inst>
                                    <?php

                                    function ordinal($number) {
                                        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
                                        if ((($number % 100) >= 11) && (($number % 100) <= 13))
                                            return $number . 'th';
                                        else
                                            return $number . $ends[$number % 10];
                                    }

                                    $installment = !empty($pArray->installment) ? json_decode($pArray->installment, true) : [];
                                    $installment = !empty($installment) && is_array($installment) ? $installment : [];
                                    for ($i = 0; $i < count($installment); $i++) {
                                        $d = $installment[$i];

                                        $names = ordinal(1 + $i);
                                        $uniqId = uniqid();

                                        $btnrmv = '<a uniqId="' . $uniqId . '" class="actbtn" onclick="removeInstallment(this);"><span class="label label-sm label-danger"><i class="fa fa-close" aria-hidden="true"></i></span></a>';
                                        ?>
                                        <div class="col-sm-12 " id="ins-<?= $uniqId ?>">
                                            <div class="form-group">
                                                <label class="ins_lable"><?= $names ?> Installment <?= $btnrmv ?></label>
                                                <input type="text" class="form-control addInstallment" value="<?= !empty($d['installment']) ? $d['installment'] : '' ?>" uniqId="<?= $uniqId ?>" placeholder="" autocomplete="off">
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </inst>




                                <div class="col-sm-12"> 
                                    <div class="form-group">
                                        <label>Meta Title</label>
                                        <input type="text" value="<?= !empty($pArray->meta_title) ? $pArray->meta_title : '' ?>" id="meta_title" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Description</label>
                                        <input type="text" value="<?= !empty($pArray->meta_description) ? $pArray->meta_description : '' ?>" id="meta_description" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Meta Keywords</label>
                                        <input type="text" value="<?= !empty($pArray->meta_keywords) ? $pArray->meta_keywords : '' ?>" id="meta_keywords" class="form-control" placeholder="" autocomplete="off">
                                    </div>
                                </div>

                                <br>
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div  class="col-sm-12"> 
                                    <form class="form" role="form">
                                        <div class="portlet box green" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Add Features
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body open" style="display: block;">
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="mt-checkbox-inline">
                                                            <ul class="hostpropertyul">
                                                                <?php
                                                                $dArray = App\Helpers\LibHelper::roomfacility();
                                                                $facility = !empty($pArray->facility) ? explode(',', $pArray->facility) : [];
                                                                for ($i = 0; $i < count($dArray); $i++) {
                                                                    $d = $dArray[$i];
                                                                    ?>
                                                                    <li>
                                                                        <label class="mt-checkbox">
                                                                            <input type="checkbox" <?= in_array($d->facility_id, $facility) ? 'checked' : '' ?> class="facilitycheckbox" value="<?= $d->facility_id ?>"> <?= $d->facility_name ?>
                                                                            <span></span>
                                                                        </label>
                                                                    <?php } ?>
                                                                </li>
                                                            </ul>
                                                        </div> 
                                                    </div>
                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->









                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div  class="col-sm-12"> 
                                    <form class="form" role="form">
                                        <div class="portlet box green" style="margin-top: 13px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    Banner Management
                                                </div>
                                                <?php if (!empty($pArray->ndid)) { ?>
                                                    <button type="button" class="btn blue" data-toggle="modal" data-target="#exampleModal3123123" style="padding: 4px;margin-top: 5px;margin-left: 5px;">Add New Banner</button>
                                                <?php } ?>
                                                <div class="tools">
                                                    <a href="" class="expand" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body " style="display: none;">
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                         <?php if (empty($pArray->ndid)) {
                                                             ?><p><strong>After saving the data you can add banner.</strong></p><?php
                                                             } ?>
                                                        <div class="table-scrollable">
                                                            
                                                            <table class="table table-striped table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> # </th>
                                                                        <th> Type </th>
                                                                        <th> File </th>

                                                                        <th> Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (!empty($pArray->ndid)) {
                                                                        $Sql = "SELECT * FROM `new_developments_banner` WHERE ndid='$pArray->ndid'";
                                                                        $database = \App\Database::select($Sql);
                                                                    if(!empty($database)){
                                                                        for ($i = 0; $i < count($database); $i++) {
                                                                            $d = $database[$i];
                                                                            ?>
                                                                            <tr>
                                                                                <td> <?= $i + 1 ?> </td>
                                                                                <td> <?= $d->banner_type ?> </td>
                                                                                <td> <a target="_blank" href="<?= url("files/hostgallery/" . $d->banner_logo) ?>">View</a> </td>

                                                                                <td>
                                                                                    <a title="Edit" href="javascript:void(0)"   condjson='{"banner_id":"<?= $d->banner_id ?>"}' filepath="<?= Config::get('constants.HOME_DIR') . "files/hostgallery/" . $d->banner_logo ?>" cmessage='Are you sure want to delete!.<br><small style="font-size: 12px;">After remove this page will auto repload</small>' dbtable="new_developments_banner" href="javascript:void(0)" class="autodelete"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }else{
                                                                        ?> <tr><td align="center" style="text-align: center;" colspan="4">No banner uploaded</td></tr><?php
                                                                    }
                                                                    }
                                                                    ?>


                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->


                            </div>

                        </div>




                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>


        <div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="hidden" id="lat" value="<?= !empty($pArray->lat) ? $pArray->lat : '' ?>">
                            <input type="hidden" id="lng" value="<?= !empty($pArray->lng) ? $pArray->lng : '' ?>">

                            <input type="text" id='store-location' value='<?= !empty($pArray->location_embedded) ? $pArray->location_embedded : "" ?>'  autocomplete="off" class="form-control">
                        </div>
                        <div class="map-wrapper-inner" id="map-page">
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>



                    </div>
                    <div class="modal-footer">

                        <div class="button-wrap wd100">
                            <button type="button" class="float-right btn btn-success confirm_location" data-dismiss="modal">CONFIRM</button>

                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- END CONTENT BODY -->
</div>


<!-- Button trigger modal -->


<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="exampleModal3123123" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Banner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Type <span style="color:red">*</span></label>
                                <select class="form-control" name="store[banner_type]">
                                    <option value="BOTTOM BANNER">BOTTOM BANNER (W*H - 1920*450px)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Banner <span style="color:red">*</span></label>
                                <input class="form-control" name="banner" required id="" type="file">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="addBanner" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>


<style>
    .ins_lable{
        width: 100%;
    }
    .actbtn{
        float: right;
    }
</style>

<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script src="{{url('/public')}}/assets/js/mapmodel_admin.js" ></script>   
<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>

<script>

                                        function addInstallment() {
                                            var leng = $('.addInstallment').length;

                                            var names = getNumberWithOrdinal(1 + parseInt(leng));
                                            var uniqId = Math.floor((Math.random() * 1000000000) + 1);
//                                                                    var btnAdd = '<a uniqId="' + uniqId + '" class="actbtn" onclick="addInstallment();"><span class="label label-sm label-success"><i class="fa fa-plus" aria-hidden="true"></i></span></a>';
                                            var btnrmv = '<a uniqId="' + uniqId + '" class="actbtn" onclick="removeInstallment(this);"><span class="label label-sm label-danger"><i class="fa fa-close" aria-hidden="true"></i></span></a>';

                                            var html = '';
                                            html = html + ' <div class="col-sm-12 " id="ins-' + uniqId + '">';
                                            html = html + '    <div class="form-group">';
                                            html = html + '        <label class="ins_lable">' + names + ' Installment ' + btnrmv + '</label>';
                                            html = html + '        <input type="text" class="form-control addInstallment" uniqId="' + uniqId + '" placeholder="" autocomplete="off">';
                                            html = html + '    </div>';
                                            html = html + '</div>';
                                            $('inst').append(html);
                                        }
                                        function removeInstallment(e) {
                                            $('#ins-' + $(e).attr('uniqId')).remove();
                                        }
                                        function getNumberWithOrdinal(n) {
                                            var s = ["th", "st", "nd", "rd"],
                                                    v = n % 100;
                                            return n + '<sup>' + (s[(v - 20) % 10] || s[v] || s[0]) + '</sup> ';
                                        }
                                        $('#parents_id').change(function () {
                                            var parents_id = $('#parents_id').val();
                                            $('#tag_idDiv').hide();
                                            if (parents_id != '0') {
                                                $('#tag_idDiv').show();
                                            }
                                        });
                                        $('#addnewdevelopments').click(function () {
                                            var form = new FormData();

                                            if ($('#heading').val() == '') {
                                                $('#heading').focus();
                                                $('#heading').css('border-color', 'red');
                                                return false;
                                            } else {
                                                $('#heading').css('border-color', '');
                                                form.append('json[heading]', $('#heading').val());
                                            }
                                            if ($('#slugs').val() == '') {
                                                $('#slugs').focus();
                                                $('#slugs').css('border-color', 'red');
                                                return false;
                                            } else {
                                                $('#slugs').css('border-color', '');
                                                form.append('json[slugs]', $('#slugs').val());
                                            }
                                            form.append('json[parents_id]', $('#parents_id').val());


                                            form.append('json[banner_title]', $('#banner_title').val());
                                            form.append('json[description_heading]', $('#description_heading').val());
                                            form.append('json[position]', $('#position').val());

                                            form.append('json[description]', $('#description').val());
                                            form.append('json[price]', $('#price').val());
                                            form.append('json[property_type]', $('#property_type').val());
                                            form.append('json[location]', $('#location').val());
                                            form.append('json[area]', $('#area').val());
                                            form.append('json[bed]', $('#bed').val());
                                            form.append('json[completion]', $('#completion').val());
                                            form.append('json[status]', $('#status').val());
                                            form.append('json[tag_id]', $('#tag_id').val());

                                            form.append('json[video_description]', $('#video_description').val());
                                            form.append('json[video_url]', $('#video_url').val());
                                            form.append('json[location_embedded]', $('#autocomplete').val());
                                            form.append('json[lat]', $('#lat').val());
                                            form.append('json[lng]', $('#lng').val());

                                            var installment = [];
                                            $('.addInstallment').each(function () {
                                                var d = {};
                                                d['installment'] = $(this).val();
                                                installment.push(d);
                                            })
                                            form.append('json[installment]', JSON.stringify(installment));
                                            form.append('json[meta_title]', $('#meta_title').val());
                                            form.append('json[meta_description]', $('#meta_description').val());
                                            form.append('json[meta_keywords]', $('#meta_keywords').val());

                                            var fileInput = document.querySelector('#banner');
                                            form.append('banner', fileInput.files[0]);

                                            var fileInput = document.querySelector('#logo_on_banner');
                                            form.append('logo_on_banner', fileInput.files[0]);

                                            var fileInput = document.querySelector('#featured_image');
                                            form.append('featured_image', fileInput.files[0]);



                                            var fileInput = document.querySelector('#register_interest_logo');
                                            form.append('register_interest_logo', fileInput.files[0]);


                                            var fileInput = document.querySelector('#brochure_file');
                                            form.append('brochure_file', fileInput.files[0]);

                                            var fileInput = document.querySelector('#floorplan_file');
                                            form.append('floorplan_file', fileInput.files[0]);

                                            var facility = [];
                                            $(".facilitycheckbox:checked").each(function () {
                                                facility.push($(this).val());
                                            });
                                            if (facility.length == false && $('#parents_id').val() != '0') {
                                                alertSimple('add features.');
                                                return false;
                                            }
                                            form.append('json[facility]', facility.join(','));

                                            form.append('json[ndid]', $('#ndid').val());
                                            form.append('_token', CSRF_TOKEN);

                                            $.confirm({
                                                title: 'Are you sure want to submit ?',
                                                content: false,
                                                type: 'green',
                                                typeAnimated: true,
                                                buttons: {
                                                    confirm: {
                                                        text: 'Submit',
                                                        btnClass: 'btn-green',
                                                        action: function () {
                                                            var json = ajaxpost(form, "/admin/addnewdevelopments");
                                                            try {
                                                                var json = jQuery.parseJSON(json);
                                                                if (json.status == true) {
                                                                    window.location = base_url + '/admin/newdevelopments';
                                                                }
                                                            } catch (e) {
                                                                alert(e);
                                                            }
                                                        }
                                                    },
                                                    cancel: {
                                                        text: 'Cancel',
                                                        btnClass: 'btn-warning',
                                                        action: function () {
                                                        }
                                                    },
                                                }
                                            });
                                        });
</script>
