@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Tags List</span>
                            <a href="<?= url('admin/addtags') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Type</th>
                                    <th>Tag Name</th>
                                    <th>Data List Connection</th>
                                    <th>Position</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $notDelete=['21','22'];
                                $select = "";
                                $join = "  ";
                                $Sql = "SELECT SC.* $select FROM `tags` SC $join WHERE 1 ORDER BY SC.position ASC";
                                $dArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= str_replace('_', ' ', $d->page_id) ?></td>
                                        <td><?= $d->tag_name ?></td>
                                        <td><?= $d->data_list_from ?></td>
                                        <td><?= $d->position ?></td>
                                        <td><?= date('d/m/Y h:i A', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Edit" href="<?= url('admin/addtags?tagId=' . base64_encode($d->tag_id)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
<a title="More View" href="<?= url('admin/tagdetails?tagId=' . base64_encode($d->tag_id)); ?>"><span class="label label-sm label-danger"><i class="fa fa-external-link" aria-hidden="true"></i></span></a>

 <?php if(!in_array($d->tag_id, $notDelete)) { ?>
                                            <a title="Edit" href="javascript:void(0)"  condjson='{"tag_id":"<?= $d->tag_id ?>"}' dbtable="tags" href="javascript:void(0)" class="autodelete"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                           <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')
