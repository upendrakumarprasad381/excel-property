@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">List With US CMS</span>

                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Area - 1</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['LISTWITHUS_AREA_1_BTN'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];



                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'LIST_WITH_US_AREA_1'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_1');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Title 1<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 2<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col2]" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 3<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 4<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <textarea  class="form-control summernote" name="json[col5]" required><?= isset($pArray->col5) ? $pArray->col5 : '' ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 5<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col6]" value="<?= !empty($pArray->col6) ? $pArray->col6 : '' ?>"  required  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="LISTWITHUS_AREA_1_BTN" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Area - 2</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['LISTWITHUS_AREA_2_BTN'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_2');


                                                        $file = !empty($_FILES['area_2_icon_1']) ? $_FILES['area_2_icon_1'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col7;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col7'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['area_2_icon_2']) ? $_FILES['area_2_icon_2'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col8;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col8'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['area_2_icon_3']) ? $_FILES['area_2_icon_3'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col9;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col9'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['area_2_icon_4']) ? $_FILES['area_2_icon_4'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col10;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col10'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['area_2_icon_5']) ? $_FILES['area_2_icon_5'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col11;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col11'] = $fileName;
                                                            }
                                                        }

                                                        $file = !empty($_FILES['area_2_icon_6']) ? $_FILES['area_2_icon_6'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldFile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $pArray->col12;
                                                                if (is_file($oldFile)) {
                                                                    unlink($oldFile);
                                                                }
                                                                $json['col12'] = $fileName;
                                                            }
                                                        }

                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'LIST_WITH_US_AREA_2'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_2');
                                                    ?>
                                                    
                                                    <div class="form-group">
                                                        <label>Heading<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col13]" value="<?= !empty($pArray->col13) ? $pArray->col13 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col14]" value="<?= !empty($pArray->col14) ? $pArray->col14 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label>Title 1<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 2<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col2]" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 3<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 4<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>"  required  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Title 5<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col5]" value="<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 6<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col6]" value="<?= !empty($pArray->col6) ? $pArray->col6 : '' ?>"  required  autocomplete="off">
                                                    </div>


                                                    <p>Image width* height  - 56*71px</p>
                                                    <div class="form-group">
                                                        <label>Icon 1
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_1" value=""   class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Icon 2
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col8) ? $pArray->col8 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_2" value=""  class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Icon 3
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col9) ? $pArray->col9 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_3" value=""   class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Icon 4
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col10) ? $pArray->col10 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_4" value=""   class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Icon 5
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col11) ? $pArray->col11 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_5" value=""   class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Icon 6
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col12) ? $pArray->col12 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="area_2_icon_6" value=""  class="form-control"  autocomplete="off">
                                                    </div>





                                                    <div class="form-group">
                                                        <button type="submit" name="LISTWITHUS_AREA_2_BTN" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Area - 3</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['LISTWITHUS_AREA_3_BTN'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];



                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'LIST_WITH_US_AREA_3'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_3');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Heading<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col9]" value="<?= !empty($pArray->col9) ? $pArray->col9 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 1<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 1<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col2]" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 2<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col3]" value="<?= !empty($pArray->col3) ? $pArray->col3 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 2<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col4]" value="<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>"  required  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Title 3<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col5]" value="<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 3<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col6]" value="<?= !empty($pArray->col6) ? $pArray->col6 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Title 4<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col7]" value="<?= !empty($pArray->col7) ? $pArray->col7 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description 4<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col8]" value="<?= !empty($pArray->col8) ? $pArray->col8 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    
                                                    
                                                    <div class="form-group">
                                                        <label>Heading<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col10]" value="<?= !empty($pArray->col10) ? $pArray->col10 : '' ?>"  required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col11]" value="<?= !empty($pArray->col11) ? $pArray->col11 : '' ?>"  required  autocomplete="off">
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" name="LISTWITHUS_AREA_3_BTN" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')



