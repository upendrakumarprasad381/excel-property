@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Team</span>
                            <a href="<?= url('admin/addteam') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Employee Name</th>
                                    <th>Designation</th>
                                    <th>Position</th>
                                    <th>Profile Picture</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $select = "";
                                $join = "  ";
                                $option = '';
                                $Sql = "SELECT SC.* $select FROM `team` SC $join WHERE SC.archive=0 ORDER BY SC.position ASC";
                                $dArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    $baseDir = "files/hostgallery/" . $d->logo;

                                    $option = $option . ' <li id="' . $d->team_id . '" >' . $d->team_name . ' </li>';
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->team_name ?></td>
                                        <td><?= $d->designation ?></td>
                                        <td><?= $d->position ?></td>
                                        <td><?= is_file(Config::get('constants.HOME_DIR') . $baseDir) ? '<img height="40" width="40" src="' . url($baseDir) . '">' : '' ?></td>

                                        <td><?= date('d/m/Y h:i A', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Edit" href="<?= url('admin/addteam?teamId=' . base64_encode($d->team_id)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
                                            <a title="Edit" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"team_id":"<?= $d->team_id ?>"}' dbtable="team" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>



                        <div class="row">

                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
                                    <?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="POSITIONSET.teamUpdatespos();" type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12" style="margin-top: 20px;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">TEAM CMS</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['teamCMSBtn'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];

                                                        $file = !empty($_FILES['banner_title']) ? $_FILES['banner_title'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $json['col4'] = $fileName;
                                                            }
                                                        }

                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        \App\Database::updates('cms', $json, array('cms_id' => 'TEAM_CMS_PAGE'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'TEAM_CMS_PAGE');
                                                    ?>
                                                    <div class="form-group">
                                                        <label>Banner Title <span style="color:red">*</span></label>
                                                        <input class="form-control" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>" id="col1" required  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Banner Sub Title<span style="color:red">*</span></label>
                                                        <input type="text" name="json[col2]" value="<?= !empty($pArray->col2) ? $pArray->col2 : '' ?>" required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Description<span style="color:red">*</span></label>
                                                        <textarea name="json[col3]" rows="2" required class="form-control summernote"  autocomplete="off"><?= !empty($pArray->col3) ? $pArray->col3 : '' ?></textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Banner <small>(Width * Height 1920 * 600 px)</small>
                                                            <?php
                                                            $file = "files/hostgallery/" . (!empty($pArray->col4) ? $pArray->col4 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?>
                                                        </label>
                                                        <input type="file" name="banner_title" value=""  class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" name="teamCMSBtn" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')
