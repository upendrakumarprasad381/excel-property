@include('admin.includes.header')
<?php
$areaId = !empty($_REQUEST['areaId']) ? base64_decode($_REQUEST['areaId']) : '';
$aArray = \App\Helpers\LibHelper::GetarealistByareaId($areaId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->tag_id) ? 'Add New' : 'Update'; ?> Area</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="area_id" value="<?= !empty($aArray->area_id) ? $aArray->area_id : '' ?>">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Emirates</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="emirates">
                                            <?php
                                            $emirates = isset($aArray->emirates) ? $aArray->emirates : '';
                                            ?>
                                            <option <?= $emirates == 'Dubai' ? 'selected' : '' ?> value="Dubai">Dubai</option>
                                            <option <?= $emirates == 'Abu Dhabi' ? 'selected' : '' ?> value="Abu Dhabi">Abu Dhabi</option>
                                            <option <?= $emirates == 'Sharjah' ? 'selected' : '' ?> value="Sharjah">Sharjah</option>
                                            <option <?= $emirates == 'Ajman' ? 'selected' : '' ?> value="Ajman">Ajman</option>
                                            <option <?= $emirates == 'Umm Al-Quwain' ? 'selected' : '' ?> value="Umm Al-Quwain">Umm Al-Quwain</option>
                                            <option <?= $emirates == 'Fujairah' ? 'selected' : '' ?> value="Fujairah">Fujairah</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Area Name</label>
                                    <div class="col-md-4">
                                        <input id="area_name" updateId="slugs" onkeyup="convertToSlug(this);" value="<?= isset($aArray->area_name) ? $aArray->area_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
 <div class="form-group">
                                    <label class="control-label col-md-3">Slugs</label>
                                    <div class="col-md-4">
                                        <input id="slugs" value="<?= isset($aArray->slugs) ? $aArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description For Buy</label>
                                    <div class="col-md-6">
                                        <textarea id="description_buy" class="form-control summernote" autocomplete="off"><?= isset($aArray->description_buy) ? $aArray->description_buy : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description For Rent</label>
                                    <div class="col-md-6">
                                        <textarea id="description_rent" class="form-control summernote" autocomplete="off"><?= isset($aArray->description_rent) ? $aArray->description_rent : '' ?></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addarealist" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '{{url('/tags')}}';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addarealist').click(function () {
        var form = new FormData();

        if ($('#area_name').val() == '') {
            $('#area_name').focus();
            $('#area_name').css('border-color', 'red');
            return false;
        } else {
            $('#area_name').css('border-color', '');

        }
        if ($('#slugs').val() == '') {
            $('#slugs').focus();
            $('#slugs').css('border-color', 'red');
            return false;
        } else {
            $('#slugs').css('border-color', '');
            form.append('json[slugs]', $('#slugs').val());
        }
        form.append('json[emirates]', $('#emirates').val());
        form.append('json[area_name]', $('#area_name').val());
        form.append('json[area_id]', $('#area_id').val());
        
          form.append('json[description_buy]', $('#description_buy').val());
          form.append('json[description_rent]', $('#description_rent').val());


        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addarealist");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/arealist';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
