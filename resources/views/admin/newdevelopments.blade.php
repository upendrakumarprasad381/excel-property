@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">New Developments</span>
                            <a href="<?= url('admin/addnewdevelopments') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Heading</th>
                                    <th>Position</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                  $option = '';
                                $select = ",P.heading AS parents_name";
                                $join = "  LEFT JOIN new_developments P ON P.ndid=SC.parents_id";
                                $Sql = "SELECT SC.* $select FROM `new_developments` SC $join WHERE SC.archive=0 AND SC.parents_id IN (0) ORDER BY SC.position ASC";
                                $dArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                       $option = $option . ' <li id="' . $d->ndid . '" >' . $d->heading . ' </li>';
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= (!empty($d->parents_name) ? $d->parents_name . ' - ' . $d->heading : $d->heading . ' *') ?></td>
                                        <td><?= $d->position ?></td>

                                        <td><?= date('d/m/Y h:i A', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a target="_blank" title="View projects" href="<?= url('admin/projects?ndid=' . base64_encode($d->ndid)); ?>" ><span class="label label-sm label-success"><i class="fa fa-external-link" aria-hidden="true"></i></span></a>
                                            <a title="Edit" href="<?= url('admin/addnewdevelopments?ndid=' . base64_encode($d->ndid)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
                                            <a title="Edit" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"ndid":"<?= $d->ndid ?>"}' dbtable="new_developments" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                            
                            
                        </table>
                        
                        
                        
                        
                         <div class="row">

                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
                                    <?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="POSITIONSET.newdevelopmentsUpdatespos();" type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')
