@include('admin.includes.header')
<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Customer List</span>
                            <a style="display: none;" href="<?= url('admin/addcustomers') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Customer Name</th>
                                    <th>Email Id</th>
                                    <th>Phone No</th>
                                    <th>Date</th>
                                    <!--<th style="width: 12%;">Action</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $join = "";
                                $select = "";
                                $qry = "SELECT name,email,mobile_no,timestamp FROM `enquiry_management` GROUP BY email ORDER BY enqid DESC";
                                $dArray = \App\Database::select($qry);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?= $d->mobile_no ?></td>
                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
<!--                                        <td style="width: 100px;">

                                        </td>-->
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')