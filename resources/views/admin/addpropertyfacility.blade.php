@include('admin.includes.header')
<?php
if (isset($_POST['addroomfacility'])) {

    $json['lastupdate'] = date('Y-m-d H:i:s');
    $json['facility_name'] = !empty($_POST['facility_name']) ? $_POST['facility_name'] : '';
    $json['facility_id'] = !empty($_POST['facility_id']) ? $_POST['facility_id'] : '';
    $rfacility_logo = empty($_FILES['rfacility_logo']['error']) ? $_FILES['rfacility_logo'] : [];
    $allowed = array('png', 'jpg');
    if (!empty($rfacility_logo) && in_array(pathinfo($rfacility_logo['name'], PATHINFO_EXTENSION), $allowed)) {
        $fileName = uniqid() . '.' . pathinfo($rfacility_logo['name'], PATHINFO_EXTENSION);
        if (move_uploaded_file($rfacility_logo['tmp_name'], Config::get('constants.HOME_DIR') . '/files/facility-logo/' . $fileName)) {
            if (is_file($_POST['oldfile'])) {
                unlink($_POST['oldfile']);
            }
            $json['rfacility_logo'] = $fileName;
        }
    }

    if (!empty($json['facility_id'])) {
        $cond = array('facility_id' => $json['facility_id']);
        \App\Database::updates('property_facility', $json, $cond);
    } else {
        $json['timestamp'] = date('Y-m-d H:i:s');
        \App\Database::insert('property_facility', $json);
    }
    header("location: ".url('admin/propertyfacility'));
    exit;
  
}
$facilityId = !empty($_REQUEST['facilityId']) ? base64_decode($_REQUEST['facilityId']) : '';
$aArray = App\Helpers\LibHelper::GetpropertyfacilityBy($facilityId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->facility_id) ? 'Add New' : 'Update'; ?> Property Amenities</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" id="rfacility_id" name="facility_id" value="<?= !empty($aArray->facility_id) ? $aArray->facility_id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input id="rfacility_name" name="facility_name" value="<?= isset($aArray->facility_name) ? $aArray->facility_name : '' ?>" type="text" class="form-control" autocomplete="off" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Image <br> <span class="size-alert">Min width and height - 78 * 78 px</span></label>
                                    <input type="hidden" id="oldfile" name="oldfile" value="<?= Config::get('constants.HOME_DIR') . 'files/facility-logo/' . (isset($aArray->rfacility_logo) ? $aArray->rfacility_logo : '' ) ?>">
                                    <div class="col-md-4">
                                        <input id="rfacility_logo" name="rfacility_logo"  type="file" class="form-control" autocomplete="off">
                                        <span><a target="__blank" href="https://www.flaticon.com/">download</a></span>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" id="addroomfacility" name="addroomfacility" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')