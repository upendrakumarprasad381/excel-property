@include('admin.includes.header')
<?php
$tagId = !empty($_REQUEST['tagId']) ? base64_decode($_REQUEST['tagId']) : '';
$aArray = \App\Helpers\LibHelper::GettagseBytagId($tagId);

//echo '<pre>';
//print_r($aArray);
//exit;
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Tag Details - <?= $aArray->tag_name ?></span>


                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">




                        <input type="hidden" value="<?= $aArray->tag_id ?>" id="tag_id">
                        <?php
                        if ($aArray->data_list_from == 'AREA_GUIDES') { 
                            $Sql = "SELECT AG.guides_id AS id, area_name AS name  FROM `area_guides` AG LEFT JOIN tag_position TP ON TP.foreign_id=AG.guides_id AND TP.tag_id='$aArray->tag_id' WHERE AG.archive=0 AND FIND_IN_SET('$aArray->tag_id',AG.tag_id) GROUP BY AG.guides_id ORDER BY TP.position ASC";
                        } else if ($aArray->data_list_from == 'PROPERTY_LISTING') {
                            $Sql = "SELECT HP.property_id AS id, property_title AS name  FROM `my_property` HP LEFT JOIN tag_position TP ON TP.foreign_id=HP.property_id AND TP.tag_id='$aArray->tag_id' WHERE HP.archive=0 AND FIND_IN_SET('$aArray->tag_id',HP.tag_id) GROUP BY HP.property_id ORDER BY TP.position ASC";
                        }if ($aArray->data_list_from == 'NEW_DEVELOPMENT') {
                            $Sql = "SELECT ND.ndid AS id,heading AS name FROM `new_developments` ND LEFT JOIN tag_position TP ON TP.foreign_id=ND.ndid AND TP.tag_id='$aArray->tag_id' WHERE ND.archive=0 AND FIND_IN_SET('$aArray->tag_id',ND.tag_id) GROUP BY ND.ndid ORDER BY TP.position ASC";
                        }
                      
                        $option = '';
                        if (!empty($Sql)) {
                            $pArray = \App\Database::select($Sql);
                            for ($i = 0; $i < count($pArray); $i++) {
                                $d = $pArray[$i];
                                $option = $option . ' <li id="' . $d->id . '" >' .$d->name.' </li>';
                            }
                        }
                        ?>




                        <div class="row">

                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
                                    <?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="POSITIONSET.myTagUpdatespos();" type="button" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')