@include('admin.includes.header')
<?php
$submasterId = !empty($_REQUEST['submasterId']) ? base64_decode($_REQUEST['submasterId']) : '';
$masterId = !empty($_REQUEST['masterId']) ? base64_decode($_REQUEST['masterId']) : '';
$rArray = App\Helpers\LibHelper::GetsubmasterdetailsBysubmasterId($submasterId);
if (!empty($rArray)) {
    $masterId = $rArray->master_id;
}


$mArray = App\Helpers\LibHelper::GetmasterdetailsBy($masterId);
if (empty($mArray)) {
    redirect(url());
}

if (isset($_POST['formsubmit'])) {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $json['lastupdate'] = date('Y-m-d H:i:s');
    if (empty($rArray)) {
        $json['timestamp'] = date('Y-m-d H:i:s');
        $json['master_id'] = $masterId;
        \App\Database::insert('submaster_details', $json);
    } else {
        \App\Database::updates('submaster_details', $json, array('submaster_id' => $submasterId));
    }
    header("location: ".url('admin/submaster?masterId=' . base64_encode($masterId)));
    exit;
 
}

// echo "<pre>";
// print_r($rArray);
// exit;
?>
<!--?masterId=' . base64_encode($masterId)-->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($rArray->task_id) ? 'Add New' : 'Update'; ?> - <?= $mArray->master_name ?></span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="POST">
                            @csrf
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title :</label>
                                    <div class="col-md-4">
                                        <input id="task_id" name="json[submaster_name]" value="<?= !empty($rArray->submaster_name) ? $rArray->submaster_name : '' ?>" type="text" class="form-control" autocomplete="off" required>
                                    </div>
                                </div>




                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" name="formsubmit" class="btn blue">
                                                <i class="fa fa-check"></i> Submit</button>
                                            <button type="button" onclick="window.location = '<?= url('admin/submaster?masterId=' . base64_encode($masterId)); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    @include('admin.includes.footer')
