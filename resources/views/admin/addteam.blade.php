@include('admin.includes.header')
<?php
$teamId = !empty($_REQUEST['teamId']) ? base64_decode($_REQUEST['teamId']) : '';
$pArray = \App\Helpers\LibHelper::GetteamId($teamId);
//print_r($pArray);
//exit;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Team <?= !empty($pArray->ndid) ? 'Updates' : 'Add' ?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->team_id) ? $pArray->team_id : '' ?>" id="team_id">
                                                <div class="row">


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Employee Name <span style="color:red">*</span></label>
                                                            <input class="form-control"  value="<?= !empty($pArray->team_name) ? $pArray->team_name : '' ?>" id="team_name" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Designation <span style="color:red">*</span></label>
                                                            <input class="form-control" type="text" value="<?= !empty($pArray->designation) ? $pArray->designation : 'Engineer' ?>" id="designation" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Description<span style="color:red">*</span></label>
                                                            <textarea id="description" class="form-control summernote" placeholder="" autocomplete="off"><?= !empty($pArray->description) ? $pArray->description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.' ?></textarea>
                                                        <h5 id="limite_vermelho" style="text-align:right;color:red"></h5>
<h5 id="limite_normal" style="text-align:right"></h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Position (Order View) <span style="color:red">*</span></label>
                                                            <input type="number" class="form-control"  value="<?= !empty($pArray->position) ? $pArray->position : '' ?>" id="position" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Profile Picture 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?>  <span class="size-alert">Min width and height - 547 * 532 px</span>
                                                            </label>
                                                            <input type="file" id="logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <br>
                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addteam" class="addteam"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/newdevelopments') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 " style="    margin-top: 89px;" >


                            </div>

                        </div>

                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
     $('.summernote').on('summernote.keyup', function(e) {
        debugger;
        var text = $(this).next('.note-editor').find('.note-editable').text();
        var length = text.length;
        var num = 500 - length;

        if (length > 500) {
            $('#limite_normal').hide();
            $('#limite_vermelho').text((500 - length)+' characters extra!').show();
        }
        else{
            $('#limite_vermelho').hide();
            $('#limite_normal').text((500 - length)+' characters remaining!').show();
        }

    });
    $('#addteam').click(function () {
        var form = new FormData();

        if ($('#team_name').val() == '') {
            $('#team_name').focus();
            $('#team_name').css('border-color', 'red');
            return false;
        } else {
            $('#team_name').css('border-color', '');

        }
        if ($('#designation').val() == '') {
            $('#designation').focus();
            $('#designation').css('border-color', 'red');
            return false;
        } else {
            $('#designation').css('border-color', '');

        }
        if ($('#description').val() == '') {
            $('#description').focus();
            $('#description').css('border-color', 'red');
            return false;
        } else {
            $('#description').css('border-color', '');

        }
        form.append('json[team_name]', $('#team_name').val());
        form.append('json[designation]', $('#designation').val());
        form.append('json[description]', $('#description').val());
        form.append('json[position]', $('#position').val());
        form.append('json[team_id]', $('#team_id').val());

     
        form.append('_token', CSRF_TOKEN);

        var fileInput = document.querySelector('#logo');
        form.append('logo', fileInput.files[0]);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addteam");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/team';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
