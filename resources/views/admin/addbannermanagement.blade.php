@include('admin.includes.header')
<?php
$bannerId = !empty($_REQUEST['bannerId']) ? base64_decode($_REQUEST['bannerId']) : '';
$pArray = \App\Helpers\LibHelper::GetbannermanagementBybannerId($bannerId);
//print_r($pArray);
//exit;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Banner Management <?= !empty($pArray->banner_id) ? 'Updates' : 'Add' ?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->banner_id) ? $pArray->banner_id : '' ?>" id="banner_id">
                                                <div class="row">


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Type <span style="color:red">*</span></label>
                                                            <select class="form-control" id="banner_type">
                                                                <?php
                                                                $banner_type = isset($pArray->banner_type) ? $pArray->banner_type : '1';
                                                                ?>
                                                                <option <?= $banner_type == 'HOME PAGE' ? 'selected' : '' ?> value="HOME PAGE">HOME PAGE (1920*800 px)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Title <span style="color:red">*</span></label>
                                                            <input class="form-control" type="text" value="<?= !empty($pArray->title) ? $pArray->title : '' ?>" id="title" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Position <span style="color:red">*</span></label>
                                                            <input id="position" value="<?= isset($pArray->position) ? $pArray->position : '' ?>" type="number" class="form-control" autocomplete="off">

                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Logo 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?> <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                            </label>
                                                            <input type="file" id="logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <br>
                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addteam" class="addteam"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/newdevelopments') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 " style="    margin-top: 89px;" >


                            </div>

                        </div>

                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addteam').click(function () {
        var form = new FormData();

        if ($('#title').val() == '') {
            $('#title').focus();
            $('#title').css('border-color', 'red');
            return false;
        } else {
            $('#title').css('border-color', '');

        }
        if ($('#description').val() == '') {
            $('#description').focus();
            $('#description').css('border-color', 'red');
            return false;
        } else {
            $('#description').css('border-color', '');

        }

        form.append('json[banner_type]', $('#banner_type').val());
        form.append('json[title]', $('#title').val());
     
        form.append('json[banner_id]', $('#banner_id').val());
        form.append('json[position]', $('#position').val());
        form.append('_token', CSRF_TOKEN);

        var fileInput = document.querySelector('#logo');
        form.append('logo', fileInput.files[0]);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addbannermanagement");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/bannermanagement';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
