@include('admin.includes.header')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">My Inquiry</span>


                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Offer No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                     <th>Property Name</th>
                                   <th>Reference</th>
                                    <th>status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cond = '';

                                $select = ",MP.property_title";
                                $join = "  LEFT JOIN my_property MP ON MP.property_id=EM.property_id";
                                $qry = "SELECT EM.* $select FROM `enquiry_management` EM $join WHERE EM.archive=0 $cond ORDER BY enqid DESC";
                                $pArray = \App\Database::select($qry);
                                for ($i = 0; $i < count($pArray); $i++) {
                                    $d = $pArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->enq_number ?></td>
                                        <td><?= $d->name ?> - <?= $d->mobile_no ?></td>
                                        <td><?= $d->email ?></td>

                                        <td><?= $d->property_title ?></td>
                                        <td><?= !empty($d->page_url) ? '<a href="'.$d->page_url.'" target="_blank">Link</a>' : '' ?></td>
                                        <td><span class="label label-sm label-info ">ACTIVE</span></td>

                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a data-toggle="tooltip"  data-placement="top" title="Edit" href="<?= url('admin/enquiry-details?enqId=' . base64_encode($d->enqid)); ?>" ><span class="label label-sm label-info"><i class="fa fa-external-link"></i></span></a> 
                                            <?php if (empty($d->enq_status)) { ?>
                                                <a data-toggle="tooltip" data-placement="top" title="Remove" title="Remove" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"enqid":"<?= $d->enqid ?>"}' dbtable="enquiry_management" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                                    <?php } ?>
                                        </td>
                                    </tr>  
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')