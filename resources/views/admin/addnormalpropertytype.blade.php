@include('admin.includes.header')
<?php
$Id = !empty($_REQUEST['Id']) ? base64_decode($_REQUEST['Id']) : '';
$aArray = \App\Helpers\LibHelper::GetpropertytypeothersById($Id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->id) ? 'Add New' : 'Update'; ?> Type</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="id" value="<?= !empty($aArray->id) ? $aArray->id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input id="nameeeee" value="<?= isset($aArray->name) ? $aArray->name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                
            



                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addkeywords" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addkeywords').click(function () {
        var form = new FormData();

        if ($('#nameeeee').val() == '') {
            $('#nameeeee').focus();
            $('#nameeeee').css('border-color', 'red');
            return false;
        } else {
            $('#nameeeee').css('border-color', '');
            form.append('json[name]', $('#nameeeee').val());
        }
     

        form.append('json[id]', $('#id').val());


        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addnormal-propertytype");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/normal-propertytype';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
