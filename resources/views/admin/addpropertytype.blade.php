@include('admin.includes.header')
<?php
$ptypeId = !empty($_REQUEST['ptypeId']) ? base64_decode($_REQUEST['ptypeId']) : '';
$aArray = \App\Helpers\LibHelper::GetpropertytypeBy($ptypeId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($aArray->ptype_id) ? 'Add New' : 'Update'; ?> Property Type</span>
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="ptype_id" value="<?= !empty($aArray->ptype_id) ? $aArray->ptype_id : '' ?>">
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="control-label col-md-3">Type</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="ptype">
                                            <?php
                                            $ptype = isset($aArray->ptype) ? $aArray->ptype : '';
                                            ?>
                                            <option  value="0">BUY</option>
                                            <option <?= $ptype == '1' ? 'selected' : '' ?> value="1">RENT</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Property Name</label>
                                    <div class="col-md-4">
                                        <input id="ptype_name" updateId="slugs" XXonkeyup="convertToSlug(this);" value="<?= isset($aArray->ptype_name) ? $aArray->ptype_name : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                              
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Banner 
                                            <?php
                                            $file = "files/hostgallery/" . (!empty($aArray->banner) ? $aArray->banner : '');
                                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                            }
                                            ?> 
                                            <br><span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                        </label>
                                          <div class="col-md-4">
                                        <input type="file" id="banner" class="form-control" autocomplete="off">
                                          </div>
                                    </div>
                               

                                <div class="form-group">
                                    <label class="control-label col-md-3">Slugs</label>
                                    <div class="col-md-4">
                                        <input id="slugs" value="<?= isset($aArray->slugs) ? $aArray->slugs : '' ?>" type="text" class="form-control slugsvalidate" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Position</label>
                                    <div class="col-md-4">
                                        <input id="position" value="<?= isset($aArray->position) ? $aArray->position : '' ?>" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea id="description" class="form-control summernote" autocomplete="off"><?= isset($aArray->description) ? $aArray->description : '' ?></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addservicecategory" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addservicecategory').click(function () {
        var form = new FormData();

        if ($('#ptype_name').val() == '') {
            $('#ptype_name').focus();
            $('#ptype_name').css('border-color', 'red');
            return false;
        } else {
            $('#ptype_name').css('border-color', '');
            form.append('json[ptype_name]', $('#ptype_name').val());
        }
        if ($('#slugs').val() == '') {
            $('#slugs').focus();
            $('#slugs').css('border-color', 'red');
            return false;
        } else {
            $('#slugs').css('border-color', '');
            form.append('json[slugs]', $('#slugs').val());
        }

        form.append('json[position]', $('#position').val());
        form.append('json[ptype]', $('#ptype').val());
        form.append('json[description]', $('#description').val());
        form.append('json[ptype_id]', $('#ptype_id').val());

        var fileInput = document.querySelector('#banner');
        form.append('banner', fileInput.files[0]);

        form.append('_token', CSRF_TOKEN);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addpropertytype");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/propertytype';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
