</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?= date('Y') ?> &copy; Excel Property&nbsp;|&nbsp;
        <a href="https://alwafaagroup.com" title="Alwafaa Group" target="_blank">Alwafaa Group</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->
<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->
<?php
$Method = '';
$roughtName = Route::getCurrentRoute()->getActionMethod();
?>
<script>
    var base_url = '<?= url('/'); ?>';
    var CSRF_TOKEN = "{{ csrf_token() }}";
    var Method = '<?= $roughtName ?>';
</script>
<script src="{{url('/public')}}/js/lightbox-plus-jquery.min.js"></script> 
<script src="{{url('/public')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{url('/public')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="{{url('/public')}}/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<script src="{{url('/public')}}/assets/pages/scripts/DatatableClass.js" type="text/javascript"></script> 
<script src="{{url('/public')}}/assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{url('/public')}}/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>

<script src="https://momentjs.com/downloads/moment.min.js" type="text/javascript"></script>
<script src="{{url('/public')}}/assets/js/bootstrap-select.min.js"></script>
<script src="{{url('/public')}}/js/jquery-confirm.min.js"></script>
<script src="{{url('/public')}}/lib/jquery-ui.js"></script>
<script src="{{url('/public')}}/lib/summernote.js"></script>
<script src="https://crm.sensationstation.ae/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="https://crm.sensationstation.ae/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<script src="{{url('/public')}}/js/common.js"></script>






<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>