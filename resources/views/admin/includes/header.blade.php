<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Excel Property</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{url('/public')}}/css/jquery-confirm.min.css"> 
        <link rel="stylesheet" href="{{url('/public')}}/js/jquery-ui.css"> 
<!--        <link href="{{url('/public')}}/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />-->
 <link href="{{url('/public')}}/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <link href="https://crm.sensationstation.ae/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('/public')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!--<link href="{{url('/public')}}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />-->
        <link href="{{url('/public')}}/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/layouts/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('/public')}}/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/public')}}/assets/css/bootstrap-select.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">

        <link href="{{url('/public')}}/css/lightbox.min.css" rel="stylesheet">
        <link href="{{url('/public')}}/css/main.css" rel="stylesheet" type="text/css" >   
        <link href="{{url('/public')}}/css/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="{{url('/public/images/favicon.ico')}}" type="image/x-icon"> 
        <link rel="icon" href="{{url('/public/images/favicon.ico')}}" type="image/x-icon">



        <link href="https://fonts.googleapis.com/css2?family=KoHo:wght@500&display=swap" rel="stylesheet">
    </head> 
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
        <div class="page-wrapper">
            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner ">
                    <?php
                    $Session = \App\Helpers\CommonHelper::GetSessionArrayVendor();

                    $permission = \App\Helpers\CommonHelper::permissionFormate($Session->permission);
                    ?>
                    <div class="page-logo">
                        <a href="<?= url('admin') ?>">
                            <img src="<?= url('/public/images/admin-logo_s.png') ?>" style="width: 150px; margin-top: 9px;" alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">

                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <?php
                                $Sql = "SELECT SQL_CALC_FOUND_ROWS * FROM `notification` WHERE admin_read=0 ORDER BY timestamp DESC LIMIT 50";
                                $nArray = \App\Database::select($Sql);

                                $calRow = "SELECT FOUND_ROWS() AS TotalRows";
                                $calRow = \App\Database::select($calRow);
                                $calRow = !empty($calRow) ? $calRow[0]->TotalRows : 0;
                                $calRowIcon = $calRow > 50 ? '+' : '';
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> <?= count($nArray) . $calRowIcon ?>  </span>
                                </a>
                                <ul class="dropdown-menu">

                                    <li class="external">
                                        <h3><span class="bold"><?= count($nArray) . $calRowIcon ?> pending</span> notifications</h3>
                                        <a onclick="ADMIN.notificationsAdminReadAll(this);" href="javascript:void(0)">clear all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283" id="HeaderNotificationHtml">
                                            <?php
                                            $addIcon = [];
                                            for ($i = 0; $i < count($nArray); $i++) {
                                                $d = $nArray[$i];
                                                $icon = '<i class="fa fa-bell-o"></i>';
                                                if (in_array($d->notification_type, $addIcon)) {
                                                    $icon = '<i class="fa fa-plus"></i>';
                                                }
                                                ?>
                                                <li>
                                                    <a href="javascript:void(0)" onclick="ADMIN.notificationsOnclick(this);" notificationId="<?= $d->notification_id ?>" fhref="<?= $d->notification_url ?>">
                                                        <span class="time"><?= \App\Helpers\CommonHelper::time_elapsed_string($d->timestamp) ?></span>
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success"><?= $icon ?></span>
                                                            <?= $d->notification_title ?>
                                                        </span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img  alt="" class="img-circle" src="<?= 'https://demo.softwarecompany.ae/call-tekky/images/AdminLogo.png' ?>" />
                                    <span class="username username-hide-on-mobile"> <?= $Session->vendor_name ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">

                                    <li>
                                        <a href="<?= url('admin-logout'); ?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="">
                            <?php
                            if (in_array('Additionalsettings', $permission) || $Session->vendor_id == '1') {
                                ?>
                                <li class="nav-item start">
                                    <a href="javascript:void(0)" class="nav-link nav-toggle">
                                        <i class="fab fa-cc-mastercard"></i>
                                        <span class="title">Additional settings</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="<?= url('admin/propertytype'); ?>" class="nav-link ">
                                                <span class="title">Property Type (Buy/Rent)</span>
                                            </a>
                                        </li>
                                         <li class="nav-item">
                                            <a href="<?= url('admin/normal-propertytype'); ?>" class="nav-link ">
                                                <span class="title">Property Type (Normal)</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('admin/propertyprice'); ?>" class="nav-link ">
                                                <span class="title">Property Price</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('admin/newdevelopments'); ?>" class="nav-link ">
                                                <span class="title">New Developments</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('admin/tags'); ?>" class="nav-link ">
                                                <span class="title">Tags</span>
                                            </a>
                                        </li>
                                        <li class="nav-item" >
                                            <a href="<?= url('admin/arealist'); ?>" class="nav-link ">
                                                <span class="title">Area List</span>
                                            </a>
                                        </li>

                                        <li class="nav-item ">
                                            <a href="<?= url('admin/team'); ?>" class="nav-link ">
                                                <span class="title">Team</span>
                                            </a>
                                        </li>

                                        <li class="nav-item ">
                                            <a href="<?= url('admin/keywords'); ?>" class="nav-link ">
                                                <span class="title">Keywords</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/tower'); ?>" class="nav-link ">
                                                <span class="title">Tower</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/agents'); ?>" class="nav-link ">
                                                <span class="title">Agents</span>
                                            </a>
                                        </li>
                                        <?php
                                        $Sql = "SELECT * FROM `master_details` WHERE archive=0 ORDER BY master_id ASC";
                                        $submaster = \App\Database::select($Sql);
                                        for ($i = 0; $i < count($submaster); $i++) {
                                            $d = $submaster[$i];
                                            ?>
                                            <li class="nav-item  ">
                                                <a href="<?= url('admin/submaster?masterId=' . base64_encode($d->master_id)); ?>" class="nav-link ">
                                                    <span class="title"><?= $d->master_name ?></span>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>







                                <li class="nav-item start">
                                    <a href="javascript:void(0)" XXXhref="<?= url('admin/customers'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-recycle"></i>
                                        <span class="title">CMS Management</span>
                                        <span class="arrow"></span>
                                    </a>

                                    <ul class="sub-menu">
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/cms'); ?>" class="nav-link ">
                                                <span class="title">General CMS</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/listwithuscms'); ?>" class="nav-link ">
                                                <span class="title">List With US CMS</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/servicescms'); ?>" class="nav-link ">
                                                <span class="title">Services CMS</span>
                                            </a>
                                        </li>




                                        <li class="nav-item ">
                                            <a href="<?= url('admin/privacy-policy-cms'); ?>" class="nav-link ">
                                                <span class="title">Privacy Policy CMS</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/terms-conditios-cms'); ?>" class="nav-link ">
                                                <span class="title">Terms Conditions CMS</span>
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a href="<?= url('admin/about-us-cms'); ?>" class="nav-link ">
                                                <span class="title">About US  CMS</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>




                            <?php } if ($Session->vendor_id == '1') { ?>

                                <li class="nav-item start">
                                    <a href="<?= url('admin/subadmin'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-users"></i>
                                        <span class="title">Sub Admin</span>
                                    </a>
                                </li>
                            <?php }if (in_array('Blogs', $permission) || $Session->vendor_id == '1') { ?>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/bannermanagement'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-users"></i>
                                        <span class="title">Banner Management</span>
                                    </a>
                                </li>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/blogs'); ?>" class="nav-link nav-toggle">
                                        <i class="fab fa-bandcamp"></i>
                                        <span class="title">Blogs</span>
                                    </a>
                                </li>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/areaguides'); ?>" class="nav-link nav-toggle">
                                        <i class="fab fa-bandcamp"></i>
                                        <span class="title">Area Guides</span>
                                    </a>
                                </li>

                            <?php }if (in_array('Testimonial', $permission) || $Session->vendor_id == '1') { ?>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/testimonial'); ?>" class="nav-link nav-toggle">
                                        <i class="fab fa-bandcamp"></i>
                                        <span class="title">Testimonial</span>
                                    </a>
                                </li>
                            <?php } if (in_array('Facility', $permission) || $Session->vendor_id == '1') { ?>

                                <li class="nav-item start">
                                    <a href="<?= url('admin/propertyfacility'); ?>" class="nav-link nav-toggle">
                                        <i class="fab fa-bandcamp"></i>
                                        <span class="title">Amenities</span>
                                    </a>
                                </li>

                            <?php } if (in_array('Customers', $permission) || $Session->vendor_id == '1') { ?>
                                <li class="nav-item start">
                                    <a href="javascript:void(0)" XXXhref="<?= url('admin/customers'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-recycle"></i>
                                        <span class="title">Careers Management</span>
                                        <span class="arrow"></span>
                                    </a>

                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="<?= url('admin/careerscms'); ?>" class="nav-link ">
                                                <span class="title">CMS</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('admin/careersenquiry'); ?>" class="nav-link ">
                                                <span class="title">Inquiry</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } if (in_array('MyProperty', $permission) || $Session->vendor_id == '1') { ?>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/myproperty'); ?>" class="nav-link nav-toggle">
                                        <i class="fab fa-bitcoin"></i>
                                        <span class="title">Listings</span>
                                    </a>
                                </li>
                            <?php } if (in_array('Enquiry', $permission) || $Session->vendor_id == '1') { ?>

                                <li class="nav-item start">
                                    <a href="<?= url('admin/myenquiry'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-at"></i>
                                        <span class="title">Property Inquiry</span>
                                    </a>
                                </li>

                            <?php } if (in_array('Contactus', $permission) || $Session->vendor_id == '1') { ?>
                                <li class="nav-item start">
                                    <a href="<?= url('admin/contactus'); ?>" class="nav-link nav-toggle">
                                        <i class="fas fa-envelope-open"></i>
                                        <span class="title">General Inquiry</span>
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                </div> 

                <style>
                
                    body, 
                    .dropdown-menu {
                       font-family: 'Proxima Nova Rg';
                    }
                
                    
                    .Technician,.Sub.admin{
                        background-color: #9C27B0 !important;
                    }
                    .label-success.Admin{
                        background-color: #E91E63 !important;
                    }
                    .dropdown-notification .icon-bell{
                        color: #fff !important;
                    }
                    .dropdown-notification .badge-default{
                        font-weight: 600 !important;
                    }
                    .dropdown-notification .dropdown-menu {
                        width: 375px !important;
                        max-width: 375px !important;
                    }
                    .dropdown-notification .time {
                        max-width: 90px !important;
                    }
                    .size-alert{
                        font-size: 12px;
                        color: #e7505a;
                        margin-left: 5px;
                    }
                    .pac-logo{
                        z-index: 999999999 !important;
                    }
                </style>