@include('admin.includes.header')
<?php
$agentsId = !empty($_REQUEST['agentsId']) ? base64_decode($_REQUEST['agentsId']) : '';
$pArray = \App\Helpers\LibHelper::GetagentsByagentsId($agentsId);
//print_r($pArray);
//exit;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 
                        <!-- BEGIN FORM-->


                        <div class="row">
                            <div class="col-md-6 ">
                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption font-green-sunglo">
                                            <span class="caption-subject bold uppercase"> Agent <?= !empty($pArray->ndid) ? 'Updates' : 'Add' ?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <input type="hidden" value="<?= !empty($pArray->agents_id) ? $pArray->agents_id : '' ?>" id="agents_id">
                                                <div class="row">


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Name <span style="color:red">*</span></label>
                                                            <input class="form-control"  value="<?= !empty($pArray->name) ? $pArray->name : '' ?>" id="name" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Post <span style="color:red">*</span></label>
                                                            <input class="form-control" type="text" value="<?= !empty($pArray->post) ? $pArray->post : 'Engineer' ?>" id="post" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Reference No<span style="color:red">*</span></label>
                                                            <input type="text" value="<?= !empty($pArray->reference_no) ? $pArray->reference_no : '' ?>" id="reference_no" class="form-control" placeholder="" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Logo 
                                                                <?php
                                                                $file = "files/hostgallery/" . (!empty($pArray->logo) ? $pArray->logo : '');
                                                                if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                                                    ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                                }
                                                                ?>  <span class="size-alert">Min width and height - 200 * 200 px</span>
                                                            </label>
                                                            <input type="file" id="logo" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>


                                                    <br>
                                                </div>
                                            </div>
                                            <alertmessage></alertmessage>
                                            <div class="form-actions">
                                                <a rtype="0" id="addagents" class="addagents"><button type="button" class="btn blue">Save</button></a>

                                                <a href="<?= url('admin/agents') ?>"> <button type="button" class="btn default">Cancel</button></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 " style="    margin-top: 89px;" >


                            </div>

                        </div>

                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>

<!-- END CONTENT -->
@include('admin.includes.footer')

<script>
    $('#addagents').click(function () {
        var form = new FormData();

        if ($('#name').val() == '') {
            $('#name').focus();
            $('#name').css('border-color', 'red');
            return false;
        } else {
            $('#name').css('border-color', '');
        }
        if ($('#post').val() == '') {
            $('#post').focus();
            $('#post').css('border-color', 'red');
            return false;
        } else {
            $('#post').css('border-color', '');
        }
        if ($('#reference_no').val() == '') {
            $('#reference_no').focus();
            $('#reference_no').css('border-color', 'red');
            return false;
        } else {
            $('#reference_no').css('border-color', '');

        }
       
        form.append('json[name]', $('#name').val());
        form.append('json[post]', $('#post').val());
        form.append('json[reference_no]', $('#reference_no').val());
      
        form.append('json[agents_id]', $('#agents_id').val());

       
        form.append('_token', CSRF_TOKEN);

        var fileInput = document.querySelector('#logo');
        form.append('logo', fileInput.files[0]);

        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addagents");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/agents';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>
