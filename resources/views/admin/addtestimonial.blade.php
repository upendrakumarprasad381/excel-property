@include('admin.includes.header')
<?php
$Id = !empty($_REQUEST['Id']) ? base64_decode($_REQUEST['Id']) : '';
$bArray = \App\Helpers\LibHelper::GettestimonialById($Id);
?>
<style>

    .modal-title {
        margin: 0;
        line-height: 1.42857143;
        font-size: 16px;
        text-transform: uppercase;
        padding: 3px 0;
    }

    .pac-container, .pac-item{ z-index: 2147483647 !important; }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= !empty($bArray->blog_id) == '' ? 'Add New' : 'Update'; ?> Testimonial</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="id" value="<?= !empty($bArray->id) ? $bArray->id : '' ?>">

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Title</label>
                                    <div class="col-md-4">
                                        <input id="titile" value="<?= isset($bArray->titile) ? $bArray->titile : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Sub Title</label>
                                    <div class="col-md-4">
                                        <input id="sub_titile" value="<?= isset($bArray->sub_titile) ? $bArray->sub_titile : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
 <div class="form-group">
                                    <label class="control-label col-md-3">Position</label>
                                    <div class="col-md-4">
                                        <input id="position" value="<?= isset($bArray->position) ? $bArray->position : '' ?>" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Link</label>
                                    <div class="col-md-4">
                                        <input id="link" value="<?= isset($bArray->link) ? $bArray->link : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3">Logo <br> <span class="size-alert">Min width and height - 128 * 128 px</span></label>
                                    <div class="col-md-4">
                                        <input id="attachment"  type="file" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Description</label>
                                    <div class="col-md-6">
                                        <textarea value="" id="description" class="form-control summernote" rows="5"><?= !empty($bArray->description) ? $bArray->description : '' ?></textarea>
                                    </div>
                                </div>







                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="addtestimonial" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '<?= url('admin/testimonial'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')
<script>
    $('#addtestimonial').click(function () {
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);

        form.append('json[id]', $('#id').val());
        form.append('json[titile]', $('#titile').val());
        form.append('json[sub_titile]', $('#sub_titile').val());
         form.append('json[position]', $('#position').val());
        form.append('json[link]', $('#link').val());
        form.append('json[description]', $('#description').val());


        if ($('#titile').val() == '') {
            $('#titile').css('border-color', 'red');
            $('#titile').focus();
            return false;
        } else {
            $('#titile').css('border-color', '');
        }
        if ($('#sub_titile').val() == '') {
            $('#sub_titile').css('border-color', 'red');
            $('#sub_titile').focus();
            return false;
        } else {
            $('#sub_titile').css('border-color', '');
        }
        if ($('#link').val() == '') {
            $('#link').css('border-color', 'red');
            $('#link').focus();
            return false;
        } else {
            $('#link').css('border-color', '');
        }
        if ($('#description').val() == '') {
            $('#description').css('border-color', 'red');
            $('#description').focus();
            return false;
        } else {
            $('#description').css('border-color', '');
        }
        var fileInput = document.querySelector('#attachment');
        form.append('attachment', fileInput.files[0]);




        $.confirm({
            title: 'Are you sure want to submit ?',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-green',
                    action: function () {
                        var json = ajaxpost(form, "/admin/addtestimonial");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                window.location = base_url + '/admin/testimonial';
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                },
                cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-warning',
                    action: function () {
                    }
                },
            }
        });
    });
</script>