@include('admin.includes.header')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Area Guides</span>
                            <a href="<?= url('admin/addareaguides') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>


                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Area</th>
                                    <th>Position</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $select = "";
                                $join = "  ";
                                $Sql = "SELECT SC.* $select FROM `area_guides` SC $join WHERE SC.archive=0 ORDER BY SC.position ASC";
                                $dArray = \App\Database::select($Sql);
                                $option = '';
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    $option = $option . ' <li id="' . $d->guides_id . '" >' . $d->area_name . ' </li>';
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->area_name ?></td>

                                        <td><?= $d->position ?></td>
                                        <td><?= date('d/m/Y h:i A', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Edit" href="<?= url('admin/addareaguides?guidesId=' . base64_encode($d->guides_id)); ?>" ><span class="label label-sm label-success"><i class="far fa-edit"></i></span></a>
                                            <a title="Edit" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"guides_id":"<?= $d->guides_id ?>"}' dbtable="area_guides" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>






                        <div class="row">

                            <div class="col-sm-12">
                                <p><strong>Note: select and drag and drop to assign the position.</strong></p>
                            </div>
                            <div class="col-sm-12">
                                <ul id="sortable">
                                    <?= $option ?>
                                </ul>
                            </div>
                            <div class="col-sm-12">
                                <button onclick="POSITIONSET.areaguidesUpdatespos();" type="button" class="btn btn-primary">Submit</button>
                            </div>




                            <div class="col-sm-12" style="margin-top: 15px;">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">CMS Settings</div>
                                    <div class="panel-body">
                                        <form action="" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <?php
                                                    if (isset($_POST['AREA_GUIDES_SETTINGS'])) {
                                                        $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
                                                        $json['lastupdate'] = date('Y-m-d H:i:s');
                                                        $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'AREA_GUIDES_SETTINGS');

                                                        $file = !empty($_FILES['img_dev_banner_col2']) ? $_FILES['img_dev_banner_col2'] : [];
                                                        if (!empty($file['name'])) {
                                                            $fileName = uniqid() . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                                                            $baseDir = Config::get('constants.HOME_DIR') . "files/hostgallery/" . $fileName;
                                                            if (move_uploaded_file($file['tmp_name'], $baseDir)) {
                                                                $oldfile = Config::get('constants.HOME_DIR') . "files/hostgallery/" . (!empty($pArray->col2) ? $pArray->col2 : '');
                                                                $json['col2'] = $fileName;
                                                                if (is_file($oldfile)) {
                                                                    unlink($oldfile);
                                                                }
                                                            }
                                                        }


                                                        \App\Database::updates('cms', $json, array('cms_id' => 'AREA_GUIDES_SETTINGS'));
                                                    }
                                                    $pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'AREA_GUIDES_SETTINGS');
                                                    ?>
                                                   
                                                    <div class="form-group">
                                                        <label>Banner Title <span style="color:red">*</span></label>
                                                        <input type="text" name="json[col1]" value="<?= !empty($pArray->col1) ? $pArray->col1 : '' ?>"  required class="form-control"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Banner
                                                            <?php
                                                            
                                                           
                                                            $file = "files/hostgallery/" . (!empty($pArray->col2) ? $pArray->col2 : '');
                                                            if (is_file(Config::get('constants.HOME_DIR') . $file) && !empty($pArray->col2)) {
                                                                ?><a target="_blank" href="<?= url($file) ?>">View</a><?php
                                                            }
                                                            ?> <span class="size-alert">Min width and height - 1920 * 1000 px</span>
                                                        </label>
                                                        <input type="file" id="img_dev_banner_col2" name="img_dev_banner_col2" class="form-control" placeholder="" autocomplete="off">
                                                    </div>

                                                    

                                                    <div class="form-group">
                                                        <button type="submit" name="AREA_GUIDES_SETTINGS" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

@include('admin.includes.footer')
