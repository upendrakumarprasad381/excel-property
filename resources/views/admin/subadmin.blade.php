@include('admin.includes.header')
<!-- BEGIN CONTENT -->
<?php
$usertype = !empty($_REQUEST['usertype']) ? $_REQUEST['usertype'] : '0';
?>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Sub admin</span>

                            <a href="<?= url('admin/addsubadmin') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $usertype = !empty($_REQUEST['usertype']) ? $_REQUEST['usertype'] : '0';
                                $cond = '';
                                
                                $Sql = "SELECT VD.* FROM `vendor_details` VD WHERE VD.archive=0 AND VD.vendor_id NOT IN(1) $cond ORDER BY VD.vendor_id DESC";
                                $dArray = \App\Database::select($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->first_name . ' ' . $d->last_name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td><?= $d->mobile ?></td>
                                      
                                        <td><?= date('d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td style="width: 100px;">
                                            <a title="Update"   href="<?= url('admin/addsubadmin?vendorId=' . base64_encode($d->vendor_id)) ?>" class=""><span class="label label-sm label-success"><i class="fas fa-edit"></i></span></a>
                                            <a title="Remove" href="javascript:void(0)" updatejson='{"archive":"1","lastupdate":"<?= date('Y-m-d H:i:s') ?>"}'  condjson='{"vendor_id":"<?= $d->vendor_id ?>"}' dbtable="vendor_details" href="javascript:void(0)" class="autoupdate"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('admin.includes.footer')