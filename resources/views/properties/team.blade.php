@include('properties.includes.header')

<?php
$TEAM_CMS_PAGE = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'TEAM_CMS_PAGE');


$file = "files/hostgallery/" . (!empty($TEAM_CMS_PAGE->col4) ? $TEAM_CMS_PAGE->col4 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
   $file= url($file);
}else{
    $file=url('files/hostgallery/615aa74cb2566.jpg');
}
?>
<div class="wd100 __innerbanner __team_banner" style="background: url(<?= $file ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)"> Team</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <h2><?= !empty($TEAM_CMS_PAGE->col1) ? $TEAM_CMS_PAGE->col1 : '' ?></h2>
        <div class="__description"><?= !empty($TEAM_CMS_PAGE->col2) ? $TEAM_CMS_PAGE->col2 : '' ?></div>
    </div>
</div>
<section class="section __teamlsPg">
    
    <div class="container">
        <div class="wd100 __teamlsPgTxbz">
            <p><?= !empty($TEAM_CMS_PAGE->col3) ? $TEAM_CMS_PAGE->col3 : '' ?></p>
        </div>
    </div>


    <div class="container ">
        <div class="wd100">
            <div class="__teamBwrap row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-3">

                <?php
                $Sql = "SELECT * FROM `team` WHERE archive=0 ORDER BY position ASC";
                $lost = \App\Database::select($Sql);
                for ($i = 0; $i < count($lost); $i++) {
                    $d = $lost[$i];
                    $baseDir = "files/hostgallery/" . $d->logo;
                    $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                    ?>
                    <div class="__teamBBoz">
                        <div class="__teamEInr wd100"> 
                            <div class="view view-first"> 
                                <img src="{{url($baseDir)}}" />
                                <div class="mask">
                                    <div class="__makInScll">
                                    <p><?= $d->description ?></p> 
                                    </div>
                                </div>

                            </div>

                            <div class="__teamDcrc wd100">
                                <h5><?= $d->team_name ?></h5>
                                <h6><?= $d->designation ?></h6>
                            </div>
                        </div> 
                    </div>
                <?php } ?>





            </div>






        </div>
    </div>
</section>
@include('properties.includes.footer')