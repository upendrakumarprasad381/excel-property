@include('properties.includes.header')

<?php

$projectsIdslug = Request::segment(3);


$Sql = "SELECT ndid,heading  FROM `new_developments` WHERE `slugs` LIKE '$projectsIdslug' AND archive=0";
$datalist = \App\Database::selectSingle($Sql);
$projectsId = !empty($datalist->ndid) ? $datalist->ndid : '';

$bannerView = url("public/images/Area-Guides.jpg");
if (empty($projectsId)) {
    $tag = App\Helpers\LibHelper::GettagseByslugsId($projectsIdslug);
}
if(!empty($tag)){
    $file = "files/hostgallery/" . (!empty($tag->banner) ? $tag->banner : '');
    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
        $bannerView = url($file);
    }
}
?>

<div class="wd100 __innerbanner __rentlbnr" data-overlay="dark" data-opacity="4" style="background: url(<?= $bannerView ?>) no-repeat center center;">


    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?= url('/dubai/real-estate-developers') ?>">New Developments In Dubai</a></li>
                    <?php if (!empty($tag)) {
                        ?><li class="breadcrumb-item"><a href=""><?= $tag->tag_name ?></a></li><?php
                        }

                        if (!empty($datalist)) {
                            ?><li class="breadcrumb-item"><a href="<?= url("/dubai/real-estate-developers/$projectsIdslug") ?>"><?= $datalist->heading ?></a></li><?php }
                        ?>
                </ol>
            </nav>
        </div>
    </div>


    <div class="container">
        <h2><?= !empty($tag->tag_name) ? $tag->tag_name : '' ?></h2>
    </div>


</div>


<section class="section __scoutinner __area_guidesPg">
    <div class="container">
       
        <div class="row">
            <?php
            $cond = "";

            if (!empty($tag)) {
                $cond = $cond . " AND FIND_IN_SET('$tag->tag_id',SC.tag_id)";
            }


            $Sql = "SELECT SC.heading,SC.ndid,SC.banner,SC.slugs,P.heading AS parents_name,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id NOT IN (0) $cond ORDER BY P.position ASC,SC.position ASC ";

            $listAr = \App\Database::select($Sql);
            for ($i = 0; $i < count($listAr); $i++) {
                $d = $listAr[$i];
                $file = "files/hostgallery/" . (!empty($d->banner) ? $d->banner : '');
                $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                $url = url("dubai/real-estate-developers/$d->parents_slugs/$d->slugs");
                ?>
                <div class="col-lg-4 col-md-6 col-sm-12 __aGBoz">
                    <a href="<?= $url ?>" class="wd100 __aGBozir">
                        <img class="img-fluid" src="{{url($file)}}" >
                        <div class="__aGBozirTX"><?=  $d->heading ?></div>
                    </a > 
                </div>
            <?php } ?>
        </div>

       
            <h3><?= $tag->tag_name ?></h3>
            <p><?= $tag->description ?></p>
      
    </div>
</section>
@include('properties.includes.footer')	
<!--flagS-->
