@include('properties.includes.header')
<?php
$TESTIMONIAL_CMS_PAGE = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'TESTIMONIAL_CMS_PAGE');

$file = "files/hostgallery/" . (!empty($TESTIMONIAL_CMS_PAGE->col1) ? $TESTIMONIAL_CMS_PAGE->col1 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("files/hostgallery/615e852f1368e.jpg");
}
?>
<div class="wd100 __innerbanner __testimonal_banner" style="background: url(<?= $file ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Testimonal</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2><?= (!empty($TESTIMONIAL_CMS_PAGE->col2) ? $TESTIMONIAL_CMS_PAGE->col2 : '') ?></h2>
        <div class="__description"> <?= (!empty($TESTIMONIAL_CMS_PAGE->col3) ? $TESTIMONIAL_CMS_PAGE->col3 : '') ?></div>
    </div>
</div>




<div class="container">
    <div class="wd100 __testimonalsTxbz">
        <h3><?= (!empty($TESTIMONIAL_CMS_PAGE->col4) ? $TESTIMONIAL_CMS_PAGE->col4 : '') ?></h3>

        <img class="img-fluid mb-3" src="{{url('/public/lib/images/google-review-graphic.png')}}">
        <div class="wd100 text-center mb-4">
            <a href="https://search.google.com/local/writereview?placeid=ChIJf5HFUbBDXz4Rsk1KTNO_9RM" target="_blank" class="__write_reviewBnt"><img src="{{url('/public/lib/images/write.png')}}" /> Write a Review</a>
        </div>
        <p><?= (!empty($TESTIMONIAL_CMS_PAGE->col5) ? $TESTIMONIAL_CMS_PAGE->col5 : '') ?></p>

    </div>
</div>


<section class="section __testimonalsPg" style="padding: 0px">
<!--    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    <div class="elfsight-app-757bcfed-1621-4b61-bec6-3e9cddae9ab3"></div>-->
    
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-f5afdc5c-8e46-43fc-bd5b-b98d35935eb7"></div>
    
    
    <div class="container">

        <div class="__sRVBwrap __testimonalsBwrap row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-2">
            <?php
            $Sql = "SELECT * FROM `testimonial` WHERE archive=0 ORDER BY position DESC";
            $blogs = App\Database::select($Sql);
            for ($i = 0; $i < count($blogs); $i++) {
                $d = $blogs[$i];
                $baseDir = "files/hostgallery/" . $d->attachment;
                $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                ?>
                <div class="__sRVBBoz" style="display: none;">
                    <a target="_blank" href="{{$d->link}}">  
                        <div class="__sRVBQEInr wd100">

                            <div class="wd100 text-center  "> 
                                <div class="__testimonalsBwrapImg">
                                    <img class="img-fluid" src="{{url($baseDir)}}"> 
                                </div>
                            </div>

                            <div class="__txtMoContDecrp wd100">
                                <p><?= $d->description ?></p>
                            </div>

                            <div class="__txtMoDecrp  ">
                                <div class="  wd100 __xdfgvsdf">
                                    <?= $d->titile ?>
                                </div>
                            </div>

                        </div>
                    </a>
                </div>
            <?php } ?>



        </div>


    </div>




    <style>
        .LoadMoreButton__Component-sc-5z801y-1{
            background-color: #fafafa !important;
            color: #000 !important;
            font-weight: bold;
                margin-bottom: 30px;
        }
        .eYDagm { 
            color: #fff !important;
        }
        .jRKmsw {
            border-radius: unset !important;
        }
    </style>


</section>
@include('properties.includes.footer')

