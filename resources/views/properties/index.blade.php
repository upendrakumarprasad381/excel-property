@include('properties.includes.header')
<?php
$whychoise = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'WHY_CHOOSE_EXCEL_PROPERTIES');
$FILTER_SETTINGS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'FILTER_SETTINGS');
$bedRoomHide = [6, 9];
?>
<section class="section __banner">
    <div class="main-slider">
        <?php
        $banner = \App\Helpers\LibHelper::GetbannermanagementBybannertype($bannerType = 'HOME PAGE');
//        echo '<pre>';
//        print_r($banner);
//        exit; 

        if (!empty($banner)) {
            for ($i = 0; $i < count($banner); $i++) {
                $d = $banner[$i];
                $baseDir = "files/hostgallery/" . $d->logo;
                $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? url($baseDir) : url('/public/lib/images/banner1.jpg');
                ?>
                <div class="item image"> 
                    <figure>
                        <div class="slide-image slide-media" style="background-image:url('{{$baseDir}}');">
                            <img data-lazy="{{$baseDir}}" class="image-entity" />
                        </div>
                    </figure>
                </div>
                <?php
            }
        } else {
            ?>

            <div class="item image">
                <figure>
                    <div class="slide-image slide-media" style="background-image:url('https://demo.softwarecompany.ae/excel_properties/images/banner1.jpg');">
                        <img data-lazy="https://demo.softwarecompany.ae/excel_properties/images/banner1.jpg" class="image-entity" />
                    </div>
                </figure>
            </div>

            <div class="item image"> 
                <figure>
                    <div class="slide-image slide-media" style="background-image:url('https://demo.softwarecompany.ae/excel_properties/images/banner2.jpg');">
                        <img data-lazy="https://demo.softwarecompany.ae/excel_properties/images/banner2.jpg" class="image-entity" />
                    </div>
                </figure>
            </div>

            <div class="item image"> 
                <figure>
                    <div class="slide-image slide-media" style="background-image:url('https://demo.softwarecompany.ae/excel_properties/images/banner3.jpg');">
                        <img data-lazy="https://demo.softwarecompany.ae/excel_properties/images/banner3.jpg" class="image-entity" />
                    </div>
                </figure>
            </div>

        <?php } ?>


<!--        <div class="item youtube">
            <iframe class="embed-player slide-media" width="980" height="520" src="https://www.youtube-nocookie.com/embed/lsOcPZ0lUlc?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=lsOcPZ0lUlc&start=1" frameborder="0" allowfullscreen></iframe> 
            <p class="caption">YouTube</p> 
        </div>

       <div class="item youtube">
            <iframe class="embed-player slide-media" width="980" height="520" src="https://www.youtube-nocookie.com/embed/-hhILh3OAbI?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=-hhILh3OAbI&start=1" frameborder="0" allowfullscreen></iframe> 
            <p class="caption">YouTube</p> 
        </div>-->
 



    </div>
    <div id="carouselExampleCaptions" style="display: none;" class="carousel slide carousel-fade" data-bs-ride="carousel">

        <div class="carousel-inner">
            <?php
            if (!empty($banner)) {
                for ($i = 0; $i < count($banner); $i++) {
                    $d = $banner[$i];
                    $baseDir = "files/hostgallery/" . $d->logo;
                    $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? url($baseDir) : url('/public/lib/images/banner1.jpg');
                    ?>
                    <div class="carousel-item <?= empty($i) ? 'active' : '' ?>">
                        <img src="{{$baseDir}}" class="d-block w-100" alt="...">
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="carousel-item active">
                    <img src="{{url('/public/lib/images/banner1.jpg')}}" class="d-block w-100" alt="...">

                </div>
                <div class="carousel-item">
                    <img src="{{url('/public/lib/images/banner2.jpg')}}" class="d-block w-100" alt="...">

                </div>
                <div class="carousel-item">
                    <img src="{{url('/public/lib/images/banner3.jpg')}}" class="d-block w-100" alt="...">
                </div>
            <?php } ?>
        </div>

    </div>
</section>

<section class="section __bnrbtmscrwrp">


    <div class="container">
        <div class="__mob_srclose">
            <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class="__brtabbz">
            <div class="__brinlogo wd100">
                <img class="img-fluid" src="{{url('/public/lib/images/logo/logo.svg')}}" />
            </div>
            <div class="__bnrSrcWrp">
                <div class="wd100 __bnrtxsct">
                    <h2>SEARCH YOUR NEXT HOME</h2>
                    <h3>WE HAVE SOMETHING FOR YOU</h3>
                </div>
                <nav class="wd100">
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link srchlink active" ptype="0" onclick="loadpropertyType();" id="nav-bnr_sr_sale-tab" data-bs-toggle="tab" data-bs-target="#nav-bnr_sr_sale" type="button" role="tab" aria-controls="nav-bnr_sr_sale" aria-selected="true">BUY</button>
                        <button class="nav-link srchlink" ptype="1" onclick="loadpropertyType();" id="nav-bnr_sr_rent-tab" data-bs-toggle="tab" data-bs-target="#nav-bnr_sr_sale" type="button" role="tab" aria-controls="nav-bnr_sr_rent" aria-selected="false">RENT</button>
                    </div>
                </nav>
                <div class="tab-content wd100" id="nav-tabContent">
                    <div class="tab-pane   show active" id="nav-bnr_sr_sale" role="tabpanel" aria-labelledby="nav-bnr_sr_sale-tab">
                        <div class="wd100 __tbzfmzwap __sr_sale_bxz">
                            <div class="row">

                                <div class="__tfdsyl __bnrkeyfld col-sm-12">
                                    <input type="text" class="form-control" placeholder="LOCATION OR TOWER" id="myInput" onkeyup="myFunction()" autocomplete="off">
                                </div>
                                <ul id="myUL" style="display: none;">                      
                                    <?php
                                    $towerList = " UNION ALL SELECT TL.tower_name AS address,COUNT(MP.property_id) AS total FROM `my_property` MP LEFT JOIN tower_list TL ON TL.tower_id=MP.tower_id WHERE MP.status=0 AND MP.archive=0  GROUP BY MP.tower_id";
                                    $Sql = "SELECT address,COUNT(property_id) AS total FROM `my_property` WHERE status=0 AND archive=0  GROUP BY address $towerList";
                                    $list = \App\Database::select($Sql);
                                    for ($i = 0; $i < count($list); $i++) {
                                        $d = $list[$i];
                                        ?>
                                        <li ><a value="{{$d->address}}" class="searchable" href="javascript:void(0)"><?= $d->address . ' (' . $d->total . ')' ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="__tfdsyl __protyp  __bedroom col-lg-4 col-md-12 col-sm-12 ">
                                    <select class="form-select" id="ptype_name" aria-label="Default select example">
                                        <option value="">PROPERTY TYPE</option>
                                    </select>
<!--                                    <input class="form-control" list="datalistOptions" id="ptype_name" placeholder="PROPERTY TYPE">
                                    <datalist id="datalistOptions">

                                    </datalist>-->
                                </div>
                                <div class="__tfdsyl __bedroom col ">
                                    <select class="form-select" id="bedroom" aria-label="Default select example">
                                        <option value="" selected>BEDROOM</option>
                                        <option value="studio">Studio</option>
                                        <?php
                                        $bathroom = $FILTER_SETTINGS->col1 > 0 ? $FILTER_SETTINGS->col1 : 10;
                                        for ($i = 1; $i <= $bathroom; $i++) {
                                            ?>
                                            <option value="{{$i}}">{{$i}} Bedroom</option>
                                        <?php } ?>

                                    </select>
                                </div>

                                <div class="__tfdsyl __bnr_pric __minPric col">
                                    <select class="form-select" id="min_price" aria-label="Default select example">
                                        <option selected>Min. Price</option>

                                    </select>
                                </div>

                                <div class="__tfdsyl __bnr_pric __maxPric col">
                                    <select class="form-select" id="max_price" aria-label="Default select example">
                                        <option selected>Max. Price</option>

                                    </select>
                                </div>
                                <div class="col-12  __srcfld">
                                    <button type="button" id="searchQry" class="btn  __search_btn">SEARCH</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section> 

<?php
$Sql = "SELECT * FROM tags WHERE page_id='HOME_PAGE' ORDER BY position ASC";
$tagsAr = \App\Database::select($Sql);


$Sql = "SELECT SC.heading,SC.ndid,SC.slugs,SC.banner,SC.parents_id,P.heading AS parents_name,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id LEFT JOIN tag_position TP ON TP.foreign_id=SC.ndid AND TP.tag_id='20' WHERE SC.archive=0 AND TP.id IS NOT NULL  AND FIND_IN_SET('20',SC.tag_id) GROUP BY SC.ndid ORDER BY TP.position ASC LIMIT 3";
$offPlain = \App\Database::select($Sql);
?>
<section class="section __scnB1 __offPlan">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="__scnB1txtboz">
                    <h1><?= !empty($tagsAr[0]->tag_name) ? $tagsAr[0]->tag_name : '' ?></h1>
                    <p><?= !empty($tagsAr[0]->description) ? $tagsAr[0]->description : '' ?></p>
                </div>
            </div>
        </div>
        <?php
        if (!empty($offPlain)) {
            ?>
            <div class="row __dekgridbz">
                <?php
                for ($j = 0; $j < count($offPlain); $j++) {
                    $fg = $offPlain[$j];
                    $file = "files/hostgallery/" . (!empty($fg->banner) ? $fg->banner : '');
                    $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                    $url = url("dubai/real-estate-developers/$fg->parents_slugs/$fg->slugs");
                    if (empty($fg->parents_id)) {
                        $url = url("dubai/real-estate-developers/$fg->slugs");
                    }
                    ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 __prligrdwrp">
                        <div class="__prligrd wd100">
                            <div class="__prligrdimg">
                                <a href="{{$url}}">
                                    <img class="img-fluid" src="{{$file}}">
                                </a>
                                <div class="__proname"> <a href="{{$url}}">{{$fg->heading}}</a> </div>
                            </div>

                        </div>
                    </div>
    <?php } ?>
            </div>


            <div class="inside_box wd100  __mobslider">
                <!-- Swiper S-->
                <div class="owl-carousel owl-theme wd100">
                    <!--<div class="swiper-wrapper">-->

    <?php
    for ($j = 0; $j < count($offPlain); $j++) {
        $fg = $offPlain[$j];
        $file = "files/hostgallery/" . (!empty($fg->banner) ? $fg->banner : '');
        $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
        $url = url("dubai/real-estate-developers/$fg->parents_slugs/$fg->slugs");
        ?>
                        <div class="swiper-slide">
                            <div class="wd100 __prligrdwrp">
                                <div class="__prligrd wd100">
                                    <div class="__prligrdimg">
                                        <a href="{{$url}}">
                                            <img class="img-fluid" src="{{$file}}" />
                                        </a>
                                        <div class="__proname"> <a href="{{$url}}">{{$fg->heading}}</a> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

    <?php } ?>
                </div>
            </div>
            <div class="wd100 text-center">
                <a href="<?= url('dubai/off-plan-properties') ?>" class="__more_properties_bnt">View All Properties</a>
            </div>
<?php } ?>

    </div>
</section>





<?php
$seiderAto = ['__forsale', '__forrent'];
for ($i = 1; $i < count($tagsAr); $i++) {
    $d = $tagsAr[$i];
    if ($d->tag_id == '3') {
        $urlviewMore = url('/dubai/properties-for-rent');
    } else {
        $urlviewMore = url('/dubai/properties-for-sale');
    }
    ?>
    <section class="section __scnB1 <?= !empty($seiderAto[$i]) ? $seiderAto[$i] : '' ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="__scnB1txtboz">
                        <h1><?= $d->tag_name ?></h1>
                        <p><?= $d->description ?></p>
                    </div>
                </div>
            </div>
    <?php
    $Sql = "SELECT MP.*,getpropertyImage(MP.property_id) AS first_image,getpropertyDetailsUrl(MP.property_id) AS urlSlug FROM `my_property` MP LEFT JOIN tag_position TP ON TP.foreign_id=MP.property_id AND TP.tag_id='$d->tag_id' WHERE MP.archive=0 AND TP.id IS NOT NULL AND MP.status=0  AND FIND_IN_SET('$d->tag_id',MP.tag_id) GROUP BY MP.property_id ORDER BY TP.position ASC LIMIT 3";
    $listAr = \App\Database::select($Sql);
    if (!empty($listAr)) {
        ?>
                <div class="row __dekgridbz">
                <?php
                for ($j = 0; $j < count($listAr); $j++) {
                    $fg = $listAr[$j];
                    $images = \App\Helpers\CommonHelper::GetPropertyImagesCover($fg);
                    $url = url($fg->urlSlug);
                    ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 __prligrdwrp">
                            <div class="__prligrd wd100">
                                <div class="__prligrdimg">
                                    <a href="{{$url}}">
                                        <img class="img-fluid" src="{{$images}}">
                                    </a>
                                    <div class="__proname"> <a href="{{$url}}">{{$fg->property_title}}</a> </div>
                                </div>
                                <div class="__prligrddrcp">
                                    <div class="  __priqustbx"> <small>PRICE</small>
                                        <br>AED <?= \App\Helpers\CommonHelper::DecimalAmount($fg->price) ?></div>
                                    <div style="display: <?= in_array($fg->property_type, $bedRoomHide) ? 'none' : '' ?>" class="  __bedsbx"> <small><?= !empty($fg->is_studio) ? '&nbsp;' : $fg->bed ?></small>
                                        <br><?= !empty($fg->is_studio) ? 'Studio' : 'BED' ?></div>
                                    <div class="  __sqftsbx"><?= \App\Helpers\CommonHelper::DecimalAmount($fg->area) ?> SQ.FT</div>
                                </div>
                            </div>
                        </div>
        <?php } ?>
                </div>


                <div class="inside_box wd100  __mobslider">
                    <!-- Swiper S-->
                    <div class="owl-carousel owl-theme wd100">
                        <!--<div class="swiper-wrapper">-->

        <?php
        for ($j = 0; $j < count($listAr); $j++) {
            $fg = $listAr[$j];
            $images = \App\Helpers\CommonHelper::GetPropertyImagesCover($fg);
            $url = url($fg->urlSlug);
            ?>
                            <div class="swiper-slide">
                                <div class="wd100 __prligrdwrp">
                                    <div class="__prligrd wd100">
                                        <div class="__prligrdimg">
                                            <a href="{{$url}}">
                                                <img class="img-fluid" src="{{$images}}" />
                                            </a>
                                            <div class="__proname"> <a href="{{$url}}">{{$fg->property_title}}</a> 
                                            </div>
                                        </div>
                                        <div class="__prligrddrcp">
                                            <div class="  __priqustbx"> <small>PRICE</small>
                                                <br />AED <?= \App\Helpers\CommonHelper::DecimalAmount($fg->price) ?></div>
                                            <div class="  __bedsbx"> <small><?= $fg->bed ?></small>
                                                <br />BED</div>
                                            <div class="  __sqftsbx"><?= \App\Helpers\CommonHelper::DecimalAmount($fg->area) ?> SQ.FT</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

        <?php } ?>


                        <!--</div>-->
                        <!-- Add Arrows -->
                        <!--                        <div class="swiper-button-next __srbk1-next"></div>
                                                <div class="swiper-button-prev __srbk1-prev"></div>-->


                    </div>
                </div>
                <div class="wd100 text-center">
                    <a href="<?= $urlviewMore ?>" class="__more_properties_bnt">View All Properties</a>
                </div>
    <?php } ?>

        </div>
    </section>
<?php } ?>


<?php
$home_location = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'HOME_PAGE_LOCATION');
$home_location_2 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'HOME_PAGE_LOCATION_2');

$file1 = "files/hostgallery/" . (!empty($home_location_2->col1) ? $home_location_2->col1 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file1) && !empty($home_location_2->col1)) {
    $file1 = url($file1);
} else {
    $file1 = url('/' . Config::get('constants.DEFAULT_PROPERTY_LOGO'));
}

$file2 = "files/hostgallery/" . (!empty($home_location_2->col2) ? $home_location_2->col2 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file2) && !empty($home_location_2->col2)) {
    $file2 = url($file2);
} else {
    $file2 = url('/' . Config::get('constants.DEFAULT_PROPERTY_LOGO'));
}
$file3 = "files/hostgallery/" . (!empty($home_location_2->col3) ? $home_location_2->col3 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file3) && !empty($home_location_2->col3)) {
    $file3 = url($file3);
} else {
    $file3 = url('/' . Config::get('constants.DEFAULT_PROPERTY_LOGO'));
}

$file4 = "files/hostgallery/" . (!empty($home_location_2->col4) ? $home_location_2->col4 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file4) && !empty($home_location_2->col4)) {
    $file4 = url($file4);
} else {
    $file4 = url('/' . Config::get('constants.DEFAULT_PROPERTY_LOGO'));
}

$img = 'https://static.retreatmi.com/files/hostgallery/60af88b8c0b29.jpg';
$tooltip1 = '<div class="infoBox">
                                  <div class="slidebox">
                                      <div class="proimg radius5 wd100">
                                         <a  href="javascript:void(0)"><img class="img-fluid" src="' . $file1 . '" > </a>
                                      </div>
                                    <a  class="cname wd100" href="javascript:void(0)">' . $home_location_2->col5 . '</a>
           
                                 </div>
                              </div>';
$tooltip2 = '<div class="infoBox">
                                  <div class="slidebox">
                                      <div class="proimg radius5 wd100">
                                         <a  href="javascript:void(0)"><img class="img-fluid" src="' . $file2 . '" > </a>
                                      </div>
                                    <a  class="cname wd100" href="javascript:void(0)">' . $home_location_2->col6 . '</a>
           
                                 </div>
                              </div>';
$tooltip3 = '<div class="infoBox">
                                  <div class="slidebox">
                                      <div class="proimg radius5 wd100">
                                         <a  href="javascript:void(0)"><img class="img-fluid" src="' . $file3 . '" > </a>
                                      </div>
                                    <a  class="cname wd100" href="javascript:void(0)">' . $home_location_2->col7 . '</a>
           
                                 </div>
                              </div>';
$tooltip4 = '<div class="infoBox">
                                  <div class="slidebox">
                                      <div class="proimg radius5 wd100">
                                         <a  href="javascript:void(0)"><img class="img-fluid" src="' . $file4 . '" > </a>
                                      </div>
                                    <a  class="cname wd100" href="javascript:void(0)">' . $home_location_2->col8 . '</a>
           
                                 </div>
                              </div>';
?>
<section class="section __scnB2 __community">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h1>OUR COMMUNITIES</h1>  <small>RESIDENTIAL COMMUNITIES IN DUBAI</small>

                <div class="__mapimgvw wd100">

                    <div class="__mappitrvwrap  ">

<?php
$Sql = "SELECT * FROM `area_guides` WHERE archive=0 AND guides_id IN (16,15,3,4) ORDER BY FIELD(guides_id,16,15,3,4) ASC LIMIT 4";
$listAr = \App\Database::select($Sql);
for ($i = 0; $i < count($listAr); $i++) {
    $d = $listAr[$i];
    $redUrl = url("dubai/area-guides/$d->slugs");
    $file = "files/hostgallery/" . (!empty($d->logo) ? $d->logo : '');
    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
        $file = url($file);
    } else {
        $file = url('/' . Config::get('constants.DEFAULT_PROPERTY_LOGO'));
    }
    $tooltip1 = '<div class="infoBox">
                                  <div class="slidebox">
                                      <div class="proimg radius5 wd100">
                                         <a  href="'.$redUrl.'"><img class="img-fluid" src="' . $file . '" > </a>
                                      </div>
                                    <a  class="cname wd100" href="'.$redUrl.'">' . $d->area_name . '</a>
           
                                 </div>
                              </div>';
    ?>
                            <div onclick="window.location ='{{$redUrl}}';" class="__epLab __mapPot<?= $i + 1 ?>" data-tippy-content='<?= $tooltip1 ?>' data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title=''>
                                <img class="img-fluid" src="<?= url('/'); ?>/public/images/ep_label.png" />
                            </div>
    <?php
}
?>
<!--                        <div class="__epLab __mapPot1" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title='<?= $tooltip1 ?>'>
<img class="img-fluid" src="<?= url('/'); ?>/public/images/ep_label.png" />
</div>

<div class="__epLab __mapPot2"  data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title='<?= $tooltip2 ?>'>
<img class="img-fluid" src="<?= url('/'); ?>/public/images/ep_label.png" />
</div>


<div class="__epLab __mapPot3"  data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title='<?= $tooltip3 ?>'>
<img class="img-fluid" src="<?= url('/'); ?>/public/images/ep_label.png" />
</div>


<div class="__epLab __mapPot4"  data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title='<?= $tooltip4 ?>'>
<img class="img-fluid" src="<?= url('/'); ?>/public/images/ep_label.png" />
</div>-->

                        <!--                        <div class="__epLab __mapPot5"  data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="top" title='<= $tooltip4 ?>'>
                                                    <img class="img-fluid" src="<= url('/'); ?>/public/images/ep_label.png" />
                                                </div>-->
                    </div>


                    <img class="img-fluid" src="<?= url('/'); ?>/public/images/__community.jpg" />
                </div>
            </div>
        </div>
    </div>

</section>
<section class="section __sbheader">
    <div class="container">
        <h2>WHY CHOOSE EXCEL PROPERTIES</h2>
        <div class="row">

            <div class="__subxz col">
                <div class="___subxz_img">
<?php
$file = "files/hostgallery/" . (!empty($whychoise->col8) ? $whychoise->col8 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url('/public/lib/images/bxz1.png');
}
?>
                    <img src="{{$file}}">
                </div>
                <h3><?= $whychoise->col1 ?> </h3>
                <p style="text-align: justify;"><?= $whychoise->col2 ?></p>
            </div>
            <div class="__subxz col __cerpsc">
                <div class="___subxz_img">
<?php
$file = "files/hostgallery/" . (!empty($whychoise->col9) ? $whychoise->col9 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url('/public/lib/images/bxz1.png');
}
?>
                    <img src="{{$file}}">
                </div>
                <h3><?= $whychoise->col3 ?> </h3>
                <p style="text-align: justify;"><?= $whychoise->col4 ?></p>
            </div>
            <div class="__subxz col ">
                <div class="___subxz_img">
<?php
$file = "files/hostgallery/" . (!empty($whychoise->col10) ? $whychoise->col10 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url('/public/lib/images/bxz1.png');
}
?>
                    <img src="{{$file}}">
                </div>
                <h3><?= $whychoise->col5 ?> </h3>
                <p style="text-align: justify;"><?= $whychoise->col6 ?></p>
            </div>
        </div>
    </div>
</section>


<section class="section  __blogwrpsldr">
    <div class="container">
        <div class="inside_box wd100">
            <!-- Swiper -->
            <div class="swiper-container mySwiper_blog">
                <div class="swiper-wrapper">
<?php
$Sql = "SELECT * FROM `blogs` WHERE archive=0 AND FIND_IN_SET('1',tags) ORDER BY position ASC";
$blogs = App\Database::select($Sql);
for ($i = 0; $i < count($blogs); $i++) {
    $d = $blogs[$i];
    $url = url('blog-detail/' . $d->slugs);
    $baseDir = "files/hostgallery/" . $d->attachment_home;
    $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
    ?>
                        <div class="swiper-slide">
                            <div class="__bgsbz">
                                <div class="__bslimgz">

                                    <div class="__blogdcrp">
                                        <h3><a href="<?= $url ?>"><?= $d->logo_title ?></a></h3>
                                    </div>

                                    <a href="<?= $url ?>">
                                        <img class="img-fluid" src="{{url($baseDir)}}">
                                    </a>

                                </div>
                                <div class="__bsliertxwrp">

                                    <h4><a href="<?= $url ?>"><?= $d->blog_titile ?></a> </h4>

    <?= $d->description_home_page ?>
                                    <div class="__readmore"> <a href="<?= $url ?>" >
                                            Read more
                                        </a> 
                                    </div>
                                </div>
                            </div>
                        </div>
<?php } ?>
                </div>
                <div class="mySwiper_blog-pagination"></div>
            </div>
        </div>
    </div>
</section>



<section class="section __fr2bk __testimonials">
    <div class="container">
        <div class="row">
            <div class="__testimonials_wrp">
                <h2>Actual Customer Reviews </h2>
                <h6>And That’s just a few</h6>
                <div class="inside_box wd100">
                    <!-- Swiper S-->
                    <div class="__srbk4 wd100">



                        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                        <div class="elfsight-app-e9e45b74-bca4-44e6-a5c8-720d24630eca"></div>

<?php /* ?>
  <div class="swiper-wrapper">
  <?php
  $Sql = "SELECT * FROM `testimonial` WHERE archive=0 ORDER BY position ASC";
  $blogs = App\Database::select($Sql);

  for ($i = 0; $i < count($blogs); $i++) {
  $d = $blogs[$i];
  $baseDir = "files/hostgallery/" . $d->attachment;
  $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
  ?>
  <div class="swiper-slide">
  <div class="wd100 __texmrwp">
  <div class="wd100 __textconts">
  <p><?= $d->description ?></p>
  </div>
  <div class="__tezm_drci">
  <div class="d-flex">
  <div class="flex-shrink-0">
  <img class="mr-3" src="{{url($baseDir)}}" alt="">
  </div>
  <div class="flex-grow-1 ms-3">
  <h5 class="mt-1"><?= $d->titile ?></h5>
  <h6><?= $d->sub_titile ?></h6>
  <div class="wd100">
  <div style="display: none;" class="__test_starWrp"> <i class="fa fa-star" aria-hidden="true"></i>  <i class="fa fa-star" aria-hidden="true"></i>  <i class="fa fa-star" aria-hidden="true"></i>  <i class="fa fa-star" aria-hidden="true"></i>  <i class="fa fa-star" aria-hidden="true"></i>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  <?php } ?>


  </div>
  <?php */ ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--tag start-->




<!--tag end-->



<!--<div class="wd100 __newsletter">
    <div class="container"> 
        <h2>Sign Up For our Weekly<br/> Newsletter for Market Update</h2>
        <div class="wd100">

            <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter your email address">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button">
                        <i class="fas fa-arrow-right"></i>
                    </button>
                </div> 	  
            </div> 

        </div>
    </div>
</div>-->


<!--<div class="wd100 __currency_units">
    <div class="container"> 
        <h2>Currency & Units</h2>

        <div class=" col-lg-12 col-md-12 col-sm-12 mb-4"> 
            <input type="text" class="__aedfld form-control"  placeholder="AED">
        </div>
        <div class="row" style="padding: 0 8%;">

            <div class="__sqftfld col "> 
                <input type="text" class="form-control"  placeholder="SQ FT">
            </div>

            <div class="__sqmfld col"> 
                <input type="text" class="form-control"  placeholder="SQ M">
            </div>


        </div>
    </div>
</div>-->


<!--<div class="wd100 __mv_follow">
    <div class="container"> 
        <h2>Follow US</h2>

        <div class="_social_icons_top">
            <a target="_blank" href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
            <a target="_blank" href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
            <a target="_blank" href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
            <a target="_blank" href="#"><i class="fab fa-youtube" aria-hidden="true"></i></a> 
            <a target="_blank" href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
        </div>

    </div>
</div>-->

<!--<div class="wd100 __mv_customer_service">
    <div class="container"> 

        <div class="__mvCsBoz">
            <div class="__cstext">Customer Service </div>
            <div class="__csnubr">
                <a href="tel:+971 50 660 6100">+971 50 660 6100</a>
            </div>
            <div class="__csWhaspp">
                <a href="#"><img src="{{url('/public/lib/images/whatsapp.svg')}}"></a>
            </div> 
        </div>

        <a href="#" class="__bntcots">
            Contact Us
        </a>

    </div>
</div>-->




@include('properties.includes.footer')


<script src='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js'></script>

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'>
<script  src="https://demo.softwarecompany.ae/excel_properties/js/slick_script.js"></script>



<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@5"></script>
<link rel="stylesheet" href="https://unpkg.com/tippy.js@5/dist/backdrop.css" />


<style>
    input::-webkit-calendar-picker-indicator {
        display: none !important;
    }
    #myUL {
        display: none; 
    }
    #myUL li a {
        display: block; /* Make it into a block element to fill the whole list */
    }
    .__testimonials_wrp img{
        width: 100px !important;
        object-fit: cover!important
    }
</style>
<script src="https://demo.softwarecompany.ae/excel_properties/js/owl.carousel.js"></script>
<script>

                                tippy('.__epLab', {
                                    allowHTML: true,
                                    interactive: true,
                                    placement: 'right',
                                });
                                $(document).ready(function () {
                                    var owl = $('.owl-carousel');
                                    owl.owlCarousel({
                                        stagePadding: 30,
                                        margin: 10,
                                        dots: false,
                                        //nav: true,
                                        loop: true,
                                        responsive: {
                                            0: {
                                                items: 1
                                            },
                                            600: {
                                                items: 1
                                            },
                                            1000: {
                                                items: 1
                                            }
                                        }
                                    })
                                })
</script>
<script>

    loadpropertyType();

    $('#searchQry').click(function () {
        var ptype = $('.srchlink.active').attr('ptype');
        var ptypeUrl = ptype == '1' ? 'properties-for-rent' : 'properties-for-sale';
        var url = base_url + '/dubai/' + ptypeUrl + '/search?';

        url = url + '&location=' + ($('#myInput').val());
        if ($('#myInput').val() == '') {
            $('#myInput').focus();
            return false;
        }
        url = url + '&ptype_name=' + ($('#ptype_name').val());
        url = url + '&bedroom=' + ($('#bedroom').val());
        url = url + '&min_price=' + ($('#min_price').val());
        url = url + '&max_price=' + ($('#max_price').val());
        window.location = url;
    });

    function loadpropertyType() {
        var ptype = $('.srchlink.active').attr('ptype');

        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('ptypeId', ptype);
        form.append('encode', true);
        form.append('helper', 'Lib');
        form.append('function', 'GetpropertyTypeByptypeHTMLId');
        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {

                    if (json.status == true) {

                        $('#ptype_name').html(json.property_type);
                        $('#max_price').html(json.max_price);
                        $('#min_price').html(json.min_price);
                        $('#myUL').html(json.searchHTML);
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
        return false;
//        $.ajax

    }
    function myFunction() {
        // Declare variables
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById('myInput');
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName('li');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
    $("#myInput").keyup(function () {
        var x = document.getElementById('myUL');
        if ($(this).val() == "") {
            x.style.display = 'none';
        } else {
            x.style.display = 'block';
        }

        var $page = $('#myUL .searchable');
        $page.each(function (i, a) {
            $a = $(a)
            $a.html($a.html().replace(/<em>/g, "").replace(/\<\/em\>/g, ""))
        })
        var searchedText = $('#myInput').val();
        if (searchedText != "") {
            $page.each(function (i, a) {
                $a = $(a)
                var html = $a.text().replace(new RegExp("(" + searchedText + ")", "igm"), "<em>$1</em>")
                $a.html(html)
            })
        }
    });
    $('body').on('click', '.searchable', function () {

        var val = $(this).attr('value');
        $('#myInput').val(val);
        $('#myUL').hide();
    });
</script>
