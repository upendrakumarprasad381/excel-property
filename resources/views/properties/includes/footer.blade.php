<?php
$contactUs = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US');
?>
<section class="section __tagwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h2>Browse Properties</h2>

                <div class="wd100 __tagwrapboz">
                    <?php
                    $listData = \App\Helpers\LibHelper::GetFootermenuClick();
                    for ($i = 0; $i < count($listData); $i++) {
                        $d = $listData[$i];
                        ?><a href="<?= $d['url'] ?>" class="__dxtags hvr-bounce-to-right"><?= $d['title'] ?></a><?php
                    }
                    $Sql = "SELECT * FROM `keywords` WHERE archive=0";
                    $listData = \App\Database::select($Sql);
                    for ($i = 0; $i < count($listData); $i++) {
                        $d = $listData[$i];
                        ?><a href="<?= $d->url ?>" class="__dxtags hvr-bounce-to-right"><?= $d->name ?></a><?php
                    }
                    ?>

                </div> 
            </div>
        </div>
    </div>
</section>
<footer class="wd100">

    <div class="footer wd100">
        <div class="container">
            <div class="row">
                <div class="col ft_menu __ftcosmll">
                    <h6>Company </h6>
                    <ul>
                        <li><a href="<?= url('dubai/about-us') ?>">About Us </a></li>    
                        <li><a href="<?= url('dubai/services') ?>">Services  </a></li>          
                        <li><a href="<?= url('dubai/careers') ?>">Careers</a></li>
                        <li><a href="<?= url('dubai/blog') ?>">Blog</a></li> 
                        <li><a href="<?= url('dubai/site-map') ?>">Site Map</a></li>  
                    </ul>  
                </div>


                <div class="col ft_menu">
                    <h6>Properties For Buy</h6>
                    <ul>
                        <?php
                        $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
                        $count = count($dArray) > 4 ? 4 : count($dArray);
                        for ($i = 0; $i < $count; $i++) {
                            $d = $dArray[$i];
                            ?><li><a href="<?= url("dubai/properties-for-sale/$d->slugs") ?>"><?= $d->ptype_name ?></a></li><?php
                        }
                        ?>
                    </ul>  
                </div>


                <div class="col ft_menu">
                    <h6>Properties For Rent</h6>
                    <ul>
                        <?php
                        $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
                        $count = count($dArray) > 4 ? 4 : count($dArray);
                        for ($i = 0; $i < $count; $i++) {
                            $d = $dArray[$i];
                            ?><li><a href="<?= url("dubai/properties-for-rent/$d->slugs") ?>"><?= $d->ptype_name ?></a></li><?php
                        }
                        ?>
                    </ul>  
                </div>

                <div class="col ft_menu">
                    <!--<h6>Popular Areas In Dubai</h6>-->
                    <h6>Areas Guides</h6>
                    <ul>
                        <?php
                        $Sql = "SELECT * FROM `area_guides` WHERE archive=0 ORDER BY position ASC LIMIT 4";
                        $dlist = App\Database::select($Sql);
                        for ($i = 0; $i < count($dlist); $i++) {
                            $d = $dlist[$i];
                            ?>
                            <li><a href="<?= url("dubai/area-guides/$d->slugs") ?>"><?= $d->area_name ?></a></li>
                        <?php } ?>

                    </ul>  
                </div>

                <!--
        
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ft_menu">
        
                                                <div class="wd100 mb-3 f_info">
        
        
                                                <div class="d-flex">
                                                  <div class="flex-shrink-0">
                                                        <img src="{{url('/public/lib/images/email.svg')}}">
                                                  </div>
                                                  <div class="flex-grow-1 ms-3">
                                                        <b>Mail</b>  <br>
                                                        <a href="mailto:booking@group-tech.ae">booking@group-tech.ae</a>
                                                  </div>
                                                </div>
        
        
        
        
                                                </div>
        
                                                <div class="wd100 mb-3 f_info">
        
                                                        <div class="d-flex">
                                                          <div class="flex-shrink-0">
                                                                <img src="{{url('/public/lib/images/phone-call.svg')}}">
                                                          </div>
                                                          <div class="flex-grow-1 ms-3">
                                                                <b>Phone Number</b>  <br>
                                                                <a href="tel:+971589540499">+971 58 9540 499</a>
                                                          </div>
                                                        </div>
        
        
                                                </div>
        
                                                <div class="wd100 mb-3 f_info">
        
                                                                <div class="d-flex">
                                                          <div class="flex-shrink-0">
                                                                <img src="{{url('/public/lib/images/pin.svg')}}">
                                                          </div>
                                                          <div class="flex-grow-1 ms-3">
                                                                <b>Address</b>  <br>
                                                                Dubai, United Arab Emirates
                                                          </div>
                                                        </div>
        
        
        
        
                                                </div>
                                        </div>
                -->




            </div>
        </div>

    </div>
    <div class="__footerlogo wd100">
        <img src="{{url('/public/lib/images/logo/footer_logo.svg')}}" />
    </div>

    <div class="copy_right wd100"> 
        <div class="container"> 
            <div class="wd100 __ftsubMenu">
                <ul>
                    <li><a href="<?= url('dubai/privacy-policy') ?>">Privacy Policy</a></li>
                    <li><a href="<?= url('dubai/terms-conditios') ?>">Terms & Condition</a></li>
                    <li><a href="<?= url('dubai/careers') ?>">Careers</a></li>

                    <li><a href="<?= url('dubai/contact-us') ?>">Contact Us</a></li>
                    <li><a href="<?= url('dubai/site-map') ?>">Site Map</a></li>  

                </ul>
            </div>
            <div class="wd100 __subftsct"> 
                <div class="__ftettxr">
                    © 2001-<?= date('Y') ?>, <span>EXCEL PROPERTIES LLC.</span> ALL RIGHT RESERVED. 
                    <!--<a target="_blank" href="https://www.alwafaagroup.com/"> Web Design | Alwafaa Group, Dubai</a>-->
                </div>

                <div class="_social_icons_top">
                    <a target="_blank" href="https://www.facebook.com/ExcelProperties1"><i class="fab fa-facebook-f"></i></a>
                    <a target="_blank" href="https://twitter.com/PropertiesExcel"><i class="fab fa-twitter"></i></a>
                    <!--<a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>-->
                    <a target="_blank" href="https://www.youtube.com/channel/UCMJwg4ZzwspdObYR-uSRypA"><i class="fab fa-youtube"></i></a> 
                    <a target="_blank" href="https://www.instagram.com/excel_properties/"><i class="fab fa-instagram"></i></a>
                </div> 

            </div>

        </div>
    </div>
</footer>

</div>
</div>
<div class="dpx-whatsapp-lower">
    <a href="https://web.whatsapp.com/send?phone=<?= $contactUs->col7 ?>&amp;text=Hello Excel Properties, I need more information regarding PROPERTIES IN DUBAI." target="_blank" class="tmg-whatsapp" title="Chat with us on WhatsApp" data-toggle="tooltip" data-placement="bottom">
        <div class="dpx-whatsapp-text"> 
            <div class="dpx-whatsapp-icon">
                <img src="data:image/webp;base64,UklGRnQIAABXRUJQVlA4TGcIAAAvO8AOEFXRjW3ttaTkHxzv3vv/t7XW4snW6glD3tcjbBIYGQKuimA8bA2dAMrrIYXxnj3mrSIFOSIFtIjgolUKpNGk0B4EoFPA/BFJjMjgxUBhTgJoiEB7OoMpUhivU2gTGQB4YOJ+VdjSpTqCERHg0wQg2+sgUJ1C41LHAotREdwqkhgVxUsAjTsp4JHCdAp4TKeA1gmMiACzU0BFoUyItm2brfplYZncFKfPtm3btm3btm3btiFBAMAyku4+P6jSNh3btm3bttf2btlEkqzK65Pzomfgc4TRF/FtMCr4WU/AhkPrHKBBkgP8OcBpUOIAFw4wG6Q4wHyohSshYxAxuJIhuILGZBTGAQwBTEpiChoHhhCNTRgKS7LQCAvOI5E8igjDOFJMkRAkPmq5heSRJYQnRGMSDBiyELXFzAw3q7y0JGt1jbN/pGxyoWxqxTUyZWtuKczJyAr0SNSWkCg+iebTuQqj0XBVgbEWki/ZWKm4MK9icafu4pH79ZPv94+P94+/5+NL/e2L6s0za2NLuruFLMS8RaJRGHEJ50pQFyouK6neu+b5/IGQvf+sbxy9Q6k2OhacS+FISxoBV2XoG5A8mV42FSsHnq8fAOpvnjmHJoorqvKSE7LDA7JDfXLjIooKC+wdPdX71wCg5uhWQXqSIkIpEsySiHEVhiKj7JOflvANgMrNs/zUhFQrLVVdhCmg+QoCTIYAJqOpKowkI4XscD/n4AQA9+sXW2sH4ygnchWBCBlcRrEQPAWZaQ2P7wD384eivKxEQwXOAgoDtDMpZQqX06JcTtEZAswBVV0oJ9xvGOD9+eXoHhiQGNySCHXlpSasB2q2TtLdrWKMJTA5jUKMMgSYm30CeH//WFs7FCGmXUSieXm6h03txSOgYnkv2VJDYQm6EEYIRGsUFlA1hKyNTYDn41tRYV4fV0AREIoYoyoZFQtbvn8WqveuJFtpdXOg3SCLMDIfMccIhX4wVXUBR1cfUHf+IN3FpPUf7eiyizxfv+B5/5LhZdd9wxOKGJdqb5Dp46BqijwREtGEJAO5qo0j+P5YrvEFmcHNF1qJWxJ1paq2L4CS0hK9zZD4iB3qr564n97ZW9ti8dCiZmaASz8a7l5lh3hJBI9mRldhdsYM1BzeSjFRmiAkASrD2+4dwPv1I8VMJRYaMQSoKpCjbwT9zoExmY9ao5Exv6lsagkozM1qa4krNMI5PHMCgKKiAsYQ4qFFFUTK9HPy8VioOb5zryxACG3ErFQ7vZqTe/C8fkpzNjOJIUAF2lUAUHf+KFFHYk1o1J6oLVaxsIGH85JiLBiXVhLGlRsT5n58g/KZlQRtMVMwBDxfc3zPDymWWnoYbVxBKa2swq2ljS0SxRPV6LKigjzP5y9Ym1pUgOYb0S/Lppd8f38AWJuanoiHMXxOTmwEUD61xBBhjqHJcIlGWBuafP8sFBUVtegkxuTFRbjfvgBV60diCoUZn5Dhaef9+YOqjbMENSGZIQhN1oxzdPYA+ZkpsTVEsXjVxgngfniVYqHBEBAO3ZHmaOR++YTqnYtEPZkvIyXm3eZ+egdUzK31cCkpPPfLB+2Gz3qkHxe16ETUI1GI972f3wBbS1tMJ6I444AJ83/wi09I97Dz/vxC1eZZgobQLtr09BUX5GyMd435REQ6E+NcQxMdgL13KEFDpGk4l1JUdRF7z5C1rSvFUkNBhHaDKMoQkBMTApRPrwzMMTQRh+TGhY1G+eSSqiVh+hkTjPKZtVuB6t1rWYFu1G2tbwAOtbd2pNjo6T9QW7SkrAwnPDI5Stp8mupoVHt6D+6nd2kOBk1+xFUghSEceN/tACrmN2wtHfBvuHuRbKq0i2AIULUlfsd5+clx/wttDIWjKBxwJlCQlaZIKS1+ZDTpVx+K4IuTzVRkBkdmhreD95eFmpN7qbY6Mo3QiAwJ4yjKz/F8/sDsRGOl9gBkzFcEiJwwn2EzTgjg/WOVz2yYa+YLe2cf+p3DU1/qpJEYl2QkV7V7BSjKz4vqAYjirzc/VD63NXqG9/uX5/vXEa7JpUQDOZkBSGGBTC+7DjQ8vMmO8OsUWtENlxVXVa1E7cm9JCMlhoAgJOa3yoiU7mhQkJXxSEl1TVaI12+rhXlM5dIW+s9uUjiKdqKrIDtrBVwTC6qm2PxgRGL4lxaKTyL5xkgCxBrDFNamNqDu6kmGh03nP6TZZ4whnAk8FWNySnzN8CBEwj+66oHiyirgpJLSor5e0kxIIlyyoVzN4Q10ZHhYjZ3wdFuMIdKUIEQUnd+UqC9ja24DRm101ioRgctyokIaHt+gavUgQVuiPUlfJtPTpiA7Izch6kbOEhgLzNej8Xh8Taz9AlWFkennUrm6B3Q4h6eGf7KEtIu32pradoVzcOJBa3PbMAAVSzv5iRGbHdiyuWmaOiXoSp3i7B0C8MyFUyQhppkiYPZWzG8A0xG05uiOrbOrMC8nOyLotkxvh6xgj4K0BGtdXcXKHgDUnj94VpHgFCGqmSIgJAqR5mhUd/YAADrq717V7F2rmNuoPb63EgG9vP8Q8O83z7cQPOYSigRZMI6inIzpo9Zv80VpTd0bx9yfaqNXWl1TtXnyt+fjGwJuvNvRu2f5Oa0eM1NQRNbIEsypVWsHpQ1NObFhKRZqZqeF5JHFuPkyzdepaohkBbgU5ufYO3vKp5cqV/YqVw/e/rykrCg3MijJRGV8a+wfCjuc3iW/qmpCFoqvSxagYiSEIajbQvJ1jWcI+Kf5yS8lnKPvy15hUKSIlp4VF4ICC9HD5LQ5f/xx8hoSgjQNTwgK0xDCIM3Da9xDaRxgNihxgB8H2AbiAJ+XBkEOcOcAZw4wBeIAx4FBkANsOMDIAYZAQ+EA" title="Chat with us on WhatsApp" alt="Chat with us on WhatsApp">
            </div>
        </div>
    </a> 
</div>
<a id="back-to-top" href="#" class="back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
    <?php
    $roughtName = Route::getCurrentRoute()->getActionMethod();
    ?>
<script>
    var base_url = '<?= url('/'); ?>';
    var CSRF_TOKEN = "{{ csrf_token() }}";
    var Method = '<?= $roughtName ?>';
</script>
<script src="{{url('/public/lib/js/jquery.min.js')}}"></script>
<script src="{{url('/public/lib/js/bootstrap.bundle.js')}}"></script>  
<script src="{{url('/public/lib/js/swiper-bundle.min.js')}}"></script>   
<script src="{{url('/public')}}/js/jquery-confirm.min.js"></script>
<!-- mmenu scripts -->

<script src="{{url('/public')}}/js/jquery.cookie.min.js"></script>
<link rel="stylesheet" href="{{url('/public/lib/js/')}}/bootstrap-select.min.css">
<script src="{{url('/public/lib/js/')}}/bootstrap-select.min.js"></script>


<script src="{{url('/public/lib/js/mmenu.polyfills.js')}}"></script>
<script src="{{url('/public/lib/js/mmenu.js')}}"></script>
<script src="{{url('/public/lib/js/jquery-ui.min.js')}}"></script>

<script src="{{url('/public/js/properties-app.js')}}"></script>
<script src="{{url('/public/js/common.js')}}"></script>

<!--slider-range E-->
<script>
    // Set a cookie
    var cookieId = 'my-notice-accepted';
    if (!$.cookie(cookieId)) {
        setTimeout(function () {
            $(".cookie-disclaimer").slideDown(1000);
        }, 100);
    }

    function acceptMycookie() {
      //  alert('okkkk')
        $.cookie(cookieId, 1, {expires: 30});

        $(".cookie-disclaimer").slideUp(2000);
    }
</script>
</html>