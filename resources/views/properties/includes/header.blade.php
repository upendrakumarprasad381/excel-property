<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
        <meta name="viewport" content="width=device-width minimum-scale=1.0 maximum-scale=1.0 user-scalable=no" /> 
        <link rel="stylesheet" href="{{url('/public/lib/css/bootstrap.css')}}">
        <!--<link rel="stylesheet" href="{{url('/public/lib/css/style.css')}}">--> 
        <!--<link type="text/css" rel="stylesheet" href="{{url('/public/lib/css/mmenu.css')}}" />-->
        <link rel="stylesheet" href="{{url('/public')}}/css/style.css"> 
        <link rel="stylesheet" href="{{url('/public')}}/css/mmenu.css"> 
        <link rel="stylesheet" href="{{url('/public')}}/css/owl.carousel.css"> 
        <link rel="stylesheet" href="{{url('/public/lib/css/swiper.css')}}"> 
        <link rel='stylesheet' href="{{url('/public/lib/css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{url('/public')}}/css/jquery-confirm.min.css"> 
        <title>Excel Properties </title>
        <link rel="apple-touch-icon" sizes="57x57" href="{{url('/public/lib/images/favicons/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{url('/public/lib/images/favicons/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{url('/public/lib/images/favicons/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{url('/public/lib/images/favicons/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{url('/public/lib/images/favicons/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{url('/public/lib/images/favicons/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{url('/public/lib/images/favicons/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{url('/public/lib/images/favicons/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{url('/public/lib/images/favicons/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{url('/public/lib/images/favicons/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{url('/public/lib/images/favicons/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{url('/public/lib/images/favicons/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{url('/public/lib/images/favicons/favicon-16x16.png')}}">
        <link rel="manifest" href="{{url('/public/lib/images/favicons/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{url('/public/lib/images/favicons/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
        <script src="https://kit.fontawesome.com/9cea2b8228.js" crossorigin="anonymous"></script>
    </head>
    <div class="cookie-disclaimer" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12 text-center"> 
                    <span class="disclaimer-content">
                        Cookies allow us to give you the best experience on our website. Please read our Cookie and Privacy Policies for more information. 
                    </span> 
                    <a href="javascript:void(0);" onclick="acceptMycookie();" class="btn btn-accept">Accept</a>
                    <a href="<?= url('dubai/privacy-policy') ?>" class="btn btn-read-policy">No, Read More</a>
                </div>
            </div>
        </div>
    </div>
    <?php
    $roughtName = Route::getCurrentRoute()->getActionMethod();
    $contactUs = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US');
    ?>
    <style>
        .jconfirm .jconfirm-box div.jconfirm-title-c .jconfirm-title, .jconfirm-content,.jconfirm-closeIcon{
            color: #000 !important;
        }
        #valid-msg{
            display: none;
        }
        .validation-msg{
            display: none; 
        }
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>
    <body class="<?= $roughtName == 'index' ? '__homepg ' : ' ' ?>">
        <!--header start-->
        <div id="page"> 
            <div class="content">

                <?php
                $REDIRECT_QUERY_STRING = !empty($_SERVER['REDIRECT_QUERY_STRING']) ? '?' . $_SERVER['REDIRECT_QUERY_STRING'] : '';
                ?>
                <input type="hidden" id="page_url" value="<?php echo URL::current() . $REDIRECT_QUERY_STRING; ?>">

                <nav id="menu" class="mm-menu_theme-dark" style="display: none;">

                    <span class="__closeMmne">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </span>

                    <ul>
                        <li class="Divider"> </li>
                        <li> <a href="javascript:void(0)">BUY</a>
                            <ul>
                                <?php
                                $offplainAprt = App\Helpers\LibHelper::GettagseBytagId($tagId = '21');
                                $offplainVilla = App\Helpers\LibHelper::GettagseBytagId($tagId = '22');
                                ?>
                                <li><a href="<?= url("/dubai/properties-for-sale/off-plan-apartments") ?>">Offplan Apartments</a></li>
                                <li><a href="<?= url("/dubai/properties-for-sale/off-plan-villas") ?>">Offplan Villas</a></li>
                                <?php
                                $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?><li><a href="<?= url("/dubai/properties-for-sale/" . $d->slugs) ?>"><?= $d->ptype_name ?></a></li><?php
                                    }
                                    ?>
                            </ul>
                        </li>
                        <li> <a href="javascript:void(0)">RENT</a>
                            <ul>
                                <?php
                                $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?><li><a href="<?= url("/dubai/properties-for-rent/" . $d->slugs) ?>"><?= $d->ptype_name ?></a></li><?php
                                }
                                ?>
                            </ul>
                        </li>
                        <li><a href="<?= url('dubai/list-with-us') ?>"> LIST WITH US </a></li>
                        <li> <a href="javascript:void(0)">NEW DEVELOPMENTS </a>
                            <ul>
                                <?php
                                $Sql = "SELECT ndid,slugs,heading FROM `new_developments` WHERE archive=0 AND parents_id=0 ORDER BY position ASC LIMIT 6";
                                $dlist = App\Database::select($Sql);
                                for ($i = 0; $i < count($dlist); $i++) {
                                    $d = $dlist[$i];
                                    $Sql = "SELECT SC.ndid,SC.heading,SC.slugs,P.slugs AS parents_slugs FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id='" . $d->ndid . "' ORDER BY SC.position ASC LIMIT 6";
                                    $sublist = App\Database::select($Sql);
                                    ?>
                                    <li><a href="<?= url("dubai/real-estate-developers/$d->slugs") ?>"><?= $d->heading ?></a>
                                        <?php if (!empty($sublist)) { ?>
                                            <ul>
                                                <?php
                                                for ($j = 0; $j < count($sublist); $j++) {
                                                    $ft = $sublist[$j];
                                                    ?>
                                                    <li><a href="<?= url("dubai/real-estate-developers/$ft->parents_slugs/$ft->slugs") ?>"><?= $ft->heading ?></a></li>
                                                <?php
                                                }
                                                if (count($sublist) == 6) {
                                                    ?><li><a  href="<?= url("dubai/real-estate-developers/$ft->parents_slugs") ?>">MORE {{$d->heading}} PROPERTIES</a></li><?php
                                                    }
                                                    ?>

                                            </ul>
                                    <?php } ?>
                                    </li>
<?php } ?>
                                <li><a href="<?= url('dubai/real-estate-developers') ?>">MORE DEVELOPMENTS </a></li>

                            </ul>
                        </li>
                        <li style="display: none;"><a href="javascript:void(0)">MARKET TRENDS</a>
                            <ul>
                                <li><a href="javascript:void(0)">Sale Transaction</a></li>
                                <li><a href="javascript:void(0)">Daily Transaction</a></li>
                                <li><a href="javascript:void(0)">Market Guide</a></li>
                            </ul>
                        </li>
                        <li> <a href="{{url('dubai/area-guides')}}">AREA GUIDES </a>
                            <ul>
                                <?php
                                $Sql = "SELECT * FROM `area_guides` WHERE archive=0 ORDER BY position ASC LIMIT 7";
                                $dlist = App\Database::select($Sql);
                                for ($i = 0; $i < count($dlist); $i++) {
                                    $d = $dlist[$i];
                                    ?>
                                    <li><a href="<?= url('dubai/area-guides/' . $d->slugs) ?>"><?= $d->area_name ?></a></li>
<?php } ?>
                                <li><a href="{{url('dubai/area-guides')}}">More Area Guides</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= url('dubai/blog') ?>"> BLOG </a></li>
                        <li><a href="javascript:void(0)">ABOUT US</a>
                            <ul>
                                <li><a href="<?= url('dubai/about-us') ?>">Who We Are</a></li>
                                <li><a href="<?= url('dubai/services') ?>">Service</a></li>
                                <li><a href="<?= url('dubai/team') ?>">Team</a></li>
                                <li><a href="<?= url('dubai/careers') ?>">Careers </a></li>
                                <li><a href="<?= url('dubai/testimonals') ?>">Testimonials</a></li>
                                <li><a href="<?= url('dubai/contact-us') ?>">Contact Us</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <header id="navbar_top" class="header">
                    <div class="container __cctest">
                        <div class="logo">

                            <a href="{{url('/')}}"><img class="img-fluid  __dklogo" src="{{url('/public/lib/images/logo/logo_icon_w.svg')}}" alt="Excel Properties Logo"></a>
                            <a href="{{url('/')}}"><img class="img-fluid __moblogo" src="{{url('/public/lib/images/logo/footer_logo.svg')}}" alt="Excel Properties Logo"></a>


                        </div>

                        <div class="__hdrtpart"> 

                            <div class="__sbrasct">

                                <div class="menu_wrap navbar-light">
                                    <nav class="navbar navbar-expand-lg"> 
                                        <div class="collapse navbar-collapse">
                                            <ul class="navbar-nav"> 

                                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="<?= url('/dubai/properties-for-sale') ?>" >BUY</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item" href="<?= url("/dubai/properties-for-sale/off-plan-apartments") ?>">Offplan Apartments</a></li>
                                                        <li><a class="dropdown-item" href="<?= url("/dubai/properties-for-sale/off-plan-villas") ?>">Offplan Villas</a></li>

                                                        <?php
                                                        $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
                                                        for ($i = 0; $i < count($dArray); $i++) {
                                                            $d = $dArray[$i];
                                                            ?><li><a class="dropdown-item" href="<?= url("/dubai/properties-for-sale/" . $d->slugs) ?>"><?= $d->ptype_name ?></a></li><?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>

                                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="<?= url('/dubai/properties-for-rent') ?>" >RENT</a>
                                                    <ul class="dropdown-menu">
                                                        <?php
                                                        $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
                                                        for ($i = 0; $i < count($dArray); $i++) {
                                                            $d = $dArray[$i];
                                                            ?><li><a class="dropdown-item" href="<?= url("/dubai/properties-for-rent/" . $d->slugs) ?>"><?= $d->ptype_name ?></a></li><?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>


                                                <li class="nav-item"><a class="nav-link" href="<?= url('dubai/list-with-us') ?>"> LIST WITH US </a></li>


                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" href="javascript:void(0)" >NEW DEVELOPMENTS </a>
                                                    <ul class="dropdown-menu">
                                                        <?php
                                                        $Sql = "SELECT heading,ndid,slugs  FROM `new_developments` WHERE archive=0 AND parents_id=0 ORDER BY position ASC LIMIT 6";
                                                        $dlist = App\Database::select($Sql);
                                                        for ($i = 0; $i < count($dlist); $i++) {
                                                            $d = $dlist[$i];
                                                            $Sql = "SELECT SC.heading,SC.ndid,SC.slugs,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id LIKE '" . $d->ndid . "' ORDER BY SC.position ASC LIMIT 6";

                                                            $sublist = App\Database::select($Sql);
                                                            ?>
                                                            <li><a class="dropdown-item" href="<?= url("dubai/real-estate-developers/$d->slugs") ?>">{{$d->heading}} <?= !empty($sublist) ? '<i class="__menu_arrow fas fa-caret-right"></i>' : '' ?>  </a>
                                                                    <?php if (!empty($sublist)) { ?>
                                                                    <ul class="submenu dropdown-menu">
                                                                        <?php
                                                                        for ($j = 0; $j < count($sublist); $j++) {
                                                                            $ft = $sublist[$j];
                                                                            ?>
                                                                            <li><a class="dropdown-item" href="<?= url("dubai/real-estate-developers/$ft->parents_slugs/$ft->slugs") ?>">{{$ft->heading}}</a></li>
                                                                        <?php
                                                                            }
                                                                            if (count($sublist) == 6) {
                                                                                ?><li><a class="dropdown-item" href="<?= url("dubai/real-estate-developers/$ft->parents_slugs") ?>">MORE {{$d->heading}} PROPERTIES</a></li><?php
                                                                }
                                                                            ?>
                                                                    </ul> 
                                                            <?php } ?>
                                                            </li>
<?php } ?>
                                                        <li ><a class="dropdown-item" href="<?= url('dubai/real-estate-developers') ?>">MORE DEVELOPMENTS </a></li>
                                                    </ul>
                                                </li> 


                                                <li style="display: none;" class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="javascript:void(0)" >MARKET TRENDS</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item" href="<?= url('sale-transaction?type=1') ?>">Sale Transaction</a></li>
                                                        <li><a class="dropdown-item" href="<?= url('sale-transaction?type=2') ?>">Daily Transaction</a></li>
                                                        <li><a class="dropdown-item" href="<?= url('sale-transaction?type=3') ?>">Market Guide</a></li> 
                                                    </ul>
                                                </li>


                                                <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="{{url('dubai/area-guides')}}" >AREA GUIDES </a>
                                                    <ul class="dropdown-menu">
                                                        <?php
                                                        $Sql = "SELECT * FROM `area_guides` WHERE archive=0 ORDER BY position ASC  LIMIT 7";
                                                        $dlist = App\Database::select($Sql);
                                                        for ($i = 0; $i < count($dlist); $i++) {
                                                            $d = $dlist[$i];
                                                            ?>
                                                            <li><a class="dropdown-item" href="<?= url('dubai/area-guides/' . $d->slugs) ?>"><?= $d->area_name ?></a></li>
<?php } ?>
                                                        <li><a class="dropdown-item" href="{{url('dubai/area-guides')}}">More Area Guides</a></li>
                                                    </ul>
                                                </li>

                                                <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="javascript:void(0)" >ABOUT US</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="dropdown-item" href="<?= url('dubai/about-us') ?>">Who We Are</a></li>
                                                        <li><a class="dropdown-item" href="<?= url('dubai/services') ?>">Services</a></li>
                                                        <li><a class="dropdown-item" href="<?= url('dubai/team') ?>">Team</a></li>
                                                        <li><a class="dropdown-item" href="<?= url('dubai/careers') ?>">Careers </a></li> 
                                                        <li><a class="dropdown-item" href="<?= url('dubai/testimonals') ?>">Testimonials</a></li> 
                                                        <li><a class="dropdown-item" href="<?= url('dubai/contact-us') ?>">Contact Us</a></li> 
                                                    </ul>
                                                </li> 

                                            </ul>
                                        </div>
                                        <!-- navbar-collapse.// -->
                                    </nav>


                                </div>
                                <!--
                                <div class="__logregdrp">
                                        <a href="loginregistration.php" class="__logretbn "> <span><img src="images/user.svg"> Login  </span> </a> 
                                </div>
                                -->
                                <a href="tel:<?= $contactUs->col7 ?>" class="__tptell __rotation_animation"> <i class="fas fa-phone-alt" aria-hidden="true"></i> </a>
                                <a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $contactUs->col7 ?>&amp;text=Hello Excel Properties, I need more information regarding PROPERTIES IN DUBAI." class="__tpwhatsapp __rotation_animation"> <img src="{{url('/public/lib/images/whatsapp.svg')}}" > </a>

                                <div class="header_nav">
                                    <a href="#menu"><span></span></a>				 
                                </div>

                            </div>
                        </div>








                    </div>
<?php if ($roughtName == 'index') { ?>
                        <div class="container" id="searchboxdiv">

                            <div class="__motbSrcfiter wd100">
                                <span class="__mbSearchbar" >
                                    <div class="form-group has-search"> 
                                        <span class="fa fa-search form-control-feedback" aria-hidden="true"></span>
                                        <!--<input onfocus="showmysearchBar();"  type="text" class="form-control" placeholder="Search..."> -->
                                        <div onclick="showmysearchBar();" class="form-control __cutSrBlie">Search...</div>
                                    </div>
                                </span> 
                            </div>

                        </div>
<?php } ?>
                </header>
                <!--header end-->