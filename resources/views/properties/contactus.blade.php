@include('properties.includes.header')

<?php
$pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CONTACT_US');
?>

<div class="wd100 __innerbanner __contact_us">  
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">About Us</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <h2>CONTACT US</h2> 
        <p><?= !empty($pArray->col1) ? $pArray->col1 : '' ?></p>
    </div>  
</div>

<style>
    .validation-msg{
        color: white;
    }
</style>
<section class="section __contactPg">
    <div class="container">
        
          
        <div class="col-lg-12 col-md-12 col-sm-12  __cont_bok1">
            <h1>Contact Us</h1> 
            <p><?= !empty($pArray->col2) ? $pArray->col2 : '' ?></p> 

</div>
        
        <div class="col-lg-5 col-md-10 col-sm-12  __cont_bok1">
         


            <form class="row  __cofrm">
                <input type="hidden" id="PAGE_ID" value="CONTACT_US">
                <div class="col-lg-12 col-md-12 col-sm-12  mb-2">
                    <!--<label for="formFile" class="form-label44">Name</label>-->
                    <input type="text" class="form-control"   id="name" placeholder="Name">
                    <span class="validation-msg">Enter your name</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12  mb-2">
                    <!--<label for="formFile" class="form-label44">Email</label>-->
                    <input type="text" class="form-control"   id="email" placeholder="Email">
                    <span class="validation-msg">Enter valid email address.</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12  mb-4"> 
                    <!--<label for="formFile" class="form-label44">Phone</label>-->
                    <input type="number" class="form-control number_only"   id="phone" placeholder="Phone"> 
                    <span id="valid-msg" class="hide">Valid</span>
                    <span id="error-msg" style="color: #f9fafb;" class="hide">Invalid number</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12  mb-2" style="display: none;">
                    <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                </div>  	 
                <div class="col-lg-12 col-md-12 col-sm-12 mb-3 text-end">
                    <button type="button" id="sendcontactus" class="btn btn-primary __ctm_btn" id="contact_us">SEND <div id="ajaxloader"></div></button>
                </div>

            </form>
        </div> 
    </div> 
</section>

<section class="section __scnB2 __community">
    <div class="container">
        <script>
            var defaultLat = '<?= $pArray->col8 ?>';
            var defaultLong = '<?= $pArray->col9 ?>';
               var locationName = 'Excel Properties'; //<= $pArray->col6 ?>
        </script>



        <div class="map-wrapper-inner" id="map-page">
            <div id="google-maps-box">
                <div id="map" style="width:100%; height:350px;"></div>
            </div>
        </div>
    </div>
</section> 

<section class="section __info_contactArs">
    <div class="container">

        T: <a href="tel:<?= !empty($pArray->col4) ? $pArray->col4 : '' ?>">+971 (0) 506606100<br/></a>
        Email: <a href="mailto:<?= !empty($pArray->col5) ? $pArray->col5 : '' ?>"><?= !empty($pArray->col5) ? $pArray->col5 : '' ?></a> <br/>
        <b>Head-Office:</b> <?= !empty($pArray->col10) ? $pArray->col10 : '' ?>



    </div>  
</section>
@include('properties.includes.footer')	


<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
<script src="{{url('/public')}}/assets/js/mapmodel_user.js" ></script>  
<!--flagS-->

<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

<script>
            var telInput = $("#phone"),
                    errorMsg = $("#error-msg"),
                    validMsg = $("#valid-msg");

// initialise plugin
            telInput.intlTelInput({
                allowExtensions: true,
                formatOnDisplay: true,
                autoFormat: true,
                autoHideDialCode: true,
                autoPlaceholder: true,
                defaultCountry: "auto",
                ipinfoToken: "yolo",
                nationalMode: false,
                numberType: "MOBILE",
//onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
                preventInvalidNumbers: true,
                separateDialCode: true,
                initialCountry: "auto",
                geoIpLookup: function (callback) {
                    $.get("http://ipinfo.io", function () {
                    }, "jsonp").always(function (resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },
                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
            });

            var reset = function () {
                telInput.removeClass("error");
                errorMsg.addClass("hide");
                validMsg.addClass("hide");
            };

// on blur: validate
            telInput.blur(function () {
                reset();
                if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput("isValidNumber")) {
                        validMsg.removeClass("hide");
                    } else {
                        telInput.addClass("error");
                        errorMsg.removeClass("hide");
                    }
                }
            });
            $("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
            telInput.on("keyup change", reset);
            $('#sendcontactus').click(function () {
                var form = new FormData();
                form.append('_token', CSRF_TOKEN);
                form.append('function', 'contactusEnq');
                form.append('helper', 'Common');
                form.append('json[page_id]', $('#PAGE_ID').val());
                form.append('json[name]', $('#name').val());
                form.append('json[email]', $('#email').val());
                form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
                form.append('json[messages]', $('#messages').val());
                form.append('json[page_url]', $('#page_url').val());

                if ($('#name').val() == '') {
                    $('#name').parent().find('.validation-msg').show();
                    $('#name').focus();
                    return false;
                } else {
                    $('#name').parent().find('.validation-msg').hide();
                }
                if (!validateEmail($('#email').val()) || $('#email').val() == '') {
                    $('#email').parent().find('.validation-msg').show();
                    $('#email').focus();
                    return false;
                } else {
                    $('#email').parent().find('.validation-msg').hide();
                }
                if (telInput.intlTelInput("isValidNumber")) {
                    validMsg.removeClass("hide");
                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                    return false;
                }
                showLoader('ajaxloader');
                setTimeout(function () {
                    var json = ajaxpost(form, "/helper");
                    try {
                        var json = jQuery.parseJSON(json);
                        if (json.status == true) {
                            //  alertSimple(json.messages);
                            setTimeout(function () {
                                window.location = base_url + '/thank-you';
                            }, 3000);
                        }
                    } catch (e) {
                        alert(e);
                    }
                }, 200);

            });
</script>
<!--flagS-->