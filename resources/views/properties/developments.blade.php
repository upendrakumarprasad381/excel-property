@include('properties.includes.header')
<?php
$Sql = "SELECT ndid  FROM `new_developments` WHERE `slugs` LIKE '$developerId' AND archive=0";
$datalist = \App\Database::selectSingle($Sql);
$id = !empty($datalist->ndid) ? $datalist->ndid : '';
$pArray = \App\Helpers\LibHelper::GetnewdevelopmentsByndid($id);
if (empty($pArray)) {
    header("location: " . url('/'));
    exit;
}

$file = "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("public/images/new_developmentsbnr.jpg");
}
?>
<div class="wd100 __innerbanner __new_developmentsbnr" data-overlay="dark" data-opacity="4" style="    background: url(<?= $file ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?= url('dubai/real-estate-developers') ?>">Real Estate Developers</a></li>
                    <li class="breadcrumb-item"><a href=""><?= $pArray->heading ?></a></li>
                </ol>
            </nav>
        </div>
    </div>
    <style>
        .validation-msg{
            color: #141519;
        }
    </style>
    <div class="container">
        <input type="hidden" id="primary_id" value="<?= $id ?>">
        <div class="row  __crmli">
            <div class="col-lg-6 col-md-6 col-sm-12 __neletxhd">
                <h2><?= $pArray->heading ?></h2> 
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 __liBnfrm  __AG_dfrboz">
                <input type="hidden" id="PAGE_ID" value="DEVELOPMENTS">
                <input type="hidden" id="title" value="<?= $pArray->heading ?>">
                <div class="wd100 __AG_dfrbozInr">
                    <h4 style="color: black;text-align: center;">Our Expert Will Help You Buy The Best Property</h4>
                    <div class="mb-3 mt-3 lable34">
                        <label>Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter your name">
                        <span class="validation-msg">Enter your name</span>
                    </div>
                    <div class="mb-3 __nubcode">
                        <label class="lable34">Phone</label>
                        <input class="form-control" id="phone" type="number" placeholder="Enter your phone">
                        <span id="valid-msg" class="hide">Valid</span>
                        <span id="error-msg" style="color: #000;" class="hide">Invalid number</span>
                        <!--  <input type="text" class="form-control number_only" id="phone" placeholder="+971  "> -->
                    </div>
                    <div class="mb-3">
                        <label class="lable34">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Enter your email"> 
                        <span class="validation-msg">Enter valid email address.</span>
                    </div>
                    <!--<div class="mb-3">
                        <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                    </div>  -->

                    <div class="wd100">
                        <button type="button" class="btn __btnListwus __AG_dfrbozInrBnt" id="sendcontactus">Request A Free Call Back <div id="ajaxloader"></div></button>
                    </div> 


                </div>

            </div>





        </div>




<!--<h2><?= $pArray->heading ?></h2>-->

    </div>



</div>


<section class="section __rentlPg">
    <div class="container">

        <div class="wd100 __rentLanbotmSc mt-0">
            <h2>About <?= $pArray->heading ?></h2> 
            <p><?= html_entity_decode($pArray->description) ?></p>
        </div>
        <?php
        $Sql = "SELECT SC.heading,SC.ndid,SC.banner,SC.slugs,P.heading AS parents_name,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id NOT IN (0) AND SC.parents_id IN ($id)  ORDER BY P.position ASC,SC.position ASC ";

        $listAr = \App\Database::select($Sql);
        if (!empty($listAr)) {
            ?>
            <div class="wd100 __rentLanbotmSc mt-0">
                <h2 style="margin-bottom: 17px;">Properties By <?= $pArray->heading ?></h2> 
            </div>
            <div class="row">
                <?php
                for ($i = 0; $i < count($listAr); $i++) {
                    $d = $listAr[$i];
                    $file = "files/hostgallery/" . (!empty($d->banner) ? $d->banner : '');
                    $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                    $url = url("dubai/real-estate-developers/$d->parents_slugs/$d->slugs");
                    ?>

                    <div class="col-lg-4 col-md-6 col-sm-12 __prligrdwrp">
                        <div class="__prligrd wd100">
                            <div class="__prligrdimg">
                                <a href="{{$url}}">
                                    <img class="img-fluid" src="{{url($file)}}">
                                </a>
                                <div class="__proname"> <a href="{{$url}}"><?=  $d->heading ?></a> 
                                </div>
                            </div>

                        </div>
                    </div>
                <?php } ?>




            </div>
        <?php } ?>


    </div>
</section>

@include('properties.includes.footer') 	


<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

<script>
var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");


telInput.intlTelInput({
    allowExtensions: true,
    formatOnDisplay: true,
    autoFormat: true,
    autoHideDialCode: true,
    autoPlaceholder: true,
    defaultCountry: "auto",
    ipinfoToken: "yolo",
    nationalMode: false,
    numberType: "MOBILE",
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
    preventInvalidNumbers: true,
    separateDialCode: true,
    initialCountry: "auto",
    geoIpLookup: function (callback) {
        $.get("http://ipinfo.io", function () {
        }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function () {
    telInput.removeClass("error");
    errorMsg.addClass("hide");
    validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function () {
    reset();
    if ($.trim(telInput.val())) {
        if (telInput.intlTelInput("isValidNumber")) {
            validMsg.removeClass("hide");
        } else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
        }
    }
});
$("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
telInput.on("keyup change", reset);
$('#sendcontactus').click(function () {
    var form = new FormData();
    form.append('_token', CSRF_TOKEN);
    form.append('function', 'areaGuideEnq');
    form.append('helper', 'Common');
    form.append('json[page_id]', $('#PAGE_ID').val());
    form.append('json[name]', $('#name').val());
    form.append('json[email]', $('#email').val());
    form.append('json[primary_id]', $('#primary_id').val());
    form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
    form.append('json[messages]', '<b>Type - ' + $('#title').val() + '</b> ');
    form.append('json[page_url]', $('#page_url').val());

    if ($('#name').val() == '') {
        $('#name').parent().find('.validation-msg').show();
        $('#name').focus();
        return false;
    } else {
        $('#name').parent().find('.validation-msg').hide();
    }


    if (telInput.intlTelInput("isValidNumber")) {
        validMsg.removeClass("hide");
    } else {
        telInput.addClass("error");
        errorMsg.removeClass("hide");
        return false;
    }

    if (!validateEmail($('#email').val()) || $('#email').val() == '') {
        $('#email').parent().find('.validation-msg').show();
        $('#email').focus();
        return false;
    } else {
        $('#email').parent().find('.validation-msg').hide();
    }
    showLoader('ajaxloader');
    setTimeout(function () {
        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                // alertSimple(json.messages);
                setTimeout(function () {
                    window.location = base_url + '/thank-you';
                }, 3000);
            }
        } catch (e) {
            alert(e);
        }
    }, 200);
});
</script>
<!--flagS-->