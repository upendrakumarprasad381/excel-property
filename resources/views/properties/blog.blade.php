@include('properties.includes.header')
<div class="wd100 __innerbanner __blog_banner">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Blog</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2>Blog</h2>
        <!-- <div class="__description"></div>-->
    </div>
</div>
<section class="section __blogPg">

    <div class="container">
        <div class="__blgWrp wd100">

            <!--Left Part SS-->
            <div class="__blgLewd">


                <div class="__blgGrid row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-2">
                    <?php
                    $Sql = "SELECT * FROM `blogs` WHERE archive=0  ORDER BY position ASC";
                    $blogs = App\Database::select($Sql);
                    for ($i = 0; $i < count($blogs); $i++) {
                        $d = $blogs[$i];
                     
                          $url = url('blog-detail/' . $d->slugs);
                        $baseDir = "files/hostgallery/" . $d->attachment;
                        $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                        ?>
                        <div class="__blgGridBoz">  
                            <div class="wd100 __blgGridBozImg"> 
                                <a href="{{$url}}"><img class="img-fluid" src="<?= url($baseDir) ?>"></a>
                            </div>
                            <div class="wd100">
                                <h2><a href="{{$url}}"><?= $d->blog_titile ?></a></h2>
                                <a class="__blgmore" href="{{$url}}">Read More</a>
                            </div>
                        </div>
                    <?php } ?>







                </div>



            </div> 
            <!--Left Part EE-->


            <!--Riht Part SS-->

            <div class="__blgRtewgt"> 
                <div class="__subscribeWrp wd100">
                    <h3>Subscribe.</h3>

                    <div class="wd100 __LprtBoz"> 
                        <div class="mb-4 mt-2"> 
                            <label>Full Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Full Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>

                        <div class="mb-4"> 
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div> 
                        <a href="javascript:void(0)" id="subscribe" class="wd100 __scrnewsBnt">Subscribe for Newsletter <img src="{{url('/public/lib/images/arrow.svg')}}">       <div id="ajaxloader"></div>
                        </a> 
                    </div>




                </div> 
                <div class="wd100 __vInstWrp">
                    <a href="#" class="__vInstbnt">View On Instagram <img src="{{url('/public/lib/images/instagram.png')}}"></a>
                </div> 


                <div class="__blgTrendingWrp wd100">
                    <h2>Trending</h2>
                    <p>Dubai offers a broad range of high class accommodation opportunities for people ...</p>

                    <div class="wd100 __blgTrendingboz">
                        <?php
                        $Sql = "SELECT * FROM `blogs` WHERE archive=0 AND FIND_IN_SET('2',tags) ORDER BY position ASC";
                        $blogs = App\Database::select($Sql);
                        for ($i = 0; $i < count($blogs); $i++) {
                            $d = $blogs[$i];
                          
                              $url = url('blog-detail/' . $d->slugs);
                            $baseDir = "files/hostgallery/" . $d->attachment;
                            $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                            ?>
                            <div class="d-flex __blgTrwitm">
                                <div class="flex-shrink-0"> <a href="{{$url}}"><img src="<?= url($baseDir) ?>"> </a></div>
                                <div class="flex-grow-1 ms-3 __dcrpgltetr"> <a href="{{$url}}"><?= $d->blog_titile ?></a> </div>
                            </div>
                        <?php } ?>


                    </div>

                </div>



            </div>


            <!--Riht Part EE-->


        </div>


    </div>





</section>
@include('properties.includes.footer')
<script>
    $('#subscribe').click(function () {
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('helper', 'Common');
        form.append('function', 'subscribeBlog');
        form.append('json[name]', $('#name').val());
        form.append('json[email]', $('#email').val());


        if ($('#name').val() == '') {
            $('#name').parent().find('.validation-msg').show();
            $('#name').focus();
            return false;
        } else {
            $('#name').parent().find('.validation-msg').hide();
        }
        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').parent().find('.validation-msg').show();
            $('#email').focus();
            return false;
        } else {
            $('#email').parent().find('.validation-msg').hide();
        }
        showLoader('ajaxloader');
        setTimeout(function () {
            var json = ajaxpost(form, "/helper");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    // alertSimple(json.messages);
                    setTimeout(function () {
                        window.location = base_url + '/thank-you';
                    }, 3000);
                }
            } catch (e) {
                alert(e);
            }
        }, 200);
    });
</script>