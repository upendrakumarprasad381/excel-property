@include('properties.includes.header')
<?php
$Sql = "SELECT guides_id  FROM `area_guides` WHERE `slugs` LIKE '$areaId' AND archive=0";
$developer = \App\Database::selectSingle($Sql);
$id = !empty($developer->guides_id) ? $developer->guides_id : '';




$pArray = \App\Helpers\LibHelper::GetareaguidesByguidesId($id);
if (empty($pArray)) {
    header("location: " . url('/'));
    exit;
}


$file = "files/hostgallery/" . (!empty($pArray->banner_logo) ? $pArray->banner_logo : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("public/images/area_guide_details_bnr.jpg");
}


//echo '<pre>';
//print_r($pArray);
//exit;
?>
<style>
    .validation-msg,#error-msg{
        color: black;
    }
</style>
<div class="wd100 __innerbanner __area_guide_details_bnr" data-overlay="dark" data-opacity="4" style="background: url(<?= $file ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp" style="background: #141519b5;">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?= url('dubai/area-guides') ?>">Dubai Communities</a></li>
                    <li class="breadcrumb-item"> <a href="<?= url("dubai/area-guides/$pArray->slugs") ?>">{{$pArray->area_name}} </a></li>
                    <li class="breadcrumb-item"> <a href="">Properties</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <input type="hidden" id="primary_id" value="<?= $id ?>">

    <div class="container">
        <div class="row  __crmli">
            <div class="col-lg-6 col-md-6 col-sm-12 __neletxhd">

                <h2  ><?= $pArray->banner_title ?></h2>

            </div>






        </div>
    </div>

</div>






<?php
if (!empty($pArray->tag_id)) {
    $Sql = "SELECT * FROM tags WHERE page_id='AREA_GUIDES' AND tag_id IN ($pArray->tag_id) ORDER BY position ASC";
    $tagsAr = \App\Database::select($Sql);
    for ($i = 0; $i < count($tagsAr); $i++) {
        $d = $tagsAr[$i];

        $Sql = "SELECT MP.*,getpropertyImage(MP.property_id) AS first_image,getpropertyDetailsUrl(MP.property_id) AS urlSlug FROM `my_property` MP WHERE MP.archive=0 AND MP.status=0 AND FIND_IN_SET('$d->tag_id',MP.tag_id) ORDER BY MP.position ASC LIMIT 3";
        $listAr = \App\Database::select($Sql);
        if (!empty($listAr)) {
            ?>
            <section class="section __scnB1 __offPlan">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="__scnB1txtboz">
                                <h1><?= $d->tag_name ?></h1>
                                <p><?= $d->description ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                    ?>
                    <div class="row  __dfjo">
                        <?php
                        for ($j = 0; $j < count($listAr); $j++) {
                            $fg = $listAr[$j];
                            $images = \App\Helpers\CommonHelper::GetPropertyImagesCover($fg);
                            $url = url($fg->urlSlug);
                            ?>
                            <div class="col-lg-4 col-md-6 col-sm-12 __prligrdwrp">
                                <div class="__prligrd wd100">
                                    <div class="__prligrdimg">
                                        <a href="{{$url}}">
                                            <img class="img-fluid" src="{{$images}}">
                                        </a>
                                        <div class="__proname"> <a href="{{$url}}">{{$fg->property_title}}</a> </div>
                                    </div>
                                    <div class="__prligrddrcp">
                                        <div class="  __priqustbx"> <small>PRICE</small>
                                            <br>AED <?= \App\Helpers\CommonHelper::DecimalAmount($fg->price) ?></div>
                                        <div class="  __bedsbx"> <small><?= $fg->bed ?></small>
                                            <br>BED</div>
                                        <div class="  __sqftsbx"><?= \App\Helpers\CommonHelper::DecimalAmount($fg->area) ?> SQ.FT</div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>





                </div>
            </section>
            <?php
        }
    }
}
?>

<div class="row">


    <br>
</div>
@include('properties.includes.footer')	
<!--flagS-->

<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
<script src="{{url('/public')}}/assets/js/mapmodel_user.js" ></script>  

<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

<script>
var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");


telInput.intlTelInput({
    allowExtensions: true,
    formatOnDisplay: true,
    autoFormat: true,
    autoHideDialCode: true,
    autoPlaceholder: true,
    defaultCountry: "auto",
    ipinfoToken: "yolo",
    nationalMode: false,
    numberType: "MOBILE",
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
    preventInvalidNumbers: true,
    separateDialCode: true,
    initialCountry: "auto",
    geoIpLookup: function (callback) {
        $.get("http://ipinfo.io", function () {
        }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function () {
    telInput.removeClass("error");
    errorMsg.addClass("hide");
    validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function () {
    reset();
    if ($.trim(telInput.val())) {
        if (telInput.intlTelInput("isValidNumber")) {
            validMsg.removeClass("hide");
        } else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
        }
    }
});
$("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
telInput.on("keyup change", reset);
$('#sendcontactus').click(function () {
    var form = new FormData();
    form.append('_token', CSRF_TOKEN);
    form.append('function', 'areaGuideEnq');
    form.append('helper', 'Common');
    form.append('json[page_id]', $('#PAGE_ID').val());
    form.append('json[name]', $('#name').val());
    form.append('json[email]', $('#email').val());
    form.append('json[primary_id]', $('#primary_id').val());
    form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
    form.append('json[messages]', '<b>Type - ' + $('#title').val() + '</b> ' + $('#messages').val());
    form.append('json[page_url]', $('#page_url').val());

    if ($('#name').val() == '') {
        $('#name').parent().find('.validation-msg').show();
        $('#name').focus();
        return false;
    } else {
        $('#name').parent().find('.validation-msg').hide();
    }


    if (telInput.intlTelInput("isValidNumber")) {
        validMsg.removeClass("hide");
    } else {
        telInput.addClass("error");
        errorMsg.removeClass("hide");
        return false;
    }
    if (!validateEmail($('#email').val()) || $('#email').val() == '') {
        $('#email').parent().find('.validation-msg').show();
        $('#email').focus();
        return false;
    } else {
        $('#email').parent().find('.validation-msg').hide();
    }
    showLoader('ajaxloader');
    setTimeout(function () {
        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                // alertSimple(json.messages);
                setTimeout(function () {
                    window.location = base_url + '/thank-you';
                }, 3000);
            }
        } catch (e) {
            alert(e);
        }
    }, 200);
});
</script>
<!--flagS-->