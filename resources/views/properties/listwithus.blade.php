@include('properties.includes.header')

<?php
$LIST_WITH_US_AREA_1 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_1');
$LIST_WITH_US_AREA_2 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_2');
$LIST_WITH_US_AREA_3 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'LIST_WITH_US_AREA_3');
?>


<div class="wd100 __innerbanner __list_with_us_bnr">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">List Your Property</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="row  __crmli">

            <div class="col-lg-6 col-md-6 col-sm-12 __liBntx">
                <h2><?= $LIST_WITH_US_AREA_1->col1 ?></h2>

                <h4><?= $LIST_WITH_US_AREA_1->col2 ?></h4>
                <h5><?= $LIST_WITH_US_AREA_1->col3 ?></h5>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 __liBnfrm">

                <input type="hidden" id="PAGE_ID" value="LIST_WITH_US">
                <div class="wd100 __liBnfrmBoz" id="requestcallBack">

                    <div class="mb-2 mt-3"> 
                        <label>Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name" autocomplete="off"> 
                        <span class="validation-msg">Enter your name</span>
                    </div>

                    <div class="mb-2"> 
                        <label>Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Email" autocomplete="off">
                        <span class="validation-msg">Enter valid email address.</span>
                    </div>

                    <div class="mb-2  __nubcode"> 
                        <label>Mobile Number</label>
                        <input class="form-control" id="phone" type="number" placeholder="Mobile Number" autocomplete="off">
                        <span id="valid-msg" class="hide">Valid</span>
                        <span id="error-msg" class="hide">Invalid number</span>
                                                    <!--  <input type="text" class="form-control number_only" id="phone" placeholder="+971  "> -->
                    </div>


                    <div class="mb-2"> 
                        <label>Property Type</label>
                        <select class="form-select" id="title"> 
                            <option value="" >Select Property Type</option>
                            <?php
                            $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
                            for ($i = 0; $i < count($dArray); $i++) {
                                $d = $dArray[$i];
                                ?>
                                <option value="<?= $d->ptype_name ?>"><?= $d->ptype_name ?></option>
                                <?php
                            }
                            $dArray = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
                            for ($i = 0; $i < count($dArray); $i++) {
                                $d = $dArray[$i];
                                ?>
                                <option value="<?= $d->ptype_name ?>"><?= $d->ptype_name ?></option>
                            <?php } ?>

                        </select>
                    </div>


                    <div class="mb-2" style="display: none;"> 
                        <textarea class="form-control" id="messages" rows="2" placeholder="Message"></textarea>
                    </div>


                    <div class="wd100 mt-2">
                        <button type="button" id="sendcontactus" class="btn __btnListwus" >SEND <div id="ajaxloader"></div></button>
                    </div>


                </div>

            </div>


        </div>
    </div>

</div>


<section class="section __list_with_us_Pg">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 __list_with_us_bz">
                <h3><?= $LIST_WITH_US_AREA_1->col4 ?></h3>

                <div class=" __sulibblin" >
                    <hr/>
                </div>



                <?= $LIST_WITH_US_AREA_1->col5 ?>
            </div> 

            <!--
                                    <div class="wd100">
                                            <div class="__ListWusWrpScx row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-3"> 
                                                      
                                                    <div class=" __ltWusBoz">
                                                            <div class="wd100 __LiusImg">
                                                                    <img class="img-fluid" src="images/litsricon1.png">
                                                            </div>
                                                            <h6>DOCUMENTATION</h6>
                                                            <p>All the process of documentation and agreements we have our experts who will help you.</p>
                                                    </div>
                                                     
                                                    <div class=" __ltWusBoz">
                                                            <div class="wd100 __LiusImg">
                                                                    <img class="img-fluid" src="images/litsricon5.png">
                                                            </div>
                                                            <h6>DOCUMENTATION</h6>
                                                            <p>Arranging Viewings and getting the best deals which will make you happy.</p>
                                                    </div>
                                                    
                                                     
                                                    <div class=" __ltWusBoz">
                                                            <div class="wd100 __LiusImg">
                                                                    <img class="img-fluid" src="images/litsricon3.png">
                                                            </div>
                                                            <h6>SELLING</h6>
                                                            <p>Changing lives for more than two decades and focusing to be the first option for customers when it comes to buying, selling & renting out.</p>
                                                    </div>
                                                    
                                                      
                                                    <div class=" __ltWusBoz">
                                                            <div class="wd100 __LiusImg">
                                                                    <img class="img-fluid" src="images/litsricon4.png">
                                                            </div>
                                                            <h6>QUALITY MARKETING</h6>
                                                            <p>We market every step of the way to get maximum visibility and advertise the client till the final selling stage.</p>
                                                    </div>
                                                     
                                                     
                                                    <div class=" __ltWusBoz">
                                                            <div class="wd100 __LiusImg">
                                                                    <img class="img-fluid" src="images/litsricon2.png">
                                                            </div>
                                                            <h6>PROPERTY HOME VALUATION</h6>
                                                            <p>We market every step of the way to get maximum visibility and advertise the client till the final selling stage..</p>
                                                    </div>
                                                    
                                                    
                                                     
                                                    
                                             </div>
                                                      
                                    
                                    </div>
                                    
            -->


        </div> 
    </div> 
</section>



<section class="wd100 __innerbanner __trust_expertise">
    <div class="container"> 
        <h2><?= $LIST_WITH_US_AREA_1->col6 ?></h2> 

        <div class="wd100 text-center">
            <a href="javascript:void(0)" class="__whymoreBnt focusrequestcallBack">Inquire Now</a>
        </div>
    </div>
</section>



<section class="section __whychooseus">
    <div class="container"> 
        <div class="__whychooseus_bz"> 

            <h2><?= $LIST_WITH_US_AREA_2->col13 ?></h2> 


            <p><?= $LIST_WITH_US_AREA_2->col14 ?></p>


            <div class="row __why2pboWzp">

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col7) ? $LIST_WITH_US_AREA_2->col7 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/database.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col1 ?>
                        </div>
                    </div>
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col8) ? $LIST_WITH_US_AREA_2->col8 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/money-1.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col2 ?>
                        </div>
                    </div>
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col9) ? $LIST_WITH_US_AREA_2->col9 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/marketi.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col3 ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col10) ? $LIST_WITH_US_AREA_2->col10 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/Professional_Photography.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col4 ?>
                        </div>
                    </div>
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col11) ? $LIST_WITH_US_AREA_2->col11 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/Higher_Occupancy_Rate.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col5 ?>
                        </div>
                    </div>
                    <div class="d-flex  __why2pboitm">
                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($LIST_WITH_US_AREA_2->col12) ? $LIST_WITH_US_AREA_2->col12 : '');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $file = url($file);
                            } else {
                                $file = url('/public/lib/images/goal-3.png');
                            }
                            ?>
                            <img src="{{$file}}" class="align-self-end mr-3" >
                        </div>
                        <div class="flex-grow-1 ms-3 align-self-end">
                            <?= $LIST_WITH_US_AREA_2->col6 ?>
                        </div>
                    </div>





                </div>

                <div class="wd100 text-center">
                    <a href="javascript:void(0)" class="__whymoreBnt focusrequestcallBack">I'm Interested</a>
                </div>

            </div>


            <!--
                            <div class="__whychooseusWp">
                                    
                                    <div class="__whychxBZ">
                                            <img class="img-fluid" src="{{url('/public/lib/images/Worth_your_time.png')}}">
                                            <h6>Worth your time</h6>
                                    </div> 
                                    
                                    <div class="__whychxBZ">
                                            <img class="img-fluid" src="{{url('/public/lib/images/Value-for-money.png')}}">
                                            <h6>Value for money</h6>
                                    </div> 
                                    
                                    <div class="__whychxBZ">
                                            <img class="img-fluid" src="{{url('/public/lib/images/Unmatched-Service-.png')}}">
                                            <h6>Unmatched Services</h6>
                                    </div> 
                                    
                                    <div class="__whychxBZ">
                                            <img class="img-fluid" src="{{url('/public/lib/images/Expertise-and-Trust-.png')}}">
                                            <h6>Expertise and Trust</h6>
                                    </div>
                                     
                                    
                            </div> 
                            
            -->


            <div class="wd100 __whyNebkWp">

                <h2><?= $LIST_WITH_US_AREA_3->col9 ?></h2>
                <ul>


                    <li>
                        <div class="__whyNebkHd"><?= $LIST_WITH_US_AREA_3->col1 ?></div>
                        <div class="__whyNebkTxs">
                            <?= $LIST_WITH_US_AREA_3->col2 ?>
                        </div> 
                    </li>



                    <li>
                        <div class="__whyNebkHd"><?= $LIST_WITH_US_AREA_3->col3 ?></div>
                        <div class="__whyNebkTxs">
                            <?= $LIST_WITH_US_AREA_3->col4 ?>


                        </div> 
                    </li>



                    <li>
                        <div class="__whyNebkHd"><?= $LIST_WITH_US_AREA_3->col5 ?></div>
                        <div class="__whyNebkTxs">
                            <?= $LIST_WITH_US_AREA_3->col6 ?>


                        </div> 
                    </li>



                    <li>
                        <div class="__whyNebkHd"><?= $LIST_WITH_US_AREA_3->col7 ?></div>
                        <div class="__whyNebkTxs">
                            <?= $LIST_WITH_US_AREA_3->col8 ?>

                        </div> 
                    </li>






                </ul>

            </div>

        </div>

    </div>
</section>




<section class="wd100 __innerbanner __listlasblok">
    <div class="container"> 

        <h2><?= $LIST_WITH_US_AREA_3->col10 ?></h2> 
        <p><?= $LIST_WITH_US_AREA_3->col11 ?></p>

        <div class="wd100 text-center">
            <a href="javascript:void(0)" class="__whymoreBnt focusrequestcallBack">Get my free valuation</a>
        </div>
    </div>
</section>






@include('properties.includes.footer')	





<!--flagS-->

<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

<script>

var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput({
    allowExtensions: true,
    formatOnDisplay: true,
    autoFormat: true,
    autoHideDialCode: true,
    autoPlaceholder: true,
    defaultCountry: "auto",
    ipinfoToken: "yolo",
    nationalMode: false,
    numberType: "MOBILE",
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
    preventInvalidNumbers: true,
    separateDialCode: true,
    initialCountry: "auto",
    geoIpLookup: function (callback) {
        $.get("http://ipinfo.io", function () {
        }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function () {
    telInput.removeClass("error");
    errorMsg.addClass("hide");
    validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function () {
    reset();
    if ($.trim(telInput.val())) {
        if (telInput.intlTelInput("isValidNumber")) {
            validMsg.removeClass("hide");
        } else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
        }
    }
});
$("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
telInput.on("keyup change", reset);
$('#sendcontactus').click(function () {
    var form = new FormData();
    form.append('_token', CSRF_TOKEN);
    form.append('function', 'listwithusEnq');
    form.append('helper', 'Common');
    form.append('json[page_id]', $('#PAGE_ID').val());
    form.append('json[name]', $('#name').val());
    form.append('json[email]', $('#email').val());
    form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
    form.append('json[messages]', '<b>Type - ' + $('#title').val() + '</b> ' + $('#messages').val());
form.append('json[page_url]', $('#page_url').val());

    if ($('#name').val() == '') {
        $('#name').parent().find('.validation-msg').show();
        $('#name').focus();
        return false;
    } else {
        $('#name').parent().find('.validation-msg').hide();
    }
    if (!validateEmail($('#email').val()) || $('#email').val() == '') {
        $('#email').parent().find('.validation-msg').show();
        $('#email').focus();
        return false;
    } else {
        $('#email').parent().find('.validation-msg').hide();
    }

    if (telInput.intlTelInput("isValidNumber")) {
        validMsg.removeClass("hide");
    } else {
        telInput.addClass("error");
        errorMsg.removeClass("hide");
        return false;
    }
//    if ($('#title').val() == '') {
//        $('#title').css('border-color', 'red');
//        $('#title').focus();
//        return false;
//    } else {
//        $('#title').css('border-color', '');
//    }
    showLoader('ajaxloader');
    setTimeout(function () {
        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                // alertSimple(json.messages);
                setTimeout(function () {
                    window.location = base_url + '/thank-you';
                }, 3000);
            }
        } catch (e) {
            alert(e);
        }
    }, 200);
});
$('.focusrequestcallBack').click(function () {
    $('html, body').animate({
        scrollTop: (parseInt($("#requestcallBack").offset().top) - 200)
    }, 0);
});

</script>
<!--flagS-->

