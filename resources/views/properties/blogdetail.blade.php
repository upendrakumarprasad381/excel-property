@include('properties.includes.header')
<?php


$Sql = "SELECT blog_id  FROM `blogs` WHERE `slugs` LIKE '$blogslugId' AND archive=0";
$slug = \App\Database::selectSingle($Sql);




if (empty($slug)) {
    header("location: " . url('/'));
    exit;
}
$blogId = $slug->blog_id;
$pArray = \App\Helpers\LibHelper::GetblogsByblogsId($blogId);
?>
<div class="wd100 breadcrumb_wrap __liist_property_bnr">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="<?= url('/blog') ?>">Blog</a></li>
                <li class="breadcrumb-item"> Blog details</li>
            </ol>
        </nav>
    </div>
</div>
<input type="hidden" value="<?= $pArray->blog_id ?>" id="blog_id">

<section class="section __blogPgDlPag"> 
    <div class="container">

        <div class="wd100">
            <a href="<?= url('/blog') ?>" class="__catblog">Blog</a>
        </div>

        <div class="__blgWrp wd100"> 
            <!--Left Part SS-->
            <div class="__blgLewd __blogPgDlPagWrp"> 


                <div class="__blgDlpagViw">

                    <h2><?= isset($pArray->blog_titile) ? $pArray->blog_titile : '' ?></h2>

                    <div class="wd100 ">
                        <div class="d-flex __blgAthurinfo align-items-center">
                            <div class="flex-shrink-0">  <img style="width: 45px;" src="{{url('/public/lib/images/logo/logo_icon_w.svg')}}"> </div>
                            <div class="flex-grow-1 ms-3">
                                <span class="__bnuinfo">
                                    Excel Properties
                                </span>| 
                                <span class="__bnudatime">
                                    <?= date('d M Y', strtotime($pArray->timestamp)) ?>
                                </span> 
                            </div>
                        </div>
                    </div>


                    <div class="wd100 __blgdlpgImg"> 
                        <?php
                        $file = "files/hostgallery/" . (!empty($pArray->attachment) ? $pArray->attachment : '');
                        $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : '/public/lib/images/bdimg.jpg';
                        ?>
                        <img class="img-fluid" src="{{url($file)}}"> 
                    </div>

                    <div class="wd100 __blgdlpgDcrp">
                        <?= $pArray->blog_description ?>
                    </div>













                </div>

                <div class="wd100 __bTagWrp">
                    <div class="__bTagitHD">
                        Links 
                    </div>
                    <a class="__bTagitm" href="<?= url('/') ?>">Home Page</a>
                    <a class="__bTagitm" href="<?= url('/blog') ?>">Blogs</a>
                    <a class="__bTagitm" href="<?= url('/contact-us') ?>">Contact Us</a> 
                </div>


                <div class="__blgshareWrp wd100">
                    <div class="__bshareHD">
                        Follow  On
                    </div>

                    <div class="__bshareimg">
                        <div class="_social_icons_top">
                      <a target="_blank" href="https://www.facebook.com/ExcelProperties1"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                    <a target="_blank" href="https://twitter.com/PropertiesExcel"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                    <!--<a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>-->
                    <a target="_blank" href="https://www.youtube.com/channel/UCMJwg4ZzwspdObYR-uSRypA"><i class="fab fa-youtube" aria-hidden="true"></i></a> 
                    <a target="_blank" href="https://www.instagram.com/excel_properties/"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                </div>
                         <!--<a target="_blank" href="https://www.facebook.com/"><img src="{{url('/public/lib/images/facebook-logo.png')}}" /></a>
                        <a target="_blank" href="https://twitter.com/home"><img src="{{url('/public/lib/images/twitter.png')}}" /></a>
                       <a href="#"><img src="{{url('/public/lib/images/pinterest.png')}}" /></a>-->
                        <!--<a href="#"><img src="{{url('/public/lib/images/whatsapp_shar.png')}}" /></a>-->
                    </div> 
                </div>


               



                <div class="__feedformWrp wd100" style="background: no-repeat;">

                    <div class="row">
                        <input type="hidden" id="PAGE_ID" value="BLOG_DETAILS">
                        <input type="hidden" id="primary_id" value="<?= $blogId ?>">

                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <!--<label for="exampleInputEmail1">Full Name </label><span class="required">*</span>--> 
                            <input type="text" class="form-control" id="name" placeholder="Full Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <!--<label for="exampleInputEmail1">Email </label><span class="required">*</span>--> 
                            <input type="text" class="form-control" id="email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <!--<label for="exampleInputEmail1">Phone</label><span class="required">*</span>--> 
                            <input type="number" class="form-control" id="phone" placeholder="Phone"> 
                            <span class="validation-msg">Enter your mobile no</span>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12  mb-4" style="display: none;">
                            <!--<label for="exampleInputEmail1">Message</label>-->  
                            <textarea class="form-control" id="messages" rows="4" placeholder=""></textarea>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  text-end">
                            <button id="sendcontactus" type="button" class="btn btn-primary __ctm_btn __feedformbnt"  >Subscribe <div id="ajaxloader"></div></button>
                        </div>



                    </div>


                </div>

                <!--                <div class="__blg_prvnextWrap wd100">
                                    <div class="row">
                
                
                                        <div class="col">
                                            <a href="#" class="wd100 __blg_prv">
                                                <div class="__blgprntHD wd100">Previous</div>
                                                <div class="__blgprntText wd100">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </a>
                                        </div>
                
                                        <div class="col">
                                            <a href="#" class="wd100 __blg_next">
                
                                                <div class="__blgprntHD wd100">Next</div>
                                                <div class="__blgprntText wd100">Lorem Ipsum is simply dummy and typesetting industry. </div>
                                            </a>
                                        </div>
                
                
                
                
                                    </div>
                                </div>-->



            </div> 
            <!--Left Part EE-->


            <!--Riht Part SS-->

            <div class="__blgRtewgt"> 
                <!--                <div class="__subscribeWrp wd100">
                                    <h3>Subscribe.</h3>
                
                                    <div class="wd100 __LprtBoz"> 
                                        <div class="mb-4 mt-2"> 
                                            <input type="text" class="form-control" id="name" placeholder="Full name"> 
                                        </div>
                
                                        <div class="mb-4"> 
                                            <input type="text" class="form-control" id="email" placeholder="Email">
                                        </div> 
                                        <a href="#" class="wd100 __scrnewsBnt">Subscribe for Newsletter <img src="{{url('/public/lib/images/arrow.svg')}}"></a> 
                                    </div>
                
                
                
                
                                </div> 
                                <div class="wd100 __vInstWrp">
                                    <a href="#" class="__vInstbnt">View On Instagram <img src="{{url('/public/lib/images/instagram.png')}}"></a>
                                </div> -->


                <div class="__blgTrendingWrp wd100">
                    <h2>Trending</h2>
                    <p>Dubai offers a broad range of high class accommodation opportunities for people ...</p>

                    <div class="wd100 __blgTrendingboz">
                        <?php
                        $Sql = "SELECT * FROM `blogs` WHERE archive=0 AND FIND_IN_SET('2',tags) ORDER BY position ASC";
                        $blogs = App\Database::select($Sql);
                        for ($i = 0; $i < count($blogs); $i++) {
                            $d = $blogs[$i];
                          
                              $url = url('blog-detail/' . $d->slugs);
                            $baseDir = "files/hostgallery/" . $d->attachment;
                            $baseDir = is_file(Config::get('constants.HOME_DIR') . $baseDir) ? $baseDir : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                            ?>
                            <div class="d-flex __blgTrwitm">
                                <div class="flex-shrink-0"> <a href="<?= $url ?>"><img src="{{url($baseDir)}}"> </a></div>
                                <div class="flex-grow-1 ms-3 __dcrpgltetr"> <a href="<?= $url ?>"><?= $d->blog_titile ?></a> </div>
                            </div>
                        <?php } ?>







                    </div>

                </div>



            </div>


            <!--Riht Part EE-->


        </div>


    </div>





</section>


@include('properties.includes.footer')
<script>
    $('#sendcontactus').click(function () {
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('function', 'blogdetailpageEnq');
        form.append('helper', 'Common');
        form.append('json[page_id]', $('#PAGE_ID').val());
        form.append('json[primary_id]', $('#primary_id').val());

        form.append('json[name]', $('#name').val());
        form.append('json[email]', $('#email').val());
        form.append('json[mobile_no]', $("#phone").val());
        form.append('json[messages]', $('#messages').val());
  form.append('json[page_url]', $('#page_url').val());

        if ($('#name').val() == '') {
            $('#name').parent().find('.validation-msg').show();
            $('#name').focus();
            return false;
        } else {
            $('#name').parent().find('.validation-msg').hide();
        }
        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').parent().find('.validation-msg').show();
            $('#email').focus();
            return false;
        } else {
            $('#email').parent().find('.validation-msg').hide();
        }

        if ($('#phone').val() == '') {
            $('#phone').parent().find('.validation-msg').show();
            $('#phone').focus();
            return false;
        } else {
            $('#phone').parent().find('.validation-msg').hide();
        }

//    if ($('#title').val() == '') {
//        $('#title').css('border-color', 'red');
//        $('#title').focus();
//        return false;
//    } else {
//        $('#title').css('border-color', '');
//    }
        showLoader('ajaxloader');
        setTimeout(function () {
            var json = ajaxpost(form, "/helper");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    //s alertSimple(json.messages);
                    setTimeout(function () {
                        window.location = base_url + '/thank-you';
                    }, 3000);
                }
            } catch (e) {
                alert(e);
            }
        }, 200);
    });
</script>