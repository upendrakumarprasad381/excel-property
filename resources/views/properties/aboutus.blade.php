@include('properties.includes.header')

<?php

$ABOUT_US_CMS_PAGE = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'ABOUT_US_CMS_PAGE');

$file = "files/hostgallery/" . (!empty($ABOUT_US_CMS_PAGE->col1) ? $ABOUT_US_CMS_PAGE->col1 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("files/hostgallery/615b025f63436.jpg");
}
?>
<div class="wd100 __innerbanner __about" style="background: url(<?= $file ?>) no-repeat center center;">


    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">About Us</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2><?= !empty($ABOUT_US_CMS_PAGE->col2) ? $ABOUT_US_CMS_PAGE->col2 : '' ?></h2>
    </div>


</div>


<section class="section __scoutinner __about_us">
    <div class="container">

        <div class="wd100 __subdis">
            <?= !empty($ABOUT_US_CMS_PAGE->col3) ? $ABOUT_US_CMS_PAGE->col3 : '' ?>

        </div>

        <div class="row">

            <div class="col-lg-7 col-md-12 col-sm-12 __absublt">

                <?= !empty($ABOUT_US_CMS_PAGE->col4) ? $ABOUT_US_CMS_PAGE->col4 : '' ?>




            </div>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <div class="wd100 __company_profile">
                    <?php
                    $file = "files/hostgallery/" . (!empty($ABOUT_US_CMS_PAGE->col5) ? $ABOUT_US_CMS_PAGE->col5 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url("files/hostgallery/615bdf4945767.jpg");
                    }
                    ?>
                    <img class="img-fluid " style="margin-top: 70px;" src="{{$file}}">
                    <h5>Download Company Profile</h5>
                </div>
                <?php
                $file = "files/hostgallery/" . (!empty($ABOUT_US_CMS_PAGE->col6) ? $ABOUT_US_CMS_PAGE->col6 : '');
                ?>
                <a href="<?= url($file) ?>" target="_blank" class="__cpBnt">Click Here</a>

            </div>

        </div>
    </div>
</section>
@include('properties.includes.footer')	