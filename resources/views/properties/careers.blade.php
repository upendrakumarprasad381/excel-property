@include('properties.includes.header')
<?php
$pArray = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'CAREERS_CMS');


$file = "files/hostgallery/" . (!empty($pArray->col6) ? $pArray->col6 : '');
$file = is_file(Config::get('constants.HOME_DIR') . $file) ? url($file) : url('/public/lib/images/careers_bnner (1).jpg');
?>

<div class="wd100 __innerbanner __careers_bnner" style="background: url({{$file}}) no-repeat center center">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Careers</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2><?= $pArray->col1 ?></h2>
        <!--
        <div class="__description"> <b>EXCEL Properties provides sophisticated real estate solutions to clients around the world</b>
                <br/> The company is based around following divisions: </div>
        --></div>
</div>
<section class="section __careersPg">
    <div class="container">
        <div class="wd100 __careersTxbz">
            <h3><?= $pArray->col2 ?></h3>
            <p><?= $pArray->col3 ?></p>
            <div class="wd100 text-center"> <a href="javascript:void(0)" class="__btnListwus focusrequestcallBack __apply_career_bnt">
                    APPLY TODAY
                </a> </div>
            <div class="wd100 __creTxsc">
                <div class="row ">
                    <div class="col-lg-6 col-md-6 col-sm-12 __car1bk">
                        <p><?= $pArray->col4 ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12  __car2bk">
                        <p><?= $pArray->col5 ?></p>
                    </div>
                </div>
                <?php
                $file = "files/hostgallery/" . (!empty($pArray->col7) ? $pArray->col7 : '');
                $file = is_file(Config::get('constants.HOME_DIR') . $file) ? url($file) : url('/public/lib/images/careers.jpg');
                ?>
                <div class="wd100 __careersImg __crtSider">
                     <!--<img class="img-fluid" src="{{$file}}" /> -->


                    <div style="" id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">

                        <!--<div class="carousel-indicators">-->
                        <!--	<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>-->
                        <!--	<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>-->
                        <!--	<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>-->
                        <!--</div>-->

                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <iframe width="100%" height="500" src="https://www.youtube.com/embed/Cpph1ItyvXQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>

                            <div class="carousel-item">
                                <img src="{{$file}}" class="d-block w-100" alt="..."> 
                            </div>

                        </div>

                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>   

                    </div>




                </div>



            </div>
        </div>
    </div>


    <div  class="container text-center">
        <div id="applyHere"  class="__applyHereWrap">
            <h4  >Apply Here</h4> 


            <input type="hidden" id="title" value="">
            <div class="wd100 __liBnfrmBoz" id="requestcallBack">
                <div class="mb-2 mt-2">
                    <label for="formFile" >Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Name">
                    <span class="validation-msg">Enter your name</span>
                </div>
                <div class="mb-2">
                    <label for="formFile"  >Email</label>
                    <input type="text" class="form-control" id="email" placeholder="Email">
                    <span class="validation-msg">Enter valid email address.</span>
                </div>
                <div class="mb-2  __nubcode">
                    <label for="formFile" >Phone</label>
                    <input class="form-control" id="phone" type="number" placeholder="Phone">
                    <span id="valid-msg" class="hide">Valid</span>
                    <span id="error-msg" class="hide">Invalid number</span>
                    <!--  <input type="text" class="form-control number_only" id="phone" placeholder="+971  "> -->
                </div>
                <div class="mb-2">
                    <label for="formFile" >Upload your CV</label>
                    <input class="form-control" type="file" id="cv">
                    <span class="validation-msg">Please upload cv</span>
                </div>
                <div class="mb-2" style="display: none;">
                    <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                </div>  	 

                <div class="wd100 mt-2">
                    <button type="button" id="sendcontactus" class="btn __btnListwus">SEND <div id="ajaxloader"></div></button>
                </div>
            </div>







        </div>

    </div>		





</section>
@include('properties.includes.footer')


<!--flagS-->

<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

<script>

var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput({
    allowExtensions: true,
    formatOnDisplay: true,
    autoFormat: true,
    autoHideDialCode: true,
    autoPlaceholder: true,
    defaultCountry: "auto",
    ipinfoToken: "yolo",
    nationalMode: false,
    numberType: "MOBILE",
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
    preventInvalidNumbers: true,
    separateDialCode: true,
    initialCountry: "auto",
    geoIpLookup: function (callback) {
        $.get("http://ipinfo.io", function () {
        }, "jsonp").always(function (resp) {
            var countryCode = (resp && resp.country) ? resp.country : "";
            callback(countryCode);
        });
    },
    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function () {
    telInput.removeClass("error");
    errorMsg.addClass("hide");
    validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function () {
    reset();
    if ($.trim(telInput.val())) {
        if (telInput.intlTelInput("isValidNumber")) {
            validMsg.removeClass("hide");
        } else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
        }
    }
});
$("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
telInput.on("keyup change", reset);
$('#sendcontactus').click(function () {
    var form = new FormData();
    form.append('_token', CSRF_TOKEN);
    form.append('function', 'careersEnq');
    form.append('helper', 'Common');
    form.append('json[name]', $('#name').val());
    form.append('json[email]', $('#email').val());
    form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
    form.append('json[messages]', $('#messages').val());

    var fileInput = document.querySelector('#cv');
    form.append('cv', fileInput.files[0]);


    if ($('#name').val() == '') {
        $('#name').parent().find('.validation-msg').show();
        $('#name').focus();
        return false;
    } else {
        $('#name').parent().find('.validation-msg').hide();
    }
    if (!validateEmail($('#email').val()) || $('#email').val() == '') {
        $('#email').parent().find('.validation-msg').show();
        $('#email').focus();
        return false;
    } else {
        $('#email').parent().find('.validation-msg').hide();
    }
    if (telInput.intlTelInput("isValidNumber")) {
        validMsg.removeClass("hide");
    } else {
        telInput.addClass("error");
        errorMsg.removeClass("hide");
        return false;
    }
    if ($('#cv').val() == '') {
        $('#cv').parent().find('.validation-msg').show();
        return false;
    } else {
        $('#cv').parent().find('.validation-msg').hide();
    }
    showLoader('ajaxloader');
    setTimeout(function () {
        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                // alertSimple(json.messages);
                setTimeout(function () {
                    window.location = base_url + '/thank-you';
                }, 3000);
            }
        } catch (e) {
            alert(e);
        }
    }, 200);
});
$('.focusrequestcallBack').click(function () {
    $('html, body').animate({
        scrollTop: (parseInt($("#requestcallBack").offset().top) - 200)
    }, 0);
});

</script>
<!--flagS-->