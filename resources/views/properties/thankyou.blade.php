@include('properties.includes.header')
<div class="wd100 breadcrumb_wrap __liist_property_bnr">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <!--<li class="breadcrumb-item"><a href="<= url('/property') ?>">List Your Property</a></li>-->
                <li class="breadcrumb-item"> Thank you </li>
            </ol>
        </nav>
    </div>
</div>

<section class="section page_not_found">
    <div class="container">
        <div class="wd100 page_not_foundbz">
            <img class="img-fluid" src="{{url('/public/lib/images/checked.png')}}" />

           <?= $message ?>
        </div>


        <div class="wd100 text-center">
            <div class="__bpgWrp">
                <a href="<?= url('/') ?>" class="__bpgWBz">
                    Back to Homepage
                </a>

            </div>


            <div class="__bpgsociWrp">
                <h5>Let's connect!</h5>
                <div class="_social_icons_top">
                      <a target="_blank" href="https://www.facebook.com/ExcelProperties1"><i class="fab fa-facebook-f"></i></a>
                    <a target="_blank" href="https://twitter.com/PropertiesExcel"><i class="fab fa-twitter"></i></a>
                    <!--<a target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>-->
                    <a target="_blank" href="https://www.youtube.com/channel/UCMJwg4ZzwspdObYR-uSRypA"><i class="fab fa-youtube"></i></a> 
                    <a target="_blank" href="https://www.instagram.com/excel_properties/"><i class="fab fa-instagram"></i></a>
                </div>
            </div>


        </div>


    </div>

</section>
@include('properties.includes.footer')