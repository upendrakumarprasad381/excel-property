@include('properties.includes.header')

<?php
$SERVICES_PAGE_AREA_2 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'SERVICES_PAGE_AREA_2');
$SERVICES_PAGE_AREA_1_TITLE = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'SERVICES_PAGE_AREA_1_TITLE');
$SERVICES_PAGE_AREA_1 = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'SERVICES_PAGE_AREA_1');
?>
<div class="wd100 __innerbanner __services_bnt">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Services</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2><?= $SERVICES_PAGE_AREA_1->col13 ?></h2>
        <div class="__description"> <?= $SERVICES_PAGE_AREA_1->col14 ?> </div>
    </div>
</div>
<section class="section __servicesPg">
    <div class="container">
        <div class="wd100 __servicesTxbz">
            <p><?= $SERVICES_PAGE_AREA_2->col13 ?></p>
        </div>
    </div>



    <div class="container">

        <div class="__sRVBwrap row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-2">

            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col1) ? $SERVICES_PAGE_AREA_2->col1 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/fluctuation.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col1 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col1 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col2) ? $SERVICES_PAGE_AREA_2->col2 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/building.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col2 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col2 ?></p>
                </div>
            </div>

            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col3) ? $SERVICES_PAGE_AREA_2->col3 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/technician.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col3 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col3 ?></p>
                </div>
            </div>

            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col4) ? $SERVICES_PAGE_AREA_2->col4 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/hospital.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col4 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col4 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col5) ? $SERVICES_PAGE_AREA_2->col5 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/land.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col5 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col5 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col6) ? $SERVICES_PAGE_AREA_2->col6 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/industr.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> 
                        <img class="img-fluid" src="{{$file}}"> 
                    </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col6 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col6 ?></p>                
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col7) ? $SERVICES_PAGE_AREA_2->col7 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/building-1.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col7 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col7 ?></p>                
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col8) ? $SERVICES_PAGE_AREA_2->col8 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/RETAIL.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col8 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col8 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col9) ? $SERVICES_PAGE_AREA_2->col9 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/advisor.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col9 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col9 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col10) ? $SERVICES_PAGE_AREA_2->col10 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/residential.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col10 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col10 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col11) ? $SERVICES_PAGE_AREA_2->col11 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/assets.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col11 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col11 ?></p>
                </div>
            </div>


            <div class="__sRVBBoz">
                <div class="__sRVBQEInr wd100">
                    <?php
                    $file = "files/hostgallery/" . (!empty($SERVICES_PAGE_AREA_2->col12) ? $SERVICES_PAGE_AREA_2->col12 : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                        $file = url($file);
                    } else {
                        $file = url('/public/lib/images/student-with-graduation-cap.png');
                    }
                    ?>
                    <div class="wd100 __sRVBQEInrImg"> <img class="img-fluid" src="{{$file}}"> </div>
                    <h4><?= $SERVICES_PAGE_AREA_1_TITLE->col12 ?></h4>
                    <p><?= $SERVICES_PAGE_AREA_1->col12 ?></p>
                </div>
            </div>



        </div>


    </div>







</section>
@include('properties.includes.footer')