@include('properties.includes.header')


<?php
 $NEW_DEVELOPMENT_SETTINGS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'NEW_DEVELOPMENT_SETTINGS');
 
  $file = "files/hostgallery/" . (!empty($NEW_DEVELOPMENT_SETTINGS->col7) ? $NEW_DEVELOPMENT_SETTINGS->col7 : '');
    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
        $file = url($file);
    } else {
        $file = url("public/images/Area-Guides.jpg");
    }
?>
<div class="wd100 __innerbanner __area_guide_details_bnr" data-overlay="dark" data-opacity="4" style="    background: url(<?= $file ?>) no-repeat center center;">


    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Real Estate Developers</a></li>
                </ol>
            </nav>
        </div>
    </div>


    <div class="container">
        <h2><?= $NEW_DEVELOPMENT_SETTINGS->col3 ?></h2>
    </div>


</div>


<section class="section __scoutinner __area_guidesPg">
    <div class="container">
         <h3><?= $NEW_DEVELOPMENT_SETTINGS->col1 ?></h3>
        <p><?= $NEW_DEVELOPMENT_SETTINGS->col2 ?></p>
        <div class="row">
            <?php
            $Sql = "SELECT SC.heading,SC.slugs,SC.ndid,SC.featured_image,SC.heading AS parents_name  FROM `new_developments` SC  WHERE SC.archive=0 AND SC.parents_id  IN (0) ORDER BY SC.position ASC ";
            $listAr = \App\Database::select($Sql);
            for ($i = 0; $i < count($listAr); $i++) {
                $d = $listAr[$i];
                $file = "files/hostgallery/" . (!empty($d->featured_image) ? $d->featured_image : '');
                $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
               // $url = url('dubai/real-estate-developers/' . $d->slugs);
                 $url = url('dubai/real-estate-developers/' . $d->slugs);
                ?>
                <div class="col-lg-4 col-md-6 col-sm-12 __aGBoz">
                    <a href="<?= $url ?>" class="wd100 __aGBozir">
                        <img class="img-fluid" src="{{url($file)}}" >
                        <div class="__aGBozirTX"><?= $d->parents_name  ?></div>
                    </a > 
                </div>
            <?php } ?>
        </div>
    </div>
</section>
@include('properties.includes.footer')	
<!--flagS-->
