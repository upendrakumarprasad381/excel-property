@include('properties.includes.header')

<?php
$NEW_DEVELOPMENT_SETTINGS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'NEW_DEVELOPMENT_SETTINGS');

$projectsIdslug = Request::segment(3);



$Sql = "SELECT ndid,heading,description,banner  FROM `new_developments` WHERE `slugs` LIKE '$projectsIdslug' AND archive=0";
$datalist = \App\Database::selectSingle($Sql);
$projectsId = !empty($datalist->ndid) ? $datalist->ndid : '';

if (!empty($datalist)) {
    $file = "files/hostgallery/" . (!empty($datalist->banner) ? $datalist->banner : '');
    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
        $file = url($file);
    } else {
        $file = url("public/images/Area-Guides.jpg");
    }
} else {
    $file = url("public/images/Area-Guides.jpg");
}
?>

<div class="wd100 __innerbanner __area_guide_details_bnr" data-overlay="dark" data-opacity="4" style="    background: url(<?= $file ?>) no-repeat center center;">


    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?= url('/dubai/real-estate-developers') ?>">New Developments In Dubai</a></li>
                    <?php if (!empty($datalist)) {
                        ?><li class="breadcrumb-item"><a href="<?= url("/dubai/real-estate-developers/$projectsIdslug") ?>"><?= $datalist->heading ?></a></li><?php }
                    ?>
                </ol>
            </nav>
        </div>
    </div>


    <div class="container">
        <h2><?= !empty($datalist->heading) ? $datalist->heading : $NEW_DEVELOPMENT_SETTINGS->col4 ?></h2>
    </div>


</div>


<section class="section __scoutinner __area_guidesPg">
    <div class="container">

        <h3><?= !empty($datalist->heading) ? 'About ' . $datalist->heading : $NEW_DEVELOPMENT_SETTINGS->col5 ?></h3>
        <p><?= !empty($datalist->description) ? $datalist->description : $NEW_DEVELOPMENT_SETTINGS->col6 ?></p>   

        <div class="row">
            <?php
            $cond = "";

            if (!empty($projectsId)) {
                $cond = $cond . " AND SC.parents_id IN ($projectsId)";
            }



            $Sql = "SELECT SC.heading,SC.ndid,SC.banner,SC.slugs,P.heading AS parents_name,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id NOT IN (0) $cond ORDER BY P.position ASC,SC.position ASC ";

            $listAr = \App\Database::select($Sql);
            for ($i = 0; $i < count($listAr); $i++) {
                $d = $listAr[$i];
                $file = "files/hostgallery/" . (!empty($d->banner) ? $d->banner : '');
                $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                $url = url("dubai/real-estate-developers/$d->parents_slugs/$d->slugs");
                ?>
                <div class="col-lg-4 col-md-6 col-sm-12 __aGBoz">
                    <a href="<?= $url ?>" class="wd100 __aGBozir">
                        <img class="img-fluid" src="{{url($file)}}" >
                        <div class="__aGBozirTX"><?= $d->parents_name . ' - ' . $d->heading ?></div>
                    </a > 
                </div>
            <?php } ?>
        </div>


    </div>
</section>
@include('properties.includes.footer')	
<!--flagS-->
