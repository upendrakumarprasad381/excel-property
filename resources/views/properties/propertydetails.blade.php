@include('properties.includes.header')
<?php
$Sql = "SELECT property_id  FROM `my_property` WHERE `slugs` LIKE '$propertyslugId' AND archive=0";
$slug = \App\Database::selectSingle($Sql);

if (empty($slug)) {
    header("location: " . url('/'));
    exit;
}

$propertyId = $slug->property_id;
$pArray = App\Helpers\LibHelper::GetmypropertyBy($propertyId);


$ptype = \App\Helpers\LibHelper::GetpropertytypeBy($pArray->property_type);
$tower = \App\Helpers\LibHelper::GettowerlistBy($pArray->tower_id);
$aread = \App\Helpers\LibHelper::GetarealistByareaId($pArray->area_id);

$ptypeUrl = empty($pArray->ptype) ? 'properties-for-sale' : 'properties-for-rent';

$bedRoomHide = [6, 9];
?>

<div class="wd100 breadcrumb_wrap __liist_property_bnr">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="<?= url("dubai/$ptypeUrl/$ptype->slugs") ?>"><?= !empty($ptype->ptype_name) ? ucwords($ptype->ptype_name) : '' ?></a></li>
                <li class="breadcrumb-item"><a href="<?= url("dubai/$ptypeUrl/area/$aread->slugs") ?>"><?= !empty($aread->area_name) ? ucwords($aread->area_name) : '' ?></a></li>
                <li class="breadcrumb-item"><a href="<?= url("dubai/$ptypeUrl/tower/$tower->slugs") ?>"><?= !empty($tower->tower_id) ? ucwords($tower->tower_name) : '' ?></a></li>

            </ol>
        </nav>
    </div>
</div> 

<input type="hidden" id="property_id" value="<?= $pArray->property_id ?>">
<section class="section __liist_property_Pg">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-md-12 col-sm-12 __listProLt">

                <div class="wd100 __listProLt_slider">

                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">

                        <div class="carousel-indicators">
                            <?php
                            $slider = "";
                            $igArray = App\Helpers\LibHelper::GetmypropertygalleryBy(!empty($propertyId) ? $propertyId : '0');
                            for ($i = 0; $i < count($igArray); $i++) {
                                $d = $igArray[$i];
                                $fileName = 'files/hostgallery/' . $d->file_name;
                                $fileName = is_file(Config::get('constants.HOME_DIR') . $fileName) ? $fileName : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                                $slider = $slider . '<div class="carousel-item ' . (empty($i) ? 'active' : '') . '">
                                                   <img src="' . url($fileName) . '" class="d-block w-100" alt="...">
                                                </div>';
                                ?>
                                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?= $i ?>" class="<?= empty($i) ? 'active' : '' ?>" aria-current="true" aria-label="Slide <?= $i ?>"></button>
                                <?php
                            }
                            if (empty($igArray)) {
                                ?> <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 0"></button><?php
                            }
                            ?>
                        </div>
                        <div class="carousel-inner">
                            <?php
                            echo $slider;
                            if (empty($igArray)) {
                                ?>
                                <div class="carousel-item active">
                                    <img src="{{url('/public/lib/images/ProLt_slider.jpg')}}" class="d-block w-100" alt="...">
                                </div>
                            <?php } ?>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>


                </div>

                <div class="wd100 __liTextBk">
                    <h6><?= $pArray->address ?></h6>
                    <h3><?= $pArray->property_title ?></h3> 

                </div>


                <div class="wd100 __furcboz">

                    <div class="__furcbItm">
                        <img src="{{url('/public/lib/images/tag.png')}}"> 
                        <h6>AED <?= App\Helpers\CommonHelper::DecimalAmount($pArray->price) ?> </h6>
                    </div>

                    <div class="__furcbItm" style="display: <?= in_array($pArray->property_type, $bedRoomHide) ? 'none' : 'block' ?>">
                        <img src="{{url('/public/lib/images/bed.png')}}">
                        <h6><?= !empty($pArray->is_studio) ? 'Studio' : $pArray->bed.' Bed' ?> </h6>
                    </div>


                    <div class="__furcbItm" style="display: <?= in_array($pArray->property_type, $bedRoomHide) ? 'none' : 'block' ?>">
                        <img src="{{url('/public/lib/images/shower.png')}}">
                        <h6><?= $pArray->bathroom ?> Bath</h6>
                    </div>

                    <div class="__furcbItm">
                        <img src="{{url('/public/lib/images/square-measument.png')}}">
                        <h6><?= $pArray->area ?> Sq.Ft.</h6>
                    </div>
                    <?php if (!empty($pArray->permit_number)) { ?>
                   
                        <div class="__furcbItm">
                             <div class="__permitinr">
                            Permit No.<br/>
                        </div>
                            <!--<img src="{{url('/public/lib/images/permit.png')}}">-->
                            <h6><?= !empty($pArray->permit_number) ? $pArray->permit_number : '' ?> </h6>
                        </div>
                    <?php } ?>
                </div>


                <div class="wd100 __lipgdescri">
                    <h4>Description</h4>
                    <?= $pArray->description ?>
                </div>
            </div> 

            <div class="col-lg-4 col-md-12 col-sm-12 __listProRt" id="requestcallBack">
                <div class="wd100 __LprtBoz">
                    <h5>Our Expert Will Help You  <?= empty($pArray->ptype) ? 'Buy' : 'Rent' ?> The Best Property</h5> 
                    <div class="wd100">
                        <div class="mb-4 mt-4"> 
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>
                        <div class="mb-4"> 
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div>
                        <div class="mb-4"> 
                            <label>Phone</label>
                            <input class="form-control" id="phone" type="number"  placeholder="Phone">
                            <span id="valid-msg" class="hide">Valid</span>
                            <span id="error-msg" class="hide">Invalid number</span>
                        </div>

                        <!-- <div class="mb-3"> 
                            <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                        </div> -->
                        <div class="wd100 ">
                            <button id="sendcontactus" type="button" style="background: #540a21;color: #f9fafb;" class="btn __btnQcaB" >Request a free call back <div id="ajaxloader"></div></button>
                        </div>
                    </div>  
                </div>

                <!-- New Blk SS-->
                <div class="wd100 __rtwdgTliAds" >
                    <h6>EXCEL PROPERTIES</h6>
                    <h3>Sell / Lease Your <br/>
                        Property In Dubai</h3>
                    <p>List your property with us get the best return Sale & Rent</p>
                    <div class="wd100 text-center">
                        <a class="__rewgtBnt" href="https://pandacareapp.granddubai.com/excel-property/dubai/list-with-us" >LIST WITH US</a>
                    </div>
                </div>
                <!-- New Blk EEE-->


                <?php
                $owner = \App\Helpers\LibHelper::GetagentsByagentsId($pArray->owner_id);
                ?>
                <div class="wd100 __agent_info">
                    <h4><b>Reference:</b> <?= !empty($pArray->property_no) ? $pArray->property_no : 'NA' ?></h4>
                    <!--<h3>PROPERTY REPRESENTATIVE</h3>-->

                    <div class="d-flex __agent_wrap">

                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($owner->logo) ? $owner->logo : '');
                            $repLogo = url('/public/lib/images/logo/logo_icon_w.svg');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $repLogo = url($file);
                            }
                            ?>
                            <img style="height: 100px;" src="{{$repLogo}}" > 
                        </div> 
                        <div class="flex-grow-1 ms-3">

                            <h5><?= !empty($owner->name) ? $owner->name : 'NA' ?></h5>
                            <h6><?= !empty($owner->post) ? $owner->post : 'NA' ?></h6>
                            <h6>Broker No: <?= !empty($owner->reference_no) ? $owner->reference_no : 'NA' ?></h6>  
                        </div> 
                    </div> 

                    <div class="wd100">
                        <a href="tel:<?= $pArray->agent_phone ?>" id="" class="__bnt_contactagent">
                            Contact Agent
                        </a>
                        <a  href="javascript:void(0)" class="__bnt_download" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Download Property Info

                        </a> 





                        <!-- Modal -->
                        <div class="modal fade __dwdpuowrp " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    <div class="modal-header mb-4">
                                        <h5 class="modal-title" id="exampleModalLabel">Save to PDF</h5>

                                    </div>
                                    <div class="modal-body  p-0">

                                        <div class="wd100">


                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                                                <!--<label>Name</label>-->
                                                <input type="text" class="form-control" id="poup_name" placeholder="Name"> 
                                                <span class="validation-msg">Enter your name</span>
                                            </div>


                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                                                <!--<label>Email</label>-->
                                                <input type="text" class="form-control" id="poup_email" placeholder="Email">
                                                <span class="validation-msg">Enter valid email address.</span>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3"> 
                                                <!--<label>Phone</label>-->
                                                <input type="number" class="form-control" id="poup_phone" placeholder="Phone">
                                                <span class="validation-msg">Enter your phone</span>
                                            </div>
                                            <div class="form-check mb-2">
                                                <input class="form-check-input" name="fileDownload" checked="" type="radio" value="<?= url('data/fronted/viewPdf?withoutprice=1&propertyId=' . base64_encode($propertyId)) ?>" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Without Price
                                                </label>
                                            </div>

                                            <div class="form-check mb-2"> 
                                                <input class="form-check-input" type="radio" name="fileDownload" value="<?= url('data/fronted/viewPdf?propertyId=' . base64_encode($propertyId)) ?>" id="flexCheckChecked">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    One Page
                                                </label>
                                            </div> 

                                        </div> 


                                    </div>
                                    <div class="modal-footer">
                                        <a href="javascript:void(0)"  id="downloadpdf"  class="__bnt_download">Download 
                                            <div style="display: contents;" id="ajaxloadermodal"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            
            
            
            
            <?php
            if (!empty($pArray->facility)) {
                $Sql = "SELECT * FROM `property_facility` WHERE facility_id IN ($pArray->facility) AND archive=0";
                $dArray = \App\Database::select($Sql);
            }
            if (!empty($dArray)) {
                ?>
                <div class="wd100 __amenitiesWrp">
                    <h2>AMENITIES</h2>
                    <div class="__amenitiesWrpScx row row-cols-3 row-cols-sm-3 row-cols-md-4 row-cols-lg-6">
                        <?php
                        for ($i = 0; $i < count($dArray); $i++) {
                            $d = $dArray[$i];
                            $basepath = 'files/facility-logo/' . $d->rfacility_logo;
                            $basepath = is_file(Config::get('constants.HOME_DIR') . $basepath) ? $basepath : Config::get('constants.DEFAULT_FACILITY_LOGO');
                            ?>
                            <div class="col __amItmBoz">
                                <img class="img-fluid" src="{{url($basepath)}}">
                                <h6><?= $d->facility_name ?></h6> 
                            </div>
                        <?php } ?>
                    </div>

                </div>
            <?php } ?>






 <div class="col-lg-12 col-md-12 col-sm-12 __listProRt  __mobviewWegt" id="requestcallBack">
                <div class="wd100 __LprtBoz">
                    <h5>Our Expert Will Help You  <?= empty($pArray->ptype) ? 'Buy' : 'Rent' ?> The Best Property</h5> 
                    <div class="wd100">
                        <div class="mb-4 mt-4"> 
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>
                        <div class="mb-4"> 
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div>
                        <div class="mb-4"> 
                            <label>Phone</label>
                            <input class="form-control" id="phone" type="number"  placeholder="Phone">
                            <span id="valid-msg" class="hide">Valid</span>
                            <span id="error-msg" class="hide">Invalid number</span>
                        </div>

                        <!-- <div class="mb-3"> 
                            <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                        </div> -->
                        <div class="wd100 ">
                            <button id="sendcontactus" type="button" style="background: #540a21;color: #f9fafb;" class="btn __btnQcaB" >Request a free call back <div id="ajaxloader"></div></button>
                        </div>
                    </div>  
                </div>

                <!-- New Blk SS-->
                <div class="wd100 __rtwdgTliAds" >
                    <h6>EXCEL PROPERTIES</h6>
                    <h3>Sell / Lease Your <br/>
                        Property In Dubai</h3>
                    <p>List your property with us get the best return Sale & Rent</p>
                    <div class="wd100 text-center">
                        <a class="__rewgtBnt" href="https://pandacareapp.granddubai.com/excel-property/dubai/list-with-us" >LIST WITH US</a>
                    </div>
                </div>
                <!-- New Blk EEE-->


                <?php
                $owner = \App\Helpers\LibHelper::GetagentsByagentsId($pArray->owner_id);
                ?>
                <div class="wd100 __agent_info">
                    <h4><b>Reference:</b> <?= !empty($pArray->property_no) ? $pArray->property_no : 'NA' ?></h4>
                    <!--<h3>PROPERTY REPRESENTATIVE</h3>-->

                    <div class="d-flex __agent_wrap">

                        <div class="flex-shrink-0">
                            <?php
                            $file = "files/hostgallery/" . (!empty($owner->logo) ? $owner->logo : '');
                            $repLogo = url('/public/lib/images/logo/logo_icon_w.svg');
                            if (is_file(Config::get('constants.HOME_DIR') . $file)) {
                                $repLogo = url($file);
                            }
                            ?>
                            <img style="height: 100px;" src="{{$repLogo}}" > 
                        </div> 
                        <div class="flex-grow-1 ms-3">

                            <h5><?= !empty($owner->name) ? $owner->name : 'NA' ?></h5>
                            <h6><?= !empty($owner->post) ? $owner->post : 'NA' ?></h6>
                            <h6>Broker No: <?= !empty($owner->reference_no) ? $owner->reference_no : 'NA' ?></h6>  
                        </div> 
                    </div> 

                    <div class="wd100">
                        <a href="tel:<?= $pArray->agent_phone ?>" id="" class="__bnt_contactagent">
                            Contact Agent
                        </a>
                        <a  href="javascript:void(0)" class="__bnt_download" data-bs-toggle="modal" data-bs-target="#exampleModal2">
                            Download Property Info

                        </a> 





                        <!-- Modal -->
                        <div class="modal fade __dwdpuowrp " id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    <div class="modal-header mb-4">
                                        <h5 class="modal-title" id="exampleModalLabel">Save to PDF</h5>

                                    </div>
                                    <div class="modal-body  p-0">

                                        <div class="wd100">


                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                                                <!--<label>Name</label>-->
                                                <input type="text" class="form-control" id="poup_name" placeholder="Name"> 
                                                <span class="validation-msg">Enter your name</span>
                                            </div>


                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                                                <!--<label>Email</label>-->
                                                <input type="text" class="form-control" id="poup_email" placeholder="Email">
                                                <span class="validation-msg">Enter valid email address.</span>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-sm-12  mb-3"> 
                                                <!--<label>Phone</label>-->
                                                <input type="number" class="form-control" id="poup_phone" placeholder="Phone">
                                                <span class="validation-msg">Enter your phone</span>
                                            </div>
                                            <div class="form-check mb-2">
                                                <input class="form-check-input" name="fileDownload" checked="" type="radio" value="<?= url('data/fronted/viewPdf?withoutprice=1&propertyId=' . base64_encode($propertyId)) ?>" id="flexCheckDefault">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    Without Price
                                                </label>
                                            </div>

                                            <div class="form-check mb-2"> 
                                                <input class="form-check-input" type="radio" name="fileDownload" value="<?= url('data/fronted/viewPdf?propertyId=' . base64_encode($propertyId)) ?>" id="flexCheckChecked">
                                                <label class="form-check-label" for="flexCheckChecked">
                                                    One Page
                                                </label>
                                            </div> 

                                        </div> 


                                    </div>
                                    <div class="modal-footer">
                                        <a href="javascript:void(0)"  id="downloadpdf"  class="__bnt_download">Download 
                                            <div style="display: contents;" id="ajaxloadermodal"></div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            
            

















        </div> 
    </div> 
</section>
<?php if (empty($pArray->ptype)) { ?>
    <section class="section __mortgageWrp">
        <div class="container"> 
            <div class="wd100 __mortgageBz">
                <h2>Mortgage</h2>
                <p>Calculate and view the monthly mortgage on this Apartment</p>



                <div class="wd100 __morgbwr">

                    <div class="col __morgb __gchtg">

                        <div class="wd100 __mrgrtmb"> 
                            <div class="row align-items-center">
                                <div class="col __frtxfprt">
                                    <label  class="col-form-label">Total Price (AED)</label>
                                </div>
                                <div class="col __frrelTprt">
                                    <input type="text" class="form-control" name="loanamount" value="<?= $pArray->price ?>" id="loanamount" placeholder="1864000"  >
                                </div> 
                            </div>
                        </div>


                        <div class="wd100 __mrgrtmb"> 
                            <div class="row align-items-center">
                                <div class="col __frtxfprt">
                                    <label  class="col-form-label">Down Payment (%)</label>
                                </div>
                                <div class="col __frrelTprt">
                                    <input type="text" class="form-control easy-mortgage-input" value="<?= $pArray->downpayment ?>" min="0" max="99" name="downpayment" id="downpayment" placeholder="25"  >
                                </div> 
                            </div>
                        </div>





                        <div class="wd100 __mrgrtmb"> 
                            <div class="row align-items-center">
                                <div class="col __frtxfprt">
                                    <label  class="col-form-label">Interest Rate (%)</label>
                                </div>
                                <div class="col __frrelTprt">
                                    <input type="text" name="loanintrest" id="loanintrest" value="<?= $pArray->loanintrest ?>" class="form-control easy-mortgage-input" placeholder="3.75"  >
                                </div> 
                            </div>
                        </div>



                        <div class="wd100 __mrgrtmb"> 
                            <div class="row align-items-center">
                                <div class="col __frtxfprt">
                                    <label  class="col-form-label">Loan Period Yearly</label>
                                </div>
                                <div class="col __frrelTprt">
                                    <input type="text" class="form-control easy-mortgage-input" value="<?= $pArray->period ?>" name="period" id="period" placeholder="25"  >
                                </div> 
                            </div>
                        </div>


                    </div>


                    <div class="col __morgb __mortgage_result">


                        <ul>
                            <li class=""><strong>Total you will repay:</strong> <span  id="total_amount">AED 0.00</span></li>
                            <li class=""><strong>Down Payment:</strong> <span  id="downpament_a">AED 0.00</span></li>
                            <li class=""><strong>Total Interest:</strong> <span  id="total_interest">AED 0.00</span></li>
                            <li class=""><h4><strong><span  id="permonth">AED 0.0</span> /month</strong></h4></li>
                        </ul>
                    </div>





                </div>


            </div>


        </div>
    </section>
<?php } ?>

<?php if (!empty($pArray->lat)) { ?>
    <section class="section __scnB2 __community __mapListPro">
         <div class="container "> 
            <div class="__ofpMAwz">
                <h2>Location</h2>   
            </div>
        </div>
        <script>
                            var defaultLat = '<?= $pArray->lat ?>';
                            var defaultLong = '<?= $pArray->lng ?>';
                            var locationName = '<?= str_replace("'", '`', (!empty($tower->tower_name) ? $tower->tower_name : '')) ?>';
                        </script>
        <div class="map-wrapper-inner" id="map-page">
            <div id="google-maps-box">
                <div id="map" style="width:100%; height:350px;"></div>
            </div>
        </div>
      
    </section> 
<?php } ?>

<section class="section  __similar_properties">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="__scnB1txtboz">
                    <h1>SIMILAR PROPERTIES</h1> 
                </div>
            </div>
        </div>

        <div class="inside_box wd100">
            <!-- Swiper S-->
            <div class="__srbk5 wd100">
                <div class="swiper-wrapper">
                    <?php
                    $Sql = "SELECT MP.*,getpropertyImage(MP.property_id) AS first_image,getpropertyDetailsUrl(MP.property_id) AS urlSlug FROM `my_property` MP WHERE MP.archive=0 AND MP.status=0 AND MP.ptype='$pArray->ptype' AND MP.property_type='$pArray->property_type'  ORDER BY MP.position ASC  LIMIT 50"; // AND MP.area_id='$pArray->area_id'
                    $listAr = \App\Database::select($Sql);
                    if (!empty($listAr)) {

                        for ($j = 0; $j < count($listAr); $j++) {
                            $fg = $listAr[$j];
                            $images = \App\Helpers\CommonHelper::GetPropertyImagesCover($fg);
                            $url = url($fg->urlSlug);
                            ?>

                            <div class="swiper-slide">
                                <div class="wd100 __prligrdwrp">
                                    <div class="__prligrd wd100">
                                        <div class="__prligrdimg">
                                            <a href="{{$url}}">
                                                <img class="img-fluid" src="{{$images}}" />
                                            </a>
                                            <div class="__proname"> <a href="{{$url}}">{{$fg->property_title}}</a> 
                                            </div>
                                        </div>
                                        <div class="__prligrddrcp">
                                            <div class="  __priqustbx"> <small>PRICE</small>
                                                <br />AED <?= \App\Helpers\CommonHelper::DecimalAmount($fg->price) ?></div>
                                            <div style="display: <?= in_array($fg->property_type, $bedRoomHide) ? 'none' : '' ?>" class="  __bedsbx"> <small><?= !empty($fg->is_studio) ? '&nbsp;' : $fg->bed ?></small>
                                                <br /><?= !empty($fg->is_studio) ? 'Studio' : 'BED' ?></div>
                                            <div class="  __sqftsbx"><?= \App\Helpers\CommonHelper::DecimalAmount($fg->area) ?> SQ.FT</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>




                </div>
            </div>
        </div> 
    </div>
</section>

@include('properties.includes.footer')	



<!--flagS-->

<script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
<script src="{{url('/public')}}/assets/js/mapmodel_user.js" ></script>   

<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
<?php if (empty($pArray->ptype)) { ?>
    <script src="{{url('/public/js/mortgage.js')}}"></script>
<?php } ?>
<script>

                            var telInput = $("#phone"),
                                    errorMsg = $("#error-msg"),
                                    validMsg = $("#valid-msg");

// initialise plugin
                            telInput.intlTelInput({
                                allowExtensions: true,
                                formatOnDisplay: true,
                                autoFormat: true,
                                autoHideDialCode: true,
                                autoPlaceholder: true,
                                defaultCountry: "auto",
                                ipinfoToken: "yolo",
                                nationalMode: false,
                                numberType: "MOBILE",
//onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                                preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
                                preventInvalidNumbers: true,
                                separateDialCode: true,
                                initialCountry: "auto",
                                geoIpLookup: function (callback) {
                                    $.get("http://ipinfo.io", function () {
                                    }, "jsonp").always(function (resp) {
                                        var countryCode = (resp && resp.country) ? resp.country : "";
                                        callback(countryCode);
                                    });
                                },
                                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
                            });

                            var reset = function () {
                                telInput.removeClass("error");
                                errorMsg.addClass("hide");
                                validMsg.addClass("hide");
                            };

// on blur: validate
                            telInput.blur(function () {
                                reset();
                                if ($.trim(telInput.val())) {
                                    if (telInput.intlTelInput("isValidNumber")) {
                                        validMsg.removeClass("hide");
                                    } else {
                                        telInput.addClass("error");
                                        errorMsg.removeClass("hide");
                                    }
                                }
                            });
                            $("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
                            telInput.on("keyup change", reset);


                            $('#sendcontactus').click(function () {
                                var form = new FormData();
                                form.append('_token', CSRF_TOKEN);
                                form.append('function', 'mypropertyEnq');
                                form.append('helper', 'Common');

                                form.append('json[property_id]', $('#property_id').val());
                                form.append('json[page_url]', $('#page_url').val());
                                form.append('json[name]', $('#name').val());
                                form.append('json[email]', $('#email').val());
                                form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
                                form.append('json[messages]', $('#messages').val());


                                if ($('#name').val() == '') {
                                    $('#name').parent().find('.validation-msg').show();
                                    $('#name').focus();
                                    return false;
                                } else {
                                    $('#name').parent().find('.validation-msg').hide();
                                }
                                if (!validateEmail($('#email').val()) || $('#email').val() == '') {
                                    $('#email').parent().find('.validation-msg').show();
                                    $('#email').focus();
                                    return false;
                                } else {
                                    $('#email').parent().find('.validation-msg').hide();
                                }
                                if (telInput.intlTelInput("isValidNumber")) {
                                    validMsg.removeClass("hide");
                                } else {
                                    telInput.addClass("error");
                                    errorMsg.removeClass("hide");
                                    return false;
                                }
                                showLoader('ajaxloader');
                                setTimeout(function () {
                                    var json = ajaxpost(form, "/helper");
                                    try {
                                        var json = jQuery.parseJSON(json);
                                        if (json.status == true) {
                                            // alertSimple(json.messages);
                                            setTimeout(function () {
                                                window.location = base_url + '/thank-you';
                                            }, 3000);
                                        }
                                    } catch (e) {
                                        alert(e);
                                    }
                                }, 200);
                            });





                            $('#downloadpdf').click(function () {
                                var form = new FormData();
                                var fileName = $('input:radio[name=fileDownload]:checked').val();

                                form.append('_token', CSRF_TOKEN);
                                form.append('function', 'mypropertyEnq');
                                form.append('helper', 'Common');

                                form.append('json[property_id]', $('#property_id').val());
                                form.append('json[page_url]', $('#page_url').val());
                                form.append('json[name]', $('#poup_name').val());
                                form.append('json[email]', $('#poup_email').val());
                                form.append('json[mobile_no]', $("#poup_phone").val());
                                form.append('json[messages]', 'PDF Download');


                                if ($('#poup_name').val() == '') {
                                    $('#poup_name').parent().find('.validation-msg').show();
                                    $('#poup_name').focus();
                                    return false;
                                } else {
                                    $('#poup_name').parent().find('.validation-msg').hide();
                                }
                                if (!validateEmail($('#poup_email').val()) || $('#poup_email').val() == '') {
                                    $('#poup_email').parent().find('.validation-msg').show();
                                    $('#poup_email').focus();
                                    return false;
                                } else {
                                    $('#poup_email').parent().find('.validation-msg').hide();
                                }
                                if ($('#poup_phone').val() == '') {
                                    $('#poup_phone').parent().find('.validation-msg').show();
                                    $('#poup_phone').focus();
                                    return false;
                                } else {
                                    $('#poup_phone').parent().find('.validation-msg').hide();
                                }


                                showLoader('ajaxloadermodal');
                                setTimeout(function () {
                                    var json = ajaxpost(form, "/helper");
                                    try {
                                        var json = jQuery.parseJSON(json);
                                        if (json.status == true) {
                                            // alertSimple(json.messages);
                                            setTimeout(function () {
                                                window.location = fileName;
                                            }, 3000);
                                        }
                                    } catch (e) {
                                        alert(e);
                                    }
                                }, 200);
                            });
//                            $("input[name='fileDownload']").click(function () {
//                                var fileName = $('input:radio[name=fileDownload]:checked').val();
//                                $('#downloadpdf').attr('href', fileName);
//                            });
</script>
<!--flagS-->