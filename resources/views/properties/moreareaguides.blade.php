@include('properties.includes.header')
<?php
$AREA_GUIDES_SETTINGS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'AREA_GUIDES_SETTINGS');
$file = "files/hostgallery/" . (!empty($AREA_GUIDES_SETTINGS->col2) ? $AREA_GUIDES_SETTINGS->col2 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("public/images/Area-Guides.jpg");
}
?>

<div class="wd100 __innerbanner __area_guide_details_bnr" data-overlay="dark" data-opacity="4" style="    background: url(<?= $file ?>) no-repeat center center;">


    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="">Dubai Communities</a></li>
                </ol>
            </nav>
        </div>
    </div>


    <div class="container">
        <h2><?= $AREA_GUIDES_SETTINGS->col1 ?></h2>
    </div>


</div>





<section class="section __scoutinner __area_guidesPg">
    <div class="container">
        <div class="row">
            <?php
            $Sql = "SELECT * FROM `area_guides` WHERE archive=0 ORDER BY position ASC";
            $listAr = \App\Database::select($Sql);
            for ($i = 0; $i < count($listAr); $i++) {
                $d = $listAr[$i];
                $file = "files/hostgallery/" . (!empty($d->logo) ? $d->logo : '');
                $file = is_file(Config::get('constants.HOME_DIR') . $file) ? $file : Config::get('constants.DEFAULT_PROPERTY_LOGO');
                ?>
                <div class="col-lg-4 col-md-6 col-sm-12 __aGBoz">
                    <a href="<?= url("dubai/area-guides/$d->slugs") ?>" class="wd100 __aGBozir">
                        <img class="img-fluid" src="{{url($file)}}" >
                        <div class="__aGBozirTX"><?= $d->area_name ?></div>
                    </a > 
                </div>
            <?php } ?>
        </div>
    </div>
</section>
@include('properties.includes.footer')	
<!--flagS-->
