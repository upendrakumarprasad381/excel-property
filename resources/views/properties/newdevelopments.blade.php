@include('properties.includes.header')
<?php
$Sql = "SELECT *  FROM `new_developments` WHERE `slugs` LIKE '$developerId' AND archive=0";
$developer = \App\Database::selectSingle($Sql);
$tempId = !empty($developer->ndid) ? $developer->ndid : '';

$Sql = "SELECT ndid  FROM `new_developments` WHERE `parents_id` = '$tempId' AND slugs='$projectsId' AND archive=0";
$datalist = \App\Database::selectSingle($Sql);
$id = !empty($datalist->ndid) ? $datalist->ndid : '';


$pArray = \App\Helpers\LibHelper::GetnewdevelopmentsByndid($id);
if (empty($pArray)) {
    header("location: " . url('/'));
    exit;
}

$file = "files/hostgallery/" . (!empty($pArray->banner) ? $pArray->banner : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("public/images/new_developmentsbnr.jpg");
}
?>
<style>
    .validation-msg,error-msg{
        color: white;
    }
    .modal .validation-msg{
        color: black;
    }
</style>
<div class="wd100 __innerbanner __offPlane_bnt" data-overlay="dark" data-opacity="4" style="    background: url(<?= $file ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a href="<?= url('dubai/real-estate-developers') ?>">New Developments In Dubai</a></li>
                    <li class="breadcrumb-item"><a href="<?= url("dubai/real-estate-developers/$developer->slugs") ?>"><?= $developer->heading ?></a></li>
                    <li class="breadcrumb-item"><a href=""><?= $pArray->heading ?></a></li>
                </ol>
            </nav>
        </div>
    </div>
    <input type="hidden" id="primary_id" value="<?= $id ?>">
    <div class="container">
        <div class="wd100 text-center mt-5 mb-5">
            <?php
            $logo_on_banner = "files/hostgallery/" . (!empty($pArray->logo_on_banner) ? $pArray->logo_on_banner : '');
            if (is_file(Config::get('constants.HOME_DIR') . $logo_on_banner)) {
                $logo_on_banner = url($logo_on_banner);
            } else {
                $logo_on_banner = url('/public/lib/images/lanmer.png');
            }
            ?>
            <img class="img-fluid" src="{{$logo_on_banner}}">
        </div> 

        <h2>{{$pArray->banner_title}}</h2>
        <!-- <div class="__description">  </div>-->
        <div class="wd100 text-center mt-1 mb-1">
            <a href="javascript:void(0)" class="__reigbnt focusrequestcallBack" >Register Your Interest </a>
        </div>

    </div>
</div>




<section class="section __offPlaneAb">
    <div class="container">
        <div class="wd100 __offPlaneAbTxbz">
            <h3>{{$pArray->description_heading}}</h3>
            <p><?= html_entity_decode($pArray->description) ?></p>

            <div class="wd100 __offPlaneAbBntWrp">
                <a href="javascript:void(0)" onclick="updateModelData(this);" msg="Enquiry" title="Inquire Now" data-bs-toggle="modal" data-bs-target="#exampleModal" class="__offplnBnt">Inquire Now</a>
                <a  data-bs-toggle="modal" onclick="updateModelData(this);" msg="Download Brochure" data-bs-target="#exampleModal" title="Download Brochure" href="javascript:void(0)" class="__offplnBnt">Download Brochure</a>               
                <a  data-bs-toggle="modal" onclick="updateModelData(this);" msg="Download Floorplan" data-bs-target="#exampleModal" title="Download Floorplan" class="__offplnBnt" href="javascript:void(0)" >Download Floorplan</a>



            </div>
        </div> 
    </div> 
</section>






<section class="section __offPlaneInfoWrp">
    <div class="container">
        <div class="wd100 __offPlaneInfoTxbz">
            <h3>Details</h3>


            <div class="__offPlnGidwp row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-2"> 

                <div class="__offPlnGidibz">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/money.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Starting Price</div> 
                        <div class="__ofptexrlt"><?= $pArray->price ?></div> 
                    </div>
                </div>

                <div class="__offPlnGidibz">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/Type.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Type</div> 
                        <div class="__ofptexrlt"><?= $pArray->property_type ?></div> 
                    </div>
                </div>

                <div class="__offPlnGidibz __cbgbg">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/pin.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Location  </div> 
                        <div class="__ofptexrlt"><?= $pArray->location ?></div> 
                    </div>
                </div>

                <div class="__offPlnGidibz __cbgbg">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/square.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Area From                                                            </div> 
                        <div class="__ofptexrlt"> <?= $pArray->area ?> </div> 
                    </div>
                </div>


                <div class="__offPlnGidibz">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/bed_s.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Bedrooms                                                                                                                     </div> 
                        <div class="__ofptexrlt"><?= $pArray->bed ?></div> 
                    </div>
                </div>


                <div class="__offPlnGidibz">
                    <div class="__ofpimg"><img src="{{url('/public/lib/images/date_smll.png')}}"> </div>
                    <div class="__ofpinwp">
                        <div class="__ofphd">Completion                                                                                                                     </div> 
                        <div class="__ofptexrlt"><?= $pArray->completion ?></div> 
                    </div>
                </div>



            </div>

        </div> 
    </div> 
</section>
<?php if (!empty($pArray->video_url)) { ?>
    <section class="section __ofpVideoWrp">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 __ofpVideotext">
                    <h2>Video</h2> 
                    <p><?= !empty($pArray->video_description) ? $pArray->video_description : 'Lorem ipsum dolor sit amen, consectetur adipiscing elit Nam autoc ultrices Morbi bibendum erat sed nibbi faucibus sit amethyst ferentum ligula comseporta ipsum eget molesite. Phaseullus diam Guam, aliquman et ultrice. Lorem ipsum dolor sit amen, consectetur adipiscing elit.Lorem ipsum dolor sit amen, consectetur adipiscing elit Nam autoc ultrices Morbi bibendum erat sed nibbi faucibus sit amethyst ferentum ligula comseporta ipsum eget molesite.' ?></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 __ofpVideoboz">
                    <?php
                    $test = $pArray->video_url;
                    $test = str_replace('width', 'width="100%" ', $test);
                    echo $test;
                    ?>
                    <!--<iframe width="100%" height="315" src="https://www.youtube.com/embed/0VylpsTKIpQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
                </div>
            </div>

        </div>
    </section>
<?php
}
$Sql = "SELECT * FROM `new_developments_banner` WHERE ndid='$id' AND banner_type='BOTTOM BANNER'";

$listBanner = \App\Database::select($Sql);
if (!empty($listBanner)) {
    ?>
    <section class="section __ofpSliderWrp">

        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <?php
                for ($i = 0; $i < count($listBanner); $i++) {
                    $d = $listBanner[$i];
                    $fhru = "files/hostgallery/" . (!empty($d->banner_logo) ? $d->banner_logo : '');
                    if (is_file(Config::get('constants.HOME_DIR') . $fhru)) {
                        $fhru = url($fhru);
                    } else {
                        $fhru = url('/public/lib/images/off_plane_slider1.jpg');
                    }
                    ?>
                    <div class="carousel-item <?= empty($i) ? 'active' : '' ?>">
                        <img src="{{$fhru}}" class="d-block w-100" alt="...">
                    </div>
                    <?php
                }
                ?>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div> 
    </section>
    <?php
}
if (!empty($pArray->facility)) {
    $Sql = "SELECT * FROM `property_facility` WHERE facility_id IN ($pArray->facility) AND archive=0";
    $dArray = \App\Database::select($Sql);
}
if (!empty($dArray)) {
    ?>


    <section class="section __ofpAmenitiesWrp">
        <div class="container">
            <div class="wd100 __amenitiesWrp">
                <h2>Amenities </h2>

                <div class="__amenitiesWrpScx row row-cols-2 row-cols-sm-3 row-cols-md-4 row-cols-lg-6">
                    <?php
                    for ($i = 0; $i < count($dArray); $i++) {
                        $d = $dArray[$i];
                        $basepath = 'files/facility-logo/' . $d->rfacility_logo;
                        $basepath = is_file(Config::get('constants.HOME_DIR') . $basepath) ? $basepath : Config::get('constants.DEFAULT_FACILITY_LOGO');
                        ?>
                        <div class="col __amItmBoz">
                            <img class="img-fluid" src="{{url($basepath)}}">
                            <h6><?= $d->facility_name ?></h6> 
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </section>
<?php } ?>

<?php if (!empty($pArray->lat)) { ?>
    <section class="section __off_plane_map">
        <div class="container "> 
            <div class="__ofpMAwz">
                <h2>Location</h2>   
            </div>
        </div>
        <script>
            var defaultLat = '<?= $pArray->lat ?>';
            var defaultLong = '<?= $pArray->lng ?>';
            var locationName = '<?= $pArray->heading ?>';
        </script>
        <div class="map-wrapper-inner" id="map-page">
            <div id="google-maps-box">
                <div id="map" style="width:100%; height:350px;"></div>
            </div>
        </div>
    </section>

    <?php
}
$installment = !empty($pArray->installment) ? json_decode($pArray->installment, true) : [];
$installment = !empty($installment) && is_array($installment) ? $installment : [];
if (!empty($installment)) {
    ?>

    <section class="section __off_plane_map" style="padding-bottom: 8px;">
        <div class="container "> 
            <div class="__ofpMAwz">
                <h2>Payment Plan</h2>   
            </div>
        </div>
    </section>
    <section class="section __payment_planWrp">

        <div class="container "> 

            <div class="__ppbozwp row row-cols-1 row-cols-sm-2 row-cols-md-4 row-cols-lg-4">


                <?php

                function ordinal($number) {
                    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
                    if ((($number % 100) >= 11) && (($number % 100) <= 13))
                        return $number . 'th';
                    else
                        return $number . $ends[$number % 10];
                }

                for ($i = 0; $i < count($installment); $i++) {
                    $d = $installment[$i];
                    $names = ordinal(1 + $i);
                    ?>
                    <div class="__ppboz <?= $i >= 4 ? 'initialhide' : '' ?>">
                        <a href="javascript:void(0)"  class="__ppboziner wd100">
                            <h6><?= $names ?>  INSTALLMENT</h6>
                            <h5><?= !empty($d['installment']) ? $d['installment'] : '0' ?></h5>
                            <h6><?= empty($i) ? 'ON BOOKING' : '&nbsp;' ?></h6>
                        </a>
                    </div>
                <?php } ?>

            </div>
            <?php if (count($installment) > 4) { ?>
                <div class="wd100 text-center">
                    <a href="javascript:void(0)" id="viewall" onclick="$('.initialhide').show(500);
                            $(this).hide();
                            $('#viewless').show();" class="__showmore">View All</a>
                    <a href="javascript:void(0)" style="display: none;" id="viewless" onclick="$('.initialhide').hide(500);
                            $(this).hide();
                            $('#viewall').show();" class="__showmore">View Less</a>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>


<?php
$reginst = "files/hostgallery/" . (!empty($pArray->register_interest_logo) ? $pArray->register_interest_logo : '');
if (is_file(Config::get('constants.HOME_DIR') . $reginst)) {
    $reginst = url($reginst);
} else {
    $reginst = url('files/hostgallery/Register_Your_interest.jpg');
}
?> 

<section class="section    __oplFmWrp" style="background: url(<?= $reginst ?>) no-repeat center center;">
    <div class="container "> 

        <div class="__oplFmBoz">
            <h5>
                Our Expert Will Help You Buy The Best Property</h5>
            <input type="hidden" id="PAGE_ID" value="NEW_DEVELOPMENTS">
            <input type="hidden" id="title" value="<?= $pArray->heading ?>">
            <form class="row  __cofrm " id="requestcallBack">
                <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                    <!--<label>Name</label>-->
                    <input type="text" class="form-control" id="name" placeholder="Name"> 
                    <span class="validation-msg">Enter your name</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                    <!--<label>Email</label>-->
                    <input type="text" class="form-control" id="email" placeholder="Email">
                    <span class="validation-msg">Enter valid email address.</span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                    <!--<label>Phone</label>-->
                    <input type="number" class="form-control number_only" id="phone" placeholder="Phone">
                    <span id="valid-msg" class="hide">Valid</span>
                    <span id="error-msg" style="color: #f8f9fa;" class="hide">Invalid number</span>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12  mb-4" style="display: none;">
                    <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                </div>  	 


                <div class="col-lg-12 col-md-12 col-sm-12 mb-3 text-end">
                    <button type="button" id="sendcontactus" class="btn btn-primary __ctm_btn"  >Request A Free Call Back <div id="ajaxloader"></div></button>
                </div>

            </form>



        </div>

    </div>
</section>


<div class="modal fade __dwdpuowrp " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="modal-header mb-3">
                <h5 class="modal-title" id="exampleModalLabelHeading">Inquire Now</h5>

            </div>
            <div class="modal-body  p-0">
                <div class="wd100">
                    <input type="hidden" id="poup_messages">
                    <form class="row  modelieoe " id="requestcallBack">

                        <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                            <!--<label>Name</label>-->
                            <input type="text" class="form-control" id="poup_name" placeholder="Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12  mb-3">
                            <!--<label>Email</label>-->
                            <input type="text" class="form-control" id="poup_email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12  mb-3"> 
                            <!--<label>Phone</label>-->
                            <input type="number" class="form-control" id="poup_phone" placeholder="Phone">
                            <span class="validation-msg">Enter your phone</span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12  mb-3" style="display: none;">
                            <textarea class="form-control" id="messages" rows="4" placeholder="Message"></textarea>
                        </div>  	 

                    </form>
                </div> 


            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" id="sendcontactus_popup"  class="__bnt_download __enquirybtn">Inquire Now </a>
            </div>
        </div>
    </div>
</div>

@include('properties.includes.footer')	
<!--flagS-->
<?php if (!empty($pArray->lat)) { ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.MAP_API_KEY') ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
    <script src="{{url('/public')}}/assets/js/mapmodel_user.js" ></script>  
<?php } ?>
<link href="{{url('/public/lib/css/flag.css')}}" rel="stylesheet" media="screen"> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
<style>
    .initialhide{
        display: none;
    } 
</style>
<script>
                function updateModelData(e) {
                    var title = $(e).attr('title');
                    $('#exampleModalLabelHeading').html(title);
                    $('#sendcontactus_popup').html(title + ' <div style="display: contents;" id="ajaxloadermodal"></div>');
                    $('#poup_messages').val($(e).attr('msg'));
                }
                var telInput = $("#phone"),
                        errorMsg = $("#error-msg"),
                        validMsg = $("#valid-msg");

// initialise plugin
                telInput.intlTelInput({
                    allowExtensions: true,
                    formatOnDisplay: true,
                    autoFormat: true,
                    autoHideDialCode: true,
                    autoPlaceholder: true,
                    defaultCountry: "auto",
                    ipinfoToken: "yolo",
                    nationalMode: false,
                    numberType: "MOBILE",
                    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                    preferredCountries: ['sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
                    preventInvalidNumbers: true,
                    separateDialCode: true,
                    initialCountry: "auto",
                    geoIpLookup: function (callback) {
                        $.get("http://ipinfo.io", function () {
                        }, "jsonp").always(function (resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    },
                    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
                });

                var reset = function () {
                    telInput.removeClass("error");
                    errorMsg.addClass("hide");
                    validMsg.addClass("hide");
                };

// on blur: validate
                telInput.blur(function () {
                    reset();
                    if ($.trim(telInput.val())) {
                        if (telInput.intlTelInput("isValidNumber")) {
                            validMsg.removeClass("hide");
                        } else {
                            telInput.addClass("error");
                            errorMsg.removeClass("hide");
                        }
                    }
                });
                $("#phone").intlTelInput("setNumber", "+971 ");
// on keyup / change flag: reset
                telInput.on("keyup change", reset);

                $('#sendcontactus_popup').click(function () {
                    var form = new FormData();
                    var subject = $("#poup_messages").val();
                    form.append('_token', CSRF_TOKEN);
                    form.append('function', 'newdelevopmentEnq');
                    form.append('helper', 'Common');
                    form.append('json[page_id]', $('#PAGE_ID').val());
                    form.append('json[name]', $('#poup_name').val());
                    form.append('json[email]', $('#poup_email').val());
                    form.append('json[primary_id]', $('#primary_id').val());
                    form.append('json[messages]', subject);
                    form.append('json[mobile_no]', $("#poup_phone").val());
                    form.append('json[page_url]', $('#page_url').val());
                    var urlGo = base_url + '/thank-you';
                    if (subject == 'Download Floorplan') {
                        urlGo = urlGo + '/for-download-floor-plan';
                    } else if (subject == 'Download Brochure') {
                        urlGo = urlGo + '/for-download-brochure';
                    }


                    if ($('#poup_name').val() == '') {
                        $('#poup_name').parent().find('.validation-msg').show();
                        $('#poup_name').focus();
                        return false;
                    } else {
                        $('#poup_name').parent().find('.validation-msg').hide();
                    }
                    if (!validateEmail($('#poup_email').val()) || $('#poup_email').val() == '') {
                        $('#poup_email').parent().find('.validation-msg').show();
                        $('#poup_email').focus();
                        return false;
                    } else {
                        $('#poup_email').parent().find('.validation-msg').hide();
                    }
                    if ($('#poup_phone').val() == '') {
                        $('#poup_phone').parent().find('.validation-msg').show();
                        $('#poup_phone').focus();
                        return false;
                    } else {
                        $('#poup_phone').parent().find('.validation-msg').hide();
                    }


                    showLoader('ajaxloadermodal');

                    setTimeout(function () {
                        var json = ajaxpost(form, "/helper");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                // alertSimple(json.messages);
                                setTimeout(function () {
                                    window.location = urlGo;
                                }, 3000);
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }, 200);
                });


                $('#sendcontactus').click(function () {
                    var form = new FormData();
                    form.append('_token', CSRF_TOKEN);
                    form.append('function', 'newdelevopmentEnq');
                    form.append('helper', 'Common');
                    form.append('json[page_id]', $('#PAGE_ID').val());
                    form.append('json[name]', $('#name').val());
                    form.append('json[email]', $('#email').val());
                    form.append('json[primary_id]', $('#primary_id').val());

                    form.append('json[mobile_no]', $("#phone").intlTelInput("getNumber"));
                    form.append('json[messages]', '<b>Type - ' + $('#title').val() + '</b> ' + $('#messages').val());
                    form.append('json[page_url]', $('#page_url').val());

                    if ($('#name').val() == '') {
                        $('#name').parent().find('.validation-msg').show();
                        $('#name').focus();
                        return false;
                    } else {
                        $('#name').parent().find('.validation-msg').hide();
                    }
                    if (!validateEmail($('#email').val()) || $('#email').val() == '') {
                        $('#email').parent().find('.validation-msg').show();
                        $('#email').focus();
                        return false;
                    } else {
                        $('#email').parent().find('.validation-msg').hide();
                    }
                    if (telInput.intlTelInput("isValidNumber")) {
                        validMsg.removeClass("hide");
                    } else {
                        telInput.addClass("error");
                        errorMsg.removeClass("hide");
                        return false;
                    }

                    showLoader('ajaxloader');
                    setTimeout(function () {
                        var json = ajaxpost(form, "/helper");
                        try {
                            var json = jQuery.parseJSON(json);
                            if (json.status == true) {
                                //  alertSimple(json.messages);
                                setTimeout(function () {
                                    window.location = base_url + '/thank-you';
                                }, 3000);
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }, 200);
                });

                $('.focusrequestcallBack').click(function () {
                    $('html, body').animate({
                        scrollTop: (parseInt($("#requestcallBack").offset().top) - 200)
                    }, 0);
                });
</script>
<!--flagS-->