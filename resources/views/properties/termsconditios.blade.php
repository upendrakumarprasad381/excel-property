@include('properties.includes.header')
<?php
$TERMS_CONDITIONS_PAGE = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'TERMS_CONDITIONS_PAGE');
$file = "files/hostgallery/" . (!empty($TERMS_CONDITIONS_PAGE->col1) ? $TERMS_CONDITIONS_PAGE->col1 : '');
if (is_file(Config::get('constants.HOME_DIR') . $file)) {
    $file = url($file);
} else {
    $file = url("files/hostgallery/terms_conditios.jpg");
}
?>
<div class="wd100 __innerbanner __terms_conditiosbnr" style="    background: url(<?= $file ?>) no-repeat center center;">
	<div class="wd100 breadcrumb_wrap __hshwp">
		<div class="container">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class="breadcrumb-item"><a href="javascript:void(0)">Terms & Conditions</a></li>
			 	</ol>
			</nav>
			</div>
	</div>
	 
    <div class="container">
        <h2>{{(!empty($TERMS_CONDITIONS_PAGE->col2) ? $TERMS_CONDITIONS_PAGE->col2 : '')}}</h2>  
    </div> 
	
 	
</div>
<section class="section __privacy_policy_Pg">
	<div class="container">
		 <?= (!empty($TERMS_CONDITIONS_PAGE->col3) ? $TERMS_CONDITIONS_PAGE->col3 : '') ?>
	 </div> 
</section>
@include('properties.includes.footer')