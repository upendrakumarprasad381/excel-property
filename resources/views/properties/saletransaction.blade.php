@include('properties.includes.header')
<?php
$type = !empty($_REQUEST['type']) ? $_REQUEST['type'] : '';
$title = "Sale Transaction";
if ($type == '2') {
    $title = "Daily Transaction";
} else if ($type == '3') {
    $title = "Market Guide";
}
?>
<div class="wd100 breadcrumb_wrap __liist_property_bnr">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $title ?></a></li>
            </ol>
        </nav>
    </div>
</div> 
<style>
    table.table-bordered.dataTable th, table.table-bordered.dataTable td,th {
        color: white;
    }
</style>
<section class="section __liist_property_Pg">
    <div class="container">
        <?php
        $data = \App\Helpers\CommonHelper::Getrta_tram_stations();
        ?>
        <div class="row">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>sl</th>
                        <th>Location English</th>
                        <th>Location arabic</th>
                        <th>line Name</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                        <th>Opening Date</th>
                        <th>Closing Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $data = !empty($data) && is_array($data) ? $data : [];
                    for ($i = 0; $i < count($data); $i++) {
                        $d = $data[$i];
                        ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td><?= !empty($d['location_name_english']) ? $d['location_name_english'] : '' ?></td>
                            <td><?= !empty($d['location_name_arabic']) ? $d['location_name_arabic'] : '' ?></td>
                            <td><?= !empty($d['line_name']) ? $d['line_name'] : '' ?></td>
                            <td><?= !empty($d['station_location_longitude']) ? $d['station_location_longitude'] : '' ?></td>
                            <td><?= !empty($d['station_location_latitude']) ? $d['station_location_latitude'] : '' ?></td>
                            <td><?= !empty($d['station_opening_date']) ? $d['station_opening_date'] : '' ?></td>
                            <td><?= !empty($d['station_closing_date']) ? $d['station_closing_date'] : '' ?></td>
                        </tr>
                    <?php } ?>


                </tbody>

            </table>
        </div>
    </div>
</section>
@include('properties.includes.footer')	


<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>