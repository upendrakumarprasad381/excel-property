@include('properties.includes.header')
<?php
$ptype = Request::segment(2);
$ptype = $ptype == 'properties-for-rent' ? '1' : '0';
$location = !empty($_REQUEST['location']) ? $_REQUEST['location'] : '';
$ptype_name = isset($_REQUEST['ptype_name']) ? $_REQUEST['ptype_name'] : '';
$bedroom = !empty($_REQUEST['bedroom']) ? $_REQUEST['bedroom'] : '';
$min_price = !empty($_REQUEST['min_price']) ? $_REQUEST['min_price'] : '';
$max_price = !empty($_REQUEST['max_price']) ? $_REQUEST['max_price'] : '';

$ptypeId = Request::segment(3);
$segment4 = Request::segment(4);

$pAr = App\Helpers\LibHelper::GetpropertytypeByslug($ptype, $ptypeId);
$PROPERTY_DESCRIPTION = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'PROPERTY_DESCRIPTION');
$cond = "";
$salreRentTitle = empty($ptype) ? 'Sale' : 'Rent';
$bannerView = url("public/images/rent_bnr.jpg");
if ($ptypeId == 'search' || $ptypeId == 'area' || $ptypeId == 'tower') {
    if ($ptypeId == 'area' || $ptypeId == 'tower') {
        if ($ptypeId == 'area') {
            $areaFound = App\Helpers\LibHelper::GetarealistByslugId($segment4);
        }
        if ($ptypeId == 'tower') {
            $towerFound = App\Helpers\LibHelper::GettowerlistByslugsId($segment4);
        }

        $title4838 = '';
        if (!empty($areaFound)) {
            $title4838 = ucwords($areaFound->area_name);
        }
        if (!empty($towerFound)) {
            $title4838 = ucwords($towerFound->tower_name);
        }
        $searchTile = "Property For $salreRentTitle In " . $title4838;
    } else {
        $searchTile = "Property For $salreRentTitle In $location";
    }
} else if (!empty($pAr)) {
    $searchTile = ucwords($pAr->ptype_name) . ' In Dubai';

    $file = "files/hostgallery/" . (!empty($pAr->banner) ? $pAr->banner : '');
    if (is_file(Config::get('constants.HOME_DIR') . $file)) {
        $bannerView = url($file);
    }
} else if (empty($ptypeId)) {


    if (empty($ptype)) {
        $searchTile = $PROPERTY_DESCRIPTION->col3;
        $file = "files/hostgallery/" . (!empty($PROPERTY_DESCRIPTION->col5) ? $PROPERTY_DESCRIPTION->col5 : '');
        if (is_file(Config::get('constants.HOME_DIR') . $file)) {
            $bannerView = url($file);
        }
    } else {
        $searchTile = $PROPERTY_DESCRIPTION->col4;
        $file = "files/hostgallery/" . (!empty($PROPERTY_DESCRIPTION->col6) ? $PROPERTY_DESCRIPTION->col6 : '');
        if (is_file(Config::get('constants.HOME_DIR') . $file)) {
            $bannerView = url($file);
        }
    }
} else {
    echo '<script>window.location = "' . url('/') . '";</script>';
}

$bedRoomHide = [6, 9];
$FILTER_SETTINGS = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'FILTER_SETTINGS');
?>

<div class="wd100 __innerbanner __rentlbnr" data-overlay="dark" data-opacity="4" style="background: url(<?= $bannerView ?>) no-repeat center center;">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <?php
                    if (!empty($searchTile)) {
                        ?><li class="breadcrumb-item"><a href=""><?= $searchTile ?></a></li><?php
                            }
                            ?>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="__innerSrbroz">
            <div class="row">
                <h5>
                    <?php
                    if (!empty($searchTile)) {
                        echo $searchTile;
                    }
                    ?></h5>
            </div>

            <div class="row">
                <div class="__inrfld col">
                    <select class="form-select" id="ptype" onchange="loadpropertyType();">
                        <option value="0" <?= empty($ptype) ? 'selected' : '' ?>>Buy</option> 
                        <option value="1" <?= $ptype == '1' ? 'selected' : '' ?> >Rent</option> 
                    </select>
                </div>

                <div class="__inertextfld col">
                    <input type="text" class="form-control" placeholder="LOCATION OR TOWER" value="<?= $location ?>" id="myInput" onkeyup="myFunction()" autocomplete="off">
                    <ul id="myUL" style="display: none;">                      
                        <?php
                        $towerList = " UNION ALL SELECT TL.tower_name AS address,COUNT(MP.property_id) AS total FROM `my_property` MP LEFT JOIN tower_list TL ON TL.tower_id=MP.tower_id WHERE MP.status=0 AND MP.archive=0  GROUP BY MP.tower_id";
                        $Sql = "SELECT address,COUNT(property_id) AS total FROM `my_property` WHERE status=0 AND archive=0  GROUP BY address $towerList";
                        $list = \App\Database::select($Sql);
                        for ($i = 0; $i < count($list); $i++) {
                            $d = $list[$i];
                            ?>
                            <li ><a value="{{$d->address}}" class="searchable" href="javascript:void(0)"><?= $d->address . ' (' . $d->total . ')' ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="__inerserBtn col">
                    <button type="button" id="searchQry" class="btn __inrSrBtn">Search</button>
                </div> 
            </div>

            <div class="row">
                <div class="__inrtypedrop col ">
                    <select sel_ptype_name="<?= $ptype_name ?>" class="form-select" id="ptype_name"> 
                        <option value="">PROPERTY TYPE</option>

                    </select>
                </div>

                <div class="__inrtypedrop col" >
                    <select class="form-select" id="bedroom"> 
                        <option value="">BEDROOM</option>
                        <option <?= $bedroom == 'studio' ? 'selected' : '' ?> value="studio">Studio</option>
                        <?php
                        $bathroom = $FILTER_SETTINGS->col1 > 0 ? $FILTER_SETTINGS->col1 : 10;
                        for ($i = 1; $i <= $bathroom; $i++) {
                            ?>
                            <option <?= $bedroom == $i ? 'selected' : '' ?> value="{{$i}}">{{$i}} Bedroom</option>
                        <?php } ?>
                    </select>
                </div>


                <div class="__inrtypedrop col">
                    <select min_prices_sel="<?= $min_price ?>" class="form-select" id="min_price" aria-label="Default select example">
                        <option selected>Min. Price</option>

                    </select>
                </div>

                <div class="__inrtypedrop col">
                    <select max_prices_sel="<?= $max_price ?>" class="form-select" id="max_price" aria-label="Default select example">
                        <option selected>Max. Price</option>

                    </select>
                </div>


            </div>
        </div>
    </div>



</div>

<!--
<section class="section __innerSrbr">
    <div class="container">
       
        </div>
</section>
 
-->

<section class="section __rentlPg">

    <div class="container">
        <h5 style="margin-bottom: 15px;"><?= !empty($pAr->ptype_name) ? ucwords($pAr->ptype_name) . ' In Dubai' : (!empty($searchTile) ? $searchTile : 'Property list') ?></h5>
        <?php
        if (isset($ptype)) {
            $cond = $cond . " AND MP.ptype='$ptype'";
        }
        if (isset($ptype_name) && $ptype_name != '') {
            $cond = $cond . " AND MP.property_type_normal='$ptype_name'";
        }
        if (!empty($pAr)) {
            $cond = $cond . " AND MP.property_type='$pAr->ptype_id'";
        }
        if (!empty($bedroom)) {
            if ($bedroom == 'studio') {
                $cond = $cond . " AND MP.is_studio=1";
            } else {
                $cond = $cond . " AND MP.bed = '$bedroom'";
            }
        }

        if (!empty($min_price)) {
            $cond = $cond . " AND MP.price >= '$min_price'";
        }
        if (!empty($max_price)) {
            $cond = $cond . " AND MP.price <= '$max_price'";
        }
        if (!empty($location)) {
            $cond = $cond . " AND (MP.tower_id IN (SELECT tower_id  FROM `tower_list` WHERE `tower_name` LIKE '$location') || MP.address LIKE '$location')";
        }
        if (!empty($areaFound)) {
            $cond = $cond . " AND MP.area_id='$areaFound->area_id'";
        }
        if (!empty($towerFound)) {
            $cond = $cond . " AND MP.tower_id='$towerFound->tower_id'";
        }
      
        $Sql = "SELECT MP.*,getpropertyImage(MP.property_id) AS first_image,getpropertyDetailsUrl(MP.property_id) AS urlSlug FROM `my_property` MP WHERE MP.archive=0 AND MP.status=0 $cond ORDER BY MP.position ASC";


        $listAr = \App\Database::select($Sql);
        if (!empty($listAr)) {
            ?>
            <div class="row">
                <?php
                for ($j = 0; $j < count($listAr); $j++) {
                    $fg = $listAr[$j];
                    $images = \App\Helpers\CommonHelper::GetPropertyImagesCover($fg);
                    $url = url($fg->urlSlug);
                    ?>
                    <div class="col-lg-4 col-md-6 col-sm-12 __prligrdwrp">
                        <div class="__prligrd wd100">
                            <div class="__prligrdimg">
                                <a href="{{$url}}">
                                    <img class="img-fluid" src="{{$images}}">
                                </a>
                                <div class="__proname"> <a href="{{$url}}">{{$fg->property_title}}</a> 
                                </div>
                            </div>
                            <div class="__prligrddrcp">
                                <div class="  __priqustbx"> <small>PRICE</small>
                                    <br>AED <?= \App\Helpers\CommonHelper::DecimalAmount($fg->price) ?></div>
                                <div class="  __bedsbx" style="display: <?= !empty($pAr) && in_array($pAr->ptype_id, $bedRoomHide) ? 'none' : '' ?>"> <small><?= !empty($fg->is_studio) ? '&nbsp;' : $fg->bed ?></small>
                                    <br><?= !empty($fg->is_studio) ? 'Studio' : 'BED' ?></div>
                                <div class="  __sqftsbx"><?= \App\Helpers\CommonHelper::DecimalAmount($fg->area) ?> SQ.FT</div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php
        } else {
            ?>

            <div class="wd100 __notfondProty">
                <h2><i class="fas fa-exclamation-triangle" aria-hidden="true"></i> Sorry, we did not find anything matching your search.</h2>
                <p>Please, submit your details to get the property consultant advice: </p>

                <div class="__feedformWrp wd100">

                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <input type="text" class="form-control" id="name" placeholder="Full Name"> 
                            <span class="validation-msg">Enter your name</span>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <input type="text" class="form-control" id="email" placeholder="Email">
                            <span class="validation-msg">Enter valid email address.</span>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 mb-4">
                            <input type="number" class="form-control" id="phone" placeholder="Phone"> 
                            <span class="validation-msg">Enter your mobile no</span>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  text-end">
                            <button type="button" id="sendcontactus" class="btn btn-primary __ctm_btn __feedformbnt">Submit <div id="ajaxloader"></div> </button>
                        </div>



                    </div>


                </div>



            </div>
            <?php
        }
        ?>

        <div class="wd100 __rentLanbotmSc">
            <?php
            if (!empty($listAr)) {
                if (!empty($searchTile) && !empty($pAr->description)) {
                    ?><h2><?= $searchTile ?></h2> <?php
                    echo $pAr->description;
                } else if (!empty($searchTile) && !empty($areaFound)) {

                    if (empty($ptype) && !empty($areaFound->description_buy)) {
                        ?><h2><?= $searchTile ?></h2> <?php
                        echo $areaFound->description_buy;
                    } else if (!empty($areaFound->description_rent)) {
                        ?><h2><?= $searchTile ?></h2> <?php
                        echo $areaFound->description_rent;
                    }
                } else if (!empty($searchTile) && !empty($towerFound)) {

                    if (empty($ptype) && !empty($towerFound->description_buy)) {
                        ?><h2><?= $searchTile ?></h2> <?php
                        echo $towerFound->description_buy;
                    } else if (!empty($towerFound->description_rent)) {
                        ?><h2><?= $searchTile ?></h2> <?php
                        echo $towerFound->description_rent;
                    }
                } else if (empty($ptypeId)) {
                    $PROPERTY_DESCRIPTION = \App\Helpers\LibHelper::GetcmsBycmsId($cmsId = 'PROPERTY_DESCRIPTION');
                    ?><h2><?= $searchTile ?> </h2> <?php
                    if (empty($ptype)) {
                        echo $PROPERTY_DESCRIPTION->col1;
                    } else {
                        echo $PROPERTY_DESCRIPTION->col2;
                    }
                }
            }
            ?>


        </div>

    </div>
</section>
<style>

    #myUL {
        display: none; 
    }
    #myUL li a {
        display: block; /* Make it into a block element to fill the whole list */
    }
</style>
@include('properties.includes.footer')
<script>
    $('#sendcontactus').click(function () {
        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('function', 'propertylistPageNoRecEnq');
        form.append('helper', 'Common');
        form.append('json[page_id]', 'NO_PROPERTY_LIST_FOUND');
        form.append('json[primary_id]', '0');

        form.append('json[name]', $('#name').val());
        form.append('json[email]', $('#email').val());
        form.append('json[mobile_no]', $("#phone").val());
        form.append('json[messages]', 'Sorry, we did not find anything matching your search.');
        form.append('json[page_url]', $('#page_url').val());

        if ($('#name').val() == '') {
            $('#name').parent().find('.validation-msg').show();
            $('#name').focus();
            return false;
        } else {
            $('#name').parent().find('.validation-msg').hide();
        }
        if (!validateEmail($('#email').val()) || $('#email').val() == '') {
            $('#email').parent().find('.validation-msg').show();
            $('#email').focus();
            return false;
        } else {
            $('#email').parent().find('.validation-msg').hide();
        }

        if ($('#phone').val() == '') {
            $('#phone').parent().find('.validation-msg').show();
            $('#phone').focus();
            return false;
        } else {
            $('#phone').parent().find('.validation-msg').hide();
        }

//    if ($('#title').val() == '') {
//        $('#title').css('border-color', 'red');
//        $('#title').focus();
//        return false;
//    } else {
//        $('#title').css('border-color', '');
//    }
        showLoader('ajaxloader');
        setTimeout(function () {
            var json = ajaxpost(form, "/helper");
            try {
                var json = jQuery.parseJSON(json);
                if (json.status == true) {
                    //s alertSimple(json.messages);
                    setTimeout(function () {
                        window.location = base_url + '/thank-you';
                    }, 3000);
                }
            } catch (e) {
                alert(e);
            }
        }, 200);
    });
    setTimeout(function () {
        loadpropertyType();
    }, 500);
    $('#searchQry').click(function () {

        var ptype = $('#ptype').val();
        var ptypeUrl = ptype == '1' ? 'properties-for-rent' : 'properties-for-sale';
        var url = base_url + '/dubai/' + ptypeUrl + '/search?';
        url = url + '&location=' + ($('#myInput').val());
        if ($('#myInput').val() == '') {
            $('#myInput').focus();
            return false;
        }
        url = url + '&ptype_name=' + ($('#ptype_name').val());
        url = url + '&bedroom=' + ($('#bedroom').val());
        url = url + '&min_price=' + ($('#min_price').val());
        url = url + '&max_price=' + ($('#max_price').val());
        window.location = url;
    });
    function loadpropertyType() {
        var ptype = $('#ptype').val();


        var form = new FormData();
        form.append('_token', CSRF_TOKEN);
        form.append('ptypeId', ptype);
        form.append('encode', true);
        form.append('helper', 'Lib');
        form.append('function', 'GetpropertyTypeByptypeHTMLId');

        form.append('min_prices_sel', $('#min_price').attr('min_prices_sel'));
        form.append('max_prices_sel', $('#max_price').attr('max_prices_sel'));
        form.append('sel_ptype_name', $('#ptype_name').attr('sel_ptype_name'));


        var json = ajaxpost(form, "/helper");
        try {
            var json = jQuery.parseJSON(json);
            if (json.status == true) {
                try {

                    if (json.status == true) {

                        $('#ptype_name').html(json.property_type);
                        $('#max_price').html(json.max_price);
                        $('#min_price').html(json.min_price);
                        $('#myUL').html(json.searchHTML);
                    }
                } catch (e) {

                }
            }
        } catch (e) {
            alert(e);
        }
//        var data = {function: 'GetpropertyTypeByptypeHTMLId', ptypeId: ptype, searchprice: searchprice, encode: true, helper: 'Lib', _token: CSRF_TOKEN};

    }
    function myFunction() {
        // Declare variables
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById('myInput');
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName('li');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }
    $("#myInput").keyup(function () {
        var x = document.getElementById('myUL');
        if ($(this).val() == "") {
            x.style.display = 'none';
        } else {
            x.style.display = 'block';
        }

        var $page = $('#myUL .searchable');
        $page.each(function (i, a) {
            $a = $(a)
            $a.html($a.html().replace(/<em>/g, "").replace(/\<\/em\>/g, ""))
        })
        var searchedText = $('#myInput').val();
        if (searchedText != "") {
            $page.each(function (i, a) {
                $a = $(a)
                var html = $a.text().replace(new RegExp("(" + searchedText + ")", "igm"), "<em>$1</em>")
                $a.html(html)
            })
        }
    });

    $('body').on('click', '.searchable', function () {
        var val = $(this).attr('value');
        $('#myInput').val(val);
        $('#myUL').hide();
    });
</script>
