@include('properties.includes.header')  
<div class="wd100 __innerbanner __sitemapBnr">
    <div class="wd100 breadcrumb_wrap __hshwp">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= url('/') ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                    <li class="breadcrumb-item"><a>Sitemap</a></li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <h2>Sitemap</h2>
    </div>

</div>


<section class="section __sitemap_Pg">
    <div class="container">
        <?php
        $Sql = "SELECT SC.* FROM `emirates` SC WHERE emirates_name = 'Dubai'";
        $em = App\Database::select($Sql);
        for ($i = 0; $i < count($em); $i++) {
            $e = $em[$i];
            $date = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '0');
            ?>
            <div class="wd100">
                <h2><a href="<?= url('dubai/properties-for-sale') ?>">Properties For Buy</a></h2>

                <ul>
                    <?php
                    for ($j = 0; $j < count($date); $j++) {
                        $d = $date[$j];
                        $url = url('dubai/properties-for-sale/' . $d->slugs);
                        ?>
                        <li><a href="<?= $url ?>"><?= $d->ptype_name ?></a></li>
                    <?php } ?>

                </ul>
            </div>
        <?php } ?>

        <?php
        for ($i = 0; $i < count($em); $i++) {
            $e = $em[$i];
            $date = App\Helpers\LibHelper::GetpropertyTypeByptypeId($ptypeId = '1');
            ?>
            <div class="wd100">
                <h2><a href="<?= url('dubai/properties-for-rent') ?>">Properties For Rent</a></h2>

                <ul>
                    <?php
                    for ($j = 0; $j < count($date); $j++) {
                        $d = $date[$j];
                        $url = url('dubai/properties-for-rent/' . $d->slugs);
                        ?>
                        <li><a href="<?= $url ?>"><?= $d->ptype_name ?></a></li>
                    <?php } ?>

                </ul>
            </div>
        <?php }
        ?>


        <div class="wd100">
            <h2><a href="<?= url('dubai/real-estate-developers') ?>">Developers</a></h2>
            <ul>
                <?php
                $Sql = "SELECT SC.heading,SC.slugs,SC.ndid,SC.featured_image,SC.heading AS parents_name  FROM `new_developments` SC  WHERE SC.archive=0 AND SC.parents_id  IN (0) ORDER BY SC.position ASC ";
                $listAr = \App\Database::select($Sql);
                for ($i = 0; $i < count($listAr); $i++) {
                    $d = $listAr[$i];
                    ?><li><a href="<?= url('dubai/real-estate-developers/' . $d->slugs) ?>"><?= $d->parents_name ?></a></li><?php
                }
                ?>
            </ul>
        </div>


        <div class="wd100">
            <h2><a href="<?= url('dubai/off-plan-properties') ?>">Projects</a></h2>
            <ul>
                <?php
                $Sql = "SELECT SC.heading,SC.ndid,SC.banner,SC.slugs,P.heading AS parents_name,P.slugs AS parents_slugs  FROM `new_developments` SC LEFT JOIN new_developments P ON P.ndid=SC.parents_id WHERE SC.archive=0 AND SC.parents_id NOT IN (0)  ORDER BY P.position ASC,SC.position ASC ";

                $listAr = \App\Database::select($Sql);
                for ($i = 0; $i < count($listAr); $i++) {
                    $d = $listAr[$i];
                    ?><li><a href="<?= url("dubai/real-estate-developers/$d->parents_slugs/$d->slugs") ?>"><?= $d->parents_name . ' - ' . $d->heading ?></a></li><?php
                }
                ?>
            </ul>
        </div>



        <div class="wd100">
            <h2><a href="<?= url('dubai/area-guides') ?>">Area Guides</a></h2>
            <ul>
                <?php
                $Sql = "SELECT * FROM `area_guides` WHERE archive=0 ORDER BY position ASC";
                $listAr = \App\Database::select($Sql);
                for ($i = 0; $i < count($listAr); $i++) {
                    $d = $listAr[$i];
                    ?><li><a href="<?= url("dubai/area-guides/$d->slugs") ?>"><?= $d->area_name ?></a></li><?php
                }
                ?>
            </ul>
        </div>
        
        <div class="wd100">
            <h2><a href="<?= url('dubai/area-guides') ?>">Blog</a></h2>
            <ul>
                <?php
                 $Sql = "SELECT * FROM `blogs` WHERE archive=0  ORDER BY position ASC";
                    $listAr = App\Database::select($Sql);
                for ($i = 0; $i < count($listAr); $i++) {
                    $d = $listAr[$i];
                    ?><li><a href="<?= url('blog-detail/' . $d->slugs) ?>"><?= $d->blog_titile ?></a></li><?php
                }
                ?>
            </ul>
        </div>


    </div> 
</section>
@include('properties.includes.footer')